/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "projects/inside_out_deep_blending/renderer/Config.hpp"
#include "projects/inside_out_deep_blending/renderer/patch_cloud.h"
#include "projects/inside_out_deep_blending/renderer/mesh_ray_tracer.h"
#include "projects/inside_out_deep_blending/renderer/recon_tools.h"
#include "projects/inside_out_deep_blending/renderer/imtools.h"

#include "projects/fribr_framework/renderer/gl_wrappers.h"
#include "projects/fribr_framework/renderer/tools.h"
#include "projects/fribr_framework/renderer/io.h"
#include "projects/fribr_framework/renderer/3d.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <cstdlib>

using namespace Eigen;
using namespace fribr;
using namespace sibr;
#ifdef INRIA_WIN
size_t doAtomicOp(size_t* progress, int val);
#endif

namespace
{

	void error_callback(int error, const char* description)
	{
		fprintf(stderr, "Error %d: %s\n", error, description);
	}

	void export_as_nvm(const std::string& base_path,
		const fribr::CalibratedImage::Vector cameras,
		const std::vector<std::vector<Eigen::Vector3f>>& camera_points)
	{
		const std::string nvm_dir = base_path + "/deep_blending/nvm";
		if (!directory_exists(nvm_dir))
			make_directory(nvm_dir);

		const std::string nvm_path = nvm_dir + "/scene.nvm";
		NVM::save(nvm_path, cameras, PointCloud::Ptr());

		const std::string nvm_depth_dir = nvm_dir + "/depth_maps";
		if (!directory_exists(nvm_depth_dir))
			make_directory(nvm_depth_dir);

		for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
		{
			const CalibratedImage& camera = cameras[i];
			Eigen::Vector2i        resolution = camera.get_scaled_resolution();

			cv::Mat1f depth_map(resolution.y(), resolution.x());
			for (int y = 0; y < depth_map.rows; ++y)
				for (int x = 0; x < depth_map.cols; ++x)
				{
					int index = (depth_map.rows - y - 1) * depth_map.cols + x;
					float depth = std::max(0.0f, -camera_points[i][index].z());
					depth_map(depth_map.rows - y - 1, x) = depth;
				}

			const std::string depth_file = camera.get_image_name() + ".bin";
			const std::string depth_path = nvm_depth_dir + "/" + depth_file;
			recon::save_colmap_depth(depth_path, depth_map);
		}
	}
	std::vector<fribr::Mesh::Ptr> load_precomputed_meshes(const std::string &per_view_mesh_dir,
		const CalibratedImage::Vector &cameras)
	{
		std::vector<fribr::Mesh::Ptr> meshes;
		for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
		{
			const fribr::CalibratedImage &camera = cameras[i];
			const std::string      mesh_file = camera.get_image_name() + ".ply";
			const std::string      mesh_path = per_view_mesh_dir + "/" + mesh_file;

			std::vector<fribr::Mesh::Ptr> camera_meshes = load_meshes(mesh_path, RH_Y_UP, fribr::Mesh::CPUOnly);
			if (camera_meshes.empty())
			{
				std::cerr << "Could not load per-view mesh at: " << mesh_path << std::endl;
				exit(1);
			}

			//// [SP] If a specific per-view mesh contain more than 1 mesh, 
			//// use the mesh with largest number of mesh vertices
			//// Why is this needed?
			int best_index = 0;
			size_t best_size = 0;
			if (camera_meshes.size() > 1)
			{
				std::cout << "WARNING: per-view ply-file contains " << camera_meshes.size()
					<< " meshes! (" << mesh_path << ")" << std::endl;
				for (int j = 0; j < static_cast<int>(camera_meshes.size()); ++j)
				{
					if (camera_meshes[j]->get_vertices().size() > best_size)
					{
						best_index = j;
						best_size = camera_meshes[j]->get_vertices().size();
					}
				}
			}
			meshes.emplace_back(camera_meshes[best_index]);
		}

		return meshes;
	}

} // anonymous namespace

int main(int argc, char* argv[])
{
	if (argc < 2 || argc > 12)
	{
		std::cerr << "Usage: recon_cmd [SCENE_PATH] (inside_out) (colmap_out) (cloud_out) (grid_bounds_from_cameras) (grid_size=32) (compression_target=30) (max_width=-1) (harmonize=<path/to/harmonized>)" << std::endl;
		return 1;
	}

	bool colmap_out = false;
	bool cloud_out = false;
	int  grid_size = 32;
	bool grid_bounds_from_cameras = false;
	std::string harmonized_path = "";
	int  compression_target = 30;
	int  max_image_width = -1;
	bool inside_out = false;

	for (int i = 1; i < argc; ++i)
	{
		if (std::string(argv[i]) == "inside_out")
			inside_out = true;

		if (std::string(argv[i]) == "colmap_out")
			colmap_out = true;

		if (std::string(argv[i]) == "cloud_out")
			cloud_out = true;

		if (std::string(argv[i]) == "grid_bounds_from_cameras")
			grid_bounds_from_cameras = true;

		if (std::string(argv[i]).substr(0, 10) == "grid_size=")
			grid_size = std::stoi(std::string(argv[i]).substr(10));
		if (std::string(argv[i]) == "grid_size" && i + 1 < argc)
		{
			++i;
			grid_size = std::stoi(std::string(argv[i]));
		}

		if (std::string(argv[i]).substr(0, 19) == "compression_target=")
			compression_target = std::stoi(std::string(argv[i]).substr(19));
		if (std::string(argv[i]) == "compression_target" && i + 1 < argc)
		{
			++i;
			compression_target = std::stoi(std::string(argv[i]));
		}

		if (std::string(argv[i]).substr(0, 10) == "max_width=")
			max_image_width = std::stoi(std::string(argv[i]).substr(10));
		if (std::string(argv[i]) == "max_width" && i + 1 < argc)
		{
			++i;
			max_image_width = std::stoi(std::string(argv[i]));
		}

		if (std::string(argv[i]).substr(0, 10) == "harmonize=")
			harmonized_path = std::stoi(std::string(argv[i]).substr(10));
		if (std::string(argv[i]) == "harmonize" && i + 1 < argc)
		{
			++i;
			harmonized_path = std::string(argv[i]);
		}
	}

	std::cout << "Running with these parameters:" << std::endl;
	std::cout << "inside_out=" << (inside_out ? "true" : "false") << std::endl;
	std::cout << "colmap_out=" << (colmap_out ? "true" : "false") << std::endl;
	std::cout << "cloud_out=" << (cloud_out ? "true" : "false") << std::endl;
	std::cout << "grid_bounds_from_cameras=" << (grid_bounds_from_cameras ? "true" : "false") << std::endl;
	std::cout << "grid_size=" << grid_size << std::endl;
	std::cout << "compression_target=" << compression_target << std::endl;
	std::cout << "max_width=" << max_image_width << std::endl;
	std::cout << "harmonized_path=" << harmonized_path << std::endl;

	// We need a GL context for CalibratedImage and PatchCloud
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
	{
		std::cerr << "Could not initialize glfw." << std::endl;
		return 1;
	}

	glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	GLFWwindow* window = glfwCreateWindow(1280, 720, "IBRNext recon cmd", NULL, NULL);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(0);

	glewExperimental = GL_TRUE;
	GLenum glew_error = glewInit();
	if (GLEW_OK != glew_error)
	{
		std::cerr << "glew init failed: " << glewGetErrorString(glew_error) << std::endl;
		return 1;
	}

	std::cout << "Loading input mesh from..." << std::string(argv[1]) + "/capreal/"<<  std::endl;
	fribr::Scene::Ptr scene;
	std::vector<std::string> mesh_paths{
	std::string(argv[1]) + "/capreal/mesh_final_retextured_cleaned.obj",
	std::string(argv[1]) + "/capreal/mesh_final_retextured.obj",
	std::string(argv[1]) + "/capreal/mesh_final.obj",
	std::string(argv[1]) + "/capreal/mesh.obj",
	std::string(argv[1]) + "/capreal/mesh.ply"
	};
	for (std::string mesh_path : mesh_paths)
	{
		try
		{
			scene.reset(new fribr::Scene(mesh_path, RH_Y_UP, false, false));
		}
		catch (const std::exception &e)
		{
			std::cout << e.what() << std::endl;
		}
		if (scene) {
			std::cout << "Loaded mesh from: " << mesh_path << std::endl;
			break;
		}
	}
	if (!scene)
	{
		std::cerr << "Could not find mesh [/capreal/mesh.(ply|obj)] "
			<< "in the scene directory: " << argv[1] << std::endl;
		return 1;
	}

	std::cout << "Loading SfM scene..." << std::endl;
	fribr::CalibratedImage::Vector cameras;
	fribr::PointCloud::Ptr         point_cloud;
	fribr::PointCloudRenderer::Ptr point_renderer;
	fribr::Observations            observations;
	try
	{
		const std::string sfm_path = std::string(argv[1]) + "/colmap/database.db";
		recon::load_calibrated_images(sfm_path,
			cameras,
			point_cloud,
			point_renderer,
			observations);
		if (max_image_width > 0)
		{
			for (fribr::CalibratedImage &camera : cameras)
				camera.set_max_width(max_image_width);
		}
	}
	catch (const std::exception &e)
	{
		std::cerr << "Could not load SfM reconstruction "
			<< e.what() << std::endl;
		return 1;
	}

	std::vector<std::vector<Eigen::Vector3f>> camera_points(cameras.size());
	std::vector<std::vector<Eigen::Vector3f>> camera_normals(cameras.size());
	std::vector<int> common_points;

	std::string folderName = (inside_out) ? "inside_out" : "deep_blending";

	if (!directoryExists(std::string(argv[1]) + "/" + folderName + "/")) {
		make_directory(std::string(argv[1]) + "/" + folderName + "/");
	}

	const std::string depth_map_path = std::string(argv[1]) + "/" + folderName + "/depthmaps";
	if (directory_exists(depth_map_path) && inside_out)
	{
		std::cout << "Loading precomputed depth maps at: "
			<< depth_map_path << std::endl;

		std::vector<Eigen::Vector2i> camera_resolutions(cameras.size());
		recon::load_depth_maps(depth_map_path, cameras,
			camera_points, camera_normals,
			&camera_resolutions);
		if (camera_points[0].empty())
		{
			std::cerr << "Error loading depth maps" << std::endl;
			return 1;
		}

		int loaded_max_width = -1;
		for (int i = 0; i < static_cast<int>(camera_resolutions.size()); ++i)
		{
			Eigen::Vector2i loaded_resolution = camera_resolutions[i];
			Eigen::Vector2i original_resolution = cameras[i].get_scaled_resolution();
			if (loaded_resolution.x() == original_resolution.x())
				continue;

			loaded_max_width = std::max(loaded_max_width, loaded_resolution.x());
		}

		if (loaded_max_width > 0 && loaded_max_width != max_image_width)
		{
			std::cout << "Changing max width to match loaded depth maps ("
				<< max_image_width << " -> " << loaded_max_width << ")"
				<< std::endl;

			max_image_width = loaded_max_width;
			for (fribr::CalibratedImage &camera : cameras)
				camera.set_max_width(max_image_width);
		}

		std::cout << "Finding common points between images..." << std::endl;
		common_points = recon::find_common_points(cameras, camera_points);
	}
	else if(!cloud_out)
	{
		std::cout << "Building initial depth maps..." << std::endl;
		
		try
		{
			patcher::MeshRayTracer ray_tracer;
			ray_tracer.set_scene(scene);
			recon::compute_depth_maps_from_mesh(cameras, ray_tracer,
				camera_points, camera_normals);
		}
		catch (const std::exception & e)
		{
			std::cerr << "Could not create initial depth maps: "
				<< e.what() << std::endl;
			return 1;
		}


		std::cout << "Finding common points between images..." << std::endl;
		common_points = recon::find_common_points(cameras, camera_points);

		if (!inside_out) {
			const std::string colmap_depth_path =
				std::string(argv[1]) + "/colmap/stereo/stereo/depth_maps";
			if (directory_exists(colmap_depth_path))
			{
				std::cout << "Refining depth maps using COLMAP" << std::endl;
				recon::colmap_refine_depth_map(cameras, camera_points, camera_normals, -1);
			}
		}

		std::cout << "Running single-pixel refinement..." << std::endl;
		// We use 2% of the current depth as the sigma for the mesh prior.
		static const float prior_sigma = 0.02f;

		for (CalibratedImage &camera : cameras)
			camera.load_image();

		size_t progress = 0;
#ifdef NDEBUG
#ifdef INRIA_WIN
		// actuall a Windows 7 bug
#pragma omp parallel for 
#else
#pragma omp parallel for schedule(dynamic, 1)
#endif
#endif
		for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
		{
			recon::refine_depth_map(cameras, camera_points, camera_normals,
				common_points, i, prior_sigma, recon::BatchAll);
#ifdef INRIA_WIN

			size_t prev_progress = doAtomicOp(&progress, 1);
#else
			size_t prev_progress = __sync_fetch_and_add(&progress, 1);
#endif
			std::cout << "\rRefining depth maps: "
				<< (prev_progress + 1.0f) * 100.0f / cameras.size()
				<< "%                 " << std::flush;
		}
		std::cout << std::endl;

		for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
			cameras[i].clear_image();

		std::cout << "Storing refined depth maps... " << std::endl;
		recon::save_depth_maps(depth_map_path, cameras, camera_points, camera_normals);
		std::cout << std::endl;
	}

	if (colmap_out)
	{
		std::cout << "Exporting refined depth to COLMAP..." << std::endl;
		recon::save_colmap_depths(cameras, camera_points, recon::PhotometricDepth);
	}

	if (!cloud_out) {
		std::cout << "Running geometric consistency filter..." << std::endl;
		for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
			recon::cross_filter_depth_map(cameras, camera_points, common_points, i);
	}

	if (colmap_out)
	{
		std::cout << "Exporting depth after geometric consistency to COLMAP..." << std::endl;
		recon::save_colmap_depths(cameras, camera_points, recon::GeometricDepth);
	}

	if (!inside_out && !cloud_out)
	{
		std::cout << "Exporting scene to nvm format..." << std::endl;
		export_as_nvm(argv[1], cameras, camera_points);
	}

	// Conserve memory: Free up everything that's not in use.
	point_cloud.reset();
	point_renderer.reset();

	for (CalibratedImage &camera : cameras)
		camera.clear_image();

	observations.points.clear();
	observations.observations.clear();
	camera_normals.clear();

	const std::string per_view_mesh_dir = std::string(argv[1]) + "/" + folderName + "/pvmeshes";
	std::vector<fribr::Mesh::Ptr> meshes;
	if (directory_exists(per_view_mesh_dir))
	{
		camera_points.clear(); // No need to keep the points in memory, we already have meshes!
		std::cout << "Loading precomputed meshes" << std::endl;
		meshes = load_precomputed_meshes(per_view_mesh_dir, cameras);
	}

	if (cloud_out)
	{
		std::string pCloud_path = "/output.pcloud";
		std::cout << "Creating patch cloud..." << std::endl;
		patcher::PatchCloud cloud(Eigen::Vector3i(grid_size, grid_size, grid_size));
		if (grid_bounds_from_cameras)
		{
			Eigen::Array3f camera_min( 1e21f,  1e21f,  1e21f);
			Eigen::Array3f camera_max(-1e21f, -1e21f, -1e21f);
			for (CalibratedImage& camera : cameras)
			{
				camera_min = camera_min.min(camera.get_position().array());
				camera_max = camera_max.max(camera.get_position().array());
			}
			
			static const float SAFETY_MARGIN    = 3.0f;
			static const float EXPANSION_FACTOR = (SAFETY_MARGIN - 1.0f) / 2.0f;
			Eigen::Array3f camera_diagonal = camera_max - camera_min;
			camera_min -= EXPANSION_FACTOR * camera_diagonal;
			camera_max += EXPANSION_FACTOR * camera_diagonal;

			cloud.set_scene(scene, camera_min, camera_max);
		}
		else
		{
			cloud.set_scene(scene);
		}

		if (!harmonized_path.empty()) {
			for (int i = 0; i < cameras.size(); i++) {
				std::vector<std::string> listF = listFiles(harmonized_path);
				std::string img_path = harmonized_path + "/" + cameras[i].get_image_name() + "." + getExtension(cameras[i].get_image_path());
				if (!fileExists(img_path)) {
					std::cout << "Harmonized file " << img_path << " does not exist!!!" << std::endl;
				}
				else {
					cameras[i].set_image_path(img_path);
				}
			}
			pCloud_path = "/outputHarmonizedImages.pcloud";
		}

		if (!meshes.empty()) // Great! We have already loaded our per-view meshes.
			cloud.add_cameras(cameras, meshes, patcher::PatchCloud::DeallocateWhenDone);
		else // Otherwise, construct the meshes from scratch
			cloud.add_cameras(cameras, camera_points, compression_target,
				patcher::PatchCloud::DeallocateWhenDone);

		std::cout << std::endl;

		std::cout << "Saving result..." << std::endl;
		const std::string cloud_path = std::string(argv[1]) + "/" + folderName + pCloud_path;
		std::cout << cloud_path << std::endl;
		cloud.save(cloud_path);
	}

	return 0;
}
