/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "projects/inside_out_deep_blending/renderer/patch_cloud.h"
#include "projects/inside_out_deep_blending/renderer/recon_tools.h"
#include "projects/inside_out_deep_blending/renderer/imtools.h"

#include "projects/fribr_framework/renderer/gl_wrappers.h"
#include "projects/fribr_framework/renderer/tools.h"
#include "projects/fribr_framework/renderer/io.h"
#include "projects/fribr_framework/renderer/3d.h"
#include "projects/fribr_framework/renderer/ray_tracing.h"

#include <core/system/CommandLineArgs.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <opencv2/opencv.hpp>

#include <random>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <cstdlib>

using namespace Eigen;
using namespace fribr;

namespace
{

	Shader* create_global_dump_shader()
	{
		Shader* shader = new Shader();
		shader->vertex_shader(
			GLSL(330,
				layout(location = 0) in vec3 vertex_position;
		layout(location = 1) in vec3 vertex_normal;
		layout(location = 2) in vec2 vertex_texcoord;
		layout(location = 3) in vec3 vertex_color;

		uniform mat4 camera_to_clip;
		uniform mat4 world_to_camera;

		out vec3  position;
		out vec2  texcoord;
		out vec3  color;
		out float depth;

		void main()
		{
			vec4 position_cam = world_to_camera * vec4(vertex_position, 1.0);
			vec4 position_clip = camera_to_clip * position_cam;

			position = vertex_position;
			texcoord = vertex_texcoord;
			color = vertex_color;
			depth = -position_cam.z;
			gl_Position = position_clip;
		}
		));

		shader->fragment_shader(
			GLSL(330,
				in  vec3  position;
		in  vec2  texcoord;
		in  vec3  color;
		in  float depth;
		out vec4  frag_color;

		uniform bool use_texture;
		uniform int  display_mode;
		uniform vec3 virtual_camera;

		uniform mat4 camera_to_clip;
		uniform mat4 world_to_camera;
		uniform mat4 old_world_to_camera;
		uniform vec2 resolution;

		uniform sampler2D diffuse_sampler;

		void main()
		{
			// Default assumption is display_mode == 0 for Colors
			vec4 pixel_color;
			if (use_texture)
			{
				pixel_color = vec4(texture2D(diffuse_sampler, texcoord).rgb, 1.0f);
			}
			else
			{
				pixel_color = vec4(color, 1.0f);
			}

			if (display_mode == 1) // Depth
			{
				float clamped_depth = max(0.25, depth);
				float disparity = 1.0f / (4.0f * clamped_depth);
				pixel_color.rgb = vec3(disparity, 0.0, 0.0);
			}
			else if (display_mode == 2) // Normals
			{
				vec3 normal = normalize(cross(dFdx(position), dFdy(position)));
				pixel_color.rgb = 0.5f * (1.0f + normal);
			}
			else if (display_mode == 3) // Output dir
			{
				vec3 output_dir = normalize(virtual_camera - position);
				pixel_color.rgb = 0.5f * (1.0f + output_dir);
			}
			else if (display_mode == 5) // Flow
			{
				vec4 position_clip = camera_to_clip * world_to_camera * vec4(position, 1.0);
				vec2 screen_space_position = (vec2(1.0, 1.0) + (position_clip.xy / position_clip.w)) * 0.5;
				vec4 old_position_clip = camera_to_clip * old_world_to_camera * vec4(position, 1.0);
				vec2 old_screen_space_position = (vec2(1.0, 1.0) + (old_position_clip.xy / old_position_clip.w)) * 0.5;

				screen_space_position *= resolution;
				old_screen_space_position *= resolution;

				vec2 flow_to_old = clamp(old_screen_space_position - screen_space_position,
					vec2(-512.0, -512.0), vec2(512.0, 512.0));
				flow_to_old /= 512.0f;
				flow_to_old = 0.5f * (flow_to_old + 1.0f);
				pixel_color.rgb = vec3(flow_to_old, 0.5f);
			}

			frag_color = pixel_color;
		}
		));
		shader->link();

		return shader;
	}

	Shader* create_image_resampling_shader()
	{
		Shader* shader = new Shader();
		shader->vertex_shader(
			GLSL(330,
				layout(location = 0) in vec2 vertex_position;
		out vec2 texcoord;

		void main()
		{
			texcoord = (vertex_position + vec2(1.0, 1.0)) * 0.5;
			gl_Position = vec4(vertex_position, 0.0, 1.0);
		}
		));

		shader->fragment_shader(
			GLSL(330,
				in  vec2 texcoord;
		out vec4 frag_color;

		uniform vec2 resolution;
		uniform mat4 clip_to_camera;
		uniform mat3 intrinsics;

		uniform sampler2D diffuse_sampler;

		void main()
		{
			vec4 clip_position = vec4(texcoord * 2.0f - 1.0f, 0.0f, 1.0f);
			vec4 camera_position = clip_to_camera * clip_position;
			camera_position /= camera_position.w;

			vec3 image_position = intrinsics * camera_position.xyz;
			vec2 resampled_texcoord = image_position.xy / (resolution * image_position.z);
			frag_color = texture(diffuse_sampler, resampled_texcoord);
		}
		));
		shader->link();

		return shader;
	}

	void draw_global(const Eigen::Affine3f world_to_camera,
		const Eigen::Matrix4f camera_to_clip,
		const Eigen::Vector3f camera_position,
		const Eigen::Matrix4f old_world_to_camera,
		const Eigen::Vector2f resolution,
		Scene::Ptr scene, Shader::Ptr shader,
		patcher::PatchCloud::DisplayMode mode)
	{
		std::vector<Texture::Ptr> textures = scene->get_textures();
		std::vector<fribr::Mesh::Ptr>    meshes = scene->get_meshes();

		shader->use();
		shader->set_uniform("world_to_camera", world_to_camera);
		shader->set_uniform("old_world_to_camera", old_world_to_camera);
		shader->set_uniform("resolution", resolution);
		shader->set_uniform("camera_to_clip", camera_to_clip);
		shader->set_uniform("diffuse_sampler", 0);
		shader->set_uniform("virtual_camera", camera_position);
		shader->set_uniform("display_mode", static_cast<int>(mode));

		for (int i = 0; i < static_cast<int>(meshes.size()); ++i)
		{
			bool use_texture = !!textures[i];
			if (use_texture)
			{
				glActiveTexture(GL_TEXTURE0);
				textures[i]->bind();
			}
			assert(!meshes[i]->get_colors().empty() && "We need vertex colors for all meshes");
			shader->set_uniform("use_texture", use_texture);
			meshes[i]->draw();
			if (use_texture)
				textures[i]->unbind();
		}
		glUseProgram(0);
	}

	cv::Mat crop_to_unet_depth(const int depth, const cv::Mat& img)
	{
		const int align_to = 1 << depth;
		cv::Rect roi(0, 0,
			img.cols - (img.cols % align_to),
			img.rows - (img.rows % align_to));
		cv::Mat cropped;
		img(roi).copyTo(cropped);
		return cropped;
	}

	void error_callback(int error, const char* description)
	{
		fprintf(stderr, "Error %d: %s\n", error, description);
	}



	void fuseFrameFlow(const std::string & folder_path, const int frame_id, const int random_sample_count)
	{

		
		static const int path_sample = 1; // We only care about the flow between the first made-up frame and the reference.

		auto make_img_path = [&](int layer_id, int sample_id)
		{
			std::stringstream path_stream;
			path_stream << folder_path << "/" << std::setw(4) << std::setfill('0') << frame_id << "_";
			if (layer_id >= 0)
				path_stream << "local_layer_" << layer_id << "_";
			else
				path_stream << "global_";
			path_stream << "flow_sample_" << std::setw(4) << std::setfill('0') << sample_id << "_";
			path_stream << "path_0001.png";
			std::cout << path_stream.str() << std::endl;
			return path_stream.str();
		};

		for (int i = 0; i < random_sample_count; ++i)
		{
			cv::Mat flow_a16 = cv::imread(make_img_path(0, i), cv::IMREAD_ANYDEPTH | cv::IMREAD_COLOR);
			cv::Mat flow_b16 = cv::imread(make_img_path(1, i), cv::IMREAD_ANYDEPTH | cv::IMREAD_COLOR);
			cv::Mat flow_c16 = cv::imread(make_img_path(2, i), cv::IMREAD_ANYDEPTH | cv::IMREAD_COLOR);
			cv::Mat flow_d16 = cv::imread(make_img_path(3, i), cv::IMREAD_ANYDEPTH | cv::IMREAD_COLOR);

			cv::Mat flow_a, flow_b, flow_c, flow_d;
			flow_a16.convertTo(flow_a, CV_32FC3, 1.0f / 65535.0);
			flow_b16.convertTo(flow_b, CV_32FC3, 1.0f / 65535.0);
			flow_c16.convertTo(flow_c, CV_32FC3, 1.0f / 65535.0);
			flow_d16.convertTo(flow_d, CV_32FC3, 1.0f / 65535.0);

			cv::Mat3f combo_flow = (flow_a + flow_b + flow_c + flow_d) / 4;
			cv::Mat combo_flow16;
			combo_flow.convertTo(combo_flow16, CV_16UC3, 65535);
			cv::imwrite(make_img_path(-1, i), combo_flow16);

		}

	}

	void fuseSceneFlow(const std::string & path, const int random_sample_count) {
		// Get the count of images to process.
		int imgsCount = -1;
		std::string test_path;
		// Tiny bit hacky.
		do {
			++imgsCount;
			std::stringstream path_stream;
			path_stream << path << "/" << std::setw(4) << std::setfill('0') << imgsCount << "_";
			path_stream << "global_";
			path_stream << "flow_sample_" << std::setw(4) << std::setfill('0') << 0 << "_";
			path_stream << "path_0001.png";
			test_path = path_stream.str();

		} while (fribr::file_exists(test_path));

		if (imgsCount < 1) {
			std::cout << "No images found to process." << std::endl;
		}
		else {
			std::cout << "Processing " << imgsCount << " images." << std::endl;
		}

		#pragma omp parallel for
		for (int i = 0; i < imgsCount; ++i) {
			fuseFrameFlow(path, i, random_sample_count);
		}
	}

	Eigen::Vector3f unproject(float x, float y, float depth,
		const Eigen::Matrix3f &inv_intrinsics,
		const Eigen::Matrix4f &cam_to_world)
	{
		Eigen::Vector3f img_pos = Eigen::Vector3f(x, y, 1.0f);
		Eigen::Vector3f cam_pos = depth * (inv_intrinsics * img_pos);
		Eigen::Vector4f world_pos = cam_to_world * Eigen::Vector4f(cam_pos.x(),
			cam_pos.y(),
			cam_pos.z(),
			1.0f);
		return Eigen::Vector3f(world_pos.x(), world_pos.y(), world_pos.z());
	}

	void load_masks(const std::string& path, std::vector<cv::Mat>& masks, fribr::CalibratedImage::Vector& cameras_all)
	{
		std::cout << "Loading masks...";

		masks.resize(cameras_all.size());

#pragma omp parallel for
		for (int i = 0; i < static_cast<int>(cameras_all.size()); ++i) {
			fribr::CalibratedImage &camera = cameras_all[i];
			cv::Mat tempMask = fribr::load_image(path + "/" + camera.get_image_name() + ".png", camera.get_max_width(), cv::IMREAD_COLOR);
			masks[i] = tempMask.clone();

		}
		std::cout << " done." << std::endl;
	}
} // anonymous namespace

struct DataDumpArgs : virtual AppArgs {
    RequiredArg<std::string>  	path   					= { "path" };
    Arg<int>  					max_image_width   		= { "max_image_width",			1920 };
	Arg<Switch>					test_mode   			= { "test_mode",				false };
	Arg<Switch>					just_colors   			= { "just_colors",				false };
	Arg<Switch>					clear_white   			= { "clear_white",				false };
	Arg<Switch>					freeze_perspective  	= { "freeze_perspective",		false };
	Arg<Switch>					freeze_ibr   			= { "freeze_ibr",				false };
	Arg<Switch>					dump_flow   			= { "dump_flow",				false };
	Arg<Switch>					plug_holes   			= { "plug_holes",				false };
	Arg<Switch>					fuse_flow   			= { "fuse_flow",				false };
	Arg<Switch>					dump_masks   			= { "dump_masks",				false };
	Arg<int>  					random_samples   		= { "random_samples",			-1 };
	Arg<int>  					path_samples   			= { "path_samples",				-1 };
	Arg<std::string> 			algorithm   			= { "algorithm",				"deep_blending" };
	Arg<Switch> 				no_inpaint   			= { "no_inpaint",				false };
};


int main(int argc, char* argv[])
{

	// Parse Commad-line Args
	CommandLineArgs::parseMainArgs(argc, argv);
	DataDumpArgs myArgs;

	if (argc < 2 || argc > 10)
	{
		std::cerr << "Usage: datadump_cmd --path [SCENE_PATH] (--max_width 1920) (--test )"
			<< " (--freeze_perspective ) (--freeze_ibr ) (--plug_holes ) (--dump_flow ) (algorithm=(inside_out|deep_blending))"
			<< " (--just_colors ) (--clear_white ) (--random_samples -1) (--path_samples -1) (--fuse_flow )"
			<< std::endl;
		return 1;
	}

	if (myArgs.test_mode.get() && myArgs.random_samples.get() > 0)
	{
		std::cout << "WARNING: Test mode not compatible with random sampling. Disabling random sampling.." << std::endl;
		myArgs.random_samples = -1;
	}

	if (!myArgs.test_mode.get() && (myArgs.freeze_perspective.get() || myArgs.freeze_ibr.get()))
	{
		std::cout << "WARNING: Training mode not compatible with freezing cameras."
			<< " Turning this off." << std::endl;
		myArgs.freeze_perspective = false;
		myArgs.freeze_ibr = false;
	}

	if (myArgs.path_samples.get() <= 0 && myArgs.dump_flow.get())
	{
		std::cout << "WARNING: Flow can only be dumped with path samples."
			<< " Turning this off." << std::endl;
		myArgs.dump_flow = false;
	}

	if(myArgs.algorithm.get() != "inside_out" && myArgs.algorithm.get() != "deep_blending") {
		std::cout << "Unsupported algorithm: " << myArgs.algorithm.get() << ". quitting." << std::endl;
		return 1;
	}

	if (myArgs.fuse_flow.get()) {
		if(myArgs.random_samples.get() < 0) {
			std::cout << "WARNING: random sample count should be specified." << std::endl;
			return 1;
		}
		const std::string imgsPath(myArgs.path.get());
		std::cout << "Running flow fusion postprocess for reference frames in directory: " << imgsPath << "." << std::endl;
		fuseSceneFlow(imgsPath, myArgs.random_samples.get());
		return 0;
	}

	std::cout << "Running with these parameters:" << std::endl;
	std::cout << "path=" << myArgs.path.get() << std::endl;
	std::cout << "max_width=" << myArgs.max_image_width.get() << std::endl;
	std::cout << "test=" << (myArgs.test_mode.get() ? "true" : "false") << std::endl;
	std::cout << "just_colors=" << (myArgs.just_colors.get() ? "true" : "false") << std::endl;
	std::cout << "clear_white=" << (myArgs.clear_white.get() ? "true" : "false") << std::endl;
	std::cout << "freeze_perspective=" << (myArgs.freeze_perspective.get() ? "true" : "false") << std::endl;
	std::cout << "freeze_ibr=" << (myArgs.freeze_ibr.get() ? "true" : "false") << std::endl;
	std::cout << "plug_holes=" << (myArgs.plug_holes.get() ? "true" : "false") << std::endl;
	std::cout << "dump_flow=" << (myArgs.dump_flow.get() ? "true" : "false") << std::endl;
	std::cout << "random_samples=" << myArgs.random_samples.get() << std::endl;
	std::cout << "path_samples=" << myArgs.path_samples.get() << std::endl;
	std::cout << "dump_masks=" << (myArgs.dump_masks.get() ? "true" : "false") << std::endl;
	std::cout << "algorithm=" << myArgs.algorithm.get() << std::endl;

	// We need a GL context for CalibratedImage and PatchCloud
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
	{
		std::cerr << "Could not initialize glfw." << std::endl;
		return 1;
	}

	glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	GLFWwindow* window = glfwCreateWindow(1280, 720, "IBRNext recon cmd", NULL, NULL);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(0);

	glewExperimental = GL_TRUE;
	GLenum glew_error = glewInit();
	if (GLEW_OK != glew_error)
	{
		std::cerr << "glew init failed: " << glewGetErrorString(glew_error) << std::endl;
		return 1;
	}

	std::cout << "Loading input mesh..." << std::endl;
	Scene::Ptr scene;
	std::vector<std::string> mesh_paths {
	myArgs.path.get() + "/capreal/mesh_final_retextured_cleaned.obj",
	myArgs.path.get() + "/capreal/mesh_final_retextured.obj",
	myArgs.path.get() + "/capreal/mesh_final.obj",
	myArgs.path.get() + "/capreal/mesh.obj",
	myArgs.path.get() + "/capreal/mesh.ply"
	};
	for (std::string mesh_path : mesh_paths)
	{
		try
		{
			scene.reset(new Scene(mesh_path, RH_Y_UP));
		}
		catch (const std::exception &e)
		{
			std::cout << e.what() << std::endl;
		}
		if (scene)
			break;
	}
	if (!scene)
	{
		std::cerr << "Could not find mesh [/capreal/mesh.(ply|obj)] "
			<< "in the scene directory: " << myArgs.path.get() << std::endl;
		return 1;
	}

	std::cout << "Loading SfM scene..." << std::endl;
	CalibratedImage::Vector cameras;
	PointCloud::Ptr         point_cloud;
	PointCloudRenderer::Ptr point_renderer;
	Observations            observations;
	try
	{
		const std::string sfm_path = myArgs.path.get() + "/colmap/database.db";
		recon::load_calibrated_images(sfm_path,
			cameras,
			point_cloud,
			point_renderer,
			observations);
		if (myArgs.max_image_width.get() > 0)
		{
			for (CalibratedImage &camera : cameras)
				camera.set_max_width(myArgs.max_image_width.get());
		}
	}
	catch (const std::exception &e)
	{
		std::cerr << "Could not load SfM reconstruction "
			<< e.what() << std::endl;
		return 1;
	}

	std::cout << "Loading patch cloud..." << std::endl;
	const std::string cloud_name = myArgs.no_inpaint.get() ? "output_baseline.pcloud" : "output.pcloud";
	const std::string cloud_path = myArgs.path.get() + "/" + myArgs.algorithm.get() + "/" + cloud_name;
	const size_t      grid_size = 72;
	patcher::PatchCloud cloud(Eigen::Vector3i(grid_size, grid_size, grid_size));
	cloud.set_scene(scene);
	if (!cloud.load(cloud_path))
	{
		std::cerr << "Could not load patch cloud at " << cloud_path << std::endl;
		return 1;
	}

	fribr::CalibratedImage::Vector test_cameras;
	if (myArgs.test_mode.get())
	{
		std::cout << "Loading test path..." << std::endl;
		const std::string test_path = myArgs.path.get() + "/testpath/path.rd.out";
		test_cameras = fribr::Bundler::load_calibrated_images(test_path);
		if (test_cameras.empty())
		{
			std::cerr << "Could not load test camera path at: " << test_path << std::endl;
			return 1;
		}
	}

	std::cout << "Creating framebuffer object..." << std::endl;
	fribr::Texture::Descriptor texture_descriptor(GL_CLAMP_TO_EDGE, GL_NEAREST, 0, fribr::TextureFormat::RGBA32F);
	std::vector<fribr::Texture::Descriptor> texture_descriptors(1, texture_descriptor);
	fribr::Framebuffer output_fbo(Eigen::Vector2i(256, 256), texture_descriptors);

	// Test that we have enough GPU memory for the output resolution.
	try
	{
		const Eigen::Vector2i maximum_expected_resolution(myArgs.max_image_width.get(), myArgs.max_image_width.get());
		output_fbo.set_resolution(maximum_expected_resolution);
	}
	catch (const std::exception &e)
	{
		std::cerr << "Could not create framebuffer object (out of GPU memory?) "
			<< e.what() << std::endl;
		return 1;
	}

	std::cout << "Building shaders for global geometry..." << std::endl;
	Shader::Ptr global_shader(create_global_dump_shader());
	if (!global_shader)
	{
		std::cerr << "Could not build global geometry shader!" << std::endl;
		return 1;
	}

	Shader::Ptr resample_shader(create_image_resampling_shader());
	if (!resample_shader)
	{
		std::cerr << "Could not build image resampling shader!" << std::endl;
		return 1;
	}

	std::shared_ptr<patcher::PatchCloud> cloud_masks(new patcher::PatchCloud(Eigen::Vector3i(grid_size, grid_size, grid_size)));
	if (myArgs.dump_masks.get()) {
		
		std::cout << "Loading masks patch cloud..." << std::endl;
		const std::string cloud_mask_path = myArgs.path.get() + "/output_masks.pcloud";
		cloud_masks->set_scene(scene);
		if (!cloud_masks->load(cloud_mask_path))
		{
			std::cerr << "Could not load mask patch cloud at " << cloud_mask_path << std::endl;
			cloud_masks = nullptr;
		}
	}
	
	std::cout << "Dumping data..." << std::endl;
	const std::string data_dir = myArgs.test_mode.get() ? (myArgs.path.get() + "/testdump" + (myArgs.no_inpaint.get() ? "_base" : ""))
		: (myArgs.path.get() + "/datadump" + (myArgs.no_inpaint.get() ? "_base" : ""));
	if (!directory_exists(data_dir))
		make_directory(data_dir);

	auto read_float = [&](Eigen::Vector2i resolution)
	{
		cv::Mat screenshot = read_framebuffer(resolution, TextureFormat::RGBA32F, ReadBGR);
		screenshot.convertTo(screenshot, CV_16UC4, 65535.0);
		return screenshot;
	};
	auto read_uint8 = [&](Eigen::Vector2i resolution)
	{
		cv::Mat screenshot = read_framebuffer(resolution, TextureFormat::RGBA8, ReadBGR);
		return screenshot;
	};

	CalibratedImage::Vector& dump_cameras = myArgs.test_mode.get() ? test_cameras : cameras;
	
	// Set-up the state we need for random data augmentation (IBR center and subpixel sampling)
	std::default_random_engine            generator;
	std::uniform_real_distribution<float> uniform_distribution(0.0f, 1.0f);

	float average_baseline = 0.0f;
	if (myArgs.random_samples.get() > 0)
	{
		for (int i = 0; i < static_cast<int>(dump_cameras.size()); ++i)
		{
			float min_distance = 1e21f;
			for (int j = 0; j < static_cast<int>(dump_cameras.size()); ++j)
			{
				if (j == i)
					continue;
				float distance = (dump_cameras[i].get_position() - dump_cameras[j].get_position()).norm();
				min_distance = std::min(min_distance, distance);
			}
			average_baseline += min_distance;
		}
		average_baseline /= dump_cameras.size();
		std::cout << "Average baseline: " << average_baseline << std::endl;
	}

	// Store a world_to_camera matrix and the corresponding camera location,
	// so we can use it to freeze either the camera location or the IBR location.
	Eigen::Affine3f frozen_world_to_camera = dump_cameras[dump_cameras.size() / 2].get_world_to_camera();
	Eigen::Vector3f frozen_camera_position =
		(frozen_world_to_camera.inverse() * Eigen::Vector4f(0.0f, 0.0f, 0.0f, 1.0f)).head<3>();

	// Store all cameras for replicability.
	std::vector<Eigen::Matrix4f> cameraMats;
	std::vector<Eigen::Vector3f> cameraDetails;
	// For each camera in the input cameras or the bundle.

	for (int i = 0; i < static_cast<int>(dump_cameras.size()); ++i)
	{
		std::cout << "Dumping camera: " << std::setw(4) << std::setfill('0') << i + 1
			<< "/" << std::setw(4) << std::setfill('0') << dump_cameras.size()
			<< std::endl;

		std::stringstream filename_stream;

		CalibratedImage &camera = dump_cameras[i];
		Eigen::Vector2i  resolution = camera.get_scaled_resolution();

		const float near_plane = 0.2f;
		const float far_plane = 250.0f;
		Eigen::Affine3f real_world_to_camera = camera.get_world_to_camera();
		Eigen::Matrix4f real_camera_to_clip = camera.get_camera_to_clip(near_plane, far_plane);
		Eigen::Affine3f world_to_camera = myArgs.freeze_perspective.get() ? frozen_world_to_camera : real_world_to_camera;

		glViewport(0, 0, resolution.x(), resolution.y());
		output_fbo.set_resolution(resolution);
		output_fbo.bind();

		const int   cluster_k = 14;
		const float ibr_sigma = 0.25f;
		const float ibr_resolution_alpha = 0.05f;
		const float ibr_depth_alpha = 0.5f;
		const bool  show_wireframe = false;

		const float clear_value = myArgs.clear_white.get() ? 1.0f : 0.0f;

		Eigen::Matrix4f clip_to_camera = real_camera_to_clip.inverse();
		Eigen::Matrix3f intrinsics = camera.get_intrinsics();
		Eigen::Vector2f resolutionf = resolution.cast<float>();

		// SLightly move the camera target position.
		for (int jitter_i = 0; jitter_i < std::max(1, myArgs.random_samples.get()); ++jitter_i)
		{
			Eigen::Matrix4f camera_to_clip = real_camera_to_clip;
			Eigen::Vector3f real_position =
				(real_world_to_camera.inverse() * Eigen::Vector4f(0.0f, 0.0f, 0.0f, 1.0f)).head<3>();
			Eigen::Vector3f target_position = real_position;

			if (myArgs.random_samples.get() > 0 && jitter_i > 0 && myArgs.path_samples.get() <= 0)
			{
				Eigen::Affine3f camera_to_world = real_world_to_camera.inverse();
				Eigen::Vector3f camera_position = (camera_to_world * Eigen::Vector4f(0.0f, 0.0f, 0.0f, 1.0f)).head<3>();
				camera_position += Eigen::Vector3f((uniform_distribution(generator) - 0.5f) * average_baseline,
					(uniform_distribution(generator) - 0.5f) * average_baseline,
					(uniform_distribution(generator) - 0.5f) * average_baseline);
				cloud.set_fake_camera_position(camera_position);
				if (myArgs.dump_masks.get() && cloud_masks) {
					cloud_masks->set_fake_camera_position(camera_position);
				}

				float jitter_x = (uniform_distribution(generator) - 0.5f) / resolution.x();
				float jitter_y = (uniform_distribution(generator) - 0.5f) / resolution.y();
				camera_to_clip =
					Eigen::Affine3f(Eigen::Translation3f(jitter_x, jitter_y, 0.0f)).matrix() * camera_to_clip;
			}
			else if (myArgs.path_samples.get() > 0)
			{
				float distance_scale = average_baseline * 0.5f;
				target_position += Eigen::Vector3f((uniform_distribution(generator) - 0.5f) * distance_scale,
					(uniform_distribution(generator) - 0.5f) * distance_scale,
					(uniform_distribution(generator) - 0.5f) * distance_scale);
			}
			else
			{
				cloud.disable_fake_camera_position();
				if (myArgs.dump_masks.get() && cloud_masks) {
					cloud_masks->disable_fake_camera_position();
				}
			}

			if (!myArgs.test_mode.get()) {
				cloud.clear_voxel_memory();
				if (myArgs.dump_masks.get() && cloud_masks) {
					cloud_masks->clear_voxel_memory();
				}
			}




			// For each path sample between the true camera position and its jittered target position...
			for (int path_i = 0; path_i < std::max(myArgs.path_samples.get(), 1); ++path_i)
			{
				if (myArgs.path_samples.get() > 0)
				{

					Eigen::Vector3f interp_position =
						(real_position * (myArgs.path_samples.get() - path_i - 1) + target_position * path_i) / (myArgs.path_samples.get() - 1);
					world_to_camera = compute_world_to_cam(camera.get_orientation(), interp_position);
				}

				Eigen::Vector3f position =
					(world_to_camera.inverse() * Eigen::Vector4f(0.0f, 0.0f, 0.0f, 1.0f)).head<3>();

				// The first sample on a path gets the identity flow-field
				if (path_i == 0 && myArgs.dump_flow.get()) {
					cloud.set_old_world_to_camera(world_to_camera.matrix());
					if (myArgs.dump_masks.get() && cloud_masks) {
						cloud_masks->set_old_world_to_camera(world_to_camera.matrix());
					}
				}

				if (!myArgs.test_mode.get() && path_i == 0)
				{
					// First things first, let's output the reference image
					camera.load_image();

					glClearColor(clear_value, clear_value, clear_value, 0.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					glEnable(GL_DEPTH_TEST);

					resample_shader->use();
					resample_shader->set_uniform("clip_to_camera", clip_to_camera);
					resample_shader->set_uniform("diffuse_sampler", 0);
					resample_shader->set_uniform("intrinsics", intrinsics);
					resample_shader->set_uniform("resolution", resolutionf);

					glActiveTexture(GL_TEXTURE0);
					camera.get_texture()->bind();
					//draw_full_screen_quad();
					sibr::RenderUtility::renderScreenQuad();
					camera.get_texture()->unbind();
					glUseProgram(0);

					cv::Mat reference = read_uint8(resolution);
					cv::Mat cropped = crop_to_unet_depth(4, reference);

					filename_stream.str("");
					filename_stream << std::setw(4) << std::setfill('0') << i << "_reference.jpg";
					cv::imwrite(data_dir + "/" + filename_stream.str(), cropped);
					camera.clear_image();
				}
				else if (path_i == 0)
				{ // in test mode.
					glClearColor(clear_value, clear_value, clear_value, 0.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					glEnable(GL_DEPTH_TEST);

					draw_global(world_to_camera, real_camera_to_clip, position,
						cloud.get_old_world_to_camera(), resolutionf,
						scene, global_shader, patcher::PatchCloud::Colors);

					if (myArgs.freeze_ibr.get()) {
						cloud.set_fake_camera_position(frozen_camera_position);
						if (myArgs.dump_masks.get() && cloud_masks) {
							cloud_masks->set_fake_camera_position(frozen_camera_position);
						}
					}
					else if (myArgs.freeze_perspective.get())
					{
						Eigen::Vector3f camera_position =
							(real_world_to_camera.inverse() * Eigen::Vector4f(0.0f, 0.0f, 0.0f, 1.0f)).head<3>();
						cloud.set_fake_camera_position(camera_position);
						if (myArgs.dump_masks.get() && cloud_masks) {
							cloud_masks->set_fake_camera_position(camera_position);
						}
					}

					cloud.set_display_mode(patcher::PatchCloud::Colors);
					cloud.set_display_layer(0);
					cloud.cluster_threshold(cluster_k);
					cloud.update_voxel_memory(false);
					cloud.draw(near_plane, far_plane, ibr_sigma, ibr_resolution_alpha, ibr_depth_alpha,
						world_to_camera, real_camera_to_clip, show_wireframe);

					cv::Mat reference = read_uint8(resolution);
					cv::Mat cropped = crop_to_unet_depth(4, reference);

					filename_stream.str("");
					filename_stream << std::setw(4) << std::setfill('0') << i << "_reference.jpg";
					cv::imwrite(data_dir + "/" + filename_stream.str(), cropped);
				}

				// Save camera matrices.
				cameraMats.push_back(world_to_camera.matrix());
				cameraMats.push_back(camera_to_clip);
				cameraDetails.push_back(Eigen::Vector3f(resolutionf[0], resolutionf[1], camera.get_focal_y()));
				// Now, output all the data for the global geometry
				std::vector<patcher::PatchCloud::DisplayMode> global_modes{
					patcher::PatchCloud::Colors
				};
				if (myArgs.dump_flow.get())
					global_modes.emplace_back(patcher::PatchCloud::Flow);
				if (!myArgs.just_colors.get())
				{
					global_modes.emplace_back(patcher::PatchCloud::Depth);
					global_modes.emplace_back(patcher::PatchCloud::Normals);
					global_modes.emplace_back(patcher::PatchCloud::OutputDir);
				}

				std::vector<std::string> mode_to_strings{ "colors" };
				std::vector<std::function<cv::Mat(Eigen::Vector2i)>> mode_to_readback{ read_uint8 };
				std::vector<std::string> mode_to_extensions{ "jpg" };
				if (myArgs.dump_flow.get())
				{
					mode_to_strings.emplace_back("flow");
					mode_to_readback.emplace_back(read_float);
					mode_to_extensions.emplace_back("png");
				}
				if (!myArgs.just_colors.get())
				{
					std::vector<std::string> new_to_strings{
					"depths", "normals", "output_dir", "input_dir"
					};
					std::vector<std::function<cv::Mat(Eigen::Vector2i)>> new_to_readback{
					read_float, read_float, read_float, read_float
					};
					std::vector<std::string> new_to_extensions{
					"png", "png", "png", "png"
					};
					for (int i = 0; i < static_cast<int>(new_to_strings.size()); ++i)
					{
						mode_to_strings.emplace_back(new_to_strings[i]);
						mode_to_readback.emplace_back(new_to_readback[i]);
						mode_to_extensions.emplace_back(new_to_extensions[i]);
					}
				}

				for (int j = 0; j < static_cast<int>(global_modes.size()); ++j)
				{
					glClearColor(clear_value, clear_value, clear_value, 0.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					glEnable(GL_DEPTH_TEST);

					draw_global(world_to_camera, camera_to_clip, position,
						cloud.get_old_world_to_camera(), resolutionf,
						scene, global_shader, global_modes[j]);
					cv::Mat data = mode_to_readback[j](resolution);
					cv::Mat cropped = crop_to_unet_depth(4, data);

					filename_stream.str("");
					filename_stream << std::setw(4) << std::setfill('0') << i << "_global"
						<< "_" << mode_to_strings[j];
					if (myArgs.random_samples.get() > 0)
						filename_stream << "_sample_" << std::setw(4) << std::setfill('0') << jitter_i;
					if (myArgs.path_samples.get() > 0)
						filename_stream << "_path_" << std::setw(4) << std::setfill('0') << path_i;
					filename_stream << "." << mode_to_extensions[j];
					cv::imwrite(data_dir + "/" + filename_stream.str(), cropped);
				}

				// Finally, output all data for each blending plane
				std::vector<patcher::PatchCloud::DisplayMode> local_modes{
					patcher::PatchCloud::Colors
				};
				if (myArgs.dump_flow.get())
					local_modes.emplace_back(patcher::PatchCloud::Flow);
				if (!myArgs.just_colors.get())
				{
					local_modes.emplace_back(patcher::PatchCloud::Depth);
					local_modes.emplace_back(patcher::PatchCloud::Normals);
					local_modes.emplace_back(patcher::PatchCloud::OutputDir);
					local_modes.emplace_back(patcher::PatchCloud::InputDir);
				}

				if (!myArgs.test_mode.get()) {
					cloud.ignore_this_camera(i);
					if(cloud_masks) {
						cloud_masks->ignore_this_camera(i);
					}
				}
					

				for (int j = 0; j < static_cast<int>(local_modes.size()); ++j)
				{
					for (int k = 0; k < patcher::PatchCloud::NUM_LAYERS; ++k)
					{
						glClearColor(clear_value, clear_value, clear_value, 0.0f);
						glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
						glEnable(GL_DEPTH_TEST);

						if (myArgs.freeze_ibr.get())
							cloud.set_fake_camera_position(frozen_camera_position);
						else if (myArgs.freeze_perspective.get())
						{
							Eigen::Vector3f camera_position =
								(real_world_to_camera.inverse() * Eigen::Vector4f(0.0f, 0.0f, 0.0f, 1.0f)).head<3>();
							cloud.set_fake_camera_position(camera_position);
						}

						if (myArgs.plug_holes.get())
						{
							patcher::PatchCloud::DisplayMode global_mode = local_modes[j];
							if (global_mode == patcher::PatchCloud::InputDir)
								global_mode = patcher::PatchCloud::OutputDir;
							draw_global(world_to_camera, real_camera_to_clip, position,
								cloud.get_old_world_to_camera(), resolutionf,
								scene, global_shader, global_mode);
						}

						cloud.set_display_mode(local_modes[j]);
						cloud.set_display_layer(k + 1);
						cloud.cluster_threshold(cluster_k);

						// Make sure that the voxel memory book-keeping only occurs once per camera location
						cloud.update_voxel_memory(j == 0 && k == 0);

						cloud.draw(near_plane, far_plane, ibr_sigma, ibr_resolution_alpha, ibr_depth_alpha,
							world_to_camera, camera_to_clip, show_wireframe);

						cv::Mat data = mode_to_readback[j](resolution);
						cv::Mat cropped = crop_to_unet_depth(4, data);

						filename_stream.str("");
						filename_stream << std::setw(4) << std::setfill('0') << i << "_local" << "_layer_" << k
							<< "_" << mode_to_strings[j];
						if (myArgs.random_samples.get() > 0)
							filename_stream << "_sample_" << std::setw(4) << std::setfill('0') << jitter_i;
						if (myArgs.path_samples.get() > 0)
							filename_stream << "_path_" << std::setw(4) << std::setfill('0') << path_i;
						filename_stream << "." << mode_to_extensions[j];
						cv::imwrite(data_dir + "/" + filename_stream.str(), cropped);
					} // k
				} // j


				// read back as bgr, so labels as bgr
				std::vector<Eigen::Vector3f> labels = { {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 128.0f}, {0.0f,128,0.0f}, {0.0f,128,128} };

				// Dump semantic labels candidates.
				if(myArgs.dump_masks.get()) {
					for (int k = 0; k < patcher::PatchCloud::NUM_LAYERS; ++k)
					{
						glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
						glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
						glEnable(GL_DEPTH_TEST);

						if (myArgs.freeze_ibr.get())
							cloud_masks->set_fake_camera_position(frozen_camera_position);
						else if (myArgs.freeze_perspective.get())
						{
							const Eigen::Vector3f camera_position =
								(real_world_to_camera.inverse() * Eigen::Vector4f(0.0f, 0.0f, 0.0f, 1.0f)).head<3>();
							cloud_masks->set_fake_camera_position(camera_position);
						}

						
						cloud_masks->set_display_mode(patcher::PatchCloud::Colors);
						cloud_masks->set_display_layer(k + 1);
						cloud_masks->cluster_threshold(cluster_k);

						// Make sure that the voxel memory book-keeping only occurs once per camera location
						cloud_masks->update_voxel_memory( k == 0);

						cloud_masks->draw(near_plane, far_plane, 0.1f, 0.01f, 0.25f,
							world_to_camera, camera_to_clip, show_wireframe); /// \todo why last bool parameter?: , true);

						cv::Mat ldata = read_uint8(resolution);
						cv::Mat cropped = crop_to_unet_depth(4, ldata);
						cv::Mat labelMap(cropped.rows, cropped.cols, CV_8UC1, cv::Scalar(0));
						// The image is CV_8UC4
						for(int y = 0; y < cropped.rows; ++y) {
							for (int x = 0; x < cropped.cols; ++x) {
								const cv::Vec4b lc1 = cropped.at<cv::Vec4b>(y, x);
								const Eigen::Vector3f labelColorF = Eigen::Vector3f(float(lc1[0]), float(lc1[1]) , float(lc1[2]) );
								float dist = (labelColorF - labels[0]).lpNorm<1>();
								int label = 0;

								for (int lid = 1; lid < labels.size(); ++lid) {
									const float newDist = (labelColorF - labels[lid]).lpNorm<1>();
									if(newDist <= dist) {
										dist = newDist;
										label = lid;
									}
								}
								
								labelMap.at<uchar>(y, x) = uchar(label);
							}
						}
						
						filename_stream.str("");
						filename_stream << std::setw(4) << std::setfill('0') << i << "_local" << "_layer_" << k
							<< "_" << "labels";
						if (myArgs.random_samples.get() > 0)
							filename_stream << "_sample_" << std::setw(4) << std::setfill('0') << jitter_i;
						if (myArgs.path_samples.get() > 0)
							filename_stream << "_path_" << std::setw(4) << std::setfill('0') << path_i;
						filename_stream << "." << "png";
						cv::imwrite(data_dir + "/" + filename_stream.str(), labelMap);
					} // k
				}



				if (myArgs.dump_flow.get())
					cloud.set_old_world_to_camera(world_to_camera.matrix());
			} // path_i
		} //jitter_i

		output_fbo.unbind();
		std::cout << std::endl;
	}

	// Dump the cameras (could be useful)
	std::ofstream camerasFile(data_dir + "/" + "cameras.txt");
	if(!camerasFile.is_open()) {
		std::cerr << "Warning: unable to save camera paths." << std::endl;
		return 1;
	}
	const size_t cameraCount = cameraMats.size() / 2;
	camerasFile << cameraCount << " " << myArgs.path_samples.get() << " " << myArgs.random_samples.get() << std::endl;

	const static Eigen::IOFormat onelineFormat(15, Eigen::DontAlignCols, ", ", ", ");

	for(size_t cid = 0; cid < cameraCount; ++cid) {
		const Eigen::Matrix4f & worldToCam = cameraMats[2 * cid];
		const Eigen::Matrix4f & camToClip = cameraMats[2 * cid + 1];
		camerasFile << worldToCam.format(onelineFormat) << std::endl;
		camerasFile << camToClip.format(onelineFormat) ;
		camerasFile << cameraDetails[cid][0] << ", " << cameraDetails[cid][1] << ", " << cameraDetails[cid][2] << std::endl;
	}
	camerasFile.close();


	// fuseSceneFlow(data_dir, myArgs.random_samples.get());
	return 0;
}
