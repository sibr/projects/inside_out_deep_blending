/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <core/graphics/Mesh.hpp>
#include <core/assets/InputCamera.hpp>
#include "DepthMesher.h"
#include <core/graphics/Window.hpp>
#include <windows.h>
#include <map>
#include "core/system/CommandLineArgs.hpp"

#define PROGRAM_NAME "sibr_depth_map_mesher"
using namespace sibr;

#define DIV 1024 // used to convert to mega byte.

const char* usage = ""
"Usage: " PROGRAM_NAME " -path " "\n"
;

struct DeepBlendingPreprocessArgs :
	virtual BasicIBRAppArgs {
	Arg<bool> hedman = { "--hedman" };
	Arg<bool> evalL2 = { "--evalL2" };
	Arg<bool> genSibr = { "--genSibr" };
	
};

int main(int ac, char** av)
{
	sibr::Window	window(100, 100, PROGRAM_NAME);

	CommandLineArgs::parseMainArgs(ac, av);
	DeepBlendingPreprocessArgs myArgs;
	

	//Future input
	std::vector<std::pair<std::string, float>> pathRatScenes = {{ std::string(myArgs.dataset_path), 50.0}};
	

	bool hedman = myArgs.hedman;
	bool evalL2 = myArgs.evalL2;

	const bool genSibr = myArgs.genSibr && evalL2;
	if(genSibr) {
		std::cout << "Generating SIBR scene." << std::endl;
	}
	const bool textureAlpha = false;

	for (auto & pathRatScene : pathRatScenes) {

		std::string pathScene = pathRatScene.first;
		float ratioX = pathRatScene.second;
		if (!boost::filesystem::exists(pathScene)) {
			std::cout << "path not found" << std::endl;
			continue;
		}
		else {
			std::cout << pathScene << std::endl;
		}

		std::vector<sibr::InputCamera::Ptr> cams;
		cams = sibr::InputCamera::loadNVM(pathScene + "/" + "deep_blending"  + "/nvm/scene.nvm");
		std::vector<sibr::Vector2u> imSizes(cams.size());
		const int maxCam = cams.size();
		const int minCam = 0;

		if (!boost::filesystem::exists(pathScene + "/" + "deep_blending"  + "/sibr_scene/")) {
			sibr::makeDirectory(pathScene + "/" + "deep_blending"  + "/sibr_scene/");
			if(textureAlpha){
				sibr::makeDirectory(pathScene + "/" + "deep_blending"  + "/textureAlpha/");
			}
		}

		std::ofstream outputBundleCam;
		std::ofstream outputListIm;

		if (genSibr) {
			outputBundleCam.open(pathScene + "/" + "deep_blending"  + "/sibr_scene/bundle.out");
			outputListIm.open(pathScene + "/" + "deep_blending"  + "/sibr_scene/list_images.txt");
			outputBundleCam << "# Bundle file v0.3" << std::endl;
			outputBundleCam << maxCam << " " << 0 << std::endl;
		}


		//Very rough estimate of how much parallel thread we can have without overloading the memory
		int numPixelPerIm;
		{
			sibr::ImageRGB image;
			image.load(pathScene + "/" + "deep_blending"  + "/nvm/" + cams[0]->name());
			int w = image.w();
			int h = image.h();
			numPixelPerIm = w * h;
		}


		size_t freeMemKo = sibr::getAvailableMem();
		// From what I saw each mega pixel requires approx 0.5 Go, hence 1 pixel requires 0.5 Ko
		size_t neededMemKoPerIm = (size_t)(numPixelPerIm * 0.5);
		unsigned int maxNumImParallel = (unsigned int)(freeMemKo / neededMemKoPerIm);

		std::cout << " - Num proc : " << omp_get_num_procs()
			<< "\n - Free mem in Ko : " << freeMemKo
			<< "\n - Needed mem/im in Ko : " << neededMemKoPerIm
			<< "\n - Max number of im in parralel : " << maxNumImParallel
			<< std::endl;

		unsigned int numVertPerView = numPixelPerIm / ratioX;

		std::map<int,std::shared_ptr<DepthMesher>> DMs;
#pragma omp parallel for num_threads(std::min(maxNumImParallel, (unsigned)omp_get_num_procs()))
		for (int c = minCam; c < maxCam; c++) {
			auto & camIm = *cams[c];

			std::string baseNameFile = boost::filesystem::basename(camIm.name());

			std::string extensionFile = boost::filesystem::extension(camIm.name());
			std::cout << baseNameFile << " " << c << " of " << maxCam << std::endl;

			//Load the image
			sibr::ImageRGB image;
			image.load(pathScene + "/" + "deep_blending"  + "/nvm/" + camIm.name());
			int w = image.w();
			int h = image.h();

			imSizes[c].x() = w;
			imSizes[c].y() = h;

			//load the corresponding depthMap
			sibr::ImageL32F depthMap;
			depthMap = DepthMesher::loadDepthCOLMAP(pathScene + "/" + "deep_blending"  + "/nvm/depth_maps/" + baseNameFile + ".bin");

			std::shared_ptr<DepthMesher> DM(new DepthMesher(depthMap, image, camIm)); //297->92
			DM->genMesh(numVertPerView,textureAlpha,hedman,evalL2);
			
			//save Ply file 
			if (textureAlpha && !evalL2) {
				DM->saveTextAlpha(pathScene + "/" + "deep_blending"  + "/textureAlpha/" + baseNameFile + ".png");
				DM->genAndSavePly(pathScene + "/" + "deep_blending"  + "/pvmeshes/" + baseNameFile + ".ply", true, "../textureAlpha/" + baseNameFile + ".png");
			}
			else if(!evalL2){
				DM->genAndSavePly(pathScene + "/" + "deep_blending"  + "/pvmeshes/" + baseNameFile + ".ply", true, "../nvm/" + camIm.name());
			}

			//Copy images with good name and save pvmeshes without texture coordinates
			if (genSibr) {
				std::ostringstream ssZeroPad;
				ssZeroPad << std::setw(8) << std::setfill('0') << c;

				std::string newFileName = ssZeroPad.str() + extensionFile;
				boost::filesystem::copy_file(pathScene + "/" + "deep_blending"  + "/nvm/" + camIm.name(), pathScene + "/" + "deep_blending"  + "/sibr_scene/" + newFileName, boost::filesystem::copy_option::overwrite_if_exists);
				DM->genAndSavePly(pathScene + "/" + "deep_blending"  + "/sibr_scene/pvmeshes/pvmesh-" + std::to_string(c) + ".ply", true);

				// put info in bundle and list images
			}

			if (evalL2)
				DMs[c] = DM;
		}

		if (evalL2) {
			//Need to load cameras with good w and h, so we can render depth.
			cams = sibr::InputCamera::loadNVM(pathScene + "/" + "deep_blending"  + "/nvm/scene.nvm",0.1,250, imSizes);
			for (int c = minCam; c < maxCam; c++) {
				DMs[c]->evalL2Depth(*cams[c]);
			}
		}

		//We took this out of the parallel loop so number are ordered correctly
		if (genSibr) {
			for (int c = minCam; c < maxCam; c++) {
				auto & camIm = *cams[c];

				std::string extensionFile = boost::filesystem::extension(camIm.name());
				std::ostringstream ssZeroPad;
				ssZeroPad << std::setw(8) << std::setfill('0') << c;
				std::string newFileName = ssZeroPad.str() + extensionFile;

				outputBundleCam << camIm.toBundleString();
				outputListIm << newFileName << " " << imSizes[c].x() << " " << imSizes[c].y() << std::endl;
			}
		}

		if (genSibr) {

			outputBundleCam.close();
			outputListIm.close();

			if (!boost::filesystem::exists(pathScene + "/" + "deep_blending"  + "/sibr_scene/pmvs/models/")) {
				sibr::makeDirectory(pathScene + "/" + "deep_blending"  + "/sibr_scene/pmvs/models/");
			}
			const std::string meshPath = pathScene + "/capreal/mesh.ply";
			sibr::copyFile(meshPath, pathScene + "/" + "deep_blending"  + "/sibr_scene/pmvs/models/pmvs_recon.ply", true);
			
		}
	}
	return EXIT_SUCCESS;
}
