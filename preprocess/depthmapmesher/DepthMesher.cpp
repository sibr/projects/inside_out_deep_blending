/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "DepthMesher.h"

#include <iostream>
#include <queue>
#include <opencv2/photo/photo.hpp>
#include <core/renderer/DepthRenderer.hpp>

DepthMesher::DepthMesher(const sibr::ImageL32F& depth, const sibr::ImageRGB& img, sibr::InputCamera cam) : _depth(depth.clone()), _img(img.clone()), _cam(cam) {}

void DepthMesher::genMesh(unsigned int tarNumVert, bool textureAlpha, bool hedman, bool evalL2) {

	int w = _img.w();
	int h = _img.h();

	const int n_vert = w * h;
	std::vector<TriMesh::VertexHandle> vhandles;
	_mesh.request_face_colors();
	_mesh.request_vertex_status();


	sibr::Array2d<bool> edgeBoolMap(w, h, false);
	int lowThreshRatio;
	int highThreshRatio;
	sibr::Array2d<bool> switchTriangle(w, h, false);

	Eigen::Matrix3f K;
	K <<
		_cam.focal(), 0, (float)w / 2.0,
		0, _cam.focal(), (float)h / 2.0,
		0, 0, 1.0;

	Eigen::Matrix3f converter;
	converter <<
		1, 0, 0,
		0, -1, 0,
		0, 0, -1;

	Eigen::Matrix3f Kinv = K.inverse();

	//std::cout << _cam.transform() << std::endl;
	sibr::Vector3f camPos = _cam.position();
	Eigen::Matrix3f worldToCam = _cam.rotation().toRotationMatrix() * converter.transpose();

	if (hedman) {
		const float CUTOFF_RATIO = 0.2;// 1.033f;

		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {

				//Compute 3D pos of pixel.
				float d = _depth(i, j).x();

				float im_i = i;
				float im_j = j;
				sibr::Vector3f pos3DCamSpace = d * Kinv * sibr::Vector3f(i, j, 1.0f);

				sibr::Vector3f XYZ;


				XYZ = worldToCam * sibr::Vector3f(pos3DCamSpace.x(), pos3DCamSpace.y(), pos3DCamSpace.z()) + camPos;

				//Add vertex and texture coordinates
				TriMesh::VertexHandle vh = _mesh.add_vertex(TriMesh::Point(XYZ.x(), XYZ.y(), XYZ.z()));
				vhandles.push_back(vh);

			}
		}

		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {

				if (j < h - 1 && i < w - 1) {
					if (_depth(i, j).x() > 0.0 &&
						_depth(i + 1, j).x() > 0.0 &&
						_depth(i, j + 1).x() > 0.0 &&
						_depth(i + 1, j + 1).x() > 0.0) {

						/*double d1 = _depth(i, j).x();
						double d2 = _depth(i + 1, j).x();
						double d3 = _depth(i + 1, j + 1).x();
						double d4 = _depth(i, j + 1).x();
						*/
						TriMesh::VertexHandle vh_1 = vhandles[i + w * j];
						TriMesh::VertexHandle vh_2 = vhandles[i + 1 + w * j];
						TriMesh::VertexHandle vh_3 = vhandles[i + 1 + w * (j + 1)];
						TriMesh::VertexHandle vh_4 = vhandles[i + w * (j + 1)];

						TriMesh::Point p1 = _mesh.point(vh_1);
						TriMesh::Point p2 = _mesh.point(vh_2);
						TriMesh::Point p3 = _mesh.point(vh_3);
						TriMesh::Point p4 = _mesh.point(vh_4);

						//edges
						double l12 = (p1 - p2).norm();
						double l23 = (p2 - p3).norm();
						double l34 = (p3 - p4).norm();
						double l41 = (p4 - p1).norm();
						//diagonals
						double l24 = (p2 - p4).norm();
						double l13 = (p1 - p3).norm();

						/*if ((d1 / d2) > CUTOFF_RATIO ||
							(d1 / d3) > CUTOFF_RATIO ||
							(d1 / d4) > CUTOFF_RATIO ||
							(d2 / d3) > CUTOFF_RATIO ||
							(d2 / d4) > CUTOFF_RATIO ||
							(d3 / d4) > CUTOFF_RATIO ||
							(d1 / d2) < 1.0f / CUTOFF_RATIO ||
							(d1 / d3) < 1.0f / CUTOFF_RATIO ||
							(d1 / d4) < 1.0f / CUTOFF_RATIO ||
							(d2 / d3) < 1.0f / CUTOFF_RATIO ||
							(d2 / d4) < 1.0f / CUTOFF_RATIO ||
							(d3 / d4) < 1.0f / CUTOFF_RATIO)
						{
							edgeBoolMap(i, j) = true;
						}*/

						if (l12 > CUTOFF_RATIO ||
							l23 > CUTOFF_RATIO ||
							l34 > CUTOFF_RATIO ||
							l41 > CUTOFF_RATIO ||
							l24 > CUTOFF_RATIO ||
							l13 > CUTOFF_RATIO)
						{
							edgeBoolMap(i, j) = true;
						}
					}
					else {
						edgeBoolMap(i, j) = true;
					}
				}
			}
		}

		sibr::ImageL8 edgeMapIOVisu(w, h, 0);
		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				if (!isNull(edgeBoolMap, sibr::Vector2i(i, j))) {
					edgeMapIOVisu(i, j).x() = 255;
				}
			}
		}
	}
	else {

		//Pre-treat median to fill very small holes
		sibr::ImageFloat1 medianDepth(w, h, 0);
		cv::medianBlur(_depth.toOpenCV(), medianDepth.toOpenCV(), 5);
		sibr::Array2d<bool> noDepth(w, h, false);

		//showFloat(_depth);
		//showFloat(medianDepth);
		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				//Compute 3D pos of pixel.
				float d = _depth(i, j).x();
				if (d <= 0) {
					noDepth(i, j) = true;
				}
			}
		}

		std::vector < std::vector < sibr::Vector2i >>& compDepth = getComponents(noDepth);
		int minCompSizeDepth = 10;
		for (const std::vector<sibr::Vector2i>& comp : compDepth) {
			if (comp.size() <= minCompSizeDepth) {
				for (const sibr::Vector2i& pos : comp) {
					_depth(pos.x(), pos.y()) = medianDepth(pos.x(), pos.y());
				}
			}
		}
		//showFloat(_depth,false,0,0.01);


		std::vector<float> disparities;

		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {

				//Compute 3D pos of pixel.
				float d = _depth(i, j).x();

				float im_i = i;
				float im_j = j;
				sibr::Vector3f pos3DCamSpace = d * Kinv * sibr::Vector3f(i, j, 1.0f);

				sibr::Vector3f XYZ;


				XYZ = worldToCam * sibr::Vector3f(pos3DCamSpace.x(), pos3DCamSpace.y(), pos3DCamSpace.z()) + camPos;

				//Add vertex and texture coordinates
				TriMesh::VertexHandle vh = _mesh.add_vertex(TriMesh::Point(XYZ.x(), XYZ.y(), XYZ.z()));
				vhandles.push_back(vh);
				//_mesh.set_texcoord2D(vh, OpenMesh::Vec2f(((float)i + 0.5) / (float)w, ((float)h - ((float)j + 0.5)) / (float)h));
				if (d > 0) {
					disparities.push_back(1.0f / d);
				}
			}
		}

		std::sort(disparities.begin(), disparities.end());
		float medianDisparity = disparities[disparities.size() / 2];

		sibr::ImageFloat1 triQualIm(w, h, 1000.0);
		sibr::ImageFloat1 dispNormIm(w, h, 1.0);

		//static const float CUTOFF_RATIO = 1.033f;
		//static const float CUTOFF_RATIO_3D = 10.f;

		sibr::ImageFloat3 normals(w, h, sibr::Vector3f(0, 0, 0));
		sibr::ImageRGB normalsColor(w, h);
		//sibr::ImageInt1 normalCount(w, h, 0);
		sibr::ImageFloat1 devFromPlane(w, h, 1.0);


		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {

				if (j < h - 1 && i < w - 1) {


					if (_depth(i, j).x() > 0.0 &&
						_depth(i + 1, j).x() > 0.0 &&
						_depth(i, j + 1).x() > 0.0 &&
						_depth(i + 1, j + 1).x() > 0.0) {
						TriMesh::VertexHandle vh_1 = vhandles[i + w * j];
						TriMesh::VertexHandle vh_2 = vhandles[i + 1 + w * j];
						TriMesh::VertexHandle vh_3 = vhandles[i + 1 + w * (j + 1)];
						TriMesh::VertexHandle vh_4 = vhandles[i + w * (j + 1)];

						TriMesh::Point p1 = _mesh.point(vh_1);
						TriMesh::Point p2 = _mesh.point(vh_2);
						TriMesh::Point p3 = _mesh.point(vh_3);
						TriMesh::Point p4 = _mesh.point(vh_4);

						//edges
						double l12 = (p1 - p2).norm();
						double l23 = (p2 - p3).norm();
						double l34 = (p3 - p4).norm();
						double l41 = (p4 - p1).norm();
						//diagonals
						double l24 = (p2 - p4).norm();
						double l13 = (p1 - p3).norm();


						//normals weighted by area (times 2)
						if (_depth(i, j).x() > 0.0 &&
							_depth(i, j + 1).x() > 0.0 &&
							_depth(i + 1, j).x() > 0.0) {

							OpenMesh::Vec3f wn1 = OpenMesh::cross((p4 - p1), (p2 - p1));
							sibr::Vector3f n_1_4_2(wn1[0], wn1[1], wn1[2]);
							normals(i, j) += n_1_4_2;
							normals(i, j + 1) += n_1_4_2;
							normals(i + 1, j) += n_1_4_2;
						}

						//normalCount(i, j).x()++;
						//normalCount(i, j + 1).x()++;
						//normalCount(i + 1, j).x()++;

						//normals weighted by area (times 2)
						if (_depth(i + 1, j).x() > 0.0 &&
							_depth(i, j + 1).x() > 0.0 &&
							_depth(i + 1, j + 1).x() > 0.0) {

							OpenMesh::Vec3f wn2 = OpenMesh::cross((p4 - p2), (p3 - p2));
							sibr::Vector3f n_2_4_3(wn2[0], wn2[1], wn2[2]);
							normals(i + 1, j) += n_2_4_3;
							normals(i, j + 1) += n_2_4_3;
							normals(i + 1, j + 1) += n_2_4_3;
						}
						//normalCount(i + 1, j).x()++;
						//normalCount(i, j + 1).x()++;
						//normalCount(i + 1, j + 1).x()++;


						//first case 1,4,2 (_0) and 2,4,3 (_1):
						float rat1_0 = qualityMeasure(l41, l24, l12);
						float rat1_1 = qualityMeasure(l24, l34, l23);

						//second case 1,4,3 (_0) and 1,3,2 (_1):
						float rat2_0 = qualityMeasure(l41, l34, l13);
						float rat2_1 = qualityMeasure(l13, l23, l12);

						float rat_max_min;
						if (l24 < l13) {
							rat_max_min = std::max({ rat1_0,rat1_1 });
						}
						else {
							rat_max_min = std::max({ rat2_0,rat2_1 });
							switchTriangle(i, j) = true;
						}

						//float  minEdgeLength = std::min({ l12,l23,l34,l41 });
						//float  maxEdgeLength = std::max({ l12,l23,l34,l41 });

						triQualIm(i, j).x() = rat_max_min;//maxEdgeLength / minEdgeLength;
						if (_depth(i + 1, j).x() > 0.0001)
							dispNormIm(i, j).x() = std::max(0.5f, std::min(2.0f, (_depth(i + 1, j).x() * medianDisparity)));
					}

				}
			}
		}



		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {

				if (normals(i, j).squaredNorm() > 0) {
					normals(i, j).normalize();
					normalsColor(i, j) = (255 * 0.5 * (normals(i, j) + sibr::Vector3f(1.0, 1.0, 1.0))).cast<unsigned char>();
				}
				else {
					normalsColor(i, j) = sibr::Vector3ub(0, 0, 0);
				}
			}
		}
		//show(normalsColor);
		//Need to "inpaint the normals" to avoid black pixel in median blurring, this is a little bit dirty.
		cv::Mat mask = cv::Mat::zeros(normalsColor.toOpenCV().size(), CV_8UC1);
		cv::Mat greyMat;
		cv::cvtColor(normalsColor.toOpenCV(), greyMat, cv::COLOR_RGB2GRAY);
		mask.setTo(255, greyMat == 0);
		sibr::ImageL8 maskIm;
		maskIm.fromOpenCV(mask);

		cv::inpaint(normalsColor.toOpenCV(), mask, normalsColor.toOpenCV(), 5, cv::INPAINT_NS);
		//Normalize the color
		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				if (maskIm(i, j).x()) {
					float norm = ((2.0 * normalsColor(i, j).cast<float>() / 255.0) - sibr::Vector3f(1.0, 1.0, 1.0)).norm();
					normalsColor(i, j) = (normalsColor(i, j).cast<float>() / norm).cast<unsigned char>();
				}
			}
		}

		//Blur the normals with a median filter (had to go in 8bit chann to apply big kernel)
		cv::medianBlur(normalsColor.toOpenCV(), normalsColor.toOpenCV(), 9);

		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				normals(i, j) = (2.0 * normalsColor(i, j).cast<float>() / 255.0) - sibr::Vector3f(1.0, 1.0, 1.0);
				normals(i, j).normalize();
				normalsColor(i, j) = (255 * 0.5 * (normals(i, j) + sibr::Vector3f(1.0, 1.0, 1.0))).cast<unsigned char>();
			}
		}

		cv::Mat normalsColorCopy = normalsColor.clone().toOpenCV();
		cv::bilateralFilter(normalsColorCopy, normalsColor.toOpenCV(), 15, 50, 20);

		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				normals(i, j) = (2.0 * normalsColor(i, j).cast<float>() / 255.0) - sibr::Vector3f(1.0, 1.0, 1.0);
				normals(i, j).normalize();
			}
		}

		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {

				normalsColor(i, j) = (255 * 0.5 * (normals(i, j) + sibr::Vector3f(1.0, 1.0, 1.0))).cast<unsigned char>();

				if (j < h - 1 && i < w - 1) {


					if (_depth(i, j).x() > 0.0 &&
						_depth(i + 1, j).x() > 0.0 &&
						_depth(i, j + 1).x() > 0.0 &&
						_depth(i + 1, j + 1).x() > 0.0) {
						TriMesh::VertexHandle vh_1 = vhandles[i + w * j];
						TriMesh::VertexHandle vh_2 = vhandles[i + 1 + w * j];
						TriMesh::VertexHandle vh_3 = vhandles[i + 1 + w * (j + 1)];
						TriMesh::VertexHandle vh_4 = vhandles[i + w * (j + 1)];

						TriMesh::Point p1 = _mesh.point(vh_1);
						TriMesh::Point p2 = _mesh.point(vh_2);
						TriMesh::Point p3 = _mesh.point(vh_3);
						TriMesh::Point p4 = _mesh.point(vh_4);


						float dev1 = std::max(0.1f, (float)pow(-(sibr::Vector3f(p1[0], p1[1], p1[2]) - _cam.position()).normalized().dot(normals(i, j)), 1));

						devFromPlane(i, j).x() = dev1;


					}
				}

			}
		}

		//show(_img);
		//show(normalsColor);
		//showFloat(devFromPlane, false, 0, 1);
		// Perform max filtering on image using dilate
		cv::Mat kernel = cv::Mat::ones(3, 3, CV_32F);
		cv::dilate(devFromPlane.toOpenCV(), devFromPlane.toOpenCV(), kernel);

		//showFloat(devFromPlane, false, 0, 1);

		//showFloat(_depth, false);
		//showFloat(triQualIm, false, 1, 20);
		//showFloat(dispNormIm, false, 0, 2);

		sibr::ImageFloat1 triFeature(w, h, 0.0);
		triFeature.fromOpenCV(triQualIm.toOpenCV().mul(devFromPlane.toOpenCV()).mul(dispNormIm.toOpenCV()));


		int highThreshFeature = 25;
		int lowThreshFeature = highThreshFeature * 0.7;

		//showFloat(triFeature, false, lowThreshFeature, highThreshFeature);

		highThreshRatio = 40;
		lowThreshRatio = 10;

		cv::Scalar mean, stddev;
		cv::meanStdDev(triFeature.toOpenCV(), mean, stddev);

//		std::cout << "feat only " << mean << " " << stddev << std::endl;
		hysteresis(highThreshFeature, lowThreshFeature, edgeBoolMap, triFeature);
		std::vector < std::vector < sibr::Vector2i >>& compEdges = getComponents(edgeBoolMap);
		removeSmallComp(std::max(w, h) * 0.02, edgeBoolMap, compEdges, highThreshRatio, true, triQualIm);
	}

	//Òhysteresis(2*highThreshRatio, 1.5 * highThreshRatio, edgeBoolMap, triQualIm);
	//Insure that pixel with no depth are on edge.
	for (int j = 0; j < h; j++) {
		for (int i = 0; i < w; i++) {
			if (j < h - 1 && i < w - 1) {
				if (_depth(i, j).x() <= 0 ||
					_depth(i + 1, j).x() <= 0 ||
					_depth(i, j + 1).x() <= 0 ||
					_depth(i + 1, j + 1).x() <= 0) {
					edgeBoolMap(i, j) = true;
				}
			}
		}
	}

	sibr::ImageL8 edgeMapVisu(w, h, 0);
	for (int j = 0; j < h; j++) {
		for (int i = 0; i < w; i++) {
			if (!isNull(edgeBoolMap, sibr::Vector2i(i, j)) && _depth(i, j).x() > 0) {
				edgeMapVisu(i, j).x() = 255;
			}
		}
	}

	//show(_img);
	//show(edgeMapIOVisu);
	//show(edgeMapVisu);
	//edgeMapIOVisu.save("D:/Users/jphilip(D)/InsideOutV2Data/SaintAnne/edgeIO.png");
	//edgeMapVisu.save("D:/Users/jphilip(D)/InsideOutV2Data/Hugo-1/edgeNext.png");

	sibr::ImageRGB frontBack(w, h, 0);

	std::function<bool(int, int)> validQuad = [&edgeBoolMap](int i, int j) {
		return !edgeBoolMap.inRange(i, j) || !edgeBoolMap(i, j);
	};

	auto& depth = _depth;
	std::function<void(sibr::Vector2i, sibr::Vector2i, sibr::Vector2i, sibr::Vector2i)> colorizeForeGroundTri =
		[&frontBack, &depth](sibr::Vector2i t1, sibr::Vector2i t2, sibr::Vector2i t3, sibr::Vector2i op) {

		//foreGround/backGround computation
		float meanDepth = (depth(t1.x(), t1.y()).x() + depth(t2.x(), t2.y()).x() + depth(t3.x(), t3.y()).x());
		float div = 0;
		if (depth(t1.x(), t1.y()).x() != 0) { div++; }
		if (depth(t2.x(), t2.y()).x() != 0) { div++; }
		if (depth(t3.x(), t3.y()).x() != 0) { div++; }

		if (div > 0) {
			meanDepth /= div;
			float oppositeDepth = depth(op.x(), op.y()).x();
			if (meanDepth < oppositeDepth) {
				frontBack(t1.x(), t1.y()).y() = 255;
				frontBack(t2.x(), t2.y()).y() = 255;
				frontBack(t3.x(), t3.y()).y() = 255;

				frontBack(op.x(), op.y()).x() = 255;
			}
			else {
				frontBack(t1.x(), t1.y()).x() = 255;
				frontBack(t2.x(), t2.y()).x() = 255;
				frontBack(t3.x(), t3.y()).x() = 255;

				frontBack(op.x(), op.y()).y() = 255;
			}
		}

	};

	std::function<void(sibr::Vector2i, sibr::Vector2i, sibr::Vector2i, sibr::Vector2i)> colorizeForeGroundCut =
		[&frontBack, &depth](sibr::Vector2i s1, sibr::Vector2i s2, sibr::Vector2i op1, sibr::Vector2i op2) {

		float sDepth = 0;
		int sDdiv = 0;

		if (depth(s1.x(), s1.y()).x() > 0) {
			sDdiv++;
			sDepth += depth(s1.x(), s1.y()).x();
		}
		if (depth(s2.x(), s2.y()).x() > 0) {
			sDdiv++;
			sDepth += depth(s2.x(), s2.y()).x();
		}

		float opDepth = 0;
		int opDdiv = 0;

		if (depth(op1.x(), op1.y()).x() > 0) {
			opDdiv++;
			opDepth += depth(op1.x(), op1.y()).x();
		}
		if (depth(op2.x(), op2.y()).x() > 0) {
			opDdiv++;
			opDepth += depth(op2.x(), op2.y()).x();
		}

		if (opDdiv > 0 && sDdiv > 0) {
			opDepth /= opDdiv;
			sDepth /= sDdiv;

			if (sDepth < opDepth) {
				frontBack(s1.x(), s1.y()).y() = 255;
				frontBack(s2.x(), s2.y()).y() = 255;

				frontBack(op1.x(), op1.y()).x() = 255;
				frontBack(op2.x(), op2.y()).x() = 255;
			}
			else {
				frontBack(s1.x(), s1.y()).x() = 255;
				frontBack(s2.x(), s2.y()).x() = 255;

				frontBack(op1.x(), op1.y()).y() = 255;
				frontBack(op2.x(), op2.y()).y() = 255;
			}


		}

	};

	for (int j = 0; j < h; j++) {
		for (int i = 0; i < w; i++) {

			if (j < h - 1 && i < w - 1) {

				sibr::Vector2i c1(i, j);
				sibr::Vector2i c2(i + 1, j);
				sibr::Vector2i c3(i + 1, j + 1);
				sibr::Vector2i c4(i, j + 1);

				TriMesh::VertexHandle vh_1 = vhandles[c1.x() + w * c1.y()];
				TriMesh::VertexHandle vh_2 = vhandles[c2.x() + w * c2.y()];
				TriMesh::VertexHandle vh_3 = vhandles[c3.x() + w * c3.y()];
				TriMesh::VertexHandle vh_4 = vhandles[c4.x() + w * c4.y()];

				TriMesh::Point p1 = _mesh.point(vh_1);
				TriMesh::Point p2 = _mesh.point(vh_2);
				TriMesh::Point p3 = _mesh.point(vh_3);
				TriMesh::Point p4 = _mesh.point(vh_4);

				//edges
				float l12 = (p1 - p2).norm();
				float l23 = (p2 - p3).norm();
				float l34 = (p3 - p4).norm();
				float l41 = (p4 - p1).norm();
				//diagonals
				float l24 = (p2 - p4).norm();
				float l13 = (p1 - p3).norm();

				TriMesh::FaceHandle f_h;

				// Adding individual triangle of the quad if possible
					//first case 1,4,2 (_0) and 2,4,3 (_1):
				float rat1_0 = qualityMeasure(l41, l24, l12);
				float rat1_1 = qualityMeasure(l24, l34, l23);

				//second case 1,4,3 (_0) and 1,3,2 (_1):
				float rat2_0 = qualityMeasure(l41, l34, l13);
				float rat2_1 = qualityMeasure(l13, l23, l12);


				if (edgeBoolMap(i, j))
				{

					if (hedman)
						continue;

					if (!switchTriangle(i, j) &&
						rat1_0 < lowThreshRatio &&
						validQuad(i - 1, j) &&
						validQuad(i, j - 1) &&
						validQuad(i - 1, j - 1)) {

						f_h = _mesh.add_face(vh_1, vh_4, vh_2);
						_mesh.set_color(f_h, OpenMesh::Vec3uc(255, 0, 0));

						colorizeForeGroundTri(c1, c4, c2, c3);
						continue;
					}
					else if (!switchTriangle(i, j) &&
						rat1_1 < lowThreshRatio &&
						validQuad(i + 1, j) &&
						validQuad(i, j + 1) &&
						validQuad(i + 1, j + 1)) {

						f_h = _mesh.add_face(vh_2, vh_4, vh_3);
						_mesh.set_color(f_h, OpenMesh::Vec3uc(255, 0, 0));

						colorizeForeGroundTri(c2, c4, c3, c1);
						continue;
					}
					else if (switchTriangle(i, j) &&
						rat2_0 < lowThreshRatio &&
						validQuad(i - 1, j) &&
						validQuad(i, j + 1) &&
						validQuad(i - 1, j + 1)) {
						f_h = _mesh.add_face(vh_1, vh_4, vh_3);
						_mesh.set_color(f_h, OpenMesh::Vec3uc(255, 0, 0));

						colorizeForeGroundTri(c1, c4, c3, c2);
						continue;
					}
					else if (switchTriangle(i, j) &&
						rat2_1 < lowThreshRatio &&
						validQuad(i + 1, j) &&
						validQuad(i, j - 1) &&
						validQuad(i + 1, j - 1)) {
						f_h = _mesh.add_face(vh_1, vh_3, vh_2);
						_mesh.set_color(f_h, OpenMesh::Vec3uc(255, 0, 0));

						colorizeForeGroundTri(c1, c3, c2, c4);
						continue;
					}
					else {

						if (i > 1 && !edgeBoolMap(i - 1, j)) {
							colorizeForeGroundCut(c1, c4, c2, c3);
						}

						if (!edgeBoolMap(i + 1, j)) {
							colorizeForeGroundCut(c2, c3, c1, c4);
						}

						if (j > 1 && !edgeBoolMap(i, j - 1)) {
							colorizeForeGroundCut(c1, c2, c3, c4);
						}

						if (!edgeBoolMap(i, j + 1)) {
							colorizeForeGroundCut(c4, c3, c2, c1);
						}
					}

					continue;

				}

				if (!switchTriangle(i, j)) {

					if (rat1_0 < highThreshRatio) {
						f_h = _mesh.add_face(vh_1, vh_4, vh_2);
						_mesh.set_color(f_h, OpenMesh::Vec3uc(200, 200, 200));
					}
					if (rat1_1 < highThreshRatio) {
						f_h = _mesh.add_face(vh_4, vh_3, vh_2);
						_mesh.set_color(f_h, OpenMesh::Vec3uc(200, 200, 200));
					}

				}
				else {

					if (rat2_0 < highThreshRatio) {
						f_h = _mesh.add_face(vh_1, vh_4, vh_3);
						_mesh.set_color(f_h, OpenMesh::Vec3uc(200, 200, 200));
					}
					if (rat2_1 < highThreshRatio) {
						f_h = _mesh.add_face(vh_1, vh_3, vh_2);
						_mesh.set_color(f_h, OpenMesh::Vec3uc(200, 200, 200));
					}

				}

			}
		}
	}

	if (textureAlpha) {
		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				if (frontBack(i, j).x() != 0 && frontBack(i, j).y() != 0) {
					frontBack(i, j).x() = 0;
					frontBack(i, j).y() = 0;
				}
			}
		}

		//show(frontBack);


		std::vector<cv::Mat> channels(3);
		// split img:
		cv::split(frontBack.toOpenCV(), channels);

		cv::Mat kernels[4];
		kernels[0] = (cv::Mat_<unsigned char>(3, 3) << 0, 0, 0, 0, 0, 1, 0, 0, 0);
		kernels[1] = (cv::Mat_<unsigned char>(3, 3) << 0, 0, 0, 1, 0, 0, 0, 0, 0);
		kernels[2] = (cv::Mat_<unsigned char>(3, 3) << 0, 1, 0, 0, 0, 0, 0, 0, 0);
		kernels[3] = (cv::Mat_<unsigned char>(3, 3) << 0, 0, 0, 0, 0, 0, 0, 1, 0);

		for (int i = 0; i < 15; i++) {

			cv::Mat voteFront = cv::Mat::zeros(cv::Size(channels[0].cols, channels[0].rows), CV_8U);
			cv::Mat voteBack = cv::Mat::zeros(cv::Size(channels[0].cols, channels[0].rows), CV_8U);

			cv::Mat seedsBack = channels[0] / 255;
			cv::Mat seedsFront = channels[1] / 255;

			for (int c = 0; c < 4; c++) {
				cv::Mat dilF;
				cv::dilate(seedsFront, dilF, kernels[c]);
				voteFront += dilF;

				cv::Mat dilB;
				cv::dilate(seedsBack, dilB, kernels[c]);
				voteBack += dilB;
			}

			//Remove pixels that were already assigned
			voteFront.setTo(0, channels[0] > 0);
			voteFront.setTo(0, channels[1] > 0);
			voteBack.setTo(0, channels[0] > 0);
			voteBack.setTo(0, channels[1] > 0);

			channels[0].setTo(255, voteFront < voteBack);
			channels[1].setTo(255, voteFront > voteBack);

		}

		cv::merge(channels, frontBack.toOpenCV());
	}

	//show(frontBack);
	//Remove vertex without triangles.
	_mesh.delete_isolated_vertices();
	_mesh.garbage_collection();

	//smooth border : iterate over vertices
	int Ns = 5;
	if (!hedman) {
		smoothBorder(Ns, 0.2);
	}

	// Reduce number of points
	decimateMesh(tarNumVert, false, hedman, evalL2);
	/*if (!OpenMesh::IO::read_mesh(_mesh, "C:/IBR/data/Lumber/pvmeshes/DSC07146.ply"))
	{
		std::cout << "Error Loading mesh" << std::endl;
	}*/
	//Generate texture coordinates for rendering
	std::cout << "Final mesh face: " << _mesh.n_faces() << std::endl;
	genTextCoord();

	//In developpement : Alpha matting values
	if (textureAlpha)
		genAlpha(frontBack);

}


void DepthMesher::hysteresis(float hT, float lT, sibr::Array2d<bool>& binMap, const sibr::ImageFloat1& Im, bool display)
{

	int w = Im.w();
	int h = Im.h();


	for (int j = 0; j < h; j++) {
		for (int i = 0; i < w; i++) {
			if (Im(i, j).x() >= hT) {
				binMap(i, j) = true;
			}
		}
	}
	if (display) {
		sibr::ImageL8 edgeMapVisu(w, h, 0);
		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				if (binMap(i, j)) {
					edgeMapVisu(i, j).x() = 255;
				}
			}
		}
		//show(edgeMapVisu);
	}

	std::vector<sibr::Vector2i> deltas = {
		sibr::Vector2i(1,0),sibr::Vector2i(-1,0) ,sibr::Vector2i(0,1) ,sibr::Vector2i(0,-1)
		/*,sibr::Vector2i(1,1),sibr::Vector2i(-1,1) ,sibr::Vector2i(1,-1) ,sibr::Vector2i(-1,-1)*/ };
	bool edgeChanged = true;
	int it = 0;
	int changed = 0;
	bool topBottom = true;
	bool leftRight = true;
	while (edgeChanged) {

		edgeChanged = false;
		changed = 0;

		//This is done to alternate between direction. It drastically speed up the algorithm because hysteresis act as a propagation
		//If the flow of pixel is in the oposite direction of the propagation it will only propagate by one pixel at each image scan
		int i_dir, i_s, i_e, j_dir, j_s, j_e;
		if (topBottom) {
			j_dir = 1;
			j_s = 0;
			j_e = h;
		}
		else {
			j_dir = -1;
			j_s = h - 1;
			j_e = -1;
		}

		if (leftRight) {
			i_dir = 1;
			i_s = 0;
			i_e = w;
		}
		else {
			i_dir = -1;
			i_s = w - 1;
			i_e = -1;
		}

		for (int j = j_s; j != j_e; j += j_dir) {
			for (int i = i_s; i != i_e; i += i_dir) {

				//Check if pixel is between the two thresholds
				if (Im(i, j).x() < hT && Im(i, j).x() >= lT && !binMap(i, j)) {
					for (sibr::Vector2i d : deltas) {
						if (Im.isInRange(i + d.x(), j + d.y()) && binMap(i + d.x(), j + d.y())) {
							binMap(i, j) = true;
							edgeChanged = true;
							changed++;
							break;
						}
					}
				}

			}
		}

		topBottom = !topBottom;
		if (it % 4 == 3)
			leftRight = !leftRight;
		it++;


//		std::cout << "it im : " << it << " changed : " << changed << std::endl;
	}

	if (display) {
		sibr::ImageL8 edgeMapVisu(w, h, 0);
		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				if (binMap(i, j)) {
					edgeMapVisu(i, j).x() = 255;
				}
			}
		}
		//show(edgeMapVisu);
	}
}

template<class T> std::vector<std::vector<sibr::Vector2i>> DepthMesher::getComponents(T& map, const uint connectivity)
{
	const bool isImL8 = std::is_same<T, sibr::ImageL8>::value;
	const bool isBoolArray = std::is_same<T, sibr::Array2d<bool>>::value;

	if (!isImL8 && !isBoolArray) {
		std::cout << "removeSmallComp bad template type" << std::endl;
		SIBR_ERR;
	}

	if (connectivity != 4 && connectivity != 8) {
		std::cout << "removeSmallComp bad connectivity value, connectivity must be either 4 or 8." << std::endl;
		SIBR_ERR;
	}

	std::vector<sibr::Vector2i> deltas;
	if (connectivity == 4) {
		deltas = { sibr::Vector2i(0,1),sibr::Vector2i(0,-1) ,sibr::Vector2i(1,0) ,sibr::Vector2i(-1,0) };
	}
	else {
		deltas = { sibr::Vector2i(0,1),sibr::Vector2i(0,-1) ,sibr::Vector2i(1,0) ,sibr::Vector2i(-1,0),
			sibr::Vector2i(1,1),sibr::Vector2i(1,-1) ,sibr::Vector2i(-1,1) ,sibr::Vector2i(-1,-1) };
	}

	std::vector<std::vector<sibr::Vector2i>> components;
	std::map<sibr::Vector2i, int > compIds;

	int w = map.w();
	int h = map.h();

	for (int j = 0; j < h; j++) {
		for (int i = 0; i < w; i++) {
			if (!isNull(map, sibr::Vector2i(i, j))) {
				compIds[sibr::Vector2i(i, j)] = -1;
			}
		}
	}

	int compId = 0;
	for (int j = 0; j < h; j++) {
		for (int i = 0; i < w; i++) {
			sibr::Vector2i pos(i, j);

			if (!isNull(map, sibr::Vector2i(i, j)) && compIds[sibr::Vector2i(i, j)] == -1) {
				std::queue<sibr::Vector2i> candidates;
				std::vector<sibr::Vector2i> currComp;

				compIds[pos] = compId;
				currComp.push_back(pos);

				for (const sibr::Vector2i& d : deltas) {
					sibr::Vector2i posDelta(pos.x() + d.x(), pos.y() + d.y());

					if (map.isInRange(posDelta.x(), posDelta.y()) && !isNull(map, posDelta)) {
						candidates.push(posDelta);
						compIds[posDelta] = -2; //mark the pixel as visited so it is not added twice
					}

				}

				while (!candidates.empty()) {
					sibr::Vector2i cand = candidates.front();
					candidates.pop();

					compIds[cand] = compId;
					currComp.push_back(cand);

					for (const sibr::Vector2i& d : deltas) {
						sibr::Vector2i candDelta(cand.x() + d.x(), cand.y() + d.y());


						if (map.isInRange(candDelta.x(), candDelta.y()) &&
							!isNull(map, candDelta) &&
							compIds[candDelta] == -1) {
							candidates.push(candDelta);
							compIds[candDelta] = -2; //mark the pixel as visited so it is not added twice
						}
					}
				}

				components.push_back(currComp);
				compId++;

			}
		}
	}

	return components;
}

template<class T>
void DepthMesher::removeSmallComp(int minCompSize, T& map, const std::vector<std::vector<sibr::Vector2i>>& components, float highThld, bool useThldIm, const sibr::ImageFloat1& Im, bool display)
{

	int w = map.w();
	int h = map.h();

//	std::cout << "Num of comp: " << components.size() << std::endl;

	for (const std::vector<sibr::Vector2i>& comp : components) {
		if (comp.size() <= minCompSize) {
			bool remove = true;
			if (useThldIm) {
				for (const sibr::Vector2i& pos : comp) {
					if (Im(pos.x(), pos.y()).x() > highThld) {
						remove = false;
						break;
					}
				}
			}

			if (remove) {
				for (const sibr::Vector2i& pos : comp) {
					setToNull(map, pos);
				}
			}
		}
	}

	if (display) {

		sibr::ImageL8 edgeMapVisu(w, h, 0);
		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				if (!isNull(map, sibr::Vector2i(i, j))) {
					edgeMapVisu(i, j).x() = 255;
				}
			}
		}
		//show(edgeMapVisu);
	}

}


void DepthMesher::smoothBorder(int Ns, float pc)
{

	for (int s = 0; s < Ns; s++) {
		std::map<TriMesh::VertexHandle, TriMesh::Point>  cogs;
		for (TriMesh::VertexIter v_it = _mesh.vertices_begin();
			v_it != _mesh.vertices_end(); ++v_it)
		{
			if (!_mesh.is_boundary(*v_it)) {
				continue;
			}
			TriMesh::Scalar valence;
			TriMesh::Point cog;
			cog[0] = cog[1] = cog[2] = valence = 0.0;

			//Iterate over the outgoing halfedge
			for (TriMesh::VertexOHalfedgeIter voh_it = _mesh.voh_iter(*v_it); voh_it.is_valid(); ++voh_it)
			{
				// get the corresponding vertex is boundary
				if (_mesh.is_boundary(*voh_it)) {
					TriMesh::VertexHandle voh = _mesh.to_vertex_handle(*voh_it);
					cog += _mesh.point(voh);
					++valence;
				}
			}

			//Iterate over the ingoing halfedge
			for (TriMesh::VertexIHalfedgeIter vih_it = _mesh.vih_iter(*v_it); vih_it.is_valid(); ++vih_it)
			{
				// get the corresponding vertex is boundary
				if (_mesh.is_boundary(*vih_it)) {
					TriMesh::VertexHandle voh = _mesh.from_vertex_handle(*vih_it);
					cog += _mesh.point(voh);
					++valence;
				}
			}

			if (valence == 2.0)
				cogs[*v_it] = cog / valence;
			else {
				//std::cout << valence << std::endl;
				cogs[*v_it] = _mesh.point(*v_it);
			}

		}

		for (TriMesh::VertexIter v_it = _mesh.vertices_begin();
			v_it != _mesh.vertices_end(); ++v_it)
		{
			if (_mesh.is_boundary(*v_it)) {
				TriMesh::Point n_pos = (1.0 - pc) * _mesh.point(*v_it) + pc * cogs[*v_it];
				_mesh.set_point(*v_it, n_pos);
			}
		}

	}
}

void DepthMesher::decimateMesh(int tarNVert, bool lockBoundary, bool hedman, bool evalL2) {

	if (evalL2)
		std::cout << "EVALUATING MESH QUALITY" << std::endl;
	if (hedman)
		std::cout << "USING InsideOut MESHING" << std::endl;
	//Lock the boundary.
	// That might be already requested
	if (lockBoundary /*|| hedman*/) {
		_mesh.request_vertex_status();
		// Get an iterator over all halfedges
		TriMesh::HalfedgeIter he_it, he_end = _mesh.halfedges_end();
		// If halfedge is boundary, lock the corresponding vertices
		for (he_it = _mesh.halfedges_begin(); he_it != he_end; ++he_it)
			if (_mesh.is_boundary(*he_it)) {
				_mesh.status(_mesh.to_vertex_handle(*he_it)).set_locked(true);
				_mesh.status(_mesh.from_vertex_handle(*he_it)).set_locked(true);
			}
	}

	if (_mesh.n_vertices() - tarNVert > 0) {
		Decimater   decimater(_mesh);  // a decimater object, connected to a mesh
		HModQuadric hModQuadric;      // use a quadric module
		decimater.add(hModQuadric); // register module at the decimater
		std::cout << decimater.module(hModQuadric).name() << std::endl; // module access
																		/*
																		* since we need exactly one priority module (non-binary)
																		* we have to call set_binary(false) for our priority module
																		* in the case of HModQuadric, unset_max_err() calls set_binary(false) internally
																		*/
		decimater.module(hModQuadric).unset_max_err();
		OpenMesh::Vec3f campos(_cam.position().x(), _cam.position().y(), _cam.position().z());
		decimater.module(hModQuadric).set_camPos(campos);
		decimater.module(hModQuadric).set_hedman(hedman);
		decimater.module(hModQuadric).set_evalL2(evalL2);
		decimater.initialize();
		decimater.decimate(_mesh.n_vertices() - tarNVert);
		// after decimation: remove decimated elements from the mesh
		_mesh.garbage_collection();

		if (evalL2) {
			_meshesLoD = decimater.module(hModQuadric).getMeshes();
		}
	}
	else {
		std::cout << "Warning : Nothing to collapse, Target number of Vertices higher than mesh num of Vertices." << std::endl;
	}

}

std::vector<float> DepthMesher::evalL2Depth(const sibr::InputCamera& cam)
{
	Eigen::Matrix3f converter;
	converter <<
		1, 0, 0,
		0, -1, 0,
		0, 0, -1;

	sibr::Vector3f camPos = cam.position();
	int w = _depth.w();
	int h = _depth.h();

	Eigen::Matrix3f K;
	K <<
		cam.focal(), 0, (float)w / 2.0,
		0, cam.focal(), (float)h / 2.0,
		0, 0, 1.0;

	Eigen::Matrix3f Kinv = K.inverse();

	Eigen::Matrix3f worldToCam = cam.rotation().toRotationMatrix() * converter.transpose();
	Eigen::Matrix3f camToWorld = worldToCam.inverse();

	std::cout << "ERROR " << " ";
	//showFloat(_depth);
	int powSimpl = 1;
	for (auto& mesh : _meshesLoD) {
		sibr::Mesh meshSibr = DepthMesher::genSibrMesh(mesh);
		//std::cout << "Rate of simplification : " << pow(2, powSimpl) << " -- Vert: " << meshSibr.vertices().size() << std::endl;

		sibr::DepthRenderer rendererDepth(w, h);
		sibr::ImageL32F depthMapSIBR(w, h);

		rendererDepth.render(cam, meshSibr);
		rendererDepth._depth_RT->readBack(depthMapSIBR);

		double errorL2 = 0;
		int numPixError = 0;
		double errorRelativeWorld = 0;
		int numPixRelError = 0;
		sibr::ImageL32F depthMesh(w, h, 0);

		sibr::ImageL32F relativeError(w, h, 1.0);

		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {

				if (depthMapSIBR(i, j).x() == 1.0) {
					depthMesh(i, j).x() = 0;
				}
				else {

					sibr::Vector2i pixelPos(i, j);
					sibr::Vector3f pos3dMesh(cam.unprojectImgSpaceInvertY(pixelPos, depthMapSIBR(i, j).x()));
					sibr::Vector3f pixSpaceCoord = K * camToWorld * (pos3dMesh - camPos);
					depthMesh(i, j).x() = pixSpaceCoord.z();

					if (_depth(i, j).x() != 0) {
						sibr::Vector3f pos3DCamSpace = _depth(i, j).x() * Kinv * sibr::Vector3f(i, j, 1.0f);
						sibr::Vector3f pos3dDepth = worldToCam * sibr::Vector3f(pos3DCamSpace.x(), pos3DCamSpace.y(), pos3DCamSpace.z()) + camPos;

						double gtDist = (pos3dDepth - camPos).norm();
						double estDist = (pos3dMesh - camPos).norm();

						if ((abs(gtDist - estDist) / gtDist) < 10.0) {
							relativeError(i, j).x() = abs(gtDist - estDist) / gtDist;
							errorRelativeWorld += abs(gtDist - estDist) / gtDist;
							numPixRelError++;
						}



					}
				}

				if (depthMesh(i, j).x() != 0 && _depth(i, j).x() != 0) {
					errorL2 += pow(depthMesh(i, j).x() - _depth(i, j).x(), 2);
					numPixError++;
				}

			}
		}

		//std::cout << "MEAN L2 ERROR PER PIXEL: " << errorL2/numPixError << std::endl;

		std::cout << 100 * errorRelativeWorld / numPixRelError << " ";
		powSimpl++;
		//showFloat(depthMesh);
		//showFloat(relativeError,false,0,1);
	}

	return std::vector<float>();
}

sibr::ImageL32F DepthMesher::loadDepthCOLMAP(const std::string& filename)
{
	std::ifstream header(filename, std::ios::in | std::ios::binary);
	if (!header)
	{
		std::cerr << "Could not load depth map header at: " << filename << std::endl;
		return sibr::ImageL32F();
	}

	char temp;
	size_t width, height, depth;
	header >> width >> temp >> height >> temp >> depth >> temp;

	std::streampos header_end = header.tellg();
	header.close();

	if (width < 0 || height < 0 || depth != 1)
	{
		std::cerr << "Invalid depth map format ("
			<< width << "x" << height << "x" << depth << ")"
			<< " for file: " << filename << std::endl;
		return sibr::ImageL32F();
	}

	sibr::ImageL32F depth_map(width, height);
	std::ifstream data(filename, std::ios::in | std::ios::binary);
	if (!data)
	{
		std::cerr << "Could not load depth map data at: " << filename << std::endl;
		return sibr::ImageL32F();
	}

	data.seekg(header_end);
	data.read((char*)depth_map.data(), width * height * sizeof(float));
	data.close();

	return depth_map;
}

void DepthMesher::saveDepthCallMap(const std::string& filename, const sibr::ImageL32F& depth_map)
{
	std::ofstream header(filename, std::ios::out);
	if (!header)
	{
		std::cerr << "Could not load depth map header for writing: " << filename << std::endl;
		return;
	}

	header << depth_map.w() << "&" << depth_map.h() << "&" << 1 << "&";
	header.close();

	std::ofstream data(filename, std::ios::out | std::ios::binary | std::ios::app);
	if (!data)
	{
		std::cerr << "Could not load depth map data for writing: " << filename << std::endl;
		return;
	}

	data.write((char*)depth_map.data(), depth_map.w() * depth_map.h() * sizeof(float));
	data.close();
}

void DepthMesher::genTextCoord()
{

	int w = _img.w();
	int h = _img.h();

	_mesh.request_vertex_texcoords2D();
	_mesh.request_face_colors();

	Eigen::Matrix3f K;
	K <<
		_cam.focal(), 0, (float)w / 2.0,
		0, _cam.focal(), (float)h / 2.0,
		0, 0, 1.0;

	Eigen::Matrix3f converter;
	converter <<
		1, 0, 0,
		0, -1, 0,
		0, 0, -1;


	//std::cout << _cam.transform() << std::endl;
	sibr::Vector3f camPos = _cam.position();
	Eigen::Matrix3f worldToCam = _cam.rotation().toRotationMatrix() * converter.transpose();
	Eigen::Matrix3f camToWorld = worldToCam.inverse();


	//Iterate over each vertex
	for (TriMesh::VertexIter v_it = _mesh.vertices_begin(); v_it != _mesh.vertices_end(); ++v_it) {

		//Project the vertex on the image

		TriMesh::Point p = _mesh.point(*v_it);//Compute 3D pos of pixel.
		sibr::Vector3f XYZ(p[0], p[1], p[2]);

		sibr::Vector3f pixSpaceCoord = K * camToWorld * (XYZ - camPos);
		//Divide by depth
		pixSpaceCoord /= pixSpaceCoord.z();

		//We have to invert y axis for this.
		sibr::Vector2f texSpaceCoord((pixSpaceCoord.x() + 0.5) / (float)w, ((float)h - (pixSpaceCoord.y() + 0.5)) / (float)h);

		//Set the texture coordinates
		_mesh.set_texcoord2D(*v_it, OpenMesh::Vec2f(texSpaceCoord.x(), texSpaceCoord.y()));
	}

}

void DepthMesher::genAlpha(const sibr::ImageRGB& frontBack) {

	int w = _img.w();
	int h = _img.h();

	Eigen::Matrix3f K;
	K <<
		_cam.focal(), 0, (float)w / 2.0,
		0, _cam.focal(), (float)h / 2.0,
		0, 0, 1.0;

	Eigen::Matrix3f converter;
	converter <<
		1, 0, 0,
		0, -1, 0,
		0, 0, -1;

	sibr::Vector3f camPos = _cam.position();
	Eigen::Matrix3f worldToCam = _cam.rotation().toRotationMatrix() * converter.transpose();
	Eigen::Matrix3f camToWorld = worldToCam.inverse();

	sibr::ImageL8 finalOccEdges(w, h, 0);
	for (TriMesh::HalfedgeIter h_it = _mesh.halfedges_begin(); h_it != _mesh.halfedges_end(); ++h_it) {
		if (_mesh.is_boundary(*h_it)) {

			TriMesh::VertexHandle vh_f = _mesh.from_vertex_handle(*h_it);

			TriMesh::Point p_f = _mesh.point(vh_f);//Compute 3D pos of pixel.
			sibr::Vector3f XYZ_f(p_f[0], p_f[1], p_f[2]);
			sibr::Vector3f pixSpaceCoord_f = K * camToWorld * (XYZ_f - camPos);
			pixSpaceCoord_f /= pixSpaceCoord_f.z();

			TriMesh::VertexHandle vh_t = _mesh.to_vertex_handle(*h_it);

			TriMesh::Point p_t = _mesh.point(vh_t);//Compute 3D pos of pixel.
			sibr::Vector3f XYZ_t(p_t[0], p_t[1], p_t[2]);
			sibr::Vector3f pixSpaceCoord_t = K * camToWorld * (XYZ_t - camPos);
			pixSpaceCoord_t /= pixSpaceCoord_t.z();

			cv::line(finalOccEdges.toOpenCV(),
				cv::Point(floor(pixSpaceCoord_f.x()), floor(pixSpaceCoord_f.y())),
				cv::Point(floor(pixSpaceCoord_t.x()), floor(pixSpaceCoord_t.y())), cv::Scalar(255));
		}
	}

	//Remove small occlusion edges as alpha doesn't make sense on them.
	std::vector<std::vector<sibr::Vector2i>> components = getComponents(finalOccEdges, 8U);
	removeSmallComp(10, finalOccEdges, components, 40, false);
	//show(finalOccEdges);

	sibr::ImageL8 unknownRegions(w, h, 0);

	const int RADIUS_UNKNOWN = 2;
	cv::Mat kernel = cv::Mat::ones(cv::Size(RADIUS_UNKNOWN * 2 + 1, RADIUS_UNKNOWN * 2 + 1), CV_8U);
	cv::dilate(finalOccEdges.toOpenCV(), unknownRegions.toOpenCV(), kernel);

	//show(unknownRegions);

	sibr::ImageFloat1 alphas(w, h, 1.0);
	sibr::ImageRGB B = _img.clone();
	sibr::ImageRGB F(w, h, 0);

	cv::Mat leftUnknowns = unknownRegions.toOpenCV();
	cv::Mat kernelLayer = (cv::Mat_<unsigned char>(3, 3) << 0, 1, 0, 1, 1, 1, 0, 1, 0);

	sibr::ImageL8 layerTreated(w, h, 0);
	const float sigGauss2 = 64.0;
	bool changed;
	do {
		changed = false;

		//generate the layer
		cv::Mat erodedUnknowns;
		cv::erode(leftUnknowns, erodedUnknowns, kernelLayer);
		cv::Mat layer = leftUnknowns - erodedUnknowns;

		sibr::ImageL8 layerToTreat;
		layerToTreat.fromOpenCV(layer);

		leftUnknowns = erodedUnknowns;

		//show(layerToTreat);

		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				if (layerToTreat(i, j).x() != 0) {

					changed = true;

					std::vector<sibr::Vector3ub> colorsFront;
					std::vector<float> weightsFront;
					std::vector<sibr::Vector3ub> colorsBack;
					std::vector<float> weightsBack;

					float weightAlpha = 0;
					float meanAlpha = 0;
					for (int l = -10; l <= 10; l++) {
						for (int k = -10; k <= 10; k++) {

							bool pixelTreated = layerTreated(i, j).x() > 0;
							if (_img.isInRange(i + k, j + l)) {
								if (unknownRegions(i + k, j + l).x() == 0 || pixelTreated) {

									//Gaussian fall-off
									float g = exp(-(k * k + l * l) / (2 * sigGauss2));

									if (pixelTreated) {
										colorsBack.push_back(B(i + k, j + l));
										weightsBack.push_back(g * (1.0 - alphas(i + k, j + l).x()));

										colorsFront.push_back(F(i + k, j + l));
										weightsFront.push_back(g * alphas(i + k, j + l).x());

										meanAlpha += g * alphas(i + k, j + l).x();
										weightAlpha += g;

									}
									else if (frontBack(i + k, j + l).x() > 0) {//back
										colorsBack.push_back(_img(i + k, j + l));
										weightsBack.push_back(g);
									}
									else if (frontBack(i + k, j + l).y() > 0) {//front
										colorsFront.push_back(_img(i + k, j + l));
										weightsFront.push_back(g);
									}

								}

							}

						}
					}

					if (weightAlpha > 0) {
						meanAlpha /= weightAlpha;
					}
					else {
						meanAlpha = 0.5;
					}
					// Fill the data
					if (colorsFront.size() > 1 && colorsBack.size() > 1) {
						Eigen::MatrixX3f vecColorF(colorsFront.size(), 3);
						Eigen::VectorXf vecWF(colorsFront.size());

						Eigen::MatrixX3f vecColorB(colorsBack.size(), 3);
						Eigen::VectorXf vecWB(colorsBack.size());

						for (int r = 0; r < colorsFront.size(); r++) {
							vecColorF.row(r) = colorsFront[r].cast<float>() / 255;
							vecWF(r) = weightsFront[r];
						}
						for (int r = 0; r < colorsBack.size(); r++) {
							vecColorB.row(r) = colorsBack[r].cast<float>() / 255;
							vecWB(r) = weightsBack[r];
						}

						sibr::Vector3f C = _img(i, j).cast<float>() / 255;

						Eigen::MatrixX3f weightedColF = (vecColorF.array().colwise() * vecWF.array());
						Eigen::RowVector3f meanF = weightedColF.colwise().sum() / vecWF.sum();
						Eigen::MatrixX3f colFCentered = vecColorF.rowwise() - meanF;
						Eigen::Matrix3f sigF = ((colFCentered.array().colwise() * vecWF.array()).matrix().transpose() * colFCentered) / vecWF.sum();

						Eigen::MatrixX3f weightedColB = (vecColorB.array().colwise() * vecWB.array());
						Eigen::RowVector3f meanB = weightedColB.colwise().sum() / vecWB.array().sum();
						Eigen::MatrixX3f colBCentered = vecColorB.rowwise() - meanB;
						Eigen::Matrix3f sigB = ((colBCentered.array().colwise() * vecWB.array()).matrix().transpose() * colBCentered) / vecWB.sum();

						sibr::Vector3f meanFcol = meanF.transpose();
						sibr::Vector3f meanBcol = meanB.transpose();
						alphaMatte AM = solveAlpha(meanFcol, sigF, meanBcol, sigB, C, 0.5);
						if (AM.alpha > 0) {
							alphas(i, j).x() = AM.alpha;
							F(i, j) = (AM.F * 255 * AM.alpha).cast<unsigned char>();
							B(i, j) = (AM.B * 255).cast<unsigned char>();
						}
					}
					else if (colorsFront.size() > 1)
					{
						alphas(i, j).x() = 1.0;
					}
					else if (colorsBack.size() > 1)
					{
						alphas(i, j).x() = 0.0;
					}

				}
			}
		}

		layerTreated.toOpenCV() += layerToTreat.toOpenCV();
		//show(layerTreated);

	} while (changed);

	//showFloat(alphas);
	//show(_img);
	//show(F);
	//show(B);

	_texture = sibr::ImageRGBA(w, h, 0);
	for (int j = 0; j < h; j++) {
		for (int i = 0; i < w; i++) {
			if (alphas(i, j).x() < 1.0) {
				_texture(i, j).x() = F(i, j).x();
				_texture(i, j).y() = F(i, j).y();
				_texture(i, j).z() = F(i, j).z();
				_texture(i, j).w() = (255 * alphas(i, j).x());
			}
			else {
				_texture(i, j).x() = _img(i, j).x();
				_texture(i, j).y() = _img(i, j).y();
				_texture(i, j).z() = _img(i, j).z();
				_texture(i, j).w() = 255;
			}
		}
	}

	//show(_texture);
	/*for (std::vector<sibr::Vector2i> & comp : components) {
		sibr::ImageRGB imComp(w, h, 0);
		for (sibr::Vector2i & pos : comp) {
			imComp(pos.x(), pos.y()) = sibr::Vector3ub(255, 0, 0);
		}

		show(imComp);
	}*/
}

DepthMesher::alphaMatte DepthMesher::solveAlpha(sibr::Vector3f& meanF, Eigen::Matrix3f& sigF, sibr::Vector3f& meanB, Eigen::Matrix3f& sigB, sibr::Vector3f C, float alphaInit)
{

	Eigen::Matrix3f invSigF = sigF.inverse();
	Eigen::Matrix3f invSigB = sigB.inverse();

	Eigen::Matrix3f I = Eigen::Matrix3f::Identity();
	float invSig2 = 1 / pow(0.01, 2);
	float alpha = alphaInit;
	int iter = 1;
	int maxIter = 50;

	double minLike = 1e-6;
	double lastLike = -DBL_MAX;

	double maxLike = -DBL_MAX;
	float alphaMax = -1;
	sibr::Vector3f FMax;
	sibr::Vector3f BMax;

	while (true) {
		// solve for F, B
		Eigen::Matrix3f A11 = invSigF + I * pow(alpha, 2) * invSig2;
		Eigen::Matrix3f A12 = I * alpha * (1 - alpha) * invSig2;
		Eigen::Matrix3f A22 = invSigB + I * pow((1 - alpha), 2) * invSig2;
		Eigen::MatrixXf A(6, 6);
		A << A11, A12, A12, A22;
		//std::cout << A << std::endl;
		sibr::Vector3f b1 = invSigF * meanF + C * (alpha)*invSig2;
		sibr::Vector3f b2 = invSigB * meanB + C * (1 - alpha) * invSig2;
		Eigen::VectorXf b(6);
		b << b1, b2;

		Eigen::VectorXf X = A.colPivHouseholderQr().solve(b);
		X = X.cwiseMax(0);
		X = X.cwiseMin(1);
		sibr::Vector3f F(X[0], X[1], X[2]);
		sibr::Vector3f B(X[3], X[4], X[5]);
		// solve for alpha

		alpha = std::max(0.0f, std::min(1.0f, (C - B).dot(F - B) / (F - B).squaredNorm()));
		// calculate likelihood
		double L_C = -(C - alpha * F - (1 - alpha) * B).squaredNorm() * invSig2;
		double L_F = -((F - meanF).adjoint() * invSigF).dot(F - meanF) / 2.0;
		double L_B = -((B - meanB).adjoint() * invSigB).dot(B - meanB) / 2.0;
		double like = (L_C + L_F + L_B);
		//like = 0

		if (like > maxLike) {
			alphaMax = alpha;
			maxLike = like;
			FMax = F;
			BMax = B;
		}
		/*else if(iter==50){
			std::cout << "    like :" << like << " " << L_C << " " << L_F << " " << L_B <<std::endl;
			std::cout << "    Debug :" << C << " " << F << " " << B << std::endl << A << std::endl;

		}*/

		if (iter >= maxIter || abs(like - lastLike) <= minLike)
			break;

		lastLike = like;
		iter++;
	}

	//std::cout << "iter : " << iter << " likelyhood " << maxLike << " alpha " << alpha << std::endl;

	return alphaMatte{ alphaMax, FMax, BMax };
}

void DepthMesher::saveObj(const std::string& savePath)
{

	if (!boost::filesystem::exists(savePath)) {
		sibr::makeDirectory(savePath);
	}

	try
	{
		if (!OpenMesh::IO::write_mesh(_mesh, savePath + boost::filesystem::basename(_cam.name()) + ".obj", OpenMesh::IO::Options::VertexTexCoord))
		{
			std::cerr << "Cannot write mesh to file " << savePath << boost::filesystem::basename(_cam.name()) << ".obj" << std::endl;
		}
	}
	catch (std::exception & x)
	{
		std::cerr << x.what() << std::endl;
	}
}

void DepthMesher::genAndSavePly(const std::string& fileName, bool texCoord, const std::string& texPath)
{

#pragma omp critical
	{
		boost::filesystem::path filePath(fileName);
		if (!boost::filesystem::exists(filePath.parent_path())) {
			sibr::makeDirectory(filePath.parent_path().string());
		}
	}

	DepthMesher::genSibrMesh(_mesh, texCoord).saveToASCIIPLY(fileName, false, texPath);
}

sibr::Mesh DepthMesher::genSibrMesh(TriMesh& mesh, bool texCoord)
{

	sibr::Mesh sibrMesh;

	sibr::Mesh::Vertices vert;
	sibr::Mesh::Triangles tri;
	sibr::Mesh::UVs uvs;

	for (TriMesh::VertexIter v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it) {

		TriMesh::Point p = mesh.point(*v_it);//Compute 3D pos of pixel.
		sibr::Vector3f XYZ(p[0], p[1], p[2]);
		vert.push_back(XYZ);

		if (texCoord) {
			TriMesh::TexCoord2D tc = mesh.texcoord2D(*v_it);//Compute 3D pos of pixel.
			sibr::Vector2f UV(tc[0], tc[1]);
			uvs.push_back(UV);
		}

	}

	for (TriMesh::FaceIter f_it = mesh.faces_begin(); f_it != mesh.faces_end(); ++f_it) {

		sibr::Vector3u F;
		int id_f = 0;
		for (TriMesh::FaceVertexIter fv_it = mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it, ++id_f)
		{
			F[id_f] = (*fv_it).idx();
		}

		tri.push_back(F);
	}

	sibrMesh.vertices(vert);
	sibrMesh.triangles(tri);


	if (texCoord) {
		sibrMesh.texCoords(uvs);
	}

	return sibrMesh;

}

std::vector<uint> DepthMesher::closeCamerasAngDist(std::vector<sibr::InputCamera::Ptr> cams)
{
	std::vector<uint> out;

	// sort angle / dist combined
	struct camAng
	{
		camAng() {}
		camAng(float a, float d, int i) : ang(a), dist(d), id(i) {}
		float ang, dist;
		int id;
		static bool compare(const camAng& a, const camAng& b) { return a.ang / a.dist > b.ang / b.dist; }
	};

	int total_size = 16;// _numAnglUlr + _numDistUlr;

	std::vector<camAng> allAng;
	for (int id = 0; id < (int)cams.size(); ++id) {
		const auto& cam = *cams[id];
		float angle = sibr::dot(cam.dir(), _cam.dir());
		// reject back facing 
		if (angle > 0.001) {
			float dist = (cam.position() - _cam.position()).norm();
			allAng.push_back(camAng(angle, dist, id));
		}
	}

	std::vector<bool> wasChosen(cams.size(), false);

	std::sort(allAng.begin(), allAng.end(), camAng::compare);
	for (int id = 0; id < std::min((int)allAng.size(), total_size); ++id) {
		out.push_back(allAng[id].id);
		wasChosen[allAng[id].id] = true;
	}

	for (int id = 0; id < (int)cams.size(); ++id) {
		if (!wasChosen[id] && out.size() < total_size) {
			out.push_back(id);
		}
	}

	return out;
}

void DepthMesher::removeInconsistentTriangles(std::vector<sibr::InputCamera::Ptr> cams, std::string pathScene)
{
	std::vector<uint> camsId = closeCamerasAngDist(cams);

	std::vector<sibr::ImageL32F> depthMaps;
	for (auto& camId : camsId) {
		auto& camIm = *cams[camId];
		std::string baseNameFile = boost::filesystem::basename(camIm.name());
		std::string extensionFile = boost::filesystem::extension(camIm.name());
		depthMaps.push_back(loadDepthCOLMAP(pathScene + "/nvm/depth_maps/" + baseNameFile + ".bin"));

		//sibr::showFloat(depthMaps.back());
	}

	_mesh.request_face_status();
	_mesh.request_vertex_status();

	for (TriMesh::FaceIter f_it = _mesh.faces_sbegin(); f_it != _mesh.faces_end(); ++f_it) {

		OpenMesh::Vec3f pointA, pointB, pointC, vecU, vecV;
		TriMesh::FaceVertexIter fvIt;
		fvIt = _mesh.fv_iter(*f_it);

		pointA = _mesh.point(fvIt.handle());
		pointB = _mesh.point((++fvIt).handle());
		pointC = _mesh.point((++fvIt).handle());

		int subdivision = 10;
		vecU = (pointB - pointA) / (float)subdivision;
		vecV = (pointC - pointA) / (float)subdivision;

		int incoherentRepro = 0;

		//Iterate over close images
		for (int c = 0; c < camsId.size(); c++) {
			auto& camIm = *cams[camsId[c]];
			int w = depthMaps[c].w();
			int h = depthMaps[c].h();

			/////////////////
			//Project the vertex on the image
			Eigen::Matrix3f K;
			K <<
				camIm.focal(), 0, (float)w / 2.0,
				0, camIm.focal(), (float)h / 2.0,
				0, 0, 1.0;

			Eigen::Matrix3f converter;
			converter <<
				1, 0, 0,
				0, -1, 0,
				0, 0, -1;

			Eigen::Matrix3f Kinv = K.inverse();

			//std::cout << _cam.transform() << std::endl;
			sibr::Vector3f camPos = camIm.position();
			Eigen::Matrix3f worldToCam = camIm.rotation().toRotationMatrix() * converter.transpose();
			Eigen::Matrix3f camToWorld = worldToCam.inverse();

			int incoherentSamples = 0;

			for (int fi = 0; fi <= subdivision; fi++) {
				for (int fj = 0; fj <= subdivision - fi; fj++) {

					OpenMesh::Vec3f pointTri = pointA + (float)fi * vecU + (float)fj * vecV;
					sibr::Vector3f pos3D(pointTri[0], pointTri[1], pointTri[2]);

					sibr::Vector3f pixSpaceCoord = K * camToWorld * (pos3D - camPos);
					//Divide by depth
					if (pixSpaceCoord.z() == 0)
						continue;

					pixSpaceCoord /= pixSpaceCoord.z();

					int i = round(pixSpaceCoord.x());
					int j = round(pixSpaceCoord.y());

					///////////////
					if (!depthMaps[c].isInRange(sibr::Vector2i(i, j)))
						continue;

					float d = depthMaps[c](i, j).x();

					if (d < 0.0001)
						continue;

					sibr::Vector3f pos3DCamSpace = d * Kinv * sibr::Vector3f(i, j, 1.0f);
					sibr::Vector3f XYZ = worldToCam * sibr::Vector3f(pos3DCamSpace.x(), pos3DCamSpace.y(), pos3DCamSpace.z()) + camPos;

					//compare depth.
					if ((XYZ - camPos).norm() > 1.1f * (pos3D - camPos).norm()) {
						incoherentSamples++;
					}
				}

			}

			if (incoherentSamples >= subdivision * (subdivision - 1) / 4)
				incoherentRepro++;
		}

		if (incoherentRepro >= 3) {
			_mesh.delete_face(*f_it, true);
		}
	}

	/*for (int c = 0; c < camsId.size(); c++) {
		showFloat(depthMaps[c]);
	}*/

	_mesh.garbage_collection();
}

void DepthMesher::saveTextAlpha(std::string path)
{
	_texture.save(path);
}

template<>
bool DepthMesher::isNull(const sibr::ImageL8& im, const sibr::Vector2i& pos)
{
	if (im(pos.x(), pos.y()).x() == 0)
		return true;
	else
		return false;
}

template<>
bool DepthMesher::isNull(const sibr::Array2d<bool>& im, const sibr::Vector2i& pos)
{
	if (!im(pos.x(), pos.y()))
		return true;
	else
		return false;
}

template<>
void DepthMesher::setToNull(sibr::ImageL8& im, const sibr::Vector2i& pos)
{
	im(pos.x(), pos.y()).x() = 0;
}

template<>
void DepthMesher::setToNull(sibr::Array2d<bool>& im, const sibr::Vector2i& pos)
{
	im(pos.x(), pos.y()) = false;
}
