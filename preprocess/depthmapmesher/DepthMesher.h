/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef DEPTHMESHER_H
#define DEPTHMESHER_H

#include <vector>
#include <core/system/Vector.hpp>
#include <core/graphics/Image.hpp>
#include <core/assets/InputCamera.hpp>
#include <core/system/Array2d.hpp>
#include <core/graphics/Mesh.hpp>
// -------------------- OpenMesh
#include <OpenMesh\Core\IO\MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Tools/Decimater/DecimaterT.hh>
#include "ModQuadricOptVT.hh"
#include <OpenMesh/Tools/Decimater/ModEdgeLengthT.hh>

typedef OpenMesh::TriMesh_ArrayKernelT<>  TriMesh;
// Decimater type
typedef OpenMesh::Decimater::DecimaterT<TriMesh>               Decimater;

// Decimation Module Handle type
typedef OpenMesh::Decimater::ModQuadricOptVT< TriMesh >::Handle HModQuadric;

class DepthMesher 
{
private :
//Input data
	sibr::ImageRGB _img;
	sibr::ImageL32F _depth;
	sibr::InputCamera _cam;

//Output data
	sibr::ImageRGBA _texture;

	struct alphaMatte {
		float alpha;
		sibr::Vector3f F;
		sibr::Vector3f B;
	};

public:
	DepthMesher(const sibr::ImageL32F & depth, const sibr::ImageRGB & img, sibr::InputCamera cam);
	void genMesh(unsigned int tarNumVert, bool textureAlpha=false, bool hedman=false, bool evalL2=false);
	void decimateMesh(int tarNVert, bool lockBoundary=false, bool hedman=false, bool evalL2 = false);
	void smoothBorder(int Ns, float pc);
	void hysteresis(float hT, float lT, sibr::Array2d<bool> & binMap, const sibr::ImageFloat1 & Im, bool display = false);
	template<class T> void removeSmallComp(int minCompSize, T & binMap, const std::vector<std::vector<sibr::Vector2i>> & components, float highThld, bool useThldIm=false, const sibr::ImageFloat1 & Im= sibr::ImageFloat1(), bool display = false);
	template<class T> std::vector < std::vector < sibr::Vector2i >> getComponents(T & binMap, const uint connectivity = 4);
	static sibr::ImageL32F loadDepthCOLMAP(const std::string &filename);
	static void saveDepthCallMap(const std::string &filename, const sibr::ImageL32F &depth_map);
	void genTextCoord();
	void genAlpha(const sibr::ImageRGB & frontBack); //back is in red channel 0, front in green channel 1
	alphaMatte solveAlpha(sibr::Vector3f & meanF, Eigen::Matrix3f & sigF, sibr::Vector3f & meanB, Eigen::Matrix3f & sigB, sibr::Vector3f C, float alphaInit);
	void saveObj(const std::string& path);
	void saveTextAlpha(std::string path);
	void genAndSavePly(const std::string& path, bool texCoord=true , const std::string & texPath="" );
	static sibr::Mesh genSibrMesh(TriMesh & mesh, bool texCoord = false);
	std::vector<float> evalL2Depth(const sibr::InputCamera & cam);

	void removeInconsistentTriangles(std::vector<sibr::InputCamera::Ptr> cams, std::string pathScene);
	std::vector<uint> closeCamerasAngDist(std::vector<sibr::InputCamera::Ptr> cams);

	//for template function
	template<class T> bool isNull(const T & v, const sibr::Vector2i& pos);
	template<> bool isNull(const sibr::ImageL8& im, const sibr::Vector2i& pos);
	template<> bool isNull(const sibr::Array2d<bool>& im, const sibr::Vector2i& pos);

	template<class T> void setToNull(T & v, const sibr::Vector2i& pos);
	template<> void setToNull(sibr::ImageL8& im, const sibr::Vector2i& pos);
	template<> void setToNull(sibr::Array2d<bool>& im, const sibr::Vector2i& pos);

	double ratioRadius(double a, double b, double c) {

		//Unstable version, at 1,1,10e-6 it fails.
		/*double s = (a + b + c) / 2.0f;
		double A = sqrt(s*(s - a)*(s - b)*(s - c));
		double r = A / s;
		double R = (a*b*c) / (4.0f*A);
		//You can test the formula here :
		std::cout << R / r << " = " << (a*b*c*2.0) / ((-a + b + c)*(a - b + c)*(a + b - c)) << std::endl;*/

		if (a < pow(10.0, -6) || b < pow(10.0, -6) || c < pow(10.0, -6))
			return pow(10.0, 6);

		return (a*b*c*2.0) / ((-a + b + c)*(a - b + c)*(a + b - c));
	};

	double qualityMeasure(double a, double b, double c) {

		if (a < pow(10.0, -6) || b < pow(10.0, -6) || c < pow(10.0, -6))
			return pow(10.0, 6);

		double maxSide = std::max({ a,b,c });

		double s = (a + b + c) / 2.0f;
		double A = sqrt(s*(s - a)*(s - b)*(s - c));
		double height = 2 * A / maxSide;

		return maxSide/height;
	};

	double incircleRadius(double a, double b, double c) {

		if (a < pow(10.0, -6) || b < pow(10.0, -6) || c < pow(10.0, -6))
			return pow(10.0, -6);

		double s = (a + b + c) / 2.0;
		//double A = sqrt(s*(s - a)*(s - b)*(s - c));
		double r = sqrt((1.0 / s)*(s - a)*(s - b)*(s - c)); // A/s

		if (isnan(r)) {
			return pow(10.0, -6);
		}
		return r;
	};

	double circumcircleRadius(double a, double b, double c) {


		if (a < pow(10.0, -6) || b < pow(10.0, -6) || c < pow(10.0, -6))
			return pow(10.0, 6);

		double s = (a + b + c) / 2.0f;
		double A = sqrt(s*(s - a)*(s - b)*(s - c));
		double R = (a*b*c) / (4.0f*A);

		return R;
	};

	TriMesh _mesh;
	std::vector <TriMesh> _meshesLoD;

	~DepthMesher(void) { }
};

#endif // BOUNDINGBOX_H