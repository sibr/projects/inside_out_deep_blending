/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef OSG_MODQUADRICOPTV_HH
#define OSG_MODQUADRICOPTV_HH


//== INCLUDES =================================================================

#include <float.h>
#include <Eigen/Core>
#include <OpenMesh/Tools/Decimater/ModBaseT.hh>
#include <OpenMesh/Core/Utils/Property.hh>
#include <OpenMesh/Core/Utils/vector_cast.hh>
#include <OpenMesh/Core/Geometry/QuadricT.hh>



//== NAMESPACE ================================================================

namespace OpenMesh  {
namespace Decimater {

	const double EPS = 1e-12;
	const int LSN = 4; // line search number;
//== CLASS DEFINITION =========================================================


/** \brief Mesh decimation module computing collapse priority based on error quadrics with an optimal p1 position.
 *
 *  This module can be used as a binary and non-binary module.
 */
template <class MeshT>
class ModQuadricOptVT : public ModBaseT<MeshT>
{
public:

  // Defines the types Self, Handle, Base, Mesh, and CollapseInfo
  // and the memberfunction name()
  DECIMATING_MODULE( ModQuadricOptVT, MeshT, Quadric );

public:

  /** Constructor
   *  \internal
   */
  ModQuadricOptVT( MeshT &_mesh )
	  : Base(_mesh, false)
  {
	  hedman_ = false;
	  oriNumFaces_ = _mesh.n_faces();
	  remainingFaces_ = oriNumFaces_;
	  powSimplRatio_ = 0;
	  std::vector<MeshT> meshesSimpl_= std::vector<MeshT>();
	  unset_max_err();
	  Base::mesh().request_face_normals();
	  Base::mesh().update_face_normals();
	  Base::mesh().add_property(quadrics_);
	  Base::mesh().add_property(pOpt_);
  }


  /// Destructor
  virtual ~ModQuadricOptVT()
  {
	std::cout << "Errors " << error << "/" << total << std::endl;
    Base::mesh().remove_property(quadrics_);
	Base::mesh().remove_property(pOpt_);
	Base::mesh().release_face_normals();
  }


public: // inherited

	/// Initalize the module and prepare the mesh for decimation.
	virtual void initialize(void);

  /** Compute collapse priority based on error quadrics.
   *
   *  \see ModBaseT::collapse_priority() for return values
   *  \see set_max_err()
   */
  virtual float collapse_priority(const CollapseInfo& _ci)
  {
    using namespace OpenMesh;

    typedef Geometry::QuadricT<double> Q;

    Q q = Base::mesh().property(quadrics_, _ci.v0);
    q += Base::mesh().property(quadrics_, _ci.v1);

	//std::cout << _ci.p0 << ", " << optimalV(q) << ", " << _ci.p1 << std::endl;
	Vec3f pOpt(0,0,0);
	double err;
	if (/*!hedman_ &&*/ ( Base::mesh().is_boundary(_ci.v0v1) || Base::mesh().is_boundary(_ci.v1v0))) { // Edge to collapse is boundary
		pOpt = optimalV(q, _ci);
		if (pOpt.sqrnorm() == 0)
			pOpt = (_ci.p1 + _ci.p0) / 2.0;
		err = 500*q(pOpt); // Higher weight to keep more precision on edges.
	}
	else if (!hedman_ && (Base::mesh().is_boundary(_ci.v0))) { //First vertex is on the boundary
		pOpt = _ci.p0;
		err = 2 * q(pOpt);
	}
	else if (!hedman_ && (Base::mesh().is_boundary(_ci.v1))) { //Second vertex is on the boundary
		pOpt = _ci.p1;
		err = 2 * q(pOpt);
	}
	else { //normal case
		pOpt = optimalV(q, _ci);
		if (pOpt.sqrnorm() == 0)
			pOpt = (_ci.p1+_ci.p0) / 2.0;
		err = q(pOpt);
	}
	
	if(!hedman_)
		err = err/(((_ci.p1 + _ci.p0) / 2.0) - camPos_).sqrnorm();

	Base::mesh().property(pOpt_, _ci.v0v1) = pOpt;

	if (!hedman_ && normalError(_ci, pOpt) )
		return float(Base::ILLEGAL_COLLAPSE);
	
    return float( (err < max_err_) ? err : float( Base::ILLEGAL_COLLAPSE ) );
  }

  virtual void preprocess_collapse(const CollapseInfo& _ci)
  {
	  if (evalL2_) {
		  remainingFaces_--;
		  if (!Base::mesh().is_boundary(_ci.v0v1) && !Base::mesh().is_boundary(_ci.v1v0))
			  remainingFaces_--;
	  }
	
  }
  /// Post-process halfedge collapse (accumulate quadrics)
  virtual void postprocess_collapse(const CollapseInfo& _ci)
  {
	Geometry::QuadricT<double> & q = Base::mesh().property(quadrics_, _ci.v1);
	q += Base::mesh().property(quadrics_, _ci.v0);
	
	//Update position to optimal one.
	Base::mesh().point(_ci.v1)= Base::mesh().property(pOpt_, _ci.v0v1);

	for (TriMesh::VertexFaceIter vf_it = Base::mesh().vf_iter(_ci.v1); vf_it.is_valid() ; ++vf_it)
	{
		TriMesh::FaceHandle fh = *vf_it;
		Base::mesh().update_normal(fh);
	}

	if (evalL2_ && remainingFaces_ < floor(oriNumFaces_ / pow(2, powSimplRatio_)) ) {
		powSimplRatio_++;
		MeshT copyMesh = Base::mesh();
		copyMesh.garbage_collection();

		/*copyMesh.remove_property(quadrics_);
		copyMesh.remove_property(pOpt_);*/
		
		copyMesh.release_face_normals();

		meshesSimpl_.push_back(copyMesh);
		std::cout << remainingFaces_ << " -- actual size :" << copyMesh.n_faces()<< std::endl;
	}

  }

  /// set the percentage of maximum quadric error
  void set_error_tolerance_factor(double _factor);

  Vec3f optimalV(Geometry::QuadricT<double> & q, const CollapseInfo& _ci) {

	  Eigen::Matrix4d qm;
	  qm << q.a(), q.b(), q.c(), q.d(),
		  q.b(), q.e(), q.f(), q.g(),
		  q.c(), q.f(), q.h(), q.i(),
		  0, 0, 0, 1;

	  Vec3f p1 = _ci.p1;
	  Vec3f p0 = _ci.p0;
	  Vec3f v= (p1 + p0)/2;

	  Eigen::Vector4d vOpt(0, 0, 0, 0);
	  if (q(v) > 200 * 1e-15) {
		  if (abs(qm.determinant()) > EPS) {
			  vOpt = qm.inverse()*Eigen::Vector4d(0, 0, 0, 1);
			  v = Vec3f(vOpt.x(), vOpt.y(), vOpt.z());
		  }
		  else {
			  Vec3d vMinErr(0, 0, 0);
			  float minErr = max_err_;
			  for (int i = 0; i <= LSN; i++) {
				  Vec3f vCand = (i*p1 + (LSN - i)*p0) / (double)LSN;
				  float candErr = q(vCand);
				  if (candErr < minErr) {
					  vMinErr = vCand;
					  minErr = candErr;
				  }
			  }

			  v = vMinErr;
		  }
	  }

	  return v;
  }

  bool normalError(const CollapseInfo& _ci, Vec3f & pOpt) {

	  // simulate collapse

	  total++;

	  bool errorDetected = false;

	  for (TriMesh::VertexFaceIter vf_it = Base::mesh().vf_iter(_ci.v0); !errorDetected&& vf_it.is_valid(); ++vf_it)
	  {

		  bool valid = true;
		  TriMesh::FaceHandle fh = *vf_it;

		  for (TriMesh::FaceVertexIter fv_it = Base::mesh().fv_iter(fh); fv_it.is_valid(); ++fv_it)
		  {
			  if ((*fv_it) == _ci.v1) {
				  valid = false;
				  break;
			  }

		  }

		  if (!valid)
			  continue;
		  

		  TriMesh::FaceVertexIter fv_it(mesh().fv_iter(fh));
		  int idVert = -1;

		  Vec3f p[3];
		  if (*fv_it == _ci.v0)
			  idVert = 0;
		  p[0]=(mesh().point(*fv_it));
		  ++fv_it;
		  if (*fv_it == _ci.v0)
			  idVert = 1;
		  p[1] = (mesh().point(*fv_it));
		  ++fv_it;
		  if (*fv_it == _ci.v0)
			  idVert = 2;
		  p[2] = (mesh().point(*fv_it));

		  Vec3f normalOri = cross(p[2]-p[1], p[0] - p[1]).normalized();
		  p[idVert] = pOpt;
		  Vec3f normalSimu = cross(p[2] - p[1], p[0] - p[1]).normalized();

		  if (dot(normalOri, normalSimu) < 0.5f /*|| dot(toCamNzed, normalSimu)< -0.5f*/) {
			  error++;
			  errorDetected = true;
		  }

	  }

	  for (TriMesh::VertexFaceIter vf_it = Base::mesh().vf_iter(_ci.v1); !errorDetected && vf_it.is_valid(); ++vf_it)
	  {

		  bool valid = true;
		  TriMesh::FaceHandle fh = *vf_it;
		  for (TriMesh::FaceVertexIter fv_it = Base::mesh().fv_iter(fh); fv_it.is_valid(); ++fv_it)
		  {
			  if ((*fv_it) == _ci.v0) {
				  valid = false;
				  break;
			  }
		  }

		  if (!valid)
			  continue;

		  
		  TriMesh::FaceVertexIter fv_it(mesh().fv_iter(fh));
		  int idVert = -1;

		  Vec3f p[3];
		  if (*fv_it == _ci.v1)
			  idVert = 0;
		  p[0] = (mesh().point(*fv_it));
		  ++fv_it;
		  if (*fv_it == _ci.v1)
			  idVert = 1;
		  p[1] = (mesh().point(*fv_it));
		  ++fv_it;
		  if (*fv_it == _ci.v1)
			  idVert = 2;
		  p[2] = (mesh().point(*fv_it));

		  Vec3f normalOri = cross(p[2] - p[1], p[0] - p[1]).normalized();
		  p[idVert] = pOpt;
		  Vec3f normalSimu = cross(p[2] - p[1], p[0] - p[1]).normalized();
		  
		  if (dot(normalOri, normalSimu) < 0.5f /*|| dot(toCamNzed, normalSimu) < -0.5f*/) {
			  error++;
			  errorDetected = true;
		  }


	  }

	  // undo simulate collapse
	  Base::mesh().set_point(_ci.v0, _ci.p0);
	  Base::mesh().set_point(_ci.v1, _ci.p1);

	  return errorDetected;
  }

public: // specific methods

  void set_max_err(double _err, bool _binary = true)
  {
	  max_err_ = _err;
	  Base::set_binary(_binary);
  }

  /// Unset maximum quadric error constraint and restore non-binary mode.
  /// \see set_max_err()
  void unset_max_err(void)
  {
	  max_err_ = DBL_MAX;
	  Base::set_binary(false);
  }

  ///Set the campos for the normal test
  void set_camPos(Vec3f camPos)
  {
	  camPos_ = camPos;
	  total = 0;
	  error = 0;
  }

  void set_hedman(bool h) {
	  hedman_ = h;
  }

  void set_evalL2(bool e) {
	  evalL2_ = e;
  }

  std::vector<MeshT> & getMeshes() {
	  return meshesSimpl_;
  }

  /// Return value of max. allowed error.
  double max_err() const { return max_err_; }


private:

	// maximum quadric error
	double max_err_;

	// using hedman method
	bool hedman_;
	//For evaluation
	bool evalL2_;
	int oriNumFaces_;
	int remainingFaces_;
	int powSimplRatio_;
	std::vector<MeshT> meshesSimpl_;
	//CamPos for the image
	Vec3f camPos_;
	int total;
	int error;
	// this vertex property stores a quadric for each vertex
	VPropHandleT< Geometry::QuadricT<double> >  quadrics_;
	HPropHandleT< Vec3d >  pOpt_;

};

} // END_NS_DECIMATER
} // END_NS_OPENMESH

#if defined(OM_INCLUDE_TEMPLATES) && !defined(OPENMESH_DECIMATER_MODQUADRICOPTV_CC)
#define OSG_MODQUADRICOPTV_TEMPLATES
#include "ModQuadricOptVT.cc"
#endif

#endif // OSG_MODQUADRICOPTV_HH defined

