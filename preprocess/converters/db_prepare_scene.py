# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------
""" @package dataset_tools_preprocess
This script creates all necessary data for deep blending preprocessing; it assumes fullcolmapProcess --with_texture has been run

"""

import argparse
import subprocess
import shutil
import os, sys, getopt
import re
from utils.commands import getProcess
from utils.paths import getBinariesPath
from utils.commands import runCommand
from utils.paths import getMeshlabPath

from os import walk
import struct
import imghdr
import os
import subprocess
from generate_list_images import generateListImages
from wedge_to_vertex_uvs import convertUVs
from pathlib import Path

#===============================================================================

def mainRC():
    parser = argparse.ArgumentParser()

    # common arguments
    parser.add_argument("--ColmapPath", type=str, required=True, help="path to your colmap dataset folder")
    parser.add_argument("--RCPath", type=str, required=True, help="path to your RealityCapture dataset folder")
    parser.add_argument("--NoDBPreprocess", action='store_true', help="dont run DB preprocess")

    # TODO save colmap only version if it exists.
    
    args = vars(parser.parse_args())

    if args['RCPath'] == "" or args['ColmapPath'] == "":
        print("SIBR ERROR: RCPath and ColmapPath args required, exiting")
        sys.exit(1)

    path_input_mesh = args['RCPath'] + "/textured.obj"

    # verif RC file names
    textured_mesh_file = Path(path_input_mesh)
    if not textured_mesh_file.is_file():
        print("SIBR ERROR: the RealityCapture textured mesh file ", path_input_mesh,  " doesnt exist; \nPlease save all items with correct names (see documentation). \n Exiting...")
        sys.exit(1)

    path_input_texture = args['RCPath'] + "/textured_u1_v1.png"
    textured_tex_file = Path(path_input_texture)
    if not textured_tex_file.is_file():
        print("SIBR ERROR: the RealityCapture texture file \"textured_u1_v1.png\" doesnt exist; \nPlease save all items with correct names (see documentation). \n Exiting...")
        sys.exit(1)

    # generate list_images for RC
    generateListImages(args['RCPath'])

    # convert wedge_to_vertex_uvs -- RC saves in this format
    path_output_mesh = args['RCPath'] + "/mesh.obj"
    convertUVs(path_input_mesh, path_output_mesh)

    # copy obj to the ply file so it gets texture coords; using the ply directly fails with ASSIMP
    meshlabPath = getMeshlabPath()
    meshlabServerExe = 'meshlabserver.exe'
    path_input_mesh = args['RCPath'] + "/mesh.obj"
    path_output_mesh = args['RCPath'] + "/recon.ply"

    runCommand(os.path.abspath(os.path.join(meshlabPath, meshlabServerExe)), ['-i', path_input_mesh,
                                                                              '-o', path_output_mesh,
                                                                              '-m', 'vt' ] )
    # align RC to colmap mesh ; write it to the capreal path
    alignmesh_app = getProcess("alignMeshes")
    capreal_path = args['ColmapPath'] +  "/capreal"

    alignmesh_args = [alignmesh_app,
        "--pathRef", args['ColmapPath'],
        "--path2Align", args['RCPath'],
        "--dataset_type", "colmap", 
        "--forceLandscape", # for portrait images, eg face photos
        "--offscreen",
        "--out", capreal_path
        ]

    print("Running align meshes ", alignmesh_args)
    p_exit = subprocess.call(alignmesh_args)
    if p_exit != 0:
        print("SIBR ERROR: alignMeshes failed, exiting")
        sys.exit(1)
   
    # copy texture + fix placement and names of files (mesh.obj mesh.ply) 
    texture_file = Path(args['RCPath']+"/textured_u1_v1.png");

    if not texture_file.is_file():
        print("SIBR ERROR: Texture file named \"textured_u1_v1.png\" not found in", args['RCPath'], " Exiting...");
        sys.exit(1)

    else:
        shutil.copy(args['RCPath'] + "/textured_u1_v1.png", capreal_path + "/textured_u1_v1.png")
    
    # create ply file (needed for DB)
    path_input_mesh = capreal_path + "/mesh.obj"
    path_output_mesh = capreal_path + "/mesh.ply"
    # save textured mesh.ply 

    shutil.copy(path_output_mesh, capreal_path + "/mesh_textured.ply")

    ret = runCommand(os.path.abspath(os.path.join(meshlabPath, meshlabServerExe)), ['-i', path_input_mesh,
                                                                              '-o', path_output_mesh,
                                                                              '-m', 'vt' ] )
    if ( ret.returncode != 0 ):
        p_exit = 1
        print("Problem with meshlab conversion")


    if args['NoDBPreprocess']:
        print("You are now ready to run deep blending preprocessing\npython ibr_db_preprocess.py -r -i ",  args['ColmapPath'] , " <-w 1920> <-g 64> <-b> (see documentation)")
    else:
        print("Running deep blending preprocessing\npython ibr_db_preprocess.py -r -i ",  args['ColmapPath'])
        p_exit = subprocess.call(["python", "ibr_db_preprocess.py", "-r", "-i", args['ColmapPath']])

    return p_exit


if __name__ == "__main__":
    mainRC()
