# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/usr/bin/env python
#! -*- encoding: utf-8 -*-
# --------------------------------------------
""" @package deep_blending_preprocess
This script creates Deep Blending data from a COLMAP dataset which can be fed to a Deep Blending application

Parameters: -h help,
            -i <path to input directory>,
            -r use release w/ debug symbols executables
            -w to specify max-width in which the dataset should be preprocessed [optional]
			-m for inside_out preprocessing; defaults to deep_blending

Usage: python ibr_db_preprocess.py -r -i <path_to_sibr>\sibr\install\bin\datasets\Playroom

"""

import subprocess
import shutil
import os
import re
os.sys.path.append('../../dataset_tools/preprocess/utils')
os.sys.path.append('../../dataset_tools/preprocess/')
from utils.commands import getProcess
from utils.paths import getBinariesPath
import sys
import getopt
from os import walk
from pathlib import Path

#--------------------------------------------

#===============================================================================

import struct
import imghdr

def get_image_size(fname):
    '''Determine the image type of fhandle and return its size.
    from draco'''
    with open(fname, 'rb') as fhandle:
        head = fhandle.read(24)
        if len(head) != 24:
            return
        if imghdr.what(fname) == 'png':
            check = struct.unpack('>i', head[4:8])[0]
            if check != 0x0d0a1a0a:
                return
            width, height = struct.unpack('>ii', head[16:24])
        elif imghdr.what(fname) == 'gif':
            width, height = struct.unpack('<HH', head[6:10])
        elif imghdr.what(fname) == 'jpeg':
            try:
                fhandle.seek(0) # Read 0xff next
                size = 2
                ftype = 0
                while not 0xc0 <= ftype <= 0xcf:
                    fhandle.seek(size, 1)
                    byte = fhandle.read(1)
                    while ord(byte) == 0xff:
                        byte = fhandle.read(1)
                    ftype = ord(byte)
                    size = struct.unpack('>H', fhandle.read(2))[0] - 2
                # We are at a SOFn block
                fhandle.seek(1, 1)  # Skip `precision' byte.
                height, width = struct.unpack('>HH', fhandle.read(4))
            except Exception: #IGNORE:W0703
                return
        else:
            return
        return width, height
 
def checkOutput( output, force_continue ):
    if( output != 0):
        if( not force_continue ):
            sys.exit()
        else:
            return False
    else:
        return True
    

#===============================================================================

#--------------------------------------------
# 0. Paths, commands and options

def main(argv):
    opts, args = getopt.getopt(argv, "hi:rd:w:g:bm", ["idir=", "bin="])
    executables_suffix = ""
    executables_folder = getBinariesPath()
    path_data = ""
    m_width = 0
    grid_size = 32
    create_bounds = False
    inside_out = False
    for opt, arg in opts:
        if opt == '-h':
            print("-i path_to_rc_data_dir [-w max_width -g grid_size -b use_grid_bounds -r (use release w/ debug symbols executables)]")
            sys.exit()
        elif opt == '-i':
            path_data = arg
            print(['Setting path_data to ', path_data])
        elif opt == '-r':
            executables_suffix = "_rwdi"
            print(["Using rwdi executables."])
        elif opt == '-w':
            m_width = arg
            print(["Max width set to", m_width])
        elif opt == '-g':
            grid_size = arg
            print(["Grid size set to", grid_size])
        elif opt == '-b':
            create_bounds = True
            print(["Create bounds from cameras: ", create_bounds])
        elif opt in ('-bin', '--bin'):
            executables_folder = os.path.abspath(arg)
        elif opt == '-m':
            inside_out = True
            print(['Inside out set to ', True])

    return (path_data, executables_suffix, executables_folder, m_width, grid_size, create_bounds, inside_out)

# m_width = 0
grid_size = 32
# create_bounds = False
path_data, executables_suffix, executables_folder, m_width, grid_size, create_bounds, inside_out = main(sys.argv[1:])

if(path_data == ""):
    path_data = os.path.abspath(os.path.join(os.path.dirname(__file__), "../datasets"))

path_data = os.path.abspath(path_data + "/") + "/"
executables_folder = os.path.abspath(executables_folder + "/") + "/"

print(['Raw_data folder: ', path_data])
print(['Executables folder: ', executables_folder])

force_continue = False


recon_cmd = getProcess("recon_cmd" + executables_suffix, executables_folder)
depthmap_mesher = getProcess("SIBR_depthmapmesher" + executables_suffix, executables_folder)

#--------------------------------------------
# 1. Run recon_cmd to output an NVM scene representation  and computes refined depth maps

print(recon_cmd)

process_args = [recon_cmd, path_data, "grid_size="+str(grid_size)]

if int(m_width) > 0:
    process_args.append("max_width="+m_width)

if create_bounds:
    process_args.append("grid_bounds_from_cameras")

if inside_out:
    process_args.append("inside_out")

print([process_args, executables_folder])

program_exit = subprocess.call(process_args, cwd=executables_folder)

print("Recon cmd exited with ", program_exit)
sys.stdout.flush()
if( not checkOutput(program_exit, force_continue) ):
    sys.exit(-1)

#--------------------------------------------
# 2. Run depthmapmesher to simply mesh and get per-view meshes

if not inside_out:
    print(depthmap_mesher)

    print([depthmap_mesher, "--path", path_data, "--offscreen", executables_folder])
    program_exit = subprocess.call([depthmap_mesher, "--path", path_data, "--offscreen" ], cwd=executables_folder)

    print("Depthmapmesher exited with ", program_exit)
    sys.stdout.flush()
    if( not checkOutput(program_exit, force_continue) ):
        sys.exit(-1)
     
#--------------------------------------------
# 3. Run recon_cmd to construct the patch clouds and store them in a .pcloud file

print(recon_cmd)

process_args = [recon_cmd, path_data, "cloud_out", "grid_size="+str(grid_size)]

if int(m_width) > 0:
    process_args.append("max_width="+m_width)

if create_bounds:
    process_args.append("grid_bounds_from_cameras")

if inside_out:
    process_args.append("inside_out")

print([process_args, executables_folder])

program_exit = subprocess.call(process_args, cwd=executables_folder)

print("Recon cmd exited with ", program_exit)

# cleanup if doing fullcolmapProcess and RC alignment, so the textured mesh .ply is the one DB will read

textured_mesh_file = Path(path_data+"/capreal/mesh_textured.ply")
if textured_mesh_file.is_file():
    saved_meshes_path = path_data+"/capreal/saved_meshes";
    if not os.path.exists(saved_meshes_path):
        os.makedirs(saved_meshes_path)

    file_to_move_path = Path(path_data+"/capreal/mesh.obj")
    if( file_to_move_path.is_file()):
        shutil.move(file_to_move_path, saved_meshes_path+"/mesh.obj")
    file_to_move_path = Path(path_data+"/capreal/mesh.mtl")
    if( file_to_move_path.is_file()):
        shutil.move(file_to_move_path, saved_meshes_path+"/mesh.mtl")

    shutil.move(textured_mesh_file, path_data+"/capreal/mesh.ply")


sys.stdout.flush()
if( not checkOutput(program_exit, force_continue) ):
    sys.exit(-1)

#--------------------------------------------
