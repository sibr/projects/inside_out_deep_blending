# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/usr/bin/env python
#! -*- encoding: utf-8 -*-
# --------------------------------------------
""" @package deep_blending_preprocess
This script creates Deep Blending data from a COLMAP dataset which can be fed to a Deep Blending application

Parameters: -h help,
            -i <path to input directory>,
            -r use release w/ debug symbols executables
            -w to specify max-width in which the dataset should be preprocessed [optional]

Usage: python db_legacy_dataset_to_sibr.py -r -i <path_to_legacy_dataset>

"""

import subprocess
import shutil
import os
import re
from utils.commands import getProcess
from utils.paths import getBinariesPath

from os import walk

#--------------------------------------------

#===============================================================================

import struct
import imghdr

def get_image_size(fname):
    '''Determine the image type of fhandle and return its size.
    from draco'''
    with open(fname, 'rb') as fhandle:
        head = fhandle.read(24)
        if len(head) != 24:
            return
        if imghdr.what(fname) == 'png':
            check = struct.unpack('>i', head[4:8])[0]
            if check != 0x0d0a1a0a:
                return
            width, height = struct.unpack('>ii', head[16:24])
        elif imghdr.what(fname) == 'gif':
            width, height = struct.unpack('<HH', head[6:10])
        elif imghdr.what(fname) == 'jpeg':
            try:
                fhandle.seek(0) # Read 0xff next
                size = 2
                ftype = 0
                while not 0xc0 <= ftype <= 0xcf:
                    fhandle.seek(size, 1)
                    byte = fhandle.read(1)
                    while ord(byte) == 0xff:
                        byte = fhandle.read(1)
                    ftype = ord(byte)
                    size = struct.unpack('>H', fhandle.read(2))[0] - 2
                # We are at a SOFn block
                fhandle.seek(1, 1)  # Skip `precision' byte.
                height, width = struct.unpack('>HH', fhandle.read(4))
            except Exception: #IGNORE:W0703
                return
        else:
            return
        return width, height

#===============================================================================
# SYSTEM SETTINGS
#===============================================================================
# INSTRUCTIONS:
# 1) call 'settings = load_settings("<your_filename>")'
# 2) then get value with 'get_settings(settings, "<your-key>")' whenever
# you want.

import sys, getopt

def _load_settings_keyvalue(line):
    """Return a list with 2 elements: a key and a value.
    Return an empty list if this line doesn't contain such pair.
    """
    out = []
    if len(line) < 5:
        return out
    endbracket = line.find("]:")
    if line[0] == '[' and endbracket > 0:
        out.append(line[1:endbracket])
        out.append(line[endbracket+len("]:"):].strip())
    return out    

def load_settings(filename):
    """Return a dictionary of key/value settings for the given file.

    File Format:
    [your-key]: your-value
    # a commented line
    """
    settings_dict = {}
    f = open(os.path.join(os.path.dirname(__file__), filename), 'r')
    for line in f:
        pair = _load_settings_keyvalue(line[:-1])
        if len(pair) == 2: # else, it is either a comment or an error
            settings_dict[pair[0]] = pair[1]
    return settings_dict 

def get_settings(settings_dict, key):
    """Return the value stored in 'filename'using the given 'key'.
    Additionally, it automatically prints an error message.
    """
    if key in settings_dict:
        return settings_dict[key]
    print("ERROR: Attempting to load an unknown settings key ('" + key + "')")
    sys.exit(-1)
    return ""

    
def checkOutput( output, force_continue ):
    if( output != 0):
        if( not force_continue ):
            sys.exit()
        else:
            return False
    else:
        return True
    

#===============================================================================

#--------------------------------------------
# 0. Paths, commands and options

def main(argv):
    opts, args = getopt.getopt(argv, "hi:rw:g:b", ["idir=", "bin="])
    executables_suffix = ""
    executables_folder = getBinariesPath()
    path_data = ""
    m_width = 0
    grid_size = 32
    create_bounds = False
    for opt, arg in opts:
        if opt == '-h':
            print("-i path_to_legacy_dataset [-w max_width -g grid_size -b use_grid_bounds -r (use release w/ debug symbols executables)]")
            sys.exit()
        elif opt == '-i':
            path_data = arg
            print(['Input path ', path_data])
        elif opt == '-r':
            executables_suffix = "_rwdi"
            print(["Using rwdi executables."])
        elif opt == '-w':
            m_width = arg
            print(["Max width set to", m_width])
        elif opt == '-g':
            grid_size = arg
            print(["Grid size set to", grid_size])
        elif opt == '-b':
            create_bounds = True
            print(["Create bounds from cameras: ", create_bounds])
        elif opt in ('-bin', '--bin'):
            executables_folder = os.path.abspath(arg)

    return (path_data, executables_suffix, executables_folder, m_width, grid_size, create_bounds)

# m_width = 0
grid_size = 32
# create_bounds = False
path_data, executables_suffix, executables_folder, m_width, grid_size, create_bounds  = main(sys.argv[1:])

if(path_data == ""):
    path_data = os.path.abspath(os.path.join(os.path.dirname(__file__), "../datasets"))

## Read the folders in the path and create the correct directory structure
dataset_name = os.path.split(path_data)[-1].split("_")[0]
parent_dir = os.path.abspath(os.path.dirname(path_data))
new_path_data = os.path.join(parent_dir, dataset_name)

old_capreal_path = os.path.join(path_data, "realitycapture")
old_colmap_path = os.path.join(path_data, "colmap")
old_depthmaps_path = os.path.join(path_data, "refined_depth_maps")
old_nvm_path = os.path.join(parent_dir, dataset_name + "_IBR_Inputs_Outputs", "input_camera_poses_as_nvm")
old_pvmesh_path = os.path.join(parent_dir, dataset_name + "_IBR_Inputs_Outputs", "per_input_view_meshes")

new_capreal_path = os.path.join(new_path_data, "capreal")
new_colmap_path = os.path.join(new_path_data, "colmap/stereo")
db_path = os.path.join(new_path_data, "deep_blending")
new_depthmaps_path = os.path.join(db_path, "depthmaps")
new_nvm_path = os.path.join(db_path, "nvm")
new_pvmesh_path = os.path.join(db_path, "pvmeshes")


os.makedirs(new_colmap_path, exist_ok=True)
os.makedirs(db_path, exist_ok=True)

shutil.copytree(old_capreal_path, new_capreal_path, dirs_exist_ok=True)
shutil.copytree(old_colmap_path, new_colmap_path, dirs_exist_ok=True)


shutil.copytree(old_nvm_path, new_nvm_path, dirs_exist_ok=True)
shutil.copytree(old_pvmesh_path, new_pvmesh_path, dirs_exist_ok=True)
shutil.copytree(old_depthmaps_path, new_depthmaps_path, dirs_exist_ok=True)


print(['New dataset path', new_path_data])
print(['Executables folder: ', executables_folder])

force_continue = False

recon_cmd = getProcess("recon_cmd" + executables_suffix, executables_folder)


#--------------------------------------------
# 3. Run recon_cmd to construct the patch clouds and store them in a .pcloud file

print(recon_cmd)
if int(m_width) > 0:
    print([recon_cmd, new_path_data, "cloud_out", "max_width=", m_width, executables_folder])
    if(create_bounds):
        program_exit = subprocess.call([recon_cmd, new_path_data, "cloud_out", "max_width="+m_width, "grid_size="+str(grid_size), "grid_bounds_from_cameras"], cwd=executables_folder)
    else:
        program_exit = subprocess.call([recon_cmd, new_path_data, "cloud_out", "max_width="+m_width, "grid_size="+str(grid_size)], cwd=executables_folder)
else:
    print([recon_cmd, new_path_data, "cloud_out", executables_folder])
    if(create_bounds):
        program_exit = subprocess.call([recon_cmd, new_path_data, "cloud_out", "grid_size="+str(grid_size), "grid_bounds_from_cameras"], cwd=executables_folder)
    else:
        program_exit = subprocess.call([recon_cmd, new_path_data, "cloud_out", "grid_size="+str(grid_size)], cwd=executables_folder)

print("Recon cmd exited with ", program_exit)
sys.stdout.flush()
if( not checkOutput(program_exit, force_continue) ):
    sys.exit(-1)

#--------------------------------------------
