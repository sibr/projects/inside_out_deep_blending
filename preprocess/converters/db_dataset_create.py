# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------
""" @package dataset_tools_preprocess
This script creates all necessary data for deep blending preprocessing given an images folder and an (optional) sfm_mvs folder if you do RC

"""

import argparse
import subprocess
import shutil
import os
from utils.convert import *
from utils.paths import *
from utils.commands import *
from pathlib import Path


def checkColmapConsistent(pathdir):
    colmapDir = Path(pathdir+"/colmap")

    if not os.path.isdir(colmapDir):
        return False

    # check for mesh
    colmapmesh = Path(pathdir+"/colmap/stereo/meshed-delaunay.ply")
    if not os.path.isfile(colmapmesh):
        print("SIBR_ERROR: colmap directory exists but there is no mesh ", colmapmesh)
        print("No file", colmapmesh)
        return False

    return True

def checkRCConsistent(pathdir):
    # check for mesh
    print("RCC ", pathdir)
    RCmesh = Path(pathdir+"/textured.obj")
    # check for calibration
    bundleFile = Path(pathdir+"/bundle.out")

    if not os.path.isfile(RCmesh) or not os.path.isfile(bundleFile):
        return False
    return True

def db_create(path, maxwidth):
    p_exit = subprocess.call(["python", os.path.join("../preprocess/converters", "ibr_db_preprocess.py"), "-r", "-i", path, "-w", maxwidth])
    

def maindb():
    # common arguments
    parser = argparse.ArgumentParser()

    parser.add_argument("--path", type=str, required=True, help="path to your dataset folder")
    parser.add_argument("--withRC", action='store_true', help="Use RealityCapture data ")
    parser.add_argument("--RCPath", type=str, help="path to your RealityCapture dataset folder")
    parser.add_argument("--noColmap", action='store_true', help="Dont run colmap -- assumes you already have")
    parser.add_argument("--colmapPath", type=str, default=getColmapPath(), help="path to directory colmap.bat / colmap.bin directory")
    parser.add_argument("--doInsideOut", action='store_true', help="Do insideout preprocessing (defaulf false)")
    parser.add_argument("--dontDoDeepBlending", action='store_true', help="Dont do deep blending preprocessing (defaulf false)")
    parser.add_argument("--meshsize", type=str, help="size of the output mesh in K polygons (ie 200 == 200,000 polygons). Values allowed: 200, 250, 300, 350, 400")
    parser.add_argument("--max_width", type=str, default="0", help="max width for DB preprocessing (recommend < 1500)")

    args = vars(parser.parse_args())

    meshsize = args['meshsize']
    if( args['meshsize'] != "" ):
        if( args['meshsize'] != "200" and args['meshsize'] != "250" and 
            args['meshsize'] != "300" and args['meshsize'] != "350" and 
            args['meshsize'] != "400" ):
            print("SIBR_WRG meshsize not given a correct value, defaulting to 200")
            meshsize = "200"
            

    if not args['noColmap']:
        p_exit = subprocess.call(["python", "fullcolmapProcess.py", "--path", args['path'], "--colmapPath" , args['colmapPath'], "--with_texture", "--meshsize", meshsize ])
        if p_exit != 0:
            print("SIBR_ERROR colmap reconstruction failed; maybe try again");
            sys.exit(1)
    else:
        if not checkColmapConsistent(args['path']):
            print("SIBR_ERROR Colmap hasnt been run properly; run it first (ie dont use --noColmap)")
            sys.exit(1)



    if args['withRC']:
        # also runs the preprocess
        if args['RCPath'] == None:
            print("argument --RCPath required with \"--withRC\" ; exiting")
            sys.exit(1)

        if not checkRCConsistent(args['RCPath']):
            print("SIBR_ERROR RealityCapture output hasnt been provided; run RC first and save the output (see documentation)")
            sys.exit(1)

        # if deep_blending dir already exists (probably from colmap only), move it to deep_blending_saved
        deep_blend_dir = Path(args['path'] + "/deep_blending")
        if os.path.isdir(deep_blend_dir):
            print("SIBR_WARNGING Deep Blending data found, saving")
            save_deep_blend_dir = Path(args['path'] + "/deep_blending_saved")
            shutil.move(deep_blend_dir, save_deep_blend_dir)

        p_exit = subprocess.call(["python", "db_prepare_scene.py", "--ColmapPath", args['path'], "--RCPath" , args['RCPath']])
        if p_exit != 0:
            print("SIBR_ERROR RealityCapture to colmap alignement and preprocessing failed. Check error logs")
            sys.exit(1)

    if args['doInsideOut']:
        print("Running deep blending preprocessing\npython ibr_db_preprocess.py -r -m -i ",  args['path'])
        p_exit = subprocess.call(["python", "ibr_db_preprocess.py", "-r", "-m", "-i", args['path']])
        if p_exit != 0:
            print("SIBR_ERROR Inside Out preprocessing failed. Check error logs")
            sys.exit(1)

    if not args['dontDoDeepBlending']:
        maxwidth = args["max_width"]
        print("MAX ", type(maxwidth))
        print("Running deep blending preprocessing\npython ibr_db_preprocess.py -r -i ",  args['path'], " -w", maxwidth)
        p_exit = subprocess.call(["python", "ibr_db_preprocess.py", "-r", "-i", args['path'], "-w", maxwidth])
        if p_exit != 0:
            print("SIBR_ERROR Deep blending preprocessing failed. Check error logs")
            sys.exit(1)


if __name__ == "__main__":
    maindb()
