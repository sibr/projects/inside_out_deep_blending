# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


import tensorflow as tf
import numpy as np
import scipy
import os
import sys
import math
import tensorflow.contrib.slim as slim

OUTPUT_FILE = "model/model.pb"

sess = None
    
# Helper functions for the convolutional layers

def conv(x, num_out_layers, kernel_size, stride, activation_fn=tf.nn.relu):
    c = tf.contrib.layers.conv2d(x, num_out_layers, kernel_size, stride, 'SAME', data_format='NCHW', activation_fn=activation_fn)
    return c

def upsample_nn(x, num_in_layers):
#    x_up = tf.reshape(tf.stack([x, x], axis=3), [tf.shape(x)[0], num_in_layers, 2 * tf.shape(x)[2], tf.shape(x)[3]])
#    return tf.reshape(tf.concat([x_up[...,tf.newaxis], x_up[...,tf.newaxis]], axis=-1), \
#                      [tf.shape(x)[0], num_in_layers, 2 * tf.shape(x)[2], 2 * tf.shape(x)[3]])
    return tf.tile(x, [1, 1, 2, 2], name="UpsampleNN")

def upconv(x, num_out_layers, num_in_layers, kernel_size):
    conv1 = conv(x, num_out_layers, kernel_size, 1)
    return upsample_nn(conv1, num_out_layers)

def downconv(x, num_out_layers, kernel_size, stride = 2):
    return conv(x,     num_out_layers, kernel_size, stride)

#-------------------------------------------------------------------------------

print("Converting network to protobuf!")
data_file = "test.txt"

def blending_network(all_inputs):
    #all_inputs = tf.concat(inputs, axis=0)[np.newaxis]
    #all_inputs = tf.transpose(all_inputs, [0, 3, 1, 2])
    #all_inputs = tf.concat(inputs, axis=1)

    conv1    = conv(all_inputs,  20, 3, 1)
    conv2    = downconv(conv1,   30, 3, 2)
    conv3    = downconv(conv2,   40, 3, 2)
    conv4    = downconv(conv3,   60, 3, 2)
    conv5    = downconv(conv4,  80, 3, 2)
    conv6    = upconv(conv5,     60, 80, 3)
    concat1  = conv(tf.concat([conv6, conv4], 1), 60, 1, 1)
    conv7    = upconv(concat1,   40, 60, 3)
    concat2  = conv(tf.concat([conv7, conv3], 1), 40, 1, 1)
    conv8    = upconv(concat2,   30, 40, 3)
    concat3  = conv(tf.concat([conv8, conv2], 1), 30, 1, 1)
    conv9    = upconv(concat3,   20, 30, 3)
    features = conv(tf.concat([conv9, conv1], 1), 20, 1, 1)
    
    out_image = 0.5 * (1.0 + conv(features, 3, 3, 1, tf.nn.tanh))
    #out_image = tf.squeeze(tf.transpose(out_image, [0, 2, 3, 1]))
    #out_image = tf.transpose(out_image, [0, 2, 3, 1])

    return out_image

#w, h = None, None
#w, h = 1296, 832
w, h = 1280, 720
#inputs = [
#    tf.placeholder(tf.float32, name="input1", shape=(3, h, w)),
#    tf.placeholder(tf.float32, name="input2", shape=(3, h, w)),
#    tf.placeholder(tf.float32, name="input3", shape=(3, h, w)),
#    tf.placeholder(tf.float32, name="input4", shape=(3, h, w)),
#    tf.placeholder(tf.float32, name="input5", shape=(3, h, w))]

inputs = tf.placeholder(tf.float32, name="input1", shape=(1, 15, h, w))

#inputs = [
#    tf.zeros([1, 3, 720, 1280], tf.float32, name="input1"),
#    tf.zeros([1, 3, 720, 1280], tf.float32, name="input2"),
#    tf.zeros([1, 3, 720, 1280], tf.float32, name="input3"),
#    tf.zeros([1, 3, 720, 1280], tf.float32, name="input4"),
#    tf.zeros([1, 3, 720, 1280], tf.float32, name="input5")]

with tf.variable_scope("model", reuse=False):
    current_out = blending_network(inputs)
    #current_out = blending_network([input, input, input, input, input])

# Save intermediate models
saver = tf.train.Saver(max_to_keep=1)

# Creating a config to prevent GPU use at all
config = tf.ConfigProto()

# Start GPU memory small and allow to grow
config.gpu_options.allow_growth=True

sess = tf.Session(config=config)
init_global = tf.global_variables_initializer()
init_local = tf.local_variables_initializer()
sess.run(init_local)
sess.run(init_global)

print("FINDING CHECKPOINTS")
# Load pretrained weights
first_batch = 0
last_checkpoint = tf.train.latest_checkpoint("model")
if last_checkpoint == None:
    print("Could not load last checkpoint!")
    sys.exit(0)

print("LOADING NET")
print(last_checkpoint)
test_variables = []
for var in tf.global_variables():
    if var.name.find("Adam") == -1:
        test_variables.append(var)
test_restorer = tf.train.Saver(test_variables)
test_restorer.restore(sess, last_checkpoint)

print("OUTPUT GRAPH")
# We use a built-in TF helper to export variables to constants
output_graph_def = tf.graph_util.convert_variables_to_constants(
    sess, # The session is used to retrieve the weights
    tf.get_default_graph().as_graph_def(),
    [current_out.name[:current_out.name.rfind(":")]]
) 

# Finally we serialize and dump the output graph to the filesystem
with tf.gfile.GFile(OUTPUT_FILE, "wb") as fid:
    fid.write(output_graph_def.SerializeToString())
print("%d ops in the final graph." % len(output_graph_def.node))

#print("Input tensors:", current_input, current_images, ref_image)
print("Output tensor:", current_out)

from tensorflow.python.platform import gfile
with tf.Session() as sess:
    with gfile.FastGFile(OUTPUT_FILE, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        g_in = tf.import_graph_def(graph_def)
train_writer = tf.summary.FileWriter("model/log")
train_writer.add_graph(sess.graph)
