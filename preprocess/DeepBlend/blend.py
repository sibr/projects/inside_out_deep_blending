# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


from __future__ import print_function

import tensorflow as tf
import numpy as np
import scipy
import os
import sys
import math
import tensorflow.contrib.slim as slim
from tensorflow.contrib.slim.nets import vgg


import functools

print=functools.partial(print, flush=True)

from collections import namedtuple

from bilinear_sampler import *
from custom_vgg import *

image_loss            = 'VGG_AND_L1'
direct_regression     = False
fast_network          = False
use_temporal_loss     = True
use_global_mesh       = True
num_input_layers      = 4
debug_mode            = False
use_nchw              = True
temporal_alpha        = 0.33

parameters = namedtuple('parameters',
                        'data_folder, '
                        'is_test, '
                        'crop, '
                        'batch_size, '
                        'epochs, '
                        'num_threads')

sess = None

class IBRDataLoader(object):
    """IBR dataloader"""
    def __init__(self, filenames_file, params):
        self.params = params
        self.ref_images = None
        self.current_images = None
        self.old_images = None
        self.old_to_current = None
        self.init_height = None
        self.init_width = None

        if params.is_test:
            f = open(filenames_file, 'r')
            line = f.readline()
            f.close()
            self.init_width = int(line.split()[0])
            self.init_height = int(line.split()[1])

        with tf.variable_scope("inputloader"):
            self.data_folder = tf.constant(params.data_folder)

            input_queue = tf.train.string_input_producer([filenames_file], shuffle = (not params.is_test))
            line_reader = tf.TextLineReader()
            _, line = line_reader.read(input_queue)
            split_line = tf.string_split([line]).values

            offset = 0
            current_width = tf.string_to_number(split_line[offset], tf.int32)

            offset += 1
            current_height = tf.string_to_number(split_line[offset], tf.int32)

            offset += 1
            ref_img = self.read_jpg(split_line[offset])
            ref_img.set_shape([self.init_height, self.init_width, 3])

            flip_vert = 0
            flip_horiz = 0
            rotate_img = 0

            def augment_image(img):
                img = tf.cond(flip_vert > 0.5, lambda: tf.image.flip_up_down(img), lambda: img)
                img = tf.cond(flip_horiz > 0.5, lambda: tf.image.flip_left_right(img), lambda: img)
                img = tf.cond(rotate_img > 0.5, lambda: tf.image.rot90(img), lambda: img)
                return img

            def augment_flow(img):
                x_flow = tf.expand_dims(img[:,:,0], 2)
                y_flow = tf.expand_dims(img[:,:,1], 2)
                z_flow = tf.expand_dims(img[:,:,2], 2)

                # Vertical flipping
                y_flow = tf.cond(flip_vert > 0.5, lambda: y_flow * -1.0, lambda: y_flow)

                # Horizontal flipping
                x_flow = tf.cond(flip_horiz > 0.5, lambda: x_flow * -1.0, lambda: x_flow)

                # 90 degree rotation
                new_x_flow = tf.cond(rotate_img > 0.5, lambda: y_flow * -1.0, lambda: x_flow)
                new_y_flow = tf.cond(rotate_img > 0.5, lambda: x_flow, lambda: y_flow)

                return augment_image(tf.concat([new_x_flow, new_y_flow, z_flow], axis=2))

            crop_x = 0
            crop_y = 0
            if not params.is_test:
                flip_vert = tf.random_uniform([], 0, 1)
                flip_horiz = tf.random_uniform([], 0, 1)
                rotate_img = tf.random_uniform([], 0, 1)

                asserts_ref = [
                    tf.assert_greater_equal(current_width, params.crop, message="cur w smaller than crop"),
                    tf.assert_greater_equal(current_height, params.crop, message="cur h smaller than crop"),
                    tf.assert_greater_equal(tf.shape(ref_img)[0], crop_y + params.crop, message="ref h smaller"),
                    tf.assert_greater_equal(tf.shape(ref_img)[1], crop_x + params.crop, message="ref w smaller")]

                with tf.control_dependencies(asserts_ref):
                    crop_x = tf.random_uniform([], 0, current_width - params.crop, dtype=tf.int32)
                    crop_y = tf.random_uniform([], 0, current_height - params.crop, dtype=tf.int32)
                    ref_img = tf.image.crop_to_bounding_box(ref_img, crop_y, crop_x, params.crop, params.crop)
                    ref_img = augment_image(ref_img)

            offset += 1
            frames_color = []
            frames_flow = []
            sample_stride = 2 if not params.is_test else 1

            for j in range(2):
                local_offset = 0
                # First populate the CNN inputs with data from the global mesh
                global_color = self.read_jpg(split_line[offset + sample_stride * local_offset + j * 5 * sample_stride + 0])
                global_color.set_shape([self.init_height, self.init_width, 3])

                if not params.is_test and use_temporal_loss and j > 0:
                    average_flow = self.read_png(split_line[offset + sample_stride * local_offset + j * 5 * sample_stride + 1], 3)
                    average_flow.set_shape([self.init_height, self.init_width, 3])
                    average_flow = 512.0 * (average_flow * 2.0 - 1.0)

                if not params.is_test:
                    asserts_global = [
                        tf.assert_greater_equal(tf.shape(global_color)[0], crop_y + params.crop, message="global ch smaller"),
                        tf.assert_greater_equal(tf.shape(global_color)[1], crop_x + params.crop, message="global cw smaller")]
                    with tf.control_dependencies(asserts_global):
                        global_color = tf.image.crop_to_bounding_box(global_color, crop_y, crop_x, params.crop, params.crop)
                        global_color = augment_image(global_color)
                        if not params.is_test and use_temporal_loss and j > 0:
                            average_flow = tf.image.crop_to_bounding_box(average_flow, crop_y, crop_x, params.crop, params.crop)
                            average_flow = augment_flow(average_flow)

                if use_global_mesh:
                    frames_color.append([global_color])
                else:
                    frames_color.append([])

                if not params.is_test and use_temporal_loss:
                    flow = average_flow if j > 0 else tf.zeros_like(global_color)

                # Then incorporate information from each per-view mosaic
                local_offset += 1
                for i in range(num_input_layers):
                    colors = self.read_jpg(split_line[offset + sample_stride * (local_offset + i) + j * 5 * sample_stride + 0])
                    colors.set_shape([self.init_height, self.init_width, 3])

                    if not params.is_test:
                        asserts_local = [
                            tf.assert_greater_equal(tf.shape(colors)[0], crop_y + params.crop, message="local ch smaller"),
                            tf.assert_greater_equal(tf.shape(colors)[1], crop_x + params.crop, message="local cw smaller")]
                        with tf.control_dependencies(asserts_local):
                            colors = tf.image.crop_to_bounding_box(colors, crop_y, crop_x, params.crop, params.crop)
                            colors = augment_image(colors)
                    frames_color[-1].append(colors)

                if not params.is_test and use_temporal_loss:
                    frames_flow.append(flow)

            images_a = tf.concat(frames_color[0], axis=2)
            images_b = tf.concat(frames_color[1], axis=2)

            if not params.is_test and use_temporal_loss:
                flow_b = frames_flow[1]

            min_after_dequeue = 16
            capacity = min_after_dequeue + 4 * params.batch_size
            if params.is_test:
                self.ref_images, self.current_images, self.old_images = tf.train.batch([ref_img, images_a, images_b], params.batch_size, params.num_threads, capacity)
            elif use_temporal_loss:
                self.ref_images, self.current_images, self.old_images, self.old_to_current = tf.train.shuffle_batch([ref_img, images_a, images_b, flow_b], params.batch_size, capacity, min_after_dequeue, params.num_threads)
            else:
                self.ref_images, self.current_images, self.old_images = tf.train.shuffle_batch([ref_img, images_a, images_b], params.batch_size, capacity, min_after_dequeue, params.num_threads)


    def read_png(self, image_path, num_channels):
        image = tf.image.decode_png(tf.read_file(tf.string_join([self.data_folder, image_path], "/")), channels=num_channels, dtype=tf.uint16)
        image = tf.image.convert_image_dtype(image, tf.float32)
        return image

    def read_jpg(self, image_path):
        image = tf.image.decode_jpeg(tf.read_file(tf.string_join([self.data_folder, image_path], "/")))
        image = tf.image.convert_image_dtype(image, tf.float32)
        return image

## Tensorflow image translation op
# images:        A tensor of shape (num_images, num_rows, num_columns, num_channels) (NHWC),
#                (num_rows, num_columns, num_channels) (HWC), or (num_rows, num_columns) (HW).
# tx:            The translation in the x direction.
# ty:            The translation in the y direction.
# interpolation: If x or y are not integers, interpolation comes into play. Options are 'NEAREST' or 'BILINEAR'
def tf_image_translate(images, tx, ty, interpolation='NEAREST'):
    # got these parameters from solving the equations for pixel translations
    # on https://www.tensorflow.org/api_docs/python/tf/contrib/image/transform
    transforms = [1, 0, -tx, 0, 1, -ty, 0, 0]
    return tf.contrib.image.transform(images, transforms, interpolation)

#-------------------------------------------------------------------------------

#if len(sys.argv) != 2 and len(sys.argv) != 3:
#    print("Usage: ./" + sys.argv[0] + " [PATH_TO_TRAINING_DATA_FOLDER] (test)")
#    sys.exit(0)

import argparse

parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument("data_path", type=str,
    help="relative directory for all data")
parser.add_argument("output_path", type=str,
    help="base-level output directory for results")

parser.add_argument("--training_file", type=str, default=None,
    help="full path to the training file")
parser.add_argument("--validation_file", type=str, default=None,
    help="full path to the validation file")

parser.add_argument("--test", default=False, action="store_true")
parser.add_argument("--log_path", type=str, default="log")
parser.add_argument("--img_path", type=str, default="img")
parser.add_argument("--model_path", type=str, default="model")
parser.add_argument("--suffix", type=str, default="",
    help="appended to the output folders")

args = parser.parse_args()

#-------------------------------------------------------------------------------

dataloader            = None
dataloader_validation = None

#is_test = len(sys.argv) > 2 and "test" in sys.argv[2]
is_test = args.test
if is_test:
    print("Running in test mode!")
    params = parameters(data_folder=args.data_path, is_test=True, crop=512, batch_size=1, num_threads=1, epochs=256000)
    data_file = args.data_path + "/test.txt"
    dataloader = IBRDataLoader(data_file, params)
else:
    print("Running in training mode!")
    data_file = args.training_file
    params = parameters(data_folder=args.data_path, is_test=False, crop=256, batch_size=8, num_threads=8, epochs=512000)
    dataloader = IBRDataLoader(data_file, params)

    if args.validation_file is not None:
        validation_file = args.validation_file
        dataloader_validation = IBRDataLoader(validation_file, params)

def blending_network(all_inputs, images, reference):
    if use_nchw:
        all_inputs = tf.transpose(all_inputs, [0, 3, 1, 2])

    # Helper functions for the convolutional layers
    if use_nchw:
        def conv(x, num_out_layers, kernel_size, stride, activation_fn=tf.nn.relu):
            c = tf.contrib.layers.conv2d(
                x, num_out_layers, kernel_size, stride, 'SAME', data_format='NCHW',
                activation_fn=activation_fn)
            return c

        concat_dimension = 1
        def upsample_nn(x, num_in_layers):
            x_up = tf.reshape(
                tf.stack([x, x], axis=3),
                [tf.shape(x)[0], num_in_layers, 2 * tf.shape(x)[2], tf.shape(x)[3]])
            return tf.reshape(
                tf.concat([x_up[...,tf.newaxis], x_up[...,tf.newaxis]], axis=-1),
                [tf.shape(x)[0], num_in_layers, 2 * tf.shape(x)[2], 2 * tf.shape(x)[3]])
    else:
        def conv(x, num_out_layers, kernel_size, stride, activation_fn=tf.nn.relu):
            p = np.floor((kernel_size - 1) / 2).astype(np.int32)
            p_x = tf.pad(x, [[0, 0], [p, p], [p, p], [0, 0]], mode='REFLECT')
            c = slim.conv2d(p_x, num_out_layers, kernel_size, stride, 'VALID', activation_fn=activation_fn)
            return c

        concat_dimension = 3
        def upsample_nn(x, num_in_layers):
            s = tf.shape(x)
            h = s[1]
            w = s[2]
            oh = tf.cast(h * 2, tf.int32)
            ow = tf.cast(w * 2, tf.int32)
            return tf.image.resize_nearest_neighbor(x, [oh, ow])

    if fast_network:
        def upconv(x, num_out_layers, num_in_layers, kernel_size):
            conv1 = conv(x, num_out_layers, kernel_size, 1)
            return upsample_nn(conv1, num_out_layers)

        def downconv(x, num_out_layers, kernel_size, stride = 2):
            return conv(x,     num_out_layers, kernel_size, stride)

        conv1    = conv(all_inputs,  20, 3, 1)
        conv2    = downconv(conv1,   30, 3, 2)
        conv3    = downconv(conv2,   40, 3, 2)
        conv4    = downconv(conv3,   60, 3, 2)
        conv5    = downconv(conv4,  80, 3, 2)
        conv6    = upconv(conv5,     60, 80, 3)
        concat1  = conv(tf.concat([conv6, conv4], concat_dimension), 60, 1, 1)
        conv7    = upconv(concat1,   40, 60, 3)
        concat2  = conv(tf.concat([conv7, conv3], concat_dimension), 40, 1, 1)
        conv8    = upconv(concat2,   30, 40, 3)
        concat3  = conv(tf.concat([conv8, conv2], concat_dimension), 30, 1, 1)
        conv9    = upconv(concat3,   20, 30, 3)
        features = conv(tf.concat([conv9, conv1], concat_dimension), 20, 1, 1)

    # the big network
    else:
        def upconv(x, num_out_layers, kernel_size):
            upsample = upsample_nn(x, x.shape[1])
            conv1 = conv(upsample, num_out_layers, kernel_size, 1)
            return conv1

        def downconv(x, num_out_layers, kernel_size, stride = 2):
            conv1 = conv(x,     num_out_layers, kernel_size, 1)
            conv2 = conv(conv1, num_out_layers, kernel_size, stride)
            return conv2

        conv1    = conv(all_inputs,  32, 3, 1)
        conv2    = downconv(conv1,   48, 3, 2)
        conv3    = downconv(conv2,   64, 3, 2)
        conv4    = downconv(conv3,   96, 3, 2)
        conv5    = downconv(conv4,  128, 3, 2)
        conv6    = upconv(conv5,     96, 3)
        concat1  = tf.concat([conv6, conv4], concat_dimension)
        conv7    = upconv(concat1,   64, 3)
        concat2  = tf.concat([conv7, conv3], concat_dimension)
        conv8    = upconv(concat2,   48, 3)
        concat3  = tf.concat([conv8, conv2], concat_dimension)
        conv9    = upconv(concat3,   32, 3)
        features = tf.concat([conv9, conv1], concat_dimension)

    # obtain the final output
    num_images = num_input_layers
    if use_global_mesh:
        num_images = num_images + 1

    img_list = tf.split(images, num_images, 3)
    if direct_regression:
        out_image = 0.5 * (1.0 + conv(features, 3, 3, 1, tf.nn.tanh))
    else:
        if use_nchw:
            out = conv(features, num_images, 3, 1, None)
            softmax = tf.nn.softmax(out, 1)

            out = tf.reshape(
                all_inputs, [params.batch_size, num_images, 3, tf.shape(out)[2], tf.shape(out)[3]])
            out *= softmax[:,:,tf.newaxis]
            out_image = tf.reduce_sum(out, axis=1)
        else:
            out = conv(features, 5, 3, 1, None)
            softmax = tf.nn.softmax(out)
            weight_list = tf.split(softmax, num_images, 3)
            out_image = tf.zeros_like(img_list[0])
            for i in range(len(img_list)):
                out_image += img_list[i] * weight_list[i]

    if use_nchw:
        out_image = tf.transpose(out_image, [0, 2, 3, 1])

    return out_image, img_list

def compute_standard_error(output, reference):
    l1_image = tf.abs(output - reference)
    return l1_image

def compute_shifted_loss(output, reference, kernel_size):
    kernel = [0]
    for i in range(kernel_size):
        kernel = [-(i + 1)] + kernel + [i + 1]

    def shift_crop(img, dx, dy):
        if dx >= 0 and dy >= 0:
            return img[:,dy:,dx:,:]
        elif dx >= 0 and dy < 0:
            return img[:,0:dy,dx:,:]
        elif dx < 0 and dy >= 0:
            return img[:,dy:,0:dx,:]
        else: # dx < 0 and dy < 0
            return img[:,0:dy,0:dx,:]

    def shift_uncrop(img, dx, dy):
        return tf.pad(img, tf.constant([[0, 0], [max(dy, 0), max(-dy, 0)], [max(dx, 0), max(-dx, 0)], [0, 0]]),
                      "CONSTANT", None, 1000000.0)

    error_image = tf.ones_like(reference) * 1000000.0
    for dy in kernel:
        for dx in kernel:
            shifted_ref = tf_image_translate(reference, dx, dy)
            l1_error = shift_crop(tf.abs(output - shifted_ref), dx, dy)
            l1_sad_error = slim.avg_pool2d(l1_error, len(kernel) + 2, 1, 'SAME')

            final_error = shift_uncrop(l1_sad_error, dx, dy)
            error_image = tf.minimum(error_image, final_error)

    return error_image

vgg_mean = tf.reshape(tf.constant([123.68, 116.78, 103.94]), [1, 1, 3])
def compute_vgg_error(output, reference, layer):
    scaled_output = output * 255 - vgg_mean
    scaled_reference = reference * 255 - vgg_mean
    with slim.arg_scope(vgg.vgg_arg_scope()):
        output_a, output_b = vgg_16(scaled_output, is_training=False, spatial_squeeze=False)
        reference_a, reference_b = vgg_16(scaled_reference, is_training=False, spatial_squeeze=False)
    if layer == 0:
        return tf.abs(output_a - reference_a)
    return tf.abs(output_b - reference_b)

if image_loss == 'L1': # Standard L1 + SSIM
    def compute_error(output, reference, kernel_size):
        return compute_standard_error(output, reference)

    def compute_loss(output, reference, kernel_size):
        return tf.reduce_mean(compute_standard_error(output, reference))
elif image_loss == 'shiftable': # Our shiftable patch loss
    def compute_error(output, reference, kernel_size):
        return compute_shifted_loss(output, reference, kernel_size)

    def compute_loss(output, reference, kernel_size):
        return tf.reduce_mean(compute_shifted_loss(output, reference, kernel_size))
elif image_loss == 'VGG': # Use a VGG loss
    def compute_error(output, reference, kernel_size):
        return compute_vgg_error(output, reference, 0)

    def compute_loss(output, reference, kernel_size):
        return tf.reduce_mean(compute_vgg_error(output, reference, 0)) + \
            tf.reduce_mean(compute_vgg_error(output, reference, 1))
elif image_loss == 'VGG_AND_L1': # Mix L1 and VGG to perserve high frequency content
    def compute_error(output, reference, kernel_size):
        return 255.0 * compute_standard_error(output, reference)

    def compute_loss(output, reference, kernel_size):
        return tf.reduce_mean(compute_vgg_error(output, reference, 0)) + \
            tf.reduce_mean(compute_vgg_error(output, reference, 1)) + \
            255.0 * tf.reduce_mean(compute_standard_error(output, reference))
else:
    print("Unexpected image loss: " + image_loss)
    sys.exit(0)


def downsample_image(img):
    img_lowres = img
    for i in range(2):
        # We're building a gaussian approximation with repeated convolutions
        img_lowres = slim.avg_pool2d(img_lowres, 2, 1, 'SAME') # [1 1]
        img_lowres = slim.avg_pool2d(img_lowres, 2, 1, 'SAME') # [1 2 1]
        img_lowres = slim.avg_pool2d(img_lowres, 2, 1, 'SAME') # [1 3 3 1]
        img_lowres = slim.avg_pool2d(img_lowres, 2, 2, 'SAME') # [1 4 6 4 1]
    return img_lowres

def compute_lowres_error(a, b):
    a_lowres = downsample_image(a)
    b_lowres = downsample_image(b)
    return compute_standard_error(a_lowres, b_lowres)

def compute_lowres_loss(a, b):
    return tf.reduce_mean(compute_lowres_error(a, b))

with tf.variable_scope("standard_inputs"):
    ref_image = dataloader.ref_images
    current_images = dataloader.current_images
    old_images = dataloader.old_images
    #current_images = tf.Print(current_images, [tf.shape(current_images)], "current_image bef : ", summarize=10)
    current_input = tf.concat(current_images, 3)
    #current_input = tf.Print(current_input, [tf.shape(current_input)], "current_images aft : ", summarize=10)
    old_input = tf.concat(old_images, 3)
    if not params.is_test and use_temporal_loss:
        old_to_current = dataloader.old_to_current

if not params.is_test and args.validation_file is not None:
    with tf.variable_scope("validation_inputs"):
        v_ref_image = dataloader_validation.ref_images
        v_current_images = dataloader_validation.current_images
        v_old_images = dataloader_validation.old_images
        v_current_input = tf.concat(v_current_images, 3)
        v_old_input = tf.concat(v_old_images, 3)
        if  use_temporal_loss:
            v_old_to_current = dataloader_validation.old_to_current

with tf.variable_scope("model", reuse=False):
    current_out, current_img_list = blending_network(current_input, current_images, ref_image)
    warped_current = current_out

with tf.variable_scope("model", reuse=True):
    old_out, old_img_list = blending_network(old_input, old_images, ref_image)

if not params.is_test and use_temporal_loss:
    warped_current = bilinear_sampler_2d(current_out, old_to_current, 'edge')

if not params.is_test and args.validation_file is not None:
    with tf.variable_scope("model", reuse=True):
        v_current_out, v_current_img_list = blending_network(v_current_input, v_current_images, v_ref_image)
        v_warped_current = v_current_out

    with tf.variable_scope("model", reuse=True):
        v_old_out, v_old_img_list = blending_network(v_old_input, v_old_images, v_ref_image)

    if use_temporal_loss:
        v_warped_current = bilinear_sampler_2d(v_current_out, v_old_to_current, 'edge')

def compute_average_image(image_list):
    normalizer = 0.0
    average = tf.zeros_like(image_list[0])
    for i in range(1, len(image_list)):
        average += image_list[i]
        normalizer += 1.0
    return average / normalizer

# Losses
standard_loss = compute_loss(current_out, ref_image, 2)
temporal_loss = compute_loss(warped_current, old_out, 2)
minimize_loss = standard_loss
if use_temporal_loss:
    minimize_loss += temporal_alpha * temporal_loss
smart_loss = standard_loss / (0.0001 + compute_loss(compute_average_image(current_img_list), ref_image, 2))

if not params.is_test and args.validation_file is not None:
    validation_loss = compute_loss(v_current_out, v_ref_image, 2)
    v_temporal_loss = compute_loss(v_warped_current, v_old_out, 2)
    v_smart_loss = validation_loss / (0.0001 + compute_loss(compute_average_image(v_current_img_list),
                                                            v_ref_image, 2))

# Train the network using Adam
global_step = tf.get_variable("global_step", initializer=tf.constant(0, dtype=tf.int32), trainable=False)

ibr_train_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "model")
update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
with tf.control_dependencies(update_ops):
    train_step = tf.train.AdamOptimizer(3e-4).minimize(minimize_loss, global_step=global_step, var_list=ibr_train_vars)

# Save intermediate models
saver = tf.train.Saver(max_to_keep=1)

# Creating a config to prevent GPU use at all
config = tf.ConfigProto()

# Start GPU memory small and allow to grow
config.gpu_options.allow_growth=True

sess = tf.Session(config=config)
init_global = tf.global_variables_initializer()
init_local = tf.local_variables_initializer()
sess.run(init_local)
sess.run(init_global)

def tensorboard_normalize_hack(img):
    return tf.concat([tf.zeros_like(img[:,0:1,:,:]),
                      img,
                      tf.ones_like(img[:,0:1,:,:])], axis=1)

vis_output = tf.summary.image("result", tensorboard_normalize_hack(current_out), max_outputs=params.batch_size)
vis_reference = tf.summary.image("reference", tensorboard_normalize_hack(ref_image), max_outputs=params.batch_size)
vis_previous = tf.summary.image("previous", tensorboard_normalize_hack(old_out), max_outputs=params.batch_size)
if not params.is_test:
    if use_temporal_loss:
        vis_warped_current = tf.summary.image("current_warped", tensorboard_normalize_hack(warped_current), max_outputs=params.batch_size)
        if args.validation_file is not None:
            vis_v_warped_current = tf.summary.image("validation_current_warped", tensorboard_normalize_hack(v_warped_current), max_outputs=params.batch_size)

    if args.validation_file is not None:
        vis_v_output = tf.summary.image("validation_result", tensorboard_normalize_hack(v_current_out), max_outputs=params.batch_size)
        vis_v_reference = tf.summary.image("validation_reference", tensorboard_normalize_hack(v_ref_image), max_outputs=params.batch_size)
        vis_v_previous = tf.summary.image("validation_previous", tensorboard_normalize_hack(v_old_out), max_outputs=params.batch_size)

if debug_mode:
    image_index = 0
    if use_global_mesh:
        vis_g_c = tf.summary.image("global_color",
                                   tensorboard_normalize_hack(current_img_list[image_index]),
                                   max_outputs=params.batch_size)
        image_index = image_index + 1
    if num_input_layers > 0:
        vis_a_c = tf.summary.image("a_color",
                                   tensorboard_normalize_hack(current_img_list[image_index]),
                                   max_outputs=params.batch_size)
        image_index = image_index + 1
    if num_input_layers > 1:
        vis_b_c = tf.summary.image("b_color",
                                   tensorboard_normalize_hack(current_img_list[image_index]),
                                   max_outputs=params.batch_size)
        image_index = image_index + 1
    if num_input_layers > 2:
        vis_c_c = tf.summary.image("c_color",
                                   tensorboard_normalize_hack(current_img_list[image_index]),
                                   max_outputs=params.batch_size)
        image_index = image_index + 1
    if num_input_layers > 3:
        vis_d_c = tf.summary.image("d_color",
                                   tensorboard_normalize_hack(current_img_list[image_index]),
                                   max_outputs=params.batch_size)

    vis_loss = tf.summary.image("0_debug_shifted_error", compute_error(current_out, ref_image, 2), max_outputs=params.batch_size)
    vis_loss_lowres = tf.summary.image("0_debug_error_lowres", compute_lowres_error(current_out, old_out), max_outputs=params.batch_size)
    vis_current_lowres = tf.summary.image("0_debug_current_lowres", downsample_image(current_out), max_outputs=params.batch_size)
    vis_old_lowres = tf.summary.image("0_debug_old_lowres", downsample_image(old_out), max_outputs=params.batch_size)

standard_avg = compute_average_image(current_img_list)
vis_avg = tf.summary.image("result_avg", tensorboard_normalize_hack(standard_avg), max_outputs=params.batch_size)
if not params.is_test and args.validation_file is not None:
    validation_avg = compute_average_image(v_current_img_list)
    vis_v_avg = tf.summary.image("validation_avg", tensorboard_normalize_hack(validation_avg), max_outputs=params.batch_size)

train_summary = tf.summary.merge([tf.summary.scalar("batch_loss", standard_loss),
                                  tf.summary.scalar("smart_loss", smart_loss),
                                  tf.summary.scalar("temporal_loss", temporal_loss)])
if not params.is_test and args.validation_file is not None:
    validation_summary = tf.summary.merge([tf.summary.scalar("validation_batch_loss", validation_loss),
                                           tf.summary.scalar("validation_smart_loss", v_smart_loss),
                                           tf.summary.scalar("validation_temporal_loss", v_temporal_loss)])

def generate_path(directory_name, params):
    full_path = os.getcwd() + "/" + directory_name
    if params.is_test:
        full_path += "_test"
    if not os.path.isdir(full_path):
        os.makedirs(full_path, 0o755)
    return full_path
    
log_path = generate_path(
    os.path.join(args.output_path, args.log_path + args.suffix), params)
img_path = generate_path(
    os.path.join(args.output_path, args.img_path + args.suffix), params)
model_path = generate_path(
    os.path.join(args.output_path, args.model_path + args.suffix), params)
summary_writer = tf.summary.FileWriter(log_path, sess.graph)

# Load pretrained weights
first_batch = 0
last_checkpoint = tf.train.latest_checkpoint("model")
if params.is_test and last_checkpoint != None:
    test_variables = []
    for var in tf.global_variables():
        if var.name.startswith("model") and var.name.find("Adam") == -1:
            test_variables.append(var)
    test_restorer = tf.train.Saver(test_variables)
    test_restorer.restore(sess, last_checkpoint)
elif last_checkpoint != None:
    train_variables = []
    for var in tf.global_variables():
        train_variables.append(var)
    train_restorer = tf.train.Saver(train_variables)
    train_restorer.restore(sess, last_checkpoint)
    first_batch = int(last_checkpoint.split("-")[-1])
    print("Restarting from batch: " + str(first_batch))
elif image_loss == 'VGG' or image_loss == "VGG_AND_L1": # New fresh start, we only need to load the VGG weights
    vgg_variables = []
    for var in tf.global_variables():
        if var.name.startswith("vgg_16") and var.name.find("Adam") == -1:
            vgg_variables.append(var)
    vgg_restorer = tf.train.Saver(vgg_variables)
    vgg_restorer.restore(sess, "vgg_16.ckpt")

# Start the data loader threads
coordinator = tf.train.Coordinator()
threads = tf.train.start_queue_runners(sess=sess, coord=coordinator)

with sess.as_default():
    try:
        if params.is_test:
            test_file = open(data_file, 'r')
            test_lines = test_file.readlines()
            test_file.close()

            for i, line in enumerate(test_lines):
                if coordinator.should_stop():
                    break

                output = sess.run([current_out])
                raw_image = np.clip(output[0] * 255, 0, 255).astype(np.uint8)
                raw_image = np.reshape(raw_image, (dataloader.init_height, dataloader.init_width, 3))

                chunks = line.split(" ")
                scene_name = chunks[2].split("/")[0]
                image_index = chunks[-1].split("/")[1].split("_")[0]
                scene_path = img_path + "/" + scene_name + "/"
                if not os.path.isdir(scene_path):
                    os.makedirs(scene_path, 0o755)
                scipy.misc.imsave(scene_path + image_index + ".jpg", raw_image)

                print("Test scene: %s frame: %s total: %d" % (scene_name, image_index, i))
        else:
            for i in range(first_batch, params.epochs):
                if coordinator.should_stop():
                    break

                run_list = [train_step]
                if i == 0 or i % 8 == 7:
                    run_list = run_list + [standard_loss, train_summary]
                    if args.validation_file is not None:
                        run_list += [validation_summary]

                if i == 0 or i % 256 == 255:
                    run_list = run_list + [vis_output, vis_reference, vis_avg, vis_previous]
                    if args.validation_file is not None:
                        run_list += [vis_v_output, vis_v_reference, vis_v_avg, vis_v_previous]

                    if  use_temporal_loss:
                        run_list = run_list + [vis_warped_current]
                        if args.validation_file is not None:
                            run_list += [vis_v_warped_current]

                    if debug_mode:
                        run_list = run_list + [vis_loss, vis_loss_lowres, vis_current_lowres, vis_old_lowres]
                        if use_global_mesh:
                            run_list = run_list + [vis_g_c]
                        if num_input_layers > 0:
                            run_list = run_list + [vis_a_c]
                        if num_input_layers > 1:
                            run_list = run_list + [vis_b_c]
                        if num_input_layers > 2:
                            run_list = run_list + [vis_c_c]
                        if num_input_layers > 3:
                            run_list = run_list + [vis_d_c]

                output = sess.run(run_list)

                if i == 0 or i % 8 == 7:
                    print("Step: %d, batch loss: %f" % (i , output[1]))

                for j in range(2, len(output)):
                    summary_writer.add_summary(output[j], global_step=i)
            
                if i % 256 == 255:
                    print("saving model")
                    saver.save(sess, os.path.join(model_path, "model"), global_step=global_step)
    except Exception as e:
        # Report exceptions to the coordinator.
        coordinator.request_stop(e)
    finally:
        coordinator.request_stop()
        coordinator.join(threads)
