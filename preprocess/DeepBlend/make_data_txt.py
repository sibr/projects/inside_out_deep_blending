# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


import sys
import os
from PIL import Image

if len(sys.argv) != 2 and len(sys.argv) != 3:
    print("Usage: ./" + sys.argv[0] + " [PATH_TO_DATASET_FOLDER] (validation)")
    sys.exit(1)

is_validation = len(sys.argv) > 2 and sys.argv[2] == "validation"
is_test = len(sys.argv) > 2 and sys.argv[2] == "test"

dataset_path = sys.argv[1] + "/datadump"
if not os.path.isdir(dataset_path):
    dataset_path = sys.argv[1]

files = os.listdir(dataset_path)
if len(files) <= 0:
    print("ERROR: No files found in " + dataset_path)
    sys.exit(1)

indices = sorted(list(set([f.split("_")[0] for f in files])))

first_image_files = sorted([f for f in files if str(indices[0]) == f[0:len(indices[0])]])
first_image_index = first_image_files[0].split("_")[0]

reference_file = [f for f in first_image_files if "reference" in f][0]
reference_suffix = reference_file[len(first_image_index):-4]
reference_extension = reference_file[-4:]

num_random_samples = 1
num_path_samples   = 1
has_random_samples = len([f for f in files if "global" in f][0].split("_")) == 5
has_path_samples   = len([f for f in files if "global" in f][0].split("_")) == 7
random_suffixes    = [""]
path_suffixes      = [""]
if has_random_samples:
    num_random_samples = len([f for f in first_image_files if "global" in f])
    random_suffixes = sorted(list(set(["_" + f.split("_")[-1][0:-4] for f in first_image_files if "global" in f])))
    first_sample_files = [f for f in first_image_files if random_suffixes[0]  in f]

    type_files = [f for f in first_sample_files if "reference" not in f]
    type_suffixes = ["_" + "_".join(f.split("_")[1:-1]) for f in type_files]
    type_extensions = [f.split("_")[-1][-4:] for f in type_files]
elif has_path_samples:
    random_sample_files = [f for f in first_image_files if "global" in f and "path_0000" in f]
    num_random_samples = len(random_sample_files)
    random_suffixes = sorted(list(set(["_sample_" + f.split("_")[-3] for f in random_sample_files])))

    path_sample_files = [f for f in first_image_files if "global" in f and "sample_0000" in f]
    num_path_samples = len(path_sample_files)
    path_suffixes = sorted(list(set(["_path_" + f.split("_")[-1][0:-4] for f in path_sample_files])))
    first_sample_files = [f for f in first_image_files if random_suffixes[0] in f and path_suffixes[0] in f]

    type_files = [f for f in first_sample_files if "reference" not in f]
    type_suffixes = ["_" + "_".join(f.split("_")[1:-4]) for f in type_files]
    type_extensions = [f.split("_")[-1][-4:] for f in type_files]
else:
    type_files = [f for f in first_image_files if "reference" not in f]
    type_suffixes = ["_" + "_".join(f.split("_")[1:])[:-4] for f in type_files]
    type_extensions = [f.split("_")[-1][-4:] for f in type_files]

def make_path(i, s):
    return dataset_path + "/" + i + s

begin = 0
end = int(len(indices) * 0.9)
if is_validation:
    begin = end
    end = len(indices)
elif is_test:
    begin = 2
    end = len(indices)

for i in range(begin, end):
    for rs in random_suffixes:
        ii = indices[i]
        reference_path = make_path(ii, reference_suffix) + reference_extension
        ref = Image.open(reference_path)

        line = str(ref.size[0]) + " " \
               + str(ref.size[1]) + " " \
               + reference_path

        extra_indices = [ii]
        if is_test:
            extra_indices.append(indices[i - 1])
            extra_indices.append(indices[i - 2])
        for iii in extra_indices:
            for pi in range(len(path_suffixes)):
                for ti in range(len(type_suffixes)):
                    line += " " + make_path(iii, type_suffixes[ti]) + rs + path_suffixes[pi] + type_extensions[ti]

        print(line)
