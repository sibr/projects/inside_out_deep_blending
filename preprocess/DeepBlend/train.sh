# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/bin/sh

# set -x
# set -e

PROGRAM_NAME=train

# Parse arguments
TEMP=$(getopt -n $PROGRAM_NAME -o i:o: --long inputPath:,outputPath: -- "$@")                                                            

# Die if arguments are wrong
[ $? = 0 ] || die "Error parsing arguments. Try $PROGRAM_NAME --help"       

eval set -- "$TEMP"
while true; do     
    case $1 in 
        -i|--inputPath)
            INPUT_PATH="$2"; shift; shift; continue
        ;;                                    
        -o|--outputPath)                            
            OUTPUT_PATH="$2"; shift; shift; continue  
        ;;                                                                                                     
        --)                                                                 
            # no more arguments to parse                                
            break                                                       
        ;;                                                                  
        *)                                                                  
            printf "Unknown option %s\n" "$1"                           
            exit 1                                                      
        ;;                                                                  
    esac                                                                        
done 

# cd ~/virt_tf
# source ./bin/activate

# source /etc/profile.d/modules.sh
# module load cuda/9.1
# module load cudnn/7.0-cuda-9.1
# module load tensorflow/1.6-python3-cuda9.1


# cd ~/code/DeepBlend/

echo "python blend.py $INPUT_PATH $OUTPUT_PATH --training_file $INPUT_PATH/training.txt --validation_file $INPUT_PATH/validation.txt"
