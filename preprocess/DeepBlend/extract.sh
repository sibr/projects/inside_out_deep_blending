# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/bin/sh
cd ~/virt_tf
source ./bin/activate

source /etc/profile.d/modules.sh
module load cuda/9.1
module load cudnn/7.0-cuda-9.1
module load tensorflow/1.6-python3-cuda9.1


cd ~/code/DeepBlend/

OUTPUT_DIR="./output/"

cd ${OUTPUT_DIR}

python ../latest_blend_conv.py blend