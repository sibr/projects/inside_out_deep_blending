# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/bin/bash

script_names=(blend.py blend.py blend.py regress.py small.py)
model_names=(vggl1_blend_big_temporal vggl1_blend_big_notemporal l1_blend_big_temporal vggl1_reg_big_temporal vggl1_blend_small_temporal)
model_batch=model_512k

for ((i = 0 ; i < 5 ; i++))
do
    model_name=${model_names[$i]}
    script_name=${script_names[$i]}
    echo $i $script_name $model_name/$model_batch

    rm -rf *_test ;
    rm -rf model ;
    cp -r /media/phedman/DE28326F283247351/network_backups/$model_name/$model_batch model ;
    sed -i "s/\/.*model-/model-/g" model/checkpoint ;
    cat model/checkpoint ;

    # Run the network on all scenes
    python3 $script_name ~/Data/Video_Dumps $model_name --test ;

    # Then use ffmpeg to create high-quality mp4s
    for d in `ls $model_name/img_test`; do
        # Delete the last frame, I screwed up the pose for that one..
        rm $model_name/img_test/$d/`ls $model_name/img_test/$d | tail -n 1`
        ffmpeg -f image2 -r 60 -i $model_name/img_test/$d/%04d.jpg -vcodec libx264 -profile:v high444 -refs 5 -crf 8 -preset fast $model_name/$d.mp4 ;
    done

    model_batch=model_256k ;
done
