
# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/usr/bin/env python
#! -*- encoding: utf-8 -*-

""" @package dataset_tools_preprocess
This script processes images and creates an RealityCapture (RC) reconstruction, then creates a colmap version using the RC camera registration

Parameters: -h help,
            -path <path to your dataset folder>,

Usage: python processRC.py -path <path to your dataset folder>

"""

import os, sys, shutil
os.sys.path.append('../preprocess/converters')
os.sys.path.append('../../dataset_tools/preprocess/')
os.sys.path.append('../../dataset_tools/preprocess/realityCaptureTools')
os.sys.path.append('../../dataset_tools/preprocess/fullColmapProcess')
os.sys.path.append('../../dataset_tools/preprocess/converters')

import json
import argparse
from utils.paths import getBinariesPath, getColmapPath, getMeshlabPath
from utils.commands import  getProcess, getColmap, getRCprocess
from utils.TaskPipeline import TaskPipeline
import rc_tools
import colmap2nerf
from db_dataset_create import db_create
import selective_colmap_process

def find_file(filename):
    fname = os.path.join(os.path.abspath(os.path.dirname(__file__)), filename)
    if not os.path.exists(fname):
        fname = os.path.join("../preprocess/converters", filename)
    if not os.path.exists(fname):
        fname = os.path.join("../../dataset_tools/preprocess/fullColmapProcess", filename)
    if not os.path.exists(fname):
        fname = os.path.join("../../dataset_tools/preprocess/realityCaptureTools", filename)
    if not os.path.exists(fname):
        fname = os.path.join("../../dataset_tools/preprocess/converters", filename)
    return fname
    

def main():
    print("******************* ")
    parser = argparse.ArgumentParser()

    # common arguments
    parser.add_argument("--sibrBinariesPath", type=str, default=getBinariesPath(), help="binaries directory of SIBR")
    parser.add_argument("--colmapPath", type=str, default=getColmapPath(), help="path to directory colmap.bat / colmap.bin directory")
    parser.add_argument("--quality", type=str, default='default', choices=['default', 'low', 'medium', 'average', 'high', 'extreme'],
        help="quality of the reconstruction")
    parser.add_argument("--path", type=str, required=True, help="path to your dataset folder")
    parser.add_argument("--dry_run", action='store_true', help="run without calling commands")
    parser.add_argument("--from_step", type=str, default='default', help="Run from this step to --to_step (or end if no to_step")
    parser.add_argument("--to_step", type=str, default='default', help="up to but *excluding* this step (from --from_step); must be unique steps")
    parser.add_argument("--maxwidth", type=str, default='1500', help="max width for DeepBlending preprocesssing (def: 1500) ")

    # colmap
    #colmap performance arguments
    parser.add_argument("--numGPUs", type=int, default=2, help="number of GPUs allocated to Colmap")

    # Patch match stereo
    parser.add_argument("--PatchMatchStereo.max_image_size", type=int, dest="patchMatchStereo_PatchMatchStereoDotMaxImageSize")
    parser.add_argument("--PatchMatchStereo.window_radius", type=int, dest="patchMatchStereo_PatchMatchStereoDotWindowRadius")
    parser.add_argument("--PatchMatchStereo.window_step", type=int, dest="patchMatchStereo_PatchMatchStereoDotWindowStep")
    parser.add_argument("--PatchMatchStereo.num_samples", type=int, dest="patchMatchStereo_PatchMatchStereoDotNumSamples")
    parser.add_argument("--PatchMatchStereo.num_iterations", type=int, dest="patchMatchStereo_PatchMatchStereoDotNumIterations")
    parser.add_argument("--PatchMatchStereo.geom_consistency", type=int, dest="patchMatchStereo_PatchMatchStereoDotGeomConsistency")


    args = vars(parser.parse_args())

    from_step = args["from_step"]
    to_step = args["to_step"]

    # Update args with quality values
    fname = find_file("ColmapQualityParameters.json")
    with open(fname, "r") as qualityParamsFile:
        qualityParams = json.load(qualityParamsFile)

        for key, value in qualityParams.items():
            if not key in args or args[key] is None:
                args[key] = qualityParams[key][args["quality"]] if args["quality"] in qualityParams[key] else qualityParams[key]["default"]

    # Get process steps
    fname = find_file("processRC_DBSteps.json")
    with open(fname, "r") as processStepsFile:
        steps = json.load(processStepsFile)["steps"]

    # Fixing path values
    args["path"] = os.path.abspath(args["path"])
    args["sibrBinariesPath"] = os.path.abspath(args["sibrBinariesPath"])
    args["colmapPath"] = os.path.abspath(args["colmapPath"])
    args["gpusIndices"] = ','.join([str(i) for i in range(args["numGPUs"])])



    programs = {
        "colmap": {
            "path": getColmap(args["colmapPath"])
        },
        "RC": {
            "path": getRCprocess()
        }
    }

    # TODO: move to generic taskpipeline code; 
    if( from_step != 'default' or to_step != 'default' ):
        # check if to_step exists
        # select steps
        newsteps = []
        if from_step != 'default':
            adding_steps = False
        else:
            adding_steps = True
                
        exclude_steps = []
        for s in steps:
            if s['name'] == from_step :
                adding_steps = True
            if s['name'] == to_step :
                break
            if adding_steps and (not (s['name'] in exclude_steps)):
                newsteps.append(s)

        steps = newsteps

    pipeline = TaskPipeline(args, steps, programs)

    pipeline.runProcessSteps()
    
    print("selectiveColmapProcess has finished successfully.")
    sys.exit(0)

if __name__ == "__main__":
    main()
