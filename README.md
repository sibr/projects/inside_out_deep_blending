# Deep Blending for Free-Viewpoint Image-Based Rendering -- Scalable Inside-Out Image-Based Rendering {#inside_out_deep_blendingPage}

## Introduction

This *Project* contains the implementations of two papers:

[Hedman et al. 16] *Scalable Inside-Out Image-Based Rendering*, (http://www-sop.inria.fr/reves/Basilic/2016/HRDB16 ; UCL project page: http://visual.cs.ucl.ac.uk/pubs/insideout/)

	
[Hedman et al. 18] *Deep Blending for Free-Viewpoint Image-Based Rendering*, http://www-sop.inria.fr/reves/Basilic/2018/HPPFDB18 ; UCL project page: http://visual.cs.ucl.ac.uk/pubs/deepblending/ ; Inria project page: https://repo-sam.inria.fr/fungraph/deep-blending/)


If you use the code, we would greatly appreciate it if you could cite the corresponding papers:

```
@Article{HRDB16,
  author       = "Hedman, Peter and Ritschel, Tobias and Drettakis, George and Brostow, Gabriel",
  title        = "Scalable Inside-Out Image-Based Rendering",
  journal      = "ACM Transactions on Graphics (SIGGRAPH Asia Conference Proceedings)",
  number       = "6",
  volume       = "35",
  month        = "December",
  year         = "2016",
  url          = "http://www-sop.inria.fr/reves/publis/2016/HRDB16"
}
```

and/or:

```
@Article{HPPFDB18,
  author       = "Hedman, Peter and Philip, Julien and Price, True and Frahm, Jan-Michael and Drettakis, George and Brostow, Gabriel",
  title        = "Deep Blending for Free-Viewpoint Image-Based Rendering",
  journal      = "ACM Transactions on Graphics (SIGGRAPH Asia Conference Proceedings)",
  number       = "6",
  volume       = "37",
  month        = "November",
  year         = "2018",
  url          = "http://www-sop.inria.fr/reves/publis/2018/HPPFDB18"
}
```

and of course the actual *sibr* system:

```
@misc{sibr2020,
   author       = "Bonopera, Sebastien and Hedman, Peter and Esnault, Jerome and Prakash, Siddhant and Rodriguez, Simon and Thonat, Theo and Benadel, Mehdi and Chaurasia, Gaurav and Philip, Julien and Drettakis, George",
   title        = "sibr: A System for Image Based Rendering",
   year         = "2020",
   url          = "https://sibr.gitlabpages.inria.fr/"
```

---

## Authors

The code provided here is derived from the original *fribr* code developed by Peter Hedman at UCL. Siddhant Prakash integrated the code into *sibr* and added functionality, under the overall supervision of George Drettakis who also worked on final integration.

-----

## How to use 

The code has been developed for Windows (10), and we currently only support this platform. A Linux version will follow shortly.

### Use the binary distribution

The easiest way to use *SIBR* to run [Hedman 16] and [Hedman 18] is to download the binary distribution. All steps described below, including all preprocessing for your datasets will work using this code.
Download the distribution from the page: https://sibr.gitlabpages.inria.fr/download.html (Inside Out Deep Blending, 711Mb); unzip the file and rename the directory "install". The ULR and texturedMesh viewers are included as part of sibr-core. To run correctly, you will need to have the correct version of CUDA installed:

- [**CUDA 10.1+**](https://developer.nvidia.com/cuda-downloads) and [**CUDnn**](https://developer.nvidia.com/cudnn)
- [**tensorflow >= 1.13.1**]



### Checkout the code 

Check out the sibr core code and also:

* Tensorflow >= 1.13.1, ideally with GPU support. 
	I suggest you follow the tensorflow virtual environment installation procdure described here: https://www.tensorflow.org/install/pip
	Note: If CUDA 10.1 is NOT supported by tensorflow yet, you will get an "Unable to find DLL" error at runtime. Fallback to CUDA 10.0 or the CPU version.
* clone sibr_core along with tfgl_interop, fribr_framework, and inside_out_deep_blending in projects: 
	* this repo: inside_out_deep_blending https://gitlab.inria.fr/sibr/projects/inside_out_deep_blending
	* fribr: https://gitlab.inria.fr/sibr/fribr_framework which contains the basic *fribr* code components
	* tfgl_interop: https://gitlab.inria.fr/sibr/tfgl_interop interoperability between TensorFlow and OpenGL/

For this use the following commands:

```bash
## through HTTPS
git clone https://gitlab.inria.fr/sibr/sibr_core.git
## through SSH
git clone git@gitlab.inria.fr:sibr/sibr_core.git
```

Then go to *src/projects* and clone the other projects:

```bash
## through HTTPS
git clone https://gitlab.inria.fr/sibr/projects/inside_out_deep_blending.git
git clone https://gitlab.inria.fr/sibr/projects/fribr_framework.git
git clone https://gitlab.inria.fr/sibr/projects/tfgl_interop
```



See also the general installation [instructions](https://sibr.gitlabpages.inria.fr/docs/nightly/index.html#install_sec) for *sibr*. 

### Build & Install
 
 You can build and install *spixelwarp* via running ALL_BUILD and/or INSTALL in sibr_projects.sln solution (as mentioned in [Compiling](https://sibr.gitlabpages.inria.fr/docs/nightly/index.html#sibr_compile) or through `sibr_swarp*` specific targets in sibr_projects.sln solution.
 Dont forget to build INSTALL if you use ALL_BUILD.


---

# Running the renderer

After installing the *Projects* above, you can run the renderer as follows:

```
	SIBR_inside_out_deep_blending_app_rwdi.exe --path PATH_TO_DATSET --mesh PATH_TO_DATSET\capreal\mesh.ply --model path/to/deep/blending/model.pb --NCHW [ --texture-width 1280 --rendering-size 1280 720 ] --algorithm [deep_blending | inside_out]
	SIBR_inside_out_deep_blending_app.exe --path PATH_TO_DATASET --mesh PATH_TO_DATSET\capreal\mesh.ply --model path/to/deep/blending/model.pb --NCHW [ --texture-width 1280 --rendering-size 1280 720 ] --algorithm [deep_blending | inside_out]
```

The --algorithm with argument "deep_blending" chooses the Deep Blending [Hedman 18] algorithm, which includes the CNN-based blending step, and "inside_out" which chooses the baseline re-implementation of the Inside Out [Hedman 16] algorithm that was described in the [Hedman 18] paper. This is different from the original publication in the following ways: we dont use Kinect data, but instead we just use the global MVS mesh from RealityCapture or colmap depending on the preprocessing pipeline used (please see below). To run Inside Out, the dataset has to have an "insideout" folder, and to run Deep Blending there needs to be a "deep_blending" folder.

**Note**: If the renderer seems stuck after ~20sec, showing a white window after the message "Loading patch cloud!!!", please kill the process and start again. This is a known issue with cuda initialization.


*PATH_TO_DB_MODEL* is the directory containing the pre-trained CNN for deep blending, provided here:

```
wget https://repo-sam.inria.fr/fungraph/deep-blending/deep_blending_model/model.pb
```

Our interactive viewer has a main view running the algorithm and a top view to visualize the position of the calibrated cameras. By default you are in WASD mode, and can toggle to trackball using the "y" key. Please see the page [Interface](https://sibr.gitlabpages.inria.fr/docs/nightly/howto_sibr_useful_objects.html) for more details on the interface.



## Playing paths from the command line & comparisons

Paths can be played by the renderer by running the renderer in offscreen mode:
```
SIBR_inside_out_deep_blending.exe [OTHER OPTIONS] --offscreen --pathFile path.(out|lookat|tst|path) [--outPath optionalOutputPath --noExit]
```
By default, the application exits when this operation is performed. This is the easiest way to compare algorithms.


In addition to offline comparison there is a comparison app that allows visualization of Deep Blending compared to ulr, that can be run with the same arguments as above:

	SIBR_inside_out_deep_blending_ulr_compare_rwdi.exe --path PATH_TO_DATASET --mesh PATH_TO_DATASET\capreal\mesh.ply --model model.pb --NCHW [ --texture-width 1280 --rendering-size 1280 720 ] 


#### Bugs and Issues

We will track bugs and issues through the Issues interface on gitlab. Inria gitlab does not allow creation of external accounts, so if you have an issue/bug please email <code>sibr@inria.fr</code> and we will either create a guest account or create the issue on our side.

---

## Datasets

You can create your own dataset from images using the process described below ("Preprocessing to create a new dataset from your images").
We also provide already preprocessed datasets that you can run directly, as well as the datasets from the susupplemental website that require one (short) step to produce the streamed version of the data for fast loading (please see below).

**Note:** Preprocessing large datasets or datasets with large images can be slow and require a lot of memory. We recommend using images with width < 2500 resolution. For datasets with more than ~50 images, 64Gb of RAM may be required.

### Example Datasets
 
Some example datasets can be found here:
 		https://repo-sam.inria.fr/fungraph/sibr-datasets/

These datasets have been generated by colmap, and preprocessed with the current version of our tools you can find here.
These datasets have the *output.pcloud* file precomputed so you can directly use the renderer for both [Hedman 16] and [Hedman 18].
You can download the Museum-Front-27 dataset here:
```
https://repo-sam.inria.fr/fungraph/sibr-datasets/museum_front27_deep_blending_exec_only.zip
```
If you unzipped to <code>datasets\museum_front27</code> in <code>install\bin</code> you can run [Hedman 18] by going to 
```
	SIBR_inside_out_deep_blending_app_rwdi.exe --path museum_front27 --mesh museum_front27\capreal\mesh.ply --model path_to_pb_model --NCHW 
```
Several other datasets are available on the same page, as well as for other algorithms.

To avoid typing the arguments, copy the following into a *run_db.bat* file:

```
SIBR_inside_out_deep_blending_app_rwdi.exe --path %1 --model path_to_pb_model --NCHW --mesh %1\capreal\mesh.ply --rendering_size 1920 1080 --rendering-width 1920 1080
```
Where *path_to_pb* is the folder containing the *model.pb* file.
	

### Using data from the supplemental website

* For each dataset, 2 folders are provided. Download both folders from http://www-sop.inria.fr/reves/publis/2018/HPPFDB18/datasets.html
* Unzip both folders: \<dataset_name\>_IBR_Inputs_Outputs and \<dataset_name\>_Reconstruction_Inputs_Outputs
* Make sure both folders are in the same parent directory
* Make sure to build deep blending preprocess in Visual Studio in RelWithDebInfo mode.

Move to *install/scripts* and run the following python command:

	python db_legacy_dataset_to_sibr.py -r -i path/to/\<dataset_name\>_Reconstruction_Inputs_Outputs <-w 1920> <-g 64> <-b>

	-r: project compiled in RelWithDebInfo mode
	-i: path to dataset Recon Input & Output folder
	-w: (optional) parameter for maximum width on which the data should be preprocessed.
	-g: (optional) parameter to provide the grid size (default 32)
	-b: (optional) parameter to set the grid bounds oriented around the cameras to true (default false)

The script will run SIBR_recon_cmd_rwdi.exe to create the pcloud.


You should now have a output.pcloud file. You can now run the renderer.

The directory structure should look like this:

```
[ROOT]/deep_blending/output.pcloud
[ROOT]/deep_blending/depthmaps/*.bin
[ROOT]/deep_blending/pvmeshes/*.ply
	[ROOT]/deep_blending/nvm/images/*.jpg
	[ROOT]/deep_blending/nvm/scene.nvm

[ROOT]/capreal/mesh.obj (and associated texture files)
[ROOT]/capreal/mesh.ply
	
[ROOT]/colmap/stereo/run-colmap-geometric.sh
[ROOT]/colmap/stereo/run-colmap-photometric.sh
	[ROOT]/colmap/stereo/images/*.jpg
	[ROOT]/colmap/stereo/sparse/cameras.txt
	[ROOT]/colmap/stereo/sparse/images.txt
	[ROOT]/colmap/stereo/sparse/points3D.txt
		[ROOT]/colmap/stereo/stereo/depth_maps/*.jpg.photometric.bin
		[ROOT]/colmap/stereo/stereo/depth_maps/*.jpg.geometric.bin
		[ROOT]/colmap/stereo/stereo/normal_maps/*.jpg.photometric.bin
		[ROOT]/colmap/stereo/stereo/normal_maps/*.jpg.geometric.bin
		[ROOT]/colmap/stereo/stereo/consistency_graphs/*.jpg.photometric.bin
		[ROOT]/colmap/stereo/stereo/consistency_graphs/*.jpg.geometric.bin
		[ROOT]/colmap/stereo/stereo/refined_depth_maps/*.jpg.photometric.bin
		[ROOT]/colmap/stereo/stereo/refined_depth_maps/*.jpg.geometric.bin

```

---

## Preprocessing to create a new dataset from your images

For preprocessing you will need COLMAP (*https://colmap.github.io/* version 3.6) and Meshlab (*https://www.meshlab.net/*) installed (ATTN: Version 2020.07 *only*).

We first explain Deep Blending pre-processing [Hedman et al. 18], and then explain the version of InsideOut we provide here. 

In the original paper, we used COLMAP for the dense per-view depth maps, and RealityCapture for the more complete global proxy mesh. RealityCapture is based on the CMPMVS method that uses Delaunay tetrahedralization to fill in empty regions in the mesh, and is thus more complete. At the time of publication, COLMAP did not have Delaunay tetrahedralization. COLMAP now has this feature, so it is possible to create a Deep Blending dataset without RealityCapture. We provide code to use Deep Blending both with Reality Capture/COLMAP and only with COLMAP.
Make sure to build deep blending preprocess in Visual Studio in RelWithDebInfo mode (not applicable if using precompiled binaries).

Please note that the method we used in the original paper (i.e., use COLMAP calibration to create a RealityCApture MVS mesh) is no longer available in Reality Capture. Instead, we align the two meshes, giving a result very similar to the original approach for most scenes. We use *x-atlas* to compute UV coordinates; to do this we have to simplify the reconstructed mesh. Currently we default to 200,000 polygons that is not enough for large scenes. this is controllable with the *--meshsize* parameter to the commands below.

We provide an ``end-to-end'' script *db_dataset_create.py* that requires a directory *images* containing the multi-view dataset. By default script will run COLMAP, create the Delaunay mesh, and then run the per-view mesh refinement using this mesh instead of Reality Capture. With the option *--withRC* it expects the Reality Capture mesh and calibration in a directory *sfm_mvs_rc* (see section "Creating Dataset with RealityCapture" in the documentation), aligns the meshes, then also runs the preprocessing.

To run the script, create your dataset directory PATH_TO_DATASET, and put your input images in PATH_TO_DATASET/*images*, go to *install/scripts* and run:

	python db_dataset_create.py --path PATH_TO_DATASET  --colmapPath PATH_TO_COLMAP_EXECUTABLES [--meshsize (200|250|300|350|400) ]

where PATH_TO_COLMAP_EXECUTABLES is the directory containing COLMAP.bat. This will create all the data needed to run Deep Blending in the PATH_TO_DATASET/*deep_blending* folder and you can run the renderer. If the textured mesh is too low res, use a higher setting (e.g., 300 for 300,000 polygons); note that this may make preprocessing a lot slower (UVunwrap may take 1-2h).

If you have followed the steps in "Creating Dataset with RealityCapture", and placed the output in director *PATH_TO_RC* you can run the command:

	python db_dataset_create.py --path PATH_TO_DATASET  --withRC --RCPath PATH_TO_RC --colmapPath PATH_TO_COLMAP_EXECUTABLES

Note that you must follow the instructions for saving RealityCapture data strictly: i.e., the textured mesh must be saved as *textured.obj*, and the calibration *bundle.out*.

This will also create all the data needed to run Deep Blending in the *PATH_TO_DATASET/deep_blending* folder and you can run the renderer. If you previously ran COLMAP, use the option "--noColmap" to avoid running COLMAP again; the previous *deep_blending* folder will be saved in *deep_blending_saved*. 

In both cases you should have a *output.pcloud* file in *PATH_TO_DATASET/deep_blending* and you can run the renderer on the dataset.

The directory structure should look like this in both cases:
```
[ROOT]/deep_blending/output.pcloud
[ROOT]/deep_blending/depthmaps/*.bin
[ROOT]/deep_blending/pvmeshes/*.ply
	[ROOT]/deep_blending/nvm/depthmaps/*.bin
	[ROOT]/deep_blending/nvm/images/*.jpg
	[ROOT]/deep_blending/nvm/scene.nvm

[ROOT]/capreal/mesh.obj (and associated texture files)
[ROOT]/capreal/mesh.ply
[ROOT]/capreal/undistorted/*.jpg
[ROOT]/capreal/undistorted/*_P.txt

[ROOT]/colmap/database.db
[ROOT]/colmap/stereo/run-colmap-geometric.sh
	[ROOT]/colmap/stereo/run-colmap-photometric.sh
	[ROOT]/colmap/stereo/images/*.jpg
	[ROOT]/colmap/stereo/sparse/cameras.txt
	[ROOT]/colmap/stereo/sparse/images.txt
	[ROOT]/colmap/stereo/sparse/points3D.txt
		[ROOT]/colmap/stereo/stereo/depth_maps/*.jpg.photometric.bin
		[ROOT]/colmap/stereo/stereo/depth_maps/*.jpg.geometric.bin
		[ROOT]/colmap/stereo/stereo/normal_maps/*.jpg.photometric.bin
		[ROOT]/colmap/stereo/stereo/normal_maps/*.jpg.geometric.bin
		[ROOT]/colmap/stereo/stereo/consistency_graphs/*.jpg.photometric.bin
		[ROOT]/colmap/stereo/stereo/consistency_graphs/*.jpg.geometric.bin
		[ROOT]/colmap/stereo/stereo/refined_depth_maps/*.jpg.photometric.bin
		[ROOT]/colmap/stereo/stereo/refined_depth_maps/*.jpg.geometric.bin

[ROOT]/images/*.jpg
```

---

### Inside Out preprocessing

To prepare a dataset for running Inside Out, use the same process as above, but add the <code>--insideout</code> flag to db_dataset_create.py script.

### ``Under the hood'' Details

The first step in *db_dataset_create.py" calls the *fullcolmapProcess* script (see documentation).

The second step is optional, and calls the script *db_prepare_scene.py* to align the RealityCapture mesh to COLMAP.

The third step runs the per-view mesh refinement as described in the paper using the script as follows:

	python ibr_db_preprocess -r -i path/to/dataset <-w 1920> <-g 64> <-b>

	-r: project compiled in RelWithDebInfo mode
	-i: path to dataset [ROOT] folder
	-w: (optional) parameter for maximum width on which the data should be preprocessed.
	-g: (optional) parameter to provide the grid size (default 32)
	-b: (optional) parameter to set the grid bounds oriented around the cameras to true (default false)

The script will run SIBR_recon_cmd_rwdi.exe and SIBR_depthmapmesher_rwdi.exe to create the per-view meshes and the pcloud.

---

## Deep Blending training

NOTE: Everything below has not been tested recently.

For each scene to use in the training set, run (in sibr, slow):

```
	./datadump_cmd.exe path/to/dataset/ dump_flow just_colors plug_holes path_samples=3 random_samples=4

	./datadump_cmd.exe path/to/dataset/datadump/ path_samples=3 random_samples=4 fuse_flow
```

You will get a datadump/ directory, which will contain, for each input view, a series of layer renderings (you can check their validity).
Remove any .txt file present in this directory.

	
```
	train_sets/dataset_1/*.jpg,*.png
	train_sets/dataset_2/*.jpg,*.png
	...
```

Move to train_sets/ and, for each dataset run (make_data.py is in the DeepBlend repository, see /home/sirodrig/code/DeepBlend/):

```
	python make_data_txt.py dataset_1 > dataset_1_train.txt
	python make_data_txt.py dataset_1 validation > dataset_1_val.txt
```

Concat and shuffle the resulting txts:

```
	cat *_train.txt | shuf > training.txt
	cat *_val.txt | shuf > validation.txt
```

Update the job script 'train.sh' with the proper path:

```
	python blend.py /path/to/train_sets/ ./output_training/ --training-file /path/to/training.txt --validation-file /path/to/validation.txt
```

Note: in blend.py, the 'fast_network' option is for the 720p network. If set to false, the full 1080p netzork will be trained.

Run the training:
```
	train.sh
```

Once the training is done (after ~512k steps), the network is output in output_training/model/. If needed, you can then run latest_blend_conv.py to export the model.pb file for the real-time viewer (see with Julien for the proper script version, and for adding export of the 1080p network).

For offline evaluation:

* put your camera path at /path/to/dataset/testpath/path.rd.out
* run 
	./datadump_cmd.exe PATH_TO_DATSET test just_colors plug_holes clear_white
* copy testdump/ to location test_sets/dataset_1/testdump
* go to test_sets/dataset_1/
* python make_data_txt.py testdump test > test.txt
* make sure blend.py is setup in the same way as for the training you want to use
* edit eval.sh: 
	OUTPUT_DIR="./output_training/"
	DATASET_NAMES=(dataset_1)
	python ../blend.py /path/to/test_sets/${DATASET_NAMES}/ ./ --test
* run the job
	./eval.sh

The frames will be output in output_training/img_test/

---

Inria cluster notes
-------------------
Upload the datadump directories to the cluster using scp. The following hierarchy will be used on the cluster:

Run the training as a cluster job:
```
	oarsub -p "gpu='YES' and gpucapability >= '5.2'" -l /nodes=1,walltime=160:00:00 -S "./train.sh"
```
You can check the status of the job with:
```
	oarpeek JOB_ID -t 
	oarpeek JOB_ID -t -e
```

* upload testdump/ to the cluster (remove any .txt), with hierarchy test_sets/dataset_1/testdump/

* submit the job:
	oarsub -p "gpu='YES' and gpucapability >= '5.2'" -l /nodes=1/gpunum=2,walltime=1:00:00 -S "./eval.sh"
