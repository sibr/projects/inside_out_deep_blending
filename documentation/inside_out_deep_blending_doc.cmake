# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


set(PROJECT_PAGE "inside_out_deep_blendingPage")
set(PROJECT_LINK "https://gitlab.inria.fr/sibr/projects/inside_out_deep_blending")
set(PROJECT_DESCRIPTION "Deep Blending for Free-Viewpoint Image-Based Rendering, paper references: http://www-sop.inria.fr/reves/Basilic/2018/HPPFDB18/ , http://visual.cs.ucl.ac.uk/pubs/deepblending/ ; Scalable Inside-Out Image-Based Rendering, paper references: http://www-sop.inria.fr/reves/Basilic/2016/HRDB16 , http://visual.cs.ucl.ac.uk/pubs/insideout/ ")
set(PROJECT_TYPE "OURS")
