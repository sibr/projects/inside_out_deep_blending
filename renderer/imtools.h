/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef IMTOOLS_H
#define IMTOOLS_H

#include <opencv2/opencv.hpp>
#include <Eigen/Core>

namespace imtools
{

inline Eigen::Vector3f nearest_fetch(const cv::Mat& img, const Eigen::Vector2f& pt)
{
    const int x = static_cast<int>(pt.x());
    const int y = static_cast<int>(pt.y());
    cv::Vec3b bgr = img.at<cv::Vec3b>(y, x);
    return Eigen::Vector3f(bgr[2], bgr[1], bgr[0]);
}
    
// Adapted from
// http://stackoverflow.com/questions/13299409/how-to-get-the-image-pixel-at-real-locations-in-opencv
inline Eigen::Vector3f bilinear_fetch(const cv::Mat& img, const Eigen::Vector2f& pt)
{
    const int x0 = static_cast<int>(pt.x());
    const int x1 = std::min(img.cols - 1, x0 + 1);
    const int y0 = static_cast<int>(pt.y());
    const int y1 = std::min(img.rows - 1, y0 + 1);

    const float a = pt.x() - static_cast<float>(x0);
    const float c = pt.y() - static_cast<float>(y0);

    float b =
          (img.at<cv::Vec3b>(y0, x0)[0] * (1.f - a) +
	   img.at<cv::Vec3b>(y0, x1)[0] * a       ) * (1.f - c)
        + (img.at<cv::Vec3b>(y1, x0)[0] * (1.f - a) +
	   img.at<cv::Vec3b>(y1, x1)[0] * a       ) * c;
    float g =
          (img.at<cv::Vec3b>(y0, x0)[1] * (1.f - a) +
	   img.at<cv::Vec3b>(y0, x1)[1] * a       ) * (1.f - c)
        + (img.at<cv::Vec3b>(y1, x0)[1] * (1.f - a) +
	   img.at<cv::Vec3b>(y1, x1)[1] * a       ) * c;
    float r =
          (img.at<cv::Vec3b>(y0, x0)[2] * (1.f - a) +
	   img.at<cv::Vec3b>(y0, x1)[2] * a       ) * (1.f - c)
        + (img.at<cv::Vec3b>(y1, x0)[2] * (1.f - a) +
	   img.at<cv::Vec3b>(y1, x1)[2] * a       ) * c;

    return Eigen::Vector3f(r, g, b);
}

inline Eigen::Vector3f unproject(float x, float y, float depth,
				 const Eigen::Matrix3f &inv_intrinsics)
{
    Eigen::Vector3f img_pos = Eigen::Vector3f(x, y, 1.0f);
    return depth * (inv_intrinsics * img_pos);
}

inline Eigen::Vector3f unproject(float x, float y, float depth,
				 const Eigen::Matrix3f &inv_intrinsics,
				 const Eigen::Matrix4f &cam_to_world)
{
    Eigen::Vector3f cam_pos   = unproject(x, y, depth, inv_intrinsics);
    Eigen::Vector4f world_pos = cam_to_world * cam_pos.homogeneous();
    return Eigen::Vector3f(world_pos.x(), world_pos.y(), world_pos.z());
}

} // imtools

#endif // IMTOOLS_H
