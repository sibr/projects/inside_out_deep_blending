/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "DeepBlendingScene.hpp"
#include <projects/fribr_framework/renderer/3dmath/math_tools.h>
#include "projects/inside_out_deep_blending/renderer/recon_tools.h"
#include "projects/tfgl_interop/renderer/tfgl_interop.h"

namespace sibr {


	void DeepBlendingScene::init()
	{
		_show_mesh = _myArgs.show_mesh;
		_show_textures = _myArgs.show_textures;
		_show_patches = _myArgs.show_patches;
		set_grid_size(_myArgs.grid_size);
		_show_boxes = _myArgs.show_boxes;
		_show_wireframe = _myArgs.show_wireframe;
		_use_tf_blending = false;

		if((_myArgs.algorithm.get() != "inside_out") && (_myArgs.algorithm.get() != "deep_blending")) {
			SIBR_ERR << "Unsupported algorithm '" << _myArgs.algorithm.get() << "'." << std::endl;
		}

		
		//// [SP] Use _data instead of exclusively asking for mesh_path
		if(!sibr::fileExists(_myArgs.mesh_path.get())) {
			if(!sibr::fileExists(_myArgs.dataset_path.get() + "/" +_myArgs.mesh_path.get()))
				SIBR_LOG << "Could not find mesh. Please specify --mesh argument." << std::endl;

			_myArgs.mesh_path = _myArgs.dataset_path.get() + "/" +_myArgs.mesh_path.get();
		}

		SIBR_LOG << "Loading FRIBR scene!" << std::endl;
		fribr_scene.reset(new fribr::Scene(_myArgs.mesh_path, fribr::RH_Y_UP));
		SIBR_LOG << "FRIBR scene loaded successfully!!!!!" << std::endl;

		// Create patch cloud and setup ray tracer for the scene
		cloud.set_scene(fribr_scene);
		ray_tracer.set_scene(fribr_scene);

		
		// This is used to get the mesh for the TopView in case the scene does not have a sibr::Mesh
		if (_data->datasetType() == ParseData::Type::EMPTY) {
			sibr::Mesh::Ptr proxy(new sibr::Mesh(true));
			proxy->load(_myArgs.mesh_path);
			_proxies.reset(new ProxyMesh());
			_proxies->replaceProxy(proxy);
		}

		// Read the patch cloud data structure
		std::string pcloud_path = _myArgs.dataset_path.get() + "/" + _myArgs.algorithm.get() + "/output.pcloud";
		if(_myArgs.cloud_path.get() != "") {
			SIBR_LOG << "No patch cloud found at '" << pcloud_path << "'. trying another path..." << std::endl;
			pcloud_path = _myArgs.dataset_path.get() + "/" + _myArgs.cloud_path.get();
			if(!sibr::fileExists(pcloud_path)) {
				pcloud_path = _myArgs.dataset_path.get() + "/" + _myArgs.algorithm.get() + "/" + _myArgs.cloud_path.get();

				if(!sibr::fileExists(pcloud_path))
					SIBR_LOG << "No patch cloud found at '" << pcloud_path << "'. Please specify a correct pcloud path or use the default one (strip the pcloud argument)." << std::endl;
			}
		}

		if (sibr::fileExists(pcloud_path)) {
			SIBR_LOG << "Loading patch cloud!!!" << std::endl;
			cloud.load(pcloud_path);
			_grid_size = cloud.grid_size().x();
			SIBR_LOG << "Patch cloud loaded successfully from "<< pcloud_path << "!!!!!" << std::endl;
			_show_patches = true;
			_show_textures = true;
		}
		else {
			SIBR_ERR << "No patch cloud available to load! Please perform pre-processing!" << std::endl;
		}

		patcher::PatchCloud::DisplayMode to_display_mode[] = {
			patcher::PatchCloud::Colors,
			patcher::PatchCloud::Depth,
			patcher::PatchCloud::Normals,
			patcher::PatchCloud::OutputDir,
			patcher::PatchCloud::InputDir
		};


		_display_layer = _myArgs.display_layer;
		_cluster_k = _myArgs.cluster_thresh;

		cloud.set_display_mode(to_display_mode[display_mode]);
		cloud.set_display_layer(_display_layer);
		cloud.cluster_threshold(_cluster_k);

		if(_myArgs.algorithm.get() == "deep_blending") {
			cloud.shader_builder().use_insideout_multipass(true);
			cloud.shader_builder().depth_cost_index(0);
			cloud.shader_builder().blend_cost_index(0);
		} else {
			cloud.shader_builder().use_insideout_multipass(true);
			cloud.shader_builder().depth_cost_index(1);
			cloud.shader_builder().blend_cost_index(0);
		}

		cloud.disable_fake_camera_position();

#if defined(TF_INTEROP)
		if(_myArgs.algorithm.get() == "deep_blending") {
			_NCHW = _myArgs.nchw;
			_logging_enabled = _myArgs.log;
			
			_texWidth = 1280; _texHeight = 720;
			cloud.setTextRes(Eigen::Vector2i(_texWidth, _texHeight));
			SIBR_LOG << "[DBScene] Setting network texture resolution to: (" << _texWidth << "," << _texHeight << ")" << std::endl;


			////Setup tensorflow architecture
			_input_graph_node_names = { "input1" };
			_input_texture_mapping.push_back({ 0, 1, 2, 3, 4 });
			_input_texture_channels_vec.push_back({3, 3, 3, 3, 3});
			_graph_file_name = _myArgs.model_path.get();
			//model argument accepts both a folder and the exact model file.
			if (sibr::directoryExists(_myArgs.model_path.get())) {
				_graph_file_name += "/model.pb";
			}

			if (!sibr::fileExists(_graph_file_name)) {
				SIBR_ERR << "Could not find model file in: " << _graph_file_name << std::endl;
			}

			_output_node_names = { "model/Sum" };

			cloud.set_tf_interop(std::make_shared<sibr::TF_GL>(
				_graph_file_name,
				cloud.get_input_textures(),
				_input_texture_mapping,
				_input_texture_channels_vec,
				_input_graph_node_names,
				cloud.get_output_textures(),
				_output_node_names,
				_window,
				_NCHW,
				_logging_enabled));
		}
#endif

	}

	DeepBlendingScene::DeepBlendingScene(DeepBlendingAppArgs & myArgs, sibr::Window& window): BasicIBRScene(myArgs, true), 
		_window(&window), 
		_myArgs(myArgs)
	{
		init();
	}

	DeepBlendingScene::DeepBlendingScene(const BasicIBRScene::Ptr & scene, sibr::Window & window, DeepBlendingAppArgs & myArgs): BasicIBRScene(*scene), 
		_window(&window),
		_myArgs(myArgs)
	{
		init();
	}

}