# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


include(sibr_library)

###################
## Find OpenMesh
###################
sibr_addlibrary(
    NAME OpenMeshCore
    MSVC11 "https://repo-sam.inria.fr/fungraph/dependencies/sibr/~0.9/OpenMesh-8.0.7z"
    MSVC14 "https://repo-sam.inria.fr/fungraph/dependencies/sibr/~0.9/OpenMesh-8.0.7z"     # TODO SV: provide a valid version if required
    SET CHECK_CACHED_VAR OpenMesh_DIR PATH "openmesh 8.0"
)

sibr_gitlibrary(TARGET densecrf
	GIT_REPOSITORY 	"https://gitlab.inria.fr/sibr/libs/densecrf.git"
	GIT_TAG			"3b7cf910c3736742dafa0deff2b7b0753bd9d9d8"
)

sibr_gitlibrary(TARGET MixKitNext
	GIT_REPOSITORY 	"https://gitlab.inria.fr/sibr/libs/mixkit.git"
	GIT_TAG			"745e04f774f2a75be3e3afcc62f865ec74e886da"
	SOURCE_DIR 		"mixkit"
)

