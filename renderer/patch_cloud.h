/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef PATCHCLOUD_H
#define PATCHCLOUD_H

#include <core/graphics/Texture.hpp>
#include "projects/fribr_framework/renderer/3d.h"
#include "projects/fribr_framework/renderer/vision.h"
#include "projects/fribr_framework/renderer/gl_wrappers.h"
#include "projects/fribr_framework/renderer/ray_tracing.h"
#include "projects/inside_out_deep_blending/renderer/voxel_grid.h"
#include "projects/inside_out_deep_blending/renderer/blend_shader_builder.h"

#include <opencv2/opencv.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <fstream>
#include <unordered_map>

namespace sibr {
	class TF_GL;
	struct TexInfo;
}

namespace patcher
{


	class SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT PatchCloud
	{
		// Vertex shader attribute locations
		static const int VERTEX_LOCATION = 0;
		static const int TEXCOORD_LOCATION = 1;

		struct Component
		{
			typedef std::shared_ptr<Component> Ptr;
			VoxelGrid                          *grid;

			cv::Mat                             image;
			std::vector<fribr::float3>          vertices;
			std::vector<fribr::float3>          texcoords;
			std::unordered_map<int, int>        voxel_to_index_remap;
			std::vector<std::vector<uint32_t> > slice_indices;
			std::vector<Eigen::Vector3i>        slice_voxels;
			std::vector<bool>                   slice_valid;

			Eigen::Vector2i                     resolution;
			Eigen::Vector3f                     position;

			size_t                              vertices_start;
			std::vector<size_t>                 slices_start;

			Component(VoxelGrid *_grid);
			~Component();

			void add_camera(const fribr::float3    &camera_position,
				const cv::Mat          &camera_image,
				fribr::Mesh::Ptr       mesh,
				fribr::CalibratedImage &calibrated_camera,
				const size_t           component_index);

			void add_camera(const fribr::float3              &camera_position,
				const cv::Mat                    &camera_image,
				const std::vector<fribr::float3> &points,
				fribr::CalibratedImage           &calibrated_camera,
				const int                        compression_factor,
				const size_t                     component_index);

			bool   is_valid(Eigen::Vector3i voxel) const;
			size_t draw(Eigen::Vector3i voxel) const;
			size_t draw() const;


			void save(std::ofstream &out_file) const;
			void load(std::ifstream &in_file);
		};

		struct ImageBin {
			Eigen::Vector3i voxel;
			Eigen::Array3f  bbox_min;
			Eigen::Array3f  bbox_max;

			ImageBin(int bin_x, int bin_y, int bin_z, const VoxelGrid &grid);
		};
		std::vector<ImageBin> fetch_image_bins(const Eigen::Affine3f world_to_camera,
			const Eigen::Matrix4f camera_to_clip);

	public:
		PatchCloud();
		PatchCloud(Eigen::Vector3i _grid_size, const std::string & algorithm = "deep_blending");
		~PatchCloud();

#if defined(TF_INTEROP)
		void set_tf_interop(std::shared_ptr<sibr::TF_GL> tf_interop)
		{
			m_tf_interop = tf_interop;
		}
		void use_tf_blending(bool tf_blending)
		{
			m_use_tf_blending = tf_blending;
		}
#endif

		void cluster_k(int k)
		{
			m_cluster_k = std::max(12, k);
			std::cout << "Updating cluster k [max(12, input)] to: " << m_cluster_k << std::endl;
		}

		void cluster_threshold(int k)
		{
			m_cluster_threshold = k;
			//std::cout << "Updating cluster threshold to: " << m_cluster_threshold << std::endl;
			m_cluster_k = std::max(12, 4 + m_cluster_threshold);
			//std::cout << "Updating cluster k to: " << m_cluster_k << std::endl;
		}

		void grid_size(Eigen::Vector3i size)
		{
			bool size_changed = size != m_grid.size();
			m_grid.resize(size);
			if (size_changed)
				clear();
		}

		Eigen::Vector3i grid_size() const
		{
			return m_grid.size();
		}

		static const int VOXEL_MEMORY_SIZE = 12;
		void update_voxel_memory(bool b)
		{
			m_update_voxel_memory = b;
		}
		void clear_voxel_memory()
		{
			for (int i = 0; i < VOXEL_MEMORY_SIZE; ++i)
				m_voxel_memory[i].clear();
		}

		static const int NUM_LAYERS = 4;
		void set_display_layer(int display_layer)
		{
			m_display_layer = display_layer;
		}

		enum DisplayMode { Colors = 0, Depth = 1, Normals = 2, OutputDir = 3, InputDir = 4, Flow = 5 };
		void set_display_mode(DisplayMode mode)
		{
			m_display_mode = mode;
		}

		void ignore_this_camera(int camera_index)
		{
			m_ignored_camera = camera_index;
		}

		void set_selected_camera(int selected_camera)
		{
			m_selected_camera = selected_camera;
		}

		void set_max_tiles(int max_tiles)
		{
			m_max_tiles = max_tiles;
		}

		// Just expose the entire shader builder to the UI. Later on, we may want to encapsulate this better...
		BlendShaderBuilder& shader_builder() { return m_shader_builder; }

		enum MemoryPolicy { KeepResourcesAlive, DeallocateWhenDone };
		void add_cameras(fribr::CalibratedImage::Vector             &cameras,
			std::vector<std::vector<Eigen::Vector3f> > &depth_maps,
			int compression_factor, MemoryPolicy policy = KeepResourcesAlive);

		void add_cameras(fribr::CalibratedImage::Vector &cameras,
			std::vector<fribr::Mesh::Ptr > &meshes,
			MemoryPolicy policy = KeepResourcesAlive);

		void add_camera(fribr::CalibratedImage& camera,
			const std::vector<Eigen::Vector3f> &positions,
			int compression_factor);

		void upload();
		void clear();
		void clear_gpu();

		void set_fake_camera_position(const Eigen::Vector3f position)
		{
			m_use_fake_position = true;
			m_fake_camera_position = position;
		}
		void disable_fake_camera_position()
		{
			m_use_fake_position = false;
		}

		Eigen::Matrix4f get_old_world_to_camera() const
		{
			return m_old_world_to_camera;
		}
		void set_old_world_to_camera(const Eigen::Matrix4f &old_world_to_camera)
		{
			m_old_world_to_camera = old_world_to_camera;
		}

		void draw(float near_plane, float far_plane,
			float ibr_sigma, float resolution_alpha, float depth_alpha,
			const Eigen::Affine3f world_to_camera,
			const Eigen::Matrix4f camera_to_clip,
			bool show_wireframe);
		void draw_boxes(const Eigen::Affine3f world_to_camera,
			const Eigen::Matrix4f camera_to_clip);
		void set_scene(fribr::Scene::Ptr scene);
		void set_scene(fribr::Scene::Ptr scene, Eigen::Array3f grid_min, Eigen::Array3f grid_max);

		void create_boxes(const Eigen::Affine3f world_to_camera,
			const Eigen::Matrix4f camera_to_clip);

		void save(const std::string &filename) const;
		bool load(const std::string &filename);

		bool _clearDst = true;
		void						   setTextRes(Eigen::Vector2i res);


		Eigen::Vector3f grid_min() const
		{ 
			return m_grid.min().matrix();
		}

		Eigen::Vector3f grid_max() const
		{
			return m_grid.max().matrix();
		}

#if defined(TF_INTEROP)
			 std::vector<std::shared_ptr<sibr::TexInfo>> get_input_textures()
			{
				return _input_textures;
			}

			  std::vector<sibr::Texture2DRGBA32F::Ptr> get_output_textures()
			{
				return _output_textures;
			}


#endif

	private:
		// Deliberately left unimplemented, PatchCloud is noncopyable
		PatchCloud(const PatchCloud&);
		PatchCloud& operator=(const PatchCloud&);

		std::string                    m_algorithm;
		// Tensorflow interop, only venture here if you're brave.
		bool                           m_use_tf_blending;
#if defined(TF_INTEROP)

		std::shared_ptr<sibr::TF_GL>			   m_tf_interop;
		fribr::Framebuffer::Ptr        m_global_fbo;
		//sibr::Texture2DRGBA32F::Ptr _inputImageTex;
		std::vector<std::shared_ptr<sibr::TexInfo>>		_input_textures;
		std::vector<sibr::Texture2DRGBA32F::Ptr>	_output_textures;
		std::vector<std::string>		_run_node_names;
		//fribr::Framebuffer::Ptr        m_tf_layers[NUM_LAYERS + 1];
		std::vector<sibr::RenderTargetRGBA32F::Ptr>		m_tf_layers;
		static fribr::Shader          *s_tf_blit_shader;
#endif

		static int                     s_num_instances;

		// Shaders for fusing the output.
		static fribr::Shader          *s_dump_shader;
		static fribr::Shader          *s_average_shader;

		// Shaders for the grid acceleration structure.
		static fribr::Shader          *s_scene_geometry_shader;
		static fribr::Shader          *s_patch_depth_shader;
		static fribr::Shader          *s_box_shader;

		fribr::Shader                 *m_depth_bin_shader;
		fribr::Shader                 *m_clear_bin_shader;
		fribr::Shader                 *m_loop_check_shader;

		// We use a shader builder to support different permutations
		// of blend shaders.
		BlendShaderBuilder             m_shader_builder;

		int                            m_display_layer;
		DisplayMode                    m_display_mode;
		int                            m_cluster_k;
		int                            m_cluster_threshold;
		std::vector<Component::Ptr>    m_components;
		size_t                         m_all_triangles;
		VoxelGrid                      m_grid;
		std::vector<fribr::Mesh::Ptr>  m_meshes;
		fribr::Scene::Ptr              m_scene;

		// Give the voxel grid a finite memory, to avoid flickering
		bool                           m_update_voxel_memory;
		std::vector<bool>              m_voxel_memory[VOXEL_MEMORY_SIZE];

		// State for debug visualizations
		int                            m_selected_camera;
		int                            m_ignored_camera;
		int                            m_max_tiles;

		// Allow for a different virtual viewpoint (for the purpose of changing IBR decisions)
		bool                           m_use_fake_position;
		Eigen::Vector3f                m_fake_camera_position;

		// Only for data dumping (used to compute a flow field)
		Eigen::Matrix4f                m_old_world_to_camera;

		// State for rendering
		GLuint                         m_texture_array;
		GLuint                         m_camera_pos_texture;
		GLuint                         m_draw_calls;
		GLuint                         m_vao;
		GLuint                         m_index_buffer;
		GLuint                         m_vertex_buffer;
		GLuint                         m_texcoord_buffer;

		GLuint                         m_gpu_grid[4];
		std::vector<float>             m_all_box_meshes;
		std::vector<float>             m_current_box_meshes;
		void upload_texture_image_if_needed();

		fribr::Framebuffer::Ptr        m_voxel_fbo;
		fribr::Framebuffer::Ptr        m_depth_fbo;
		fribr::Framebuffer::Ptr        m_layers[NUM_LAYERS];
		void update_fbos_if_needed();


	};

}

#endif
