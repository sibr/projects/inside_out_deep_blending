/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


// Adapted from:
// http://userpages.uni-koblenz.de/~fneuhaus/wp/code-snippets/
// https://github.com/cfneuhaus/snippets/blob/master/SimpleRansac.h
// https://github.com/cfneuhaus/snippets/blob/master/PlaneModel.h

#ifndef SIMPLERANSAC_H
#define SIMPLERANSAC_H

#include <Eigen/Core>
#include <Eigen/Eigenvalues>

#include <vector>
#include <array>
#include <random>
#include <set>

namespace RANSAC
{

struct PlaneModel
{
    static const int ModelSize=3;

    Eigen::Vector3f n;
    float d;
    int i;

    void compute(const std::vector<Eigen::Vector3f>& data, const std::array<size_t,3>& indices)
    {
        Eigen::Vector3f a=data[indices[1]]-data[indices[0]];
        Eigen::Vector3f b=data[indices[2]]-data[indices[0]];
        n=a.cross(b).normalized();
        d=n.dot(data[indices[0]]);
    }
    int computeInliers(const std::vector<Eigen::Vector3f>& data, float threshold)
    {
        int inliers=0;
        for (size_t i=0;i<data.size();i++)
        {
            if (fabs(n.dot(data[i])-d)<threshold)
                inliers++;
        }
        return inliers;
    }
    void refine(const std::vector<Eigen::Vector3f>& data, float threshold)
    {
        Eigen::Vector3f Ex=Eigen::Vector3f::Zero();
        Eigen::Matrix3f Exsqr=Eigen::Matrix3f::Zero();
        int inliers=0;
        for (size_t i=0;i<data.size();i++)
        {
            if (fabs(n.dot(data[i])-d)<threshold)
            {
                inliers++;
                Ex+=data[i];
                Exsqr+=data[i]*data[i].transpose();
            }
        }
        Ex/=float(inliers);
        Exsqr/=float(inliers);

        Eigen::Matrix3f cov=Exsqr-Ex*Ex.transpose();

        Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eig(cov);
        n=eig.eigenvectors().col(0);
        d=n.dot(Ex);

        i = inliers;
    }
};

template<typename Model_, typename DataType>
Model_ fit(const std::vector<DataType>& data, float threshold, int numIterations, unsigned int randomSeed=std::random_device{}())
{
    if (data.size()<Model_::ModelSize)
        throw std::runtime_error("Not enough data");

    std::mt19937 engine{randomSeed};
    std::uniform_int_distribution<size_t> dis{0,data.size()-1};

    int bestInliers=-1;
    Model_ bestModel;

    for (int it=0;it<numIterations;it++)
    {
        // select points
        int found=0;
        std::array<size_t,Model_::ModelSize> indices;
        std::set<size_t> usedIndices;
        while (found < Model_::ModelSize)
        {
            size_t sample=dis(engine);
            if (usedIndices.find(sample)!=usedIndices.end())
                continue;
            usedIndices.insert(sample);
            indices[found++]=sample;
        }

        // compute model
        Model_ m;
        m.compute(data,indices);

        int inliers=m.computeInliers(data,threshold);
        if (inliers>bestInliers)
        {
            bestModel=m;
            bestInliers=inliers;
        }
    }
    bestModel.refine(data,threshold);
    return bestModel;
}

}

#endif
