/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "blend_shader_builder.h"

#include "projects/fribr_framework/renderer/tools/gl_tools.h"
#include <memory>
#include <sstream>

using namespace fribr;

namespace patcher
{

BlendShaderBuilder::BlendShaderBuilder()
{
    //
    // Depth costs
    //

    add_depth_cost("depth_cost", GLSL_RAW(
     float compute_depth_cost(float current_depth, float closest_depth)
     {
         float min_depth = closest_depth;
         float max_depth = 2.0f * closest_depth;
         return clamp((current_depth - min_depth) / (max_depth - min_depth), 0.0f, 1.0f);
     }));

    add_depth_cost("fuzzy_test", GLSL_RAW(
     float compute_depth_cost(float current_depth, float closest_depth)
     {
         float depth_threshold = 1.1 * closest_depth;
	 return current_depth > depth_threshold ? -1.0 : 0.0;
     }));

    add_depth_cost("no_test", GLSL_RAW(
     float compute_depth_cost(float current_depth, float closest_depth)
     {
	 return 0.0;
     }));

    //
    // Blend costs
    //

    add_blend_cost("ulr", GLSL_RAW(
     float compute_blend_cost(vec3 world_space_position,
			      vec3 world_space_normal,
			      vec3 output_camera_position,
			      vec3 input_camera_position)
     {
         float view_angle     = 1.0 / 3.145926536
                              * acos(clamp(dot(normalize(output_camera_position - world_space_position),
                                               normalize(input_camera_position  - world_space_position)), -0.99, 0.99));
         float distance_delta = 1.0 / max(0.01, length(input_camera_position - world_space_position))
                              * max(0.0, length(input_camera_position  - world_space_position) -
				         length(output_camera_position - world_space_position));
         return (1.0 - resolution_alpha) * view_angle + resolution_alpha * distance_delta;
     }));

    add_blend_cost("none", GLSL_RAW(
     float compute_blend_cost(vec3 world_space_position,
			      vec3 world_space_normal,
			      vec3 output_camera_position,
			      vec3 input_camera_position)
     {
	 return 0.0;
     }));
}

void BlendShaderBuilder::rebuild_shaders_if_needed()
{
    if (!m_dirty)
	return;

    static const std::string shared_vertex_shader = GLSL(330,
     layout(location = 0) in vec3 vertex_position;
     layout(location = 1) in vec3 vertex_texcoord;

     uniform mat4 camera_to_clip;
     uniform mat4 world_to_camera;

     out vec2  screen_space_position;
     out vec3  world_space_position;
     out float depth;
     out vec3  texture_coordinates;

     void main()
     {
         vec4 position_cam  = world_to_camera * vec4(vertex_position, 1.0);
         vec4 position_clip = camera_to_clip * position_cam;

         world_space_position  = vertex_position;
	 texture_coordinates   = vertex_texcoord;
         depth                 = -position_cam.z;
         screen_space_position = (vec2(1.0, 1.0) + (position_clip.xy / position_clip.w)) * 0.5;

	 gl_Position = position_clip;
     }
    );

    m_peeling_shader.reset(new fribr::Shader());
    m_peeling_shader->vertex_shader(shared_vertex_shader);
    m_peeling_shader->fragment_shader(
    GLSL(330,
     in  vec2  screen_space_position;
     in  vec3  world_space_position;
     in  float depth;
     in  vec3  texture_coordinates;
     out vec4  frag_color;

     uniform float          sigma;
     uniform float          depth_alpha;
     uniform float          resolution_alpha;

     uniform int            num_cameras;
     uniform int            display_mode;
     uniform vec3           output_camera_position;

     uniform mat4           old_world_to_camera;
     uniform mat4           camera_to_clip;
     uniform vec2           resolution;

     uniform sampler2DArray diffuse_sampler;
     uniform sampler2D      previous_sampler;
     uniform sampler2D      depth_sampler;
     uniform sampler1D      camera_sampler;

    ) + m_depth_cost_functions[m_depth_cost_index]
      + m_blend_cost_functions[m_blend_cost_index]
      + GLSL_RAW(

     void main()
     {
         vec4 pixel_color = texture(diffuse_sampler, texture_coordinates);
         if (pixel_color.a < 0.5)
             discard;

	 float current_depth = depth;
	 float closest_depth = texture(depth_sampler, screen_space_position).x;
	 float depth_cost    = compute_depth_cost(current_depth, closest_depth);
	 if (depth_cost < 0.0)
	     discard;

	 vec3  world_space_normal    = normalize(cross(dFdx(world_space_position),
						       dFdy(world_space_position)));
	 vec3  input_camera_position = texture(camera_sampler, (0.5f + texture_coordinates.z) / num_cameras).xyz;
	 float blend_cost            = compute_blend_cost(world_space_position,
							  world_space_normal,
							  output_camera_position,
							  input_camera_position);
	 float cost = depth_alpha * depth_cost + (1.0f - depth_alpha) * blend_cost;

	 // Default assumption is display_mode == 0 for Colors
	 if (display_mode == 1) // Depth
	 {
	     float clamped_depth = max(0.25, depth);
	     float disparity = 1.0f / (4.0f * clamped_depth);
	     pixel_color.rgb = vec3(disparity, 0.0, 0.0);
	 }
	 else if (display_mode == 2) // Normals
	 {
	     pixel_color.rgb = 0.5f * (1.0f + world_space_normal);
	 }
	 else if (display_mode == 3) // Output dir
	 {
	     vec3 output_dir = normalize(output_camera_position - world_space_position);
	     pixel_color.rgb = 0.5f * (1.0f + output_dir);
	 }
	 else if (display_mode == 4) // Input dir
	 {
	     vec3 input_dir = normalize(input_camera_position - world_space_position);
	     pixel_color.rgb = 0.5f * (1.0f + input_dir);
	 }
	 else if (display_mode == 5) // Flow
	 {
	     vec4 old_position_clip         = camera_to_clip * old_world_to_camera * vec4(world_space_position, 1.0);
	     vec2 old_screen_space_position = (vec2(1.0, 1.0) + (old_position_clip.xy / old_position_clip.w)) * 0.5;
	     old_screen_space_position *= resolution;

	     vec2 flow_to_old = clamp(old_screen_space_position - screen_space_position * resolution,
				      vec2(-512.0, -512.0), vec2(512.0, 512.0));
	     flow_to_old /= 512.0f;
	     flow_to_old = 0.5f * (flow_to_old + 1.0f);
	     pixel_color.rgb = vec3(flow_to_old, 0.5f);
	 }


	 vec4 previous_color = texture(previous_sampler, screen_space_position);
	 if (previous_color.a > 0.0 && cost <= previous_color.a)
             discard;
	 pixel_color.a = cost;

	 frag_color = pixel_color;
	 gl_FragDepth = cost;
     }
    ));
    m_peeling_shader->link();

    const std::string cost_to_weight_function = m_use_insideout_multipass
	? GLSL_RAW(

     float cost_to_weight(float cost, float prev_cost)
     {
         float weight = 1.0;
	 if (prev_cost > 0.0)
             weight = exp(-cost / (sigma * prev_cost));
	 return weight;
     }

    )   : GLSL_RAW(

     float cost_to_weight(float cost, float prev_cost)
     {
	 return exp(-cost / sigma);
     }

    );

    m_blend_shader.reset(new fribr::Shader());
    m_blend_shader->vertex_shader(shared_vertex_shader);
    m_blend_shader->fragment_shader(
    GLSL(330,
     in  vec2  screen_space_position;
     in  vec3  world_space_position;
     in  float depth;
     in  vec3  texture_coordinates;
     out vec4  frag_color;

     uniform float          sigma;
     uniform float          depth_alpha;
     uniform float          resolution_alpha;

     uniform int            num_cameras;
     uniform vec3           output_camera_position;

     uniform sampler2DArray diffuse_sampler;
     uniform sampler2D      previous_sampler;
     uniform sampler2D      depth_sampler;
     uniform sampler1D      camera_sampler;

    ) + m_depth_cost_functions[m_depth_cost_index]
      + m_blend_cost_functions[m_blend_cost_index]
      + cost_to_weight_function
      + GLSL_RAW(

     void main()
     {
         vec4 pixel_color = texture(diffuse_sampler, texture_coordinates);
         if (pixel_color.a < 0.5)
             discard;

	 float current_depth = depth;
	 float closest_depth = texture(depth_sampler, screen_space_position).x;
	 float depth_cost    = compute_depth_cost(current_depth, closest_depth);
	 if (depth_cost < 0.0)
	     discard;

	 vec3  world_space_normal    = normalize(cross(dFdx(world_space_position),
						       dFdy(world_space_position)));
	 vec3  input_camera_position = texture(camera_sampler, (0.5f + texture_coordinates.z) / num_cameras).xyz;
	 float blend_cost            = compute_blend_cost(world_space_position,
							  world_space_normal,
							  output_camera_position,
							  input_camera_position);

	 float cost      = depth_alpha * depth_cost + (1.0f - depth_alpha) * blend_cost;
         float prev_cost = texture(previous_sampler, screen_space_position).a;
	 float weight    = cost_to_weight(cost, prev_cost);

         pixel_color.a = weight;
         pixel_color.xyz *= pixel_color.a;

	 frag_color = pixel_color;
	 gl_FragDepth = cost;
     }
    ));
    m_blend_shader->link();

    m_dirty = false;
}

Shader::Ptr BlendShaderBuilder::peeling_shader()
{
    rebuild_shaders_if_needed();
    return m_peeling_shader;
}

Shader::Ptr BlendShaderBuilder::blend_shader()
{
    rebuild_shaders_if_needed();
    return m_blend_shader;
}
    
} // patcher
