/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef VOXELGRID_H
#define VOXELGRID_H

#include "projects/fribr_framework/renderer/3d.h"
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <functional>
#include "Config.hpp"

namespace patcher
{

class SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT VoxelGrid
{
public:
    VoxelGrid(Eigen::Vector3i _size);

    void set_scene(fribr::Scene::Ptr scene);
    void resize(Eigen::Vector3i _size);
    void clear();
    void reset(Eigen::Array3f _min, Eigen::Array3f _max, Eigen::Vector3i _size);

	void min(Eigen::Array3f _min) { m_min = _min; }
	void max(Eigen::Array3f _max) { m_max = _max; }

    Eigen::Array3f  min()  const { return m_min;  }
    Eigen::Array3f  max()  const { return m_max;  } 
    Eigen::Vector3i size() const { return m_size; }

    size_t linear_index(Eigen::Vector3i voxel) const
    {
        return voxel.x() + m_size.x() * (voxel.y() + m_size.y() * voxel.z());
    }

    Eigen::Vector3f voxel_to_world(Eigen::Vector3f v) const
    {
        Eigen::Array3f fractional = v.array() / m_size.array().cast<float>();
        return (m_min + fractional * (m_max - m_min)).matrix();
    }    

    Eigen::Vector3i world_to_voxel(Eigen::Vector3f v) const
    {
        Eigen::Array3f fractional = (v.array() - m_min) / (m_max - m_min);
        fractional = fractional.min(Eigen::Array3f(1.0f, 1.0f, 1.0f))
                               .max(Eigen::Array3f(0.0f, 0.0f, 0.0f));
        return (fractional * m_size.cast<float>().array()).matrix().cast<int>();
    }

    std::vector<int> &get_components(Eigen::Vector3i voxel)
    {
        return m_grid[linear_index(voxel)];
    }

    void add_component(Eigen::Vector3i voxel, int component_index)
    {
        m_grid[linear_index(voxel)].push_back(component_index);
    }

    void rasterize_triangle(Eigen::Vector3f a, Eigen::Vector3f b, Eigen::Vector3f c,
                            std::function<void(Eigen::Vector3i)> function) const;

private:
    Eigen::Array3f                 m_min;
    Eigen::Array3f                 m_max;
    Eigen::Vector3i                m_size;
    std::vector<std::vector<int> > m_grid;
};

} // patcher

#endif // VOXELGRID_H
