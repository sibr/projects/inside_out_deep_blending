/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "patch_cloud.h"
#include "GL/glu.h"

#include "projects/fribr_framework/renderer/tools/file_tools.h"
#include "projects/fribr_framework/renderer/tools/gl_tools.h"
#include "projects/fribr_framework/renderer/tools/image_tools.h"
#include "projects/fribr_framework/renderer/tools/sized_priority_queue.h"
#include "projects/fribr_framework/renderer/tools/profiling.h"
#include "projects/fribr_framework/renderer/tools/geometry_tools.h"
#include "projects/inside_out_deep_blending/renderer/simple_ransac.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic warning "-fpermissive"

#include <MixKit/stdmix.h>
#include "MixKit/MxStdModel.h"
#include "MixKit/MxQSlim.h"

#pragma GCC diagnostic pop



#if defined(TF_INTEROP)
#include "projects/tfgl_interop/renderer/tfgl_interop.h"
#endif

#include <cmath>
#include <fstream>
#include <memory>
#include <sstream>
#include <unordered_map>
#include <unordered_set>

#define DO_PROFILING 0
#ifdef INRIA_WIN
size_t doAtomicOp(size_t* progress, int val);
#endif

namespace
{

	fribr::Shader* create_scene_geometry_shader()
	{
		fribr::Shader* shader = new fribr::Shader();
		shader->vertex_shader(
			GLSL(330,
				layout(location = 0) in vec3 vertex_position;

		uniform mat4 camera_to_clip;
		uniform mat4 world_to_camera;

		out vec3 position;

		void main()
		{
			position = vertex_position.xyz;
			gl_Position = camera_to_clip * world_to_camera * vec4(position, 1.0);
		}
		));
		shader->fragment_shader(
			GLSL(330,
				in  vec3 position;
		out vec4 frag_color;

		void main()
		{
			frag_color = vec4(position, 1.0f);
		}
		));
		shader->link();

		return shader;
	}

	fribr::Shader* create_patch_depth_shader()
	{
		fribr::Shader* shader = new fribr::Shader();
		shader->vertex_shader(
			GLSL(330,
				layout(location = 0) in vec3 vertex_position;
		layout(location = 1) in vec2 vertex_texcoord;

		uniform mat4 camera_to_clip;
		uniform mat4 world_to_camera;

		out vec3 position;

		void main()
		{
			position = (world_to_camera * vec4(vertex_position, 1.0)).xyz;
			gl_Position = camera_to_clip * vec4(position, 1.0);
		}
		));

		shader->fragment_shader(
			GLSL(330,
				in  vec3 position;
		out vec4 frag_color;

		void main()
		{
			vec4 pixel_color = vec4(-position.z, 0.0, 0.0, 1.0);
			frag_color = pixel_color;
		}
		));
		shader->link();

		return shader;
	}

	fribr::Shader* create_dump_shader(int k)
	{
		fribr::Shader* shader = new fribr::Shader();
		shader->vertex_shader(GLSL(330,
			layout(location = 0) in vec2 vertex_position;
		out vec2 texcoord;

		void main()
		{
			texcoord = (vertex_position + vec2(1.0, 1.0)) * 0.5;
			gl_Position = vec4(vertex_position, 0.0, 1.0);
		}
		));

		std::stringstream sampler_stream;
		std::stringstream fetch_stream;
		for (int i = 0; i < k; ++i)
		{
			sampler_stream << "uniform sampler2D sampler_" << i << ";" << std::endl;
			fetch_stream << "if (selected_layer == " << i << ") "
				<< "{ patch_color += texture2D(sampler_" << i << ", texcoord); }" << std::endl;
		}

		shader->fragment_shader(
			GLSL(330,
				in  vec2 texcoord;
		out vec4 frag_color;
		uniform int selected_layer;
		) +
			sampler_stream.str() +
			GLSL_RAW(
				void main()
		{
			vec4 patch_color = vec4(0.0, 0.0, 0.0, 0.0);
			) +
			fetch_stream.str() +
				GLSL_RAW(
					if (patch_color.a <= 0.0)
						discard;

			patch_color.a = 1.0;
			frag_color = patch_color;
		}
		));
		shader->link();

		return shader;
	}

	fribr::Shader* create_average_shader()
	{
		fribr::Shader* shader = new fribr::Shader();
		shader->vertex_shader(GLSL(330,
			layout(location = 0) in vec2 vertex_position;
		out vec2 texcoord;

		void main()
		{
			texcoord = (vertex_position + vec2(1.0, 1.0)) * 0.5;
			gl_Position = vec4(vertex_position, 0.0, 1.0);
		}
		));

		shader->fragment_shader(
			GLSL(330,
				in  vec2 texcoord;
		out vec4 frag_color;
		uniform sampler2D weighted_average_sampler;

		void main()
		{
			vec4 patch_color = texture(weighted_average_sampler, texcoord);
			if (patch_color.a <= 0.0)
				discard;

			patch_color /= patch_color.a;
			frag_color = vec4(patch_color.rgb, 1.0);
		}
		));
		shader->link();

		return shader;
	}

	fribr::Shader* create_clear_bin_shader(Eigen::Vector3i grid_size)
	{
		fribr::Shader* shader = new fribr::Shader();
		shader->vertex_shader(GLSL(450,
			layout(location = 0) in vec2 vertex_position;
		out vec2 texcoord;

		void main()
		{
			texcoord = (vertex_position + vec2(1.0, 1.0)) * 0.5;
			gl_Position = vec4(vertex_position, 0.0, 1.0);
		}
		));

		std::stringstream header_stream;
		header_stream << "#version 450" << std::endl
			<< "#extension GL_ARB_shader_image_load_store : enable" << std::endl
			<< "#define GRID_SIZE_X " << grid_size.x() << std::endl
			<< "#define GRID_SIZE_Y " << grid_size.y() << std::endl
			<< "#define GRID_SIZE_Z " << grid_size.z() << std::endl;

		shader->fragment_shader(
			header_stream.str() +
			GLSL_RAW(
				layout(r32ui, binding = 0) writeonly uniform uimage2D gpu_grid_a;
		layout(r32ui, binding = 1) writeonly uniform uimage2D gpu_grid_b;
		layout(r32ui, binding = 2) writeonly uniform uimage2D gpu_grid_c;
		layout(r32ui, binding = 3) writeonly uniform uimage2D gpu_grid_d;

		uniform ivec2     resolution;
		uniform vec3      grid_min;
		uniform vec3      grid_max;

		in  vec2 texcoord;
		out vec4 frag_color;

		void main()
		{
			ivec2 image_coords = ivec2(texcoord * resolution);
			uint  index = image_coords.y * resolution.x + image_coords.x;

			uint channel_idx = index % 4;           index /= 4;
			uint grid_y = index % GRID_SIZE_Y; index /= GRID_SIZE_Y;
			uint grid_z = index;
			if (grid_z >= GRID_SIZE_Z)
				discard;

			if (channel_idx == 0)
				imageStore(gpu_grid_a, ivec2(grid_y, grid_z), uvec4(0));
			if (channel_idx == 1)
				imageStore(gpu_grid_b, ivec2(grid_y, grid_z), uvec4(0));
			if (channel_idx == 2)
				imageStore(gpu_grid_c, ivec2(grid_y, grid_z), uvec4(0));
			if (channel_idx == 3)
				imageStore(gpu_grid_d, ivec2(grid_y, grid_z), uvec4(0));

			discard;
		}
		));
		shader->link();

		return shader;
	}

	fribr::Shader* create_depth_bin_shader(Eigen::Vector3i grid_size)
	{
		fribr::Shader* shader = new fribr::Shader();
		shader->vertex_shader(GLSL(450,
			layout(location = 0) in vec2 vertex_position;
		out vec2 texcoord;

		void main()
		{
			texcoord = (vertex_position + vec2(1.0, 1.0)) * 0.5;
			gl_Position = vec4(vertex_position, 0.0, 1.0);
		}
		));

		std::stringstream header_stream;
		header_stream << "#version 450" << std::endl
			<< "#extension GL_ARB_shader_image_load_store : enable" << std::endl
			<< "#define GRID_SIZE_X " << grid_size.x() << std::endl
			<< "#define GRID_SIZE_Y " << grid_size.y() << std::endl
			<< "#define GRID_SIZE_Z " << grid_size.z() << std::endl;

		shader->fragment_shader(
			header_stream.str() +
			GLSL_RAW(
				layout(r32ui, binding = 0) uniform uimage2D gpu_grid_a;
		layout(r32ui, binding = 1) uniform uimage2D gpu_grid_b;
		layout(r32ui, binding = 2) uniform uimage2D gpu_grid_c;
		layout(r32ui, binding = 3) uniform uimage2D gpu_grid_d;

		uniform sampler2D position_sampler;
		uniform vec3      grid_min;
		uniform vec3      grid_max;
		uniform float     tolerance;

		in  vec2 texcoord;
		out vec4 frag_color;

		void set_voxel(uint grid_x, uint grid_y, uint grid_z)
		{
			uint channel_idx = grid_x / 32;
			uint channel_bit = grid_x - channel_idx * 32;
			uint channel_mask = 1 << channel_bit;

			if (channel_idx == 0)
				imageAtomicOr(gpu_grid_a, ivec2(grid_y, grid_z), channel_mask);
			if (channel_idx == 1)
				imageAtomicOr(gpu_grid_b, ivec2(grid_y, grid_z), channel_mask);
			if (channel_idx == 2)
				imageAtomicOr(gpu_grid_c, ivec2(grid_y, grid_z), channel_mask);
			if (channel_idx == 3)
				imageAtomicOr(gpu_grid_d, ivec2(grid_y, grid_z), channel_mask);
		}

		void main()
		{
			vec4 position = texture2D(position_sampler, texcoord);
			if (position.w < 1.0f)
				discard;

			float radius_sfm = -position.z * tolerance;

			// TODO: DOES THE CHANGE BELOW BREAK LUMBER? CHECK ORRESPONDING CHANGE IN VOXEL_GRID.CPP VoxelGrid::rasterize_triangle(...)
			// vec3 normalized_position = clamp((position.xyz - grid_min) / (grid_max - grid_min), vec3(0, 0, 0), vec3(1, 1, 1));
			vec3 normalized_position = (position.xyz - grid_min) / (grid_max - grid_min);
			if (min(normalized_position.x, min(normalized_position.y, normalized_position.z)) < 0.0 ||
				max(normalized_position.x, max(normalized_position.y, normalized_position.z)) > 1.0)
				discard;

			normalized_position = clamp(normalized_position, vec3(0, 0, 0), vec3(1, 1, 1));

			uint grid_x = min(GRID_SIZE_X - 1, int(normalized_position.x * GRID_SIZE_X));
			uint grid_y = min(GRID_SIZE_Y - 1, int(normalized_position.y * GRID_SIZE_Y));
			uint grid_z = min(GRID_SIZE_Z - 1, int(normalized_position.z * GRID_SIZE_Z));

			float grid_radius = length(vec3(GRID_SIZE_X, GRID_SIZE_Y, GRID_SIZE_Z))
				* radius_sfm / length(grid_max - grid_min);
			float gridf_x = normalized_position.x * GRID_SIZE_X;
			float gridf_y = normalized_position.y * GRID_SIZE_Y;
			float gridf_z = normalized_position.z * GRID_SIZE_Z;

			set_voxel(grid_x, grid_y, grid_z);

			uint next_x = -1;
			if (gridf_x - grid_x > 0.5 && grid_x < GRID_SIZE_X - 1)
				next_x = grid_x + 1;
			else if (grid_x > 0)
				next_x = grid_x - 1;

			uint next_y = -1;
			if (gridf_y - grid_y > 0.5 && grid_y < GRID_SIZE_Y - 1)
				next_y = grid_y + 1;
			else if (grid_y > 0)
				next_y = grid_y - 1;

			uint next_z = -1;
			if (gridf_z - grid_z > 0.5 && grid_z < GRID_SIZE_Z - 1)
				next_z = grid_z + 1;
			else if (grid_z > 0)
				next_z = grid_z - 1;

			if (next_x >= 0 && abs(gridf_x - next_x) < grid_radius)
				set_voxel(next_x, grid_y, grid_z);
			if (next_y >= 0 && abs(gridf_y - next_y) < grid_radius)
				set_voxel(grid_x, next_y, grid_z);
			if (next_z >= 0 && abs(gridf_z - next_z) < grid_radius)
				set_voxel(grid_x, grid_y, next_z);

			if (next_x >= 0 && next_y >= 0 &&
				length(vec2(gridf_x, gridf_y) - vec2(next_x, next_y)) < grid_radius)
				set_voxel(next_x, next_y, grid_z);
			if (next_x >= 0 && next_z >= 0 &&
				length(vec2(gridf_x, gridf_z) - vec2(next_x, next_z)) < grid_radius)
				set_voxel(next_x, grid_y, next_z);
			if (next_x >= 0 && next_z >= 0 &&
				length(vec2(gridf_y, gridf_z) - vec2(next_y, next_z)) < grid_radius)
				set_voxel(grid_x, next_y, next_z);

			if (next_x >= 0 && next_y >= 0 && next_z >= 0 &&
				length(vec3(gridf_x, gridf_y, gridf_z) - vec3(next_x, next_y, next_z)) < grid_radius)
				set_voxel(next_x, next_y, next_z);

			discard;
		}
		));
		shader->link();

		return shader;
	}

	fribr::Shader* create_box_shader()
	{
		fribr::Shader* shader = new fribr::Shader();
		shader->vertex_shader(
			GLSL(330,
				layout(location = 0) in vec3 vertex_position;
		uniform mat4 camera_to_clip;
		uniform mat4 world_to_camera;
		void main()
		{
			vec3 position = (world_to_camera * vec4(vertex_position, 1.0)).xyz;
			gl_Position = camera_to_clip * vec4(position, 1.0);
		}
		));
		shader->fragment_shader(
			GLSL(330,
				uniform vec3 color;
		out     vec4 frag_color;
		void main()
		{
			frag_color = vec4(color, 1.0);
		}
		));
		shader->link();
		return shader;
	}

#if defined(TF_INTEROP)
	fribr::Shader* create_tf_blit_shader()
	{
		fribr::Shader* shader = new fribr::Shader();
		shader->vertex_shader(
			GLSL(330,
				layout(location = 0) in vec2 vertex_position;
		out vec2 texcoord;

		void main()
		{
			texcoord = (vec2(vertex_position.x, -vertex_position.y) + vec2(1.0, 1.0)) * 0.5;
			gl_Position = vec4(vertex_position, 0.0, 1.0);
		}
		));

		shader->fragment_shader(
			GLSL(330,
				in  vec2 texcoord;
		out vec4 frag_color;
		uniform sampler2D global_sampler;
		uniform sampler2D diffuse_sampler;
		uniform int layer;

		void main()
		{
			vec4 patch_color  = texture(diffuse_sampler, texcoord);
			vec4 global_color = texture(global_sampler, texcoord);
			frag_color = vec4(patch_color.bgr, 1.0);
			if (patch_color.a <= 0.0 || layer >= 4)
				frag_color = vec4(global_color.bgr, 1.0);
		}
		));
		shader->link();

		return shader;
	}
#endif

	void write_float(std::ofstream &file, float f)
	{
		file.write((char*)&f, 1 * sizeof(float) / sizeof(char));
	}

	void write_float2(std::ofstream &file, fribr::float2 f)
	{
		file.write((char*)&f, 2 * sizeof(float) / sizeof(char));
	}

	void write_float3(std::ofstream &file, fribr::float3 f)
	{
		file.write((char*)&f, 3 * sizeof(float) / sizeof(char));
	}

	void write_float4(std::ofstream &file, fribr::float4 f)
	{
		file.write((char*)&f, 4 * sizeof(float) / sizeof(char));
	}

	float read_float(std::ifstream &file)
	{
		float buff[1];
		file.read((char*)buff, 1 * sizeof(float) / sizeof(char));
		return buff[0];
	}

	fribr::float2 read_float2(std::ifstream &file)
	{
		float buff[2];
		file.read((char*)buff, 2 * sizeof(float) / sizeof(char));
		return fribr::make_float2(buff[0], buff[1]);
	}

	fribr::float3 read_float3(std::ifstream &file)
	{
		float buff[3];
		file.read((char*)buff, 3 * sizeof(float) / sizeof(char));
		return fribr::make_float3(buff[0], buff[1], buff[2]);
	}

	fribr::float4 read_float4(std::ifstream &file)
	{
		float buff[4];
		file.read((char*)buff, 4 * sizeof(float) / sizeof(char));
		return fribr::make_float4(buff[0], buff[1], buff[2], buff[3]);
	}

	void write_int(std::ofstream &file, int i)
	{
		file.write((char*)&i, 1 * sizeof(int) / sizeof(char));
	}

	void write_int2(std::ofstream &file, fribr::int2 i)
	{
		file.write((char*)&i, 2 * sizeof(int) / sizeof(char));
	}

	void write_int3(std::ofstream &file, fribr::int3 i)
	{
		file.write((char*)&i, 3 * sizeof(int) / sizeof(char));
	}

	int read_int(std::ifstream &file)
	{
		int buff[1];
		file.read((char*)buff, 1 * sizeof(int) / sizeof(char));
		return buff[0];
	}

	fribr::int2 read_int2(std::ifstream &file)
	{
		int buff[2];
		file.read((char*)buff, 2 * sizeof(int) / sizeof(char));
		return fribr::make_int2(buff[0], buff[1]);
	}

	fribr::int3 read_int3(std::ifstream &file)
	{
		int buff[3];
		file.read((char*)buff, 3 * sizeof(int) / sizeof(char));
		return fribr::make_int3(buff[0], buff[1], buff[2]);
	}

	void copy_mesh(MxStdModel *from, MxStdModel *to)
	{
		for (size_t i = 0; i < from->vert_count(); ++i)
			to->add_vertex(from->vertex(i)[0], from->vertex(i)[1], from->vertex(i)[2]);
		for (size_t i = 0; i < from->face_count(); ++i)
		{
			if (!from->face_is_valid(i))
				continue;
			to->add_face(from->face(i)[0], from->face(i)[1], from->face(i)[2]);
		}
	}

}

namespace patcher
{

#if defined(TF_INTEROP)
	fribr::Shader *PatchCloud::s_tf_blit_shader = 0;
#endif

	int            PatchCloud::s_num_instances = 0;

	// Shaders for fusing the output.
	fribr::Shader *PatchCloud::s_dump_shader = 0;
	fribr::Shader *PatchCloud::s_average_shader = 0;

	// Shaders for the grid acceleration structure.
	fribr::Shader *PatchCloud::s_scene_geometry_shader = 0;
	fribr::Shader *PatchCloud::s_patch_depth_shader = 0;
	fribr::Shader *PatchCloud::s_box_shader = 0;

	PatchCloud::Component::Component(VoxelGrid *_grid)
		: grid(_grid),
		vertices_start(0)
	{
	}

	PatchCloud::Component::~Component()
	{
	}

	// works with a mesh (no code for meshing and simplification): This was used in deep-blending.
	//// [SP] To save a per-view mesh (or input tiles) as a component of the grid
	//// [SP] Each component corresponds to a particular camera
	//// [SP] and contains the input tiles (triangles corresponding to the camera)
	//// [SP] The voxel locations are found based on the camera position
	//// [SP] as they are indexed using a linear_index function based on the position of the "" as:
	//// [SP] voxel.x() + m_size.x() * (voxel.y() + m_size.y() * voxel.z());
	void PatchCloud::Component::add_camera(
		const fribr::float3              &camera_position,
		const cv::Mat                    &camera_image,
		fribr::Mesh::Ptr				 mesh,
		fribr::CalibratedImage           &calibrated_camera,
		const size_t                     component_index)
	{
		//assert(is_empty());
		image = cv::Mat::zeros(camera_image.rows, camera_image.cols, CV_8UC4);
		for (int y = 0; y < camera_image.rows; ++y)
			for (int x = 0; x < camera_image.cols; ++x)
			{
				cv::Vec3b c = camera_image.at<cv::Vec3b>(y, x);
				image.at<cv::Vec4b>(y, x) = cv::Vec4b(c[0], c[1], c[2], 255);
			}

		//// [SP] Why is the image position used when we have camera_position?
		//// [SP] It's the same thing.
		position = calibrated_camera.get_position();
		resolution = Eigen::Vector2i(image.cols, image.rows);

		Eigen::Matrix3f intrinsics = calibrated_camera.get_intrinsics();
		Eigen::Matrix4f world_to_camera = calibrated_camera.get_world_to_camera().matrix();

		assert(mesh);
		assert(!mesh->get_indices().empty());
		assert(!mesh->get_vertices().empty());


		//// [SP] What does this do?
		//// [SP] Duplicates the vertices to avoid conflict??
		MxStdModel model(mesh->get_vertices().size() / 3, mesh->get_indices().size() / 3);
		for (size_t i = 0; i < mesh->get_vertices().size(); i += 3)
			model.add_vertex(mesh->get_vertices()[i + 0],
				mesh->get_vertices()[i + 1],
				mesh->get_vertices()[i + 2]);

		for (size_t i = 0; i < mesh->get_indices().size(); i += 3)
		{
			model.add_face(mesh->get_indices()[i + 0],
				mesh->get_indices()[i + 1],
				mesh->get_indices()[i + 2]);

			for (size_t j = 0; j < 3; ++j)
			{
				size_t ii = mesh->get_indices()[i + j];
				vertices.emplace_back(fribr::make_float3(mesh->get_vertices()[ii * 3 + 0],
					mesh->get_vertices()[ii * 3 + 1],
					mesh->get_vertices()[ii * 3 + 2]));

				fribr::float3   v = vertices.back();
				Eigen::Vector4f v_cam = world_to_camera * Eigen::Vector4f(v.x, v.y, v.z, 1.0f);
				Eigen::Vector3f v_img = intrinsics      * v_cam.head<3>();
				fribr::float2   v_tex = fribr::make_float2(v_img.x() / (camera_image.cols * v_img.z()),
					v_img.y() / (camera_image.rows * v_img.z()));
				texcoords.emplace_back(fribr::make_float3(v_tex.x, v_tex.y, component_index));
			}
		}

		// [SP/PH] Split the per-view mesh into slices, by checking which voxels intersect with the triangle.
		// [SP/PH] A slice is a part of a per-view mesh (Component) that intersects with one world-space voxel.

		for (size_t i = 0; i < vertices.size(); i += 3)
		{
			//// [SP] We rasterize the triangle made up of 3 contiguous vertices and find the voxel in which it lies
			//// [SP] We return the voxel location as index parameter 
			//// [SP] which is used to create the map between voxel and the triangle slice
			grid->rasterize_triangle(fribr::make_vec3f(vertices[i + 0]),
				fribr::make_vec3f(vertices[i + 1]),
				fribr::make_vec3f(vertices[i + 2]),
				[&](Eigen::Vector3i index) {
				size_t linear_index = grid->linear_index(index);
				//// [SP] voxel_to_index_remap is an unordered map between voxel_index and triangle_slices
				//// [SP] Using the map, we should get the triangle_slices contained in that voxel
				//// [SP] If current voxel index is not present in the map; i.e. the voxel is encountered for the first time
				if (voxel_to_index_remap.find(linear_index) == voxel_to_index_remap.end())
				{
					//// [SP] 1. Create the triangle slice which stores the vertex location
					slice_indices.push_back(std::vector<uint32_t>());
					//// [SP] 2. Add the voxel location to the corresponding slice_voxel list to easily fetch the voxel 
					slice_voxels.push_back(index);
					//// [SP] 3. Mark the voxel as valid for rendering which will be used in culling while online rendering
					slice_valid.push_back(true);
					//// [SP] Create the map from voxel index to slice index
					voxel_to_index_remap[linear_index] = slice_indices.size() - 1;
				}
				//// [SP] Add the 3 vertex ids to the triangle slice
				size_t compressed_index = voxel_to_index_remap[linear_index];

				slice_indices[compressed_index].push_back(i + 0);
				slice_indices[compressed_index].push_back(i + 1);
				slice_indices[compressed_index].push_back(i + 2);
			});
		}

		// Detect voxels that have discontinuities in them, and make sure to use more slices when rendering these parts of the scene.
		MxVertexList star;
		MxFaceList faces;
		for (size_t i = 0; i < mesh->get_indices().size(); i++)
		{
			size_t ii = mesh->get_indices()[i];
			Eigen::Vector3f v(model.vertex(ii)[0], model.vertex(ii)[1], model.vertex(ii)[2]);
			Eigen::Vector3i voxel = grid->world_to_voxel(v);

			size_t linear_idx = grid->linear_index(voxel);
			if (voxel_to_index_remap.find(linear_idx) == voxel_to_index_remap.end())
				continue;

			size_t index = voxel_to_index_remap[linear_idx];
			star.reset();
			model.collect_vertex_star(ii, star);

			for (uint j = 0; j < star.length(); j++)
				if (ii < star(j))
				{
					faces.reset();
					model.collect_edge_neighbors(ii, star(j), faces);

					// number of neighbors along the edge == 1 -> This vertex is at a discontinuity.
					if (faces.length() != 1)
						continue;

					slice_valid[index] = false;

					size_t index_a = model.face(faces(0))[0];
					size_t index_b = model.face(faces(0))[1];
					size_t index_c = model.face(faces(0))[2];
					grid->rasterize_triangle(
						Eigen::Vector3f(model.vertex(index_a)[0],
							model.vertex(index_a)[1],
							model.vertex(index_a)[2]),
						Eigen::Vector3f(model.vertex(index_b)[0],
							model.vertex(index_b)[1],
							model.vertex(index_b)[2]),
						Eigen::Vector3f(model.vertex(index_c)[0],
							model.vertex(index_c)[1],
							model.vertex(index_c)[2]),
						[&](Eigen::Vector3i index) {
						size_t linear_index = grid->linear_index(index);
						size_t compressed_index = voxel_to_index_remap[linear_index];
						slice_valid[compressed_index] = false;
					});
				}
		}
	}

	// original inside-out function; does meshing, triangle cutting, boundary smoothing etc. but otherwise identical to above.
	void PatchCloud::Component::add_camera(const fribr::float3              &camera_position,
		const cv::Mat                    &camera_image,
		const std::vector<fribr::float3> &points,
		fribr::CalibratedImage           &calibrated_camera,
		const int                        compression_factor,
		const size_t                     component_index)
	{
		//assert(is_empty());
		// sets up data structures for mesh siplification tool
		std::vector<MxVertexID> vertex_ids;
		std::unique_ptr<MxStdModel> model(new MxStdModel(camera_image.rows * camera_image.cols,
			(camera_image.rows - 1) * (camera_image.cols - 1) * 2));
		for (int y = 0; y < camera_image.rows; ++y)
			for (int x = 0; x < camera_image.cols; ++x)
			{
				fribr::float3 v = points[y * camera_image.cols + x];
				vertex_ids.push_back(model->add_vertex(v.x, v.y, v.z));
			}

		Eigen::Matrix3f intrinsics = calibrated_camera.get_intrinsics();
		Eigen::Matrix4f world_to_camera = calibrated_camera.get_world_to_camera().matrix();
		Eigen::Matrix4f camera_to_world = world_to_camera.inverse();

		// identifies which vertices to exclude from simplification and which triangles to cut
		// in this case, vertices at occlusion boundaries should be exluded from simplification
		// and triangle connecting FG and BG at occlusion boundaries should be cut
		std::vector<bool> vertex_fixed(vertex_ids.size(), false);
		for (int face_y = 0; face_y < camera_image.rows - 1; ++face_y)
			for (int face_x = 0; face_x < camera_image.cols - 1; ++face_x)
			{
				fribr::float3 va = points[(face_y + 1) * camera_image.cols + (face_x + 0)];
				fribr::float3 vb = points[(face_y + 1) * camera_image.cols + (face_x + 1)];
				fribr::float3 vc = points[(face_y + 0) * camera_image.cols + (face_x + 0)];
				fribr::float3 vd = points[(face_y + 0) * camera_image.cols + (face_x + 1)];

				MxVertexID a = vertex_ids[(face_y + 1) * camera_image.cols + (face_x + 0)];
				MxVertexID b = vertex_ids[(face_y + 1) * camera_image.cols + (face_x + 1)];
				MxVertexID c = vertex_ids[(face_y + 0) * camera_image.cols + (face_x + 0)];
				MxVertexID d = vertex_ids[(face_y + 0) * camera_image.cols + (face_x + 1)];

				Eigen::Vector4f cam_a = world_to_camera * Eigen::Vector4f(va.x, va.y, va.z, 1.0f);
				Eigen::Vector4f cam_b = world_to_camera * Eigen::Vector4f(vb.x, vb.y, vb.z, 1.0f);
				Eigen::Vector4f cam_c = world_to_camera * Eigen::Vector4f(vc.x, vc.y, vc.z, 1.0f);
				Eigen::Vector4f cam_d = world_to_camera * Eigen::Vector4f(vd.x, vd.y, vd.z, 1.0f);

				static const float CUTOFF_RATIO = 1.033f;
				if ((cam_a.z() / cam_b.z()) > CUTOFF_RATIO ||
					(cam_a.z() / cam_c.z()) > CUTOFF_RATIO ||
					(cam_a.z() / cam_d.z()) > CUTOFF_RATIO ||
					(cam_b.z() / cam_c.z()) > CUTOFF_RATIO ||
					(cam_b.z() / cam_d.z()) > CUTOFF_RATIO ||
					(cam_c.z() / cam_d.z()) > CUTOFF_RATIO ||
					(cam_a.z() / cam_b.z()) < 1.0f / CUTOFF_RATIO ||
					(cam_a.z() / cam_c.z()) < 1.0f / CUTOFF_RATIO ||
					(cam_a.z() / cam_d.z()) < 1.0f / CUTOFF_RATIO ||
					(cam_b.z() / cam_c.z()) < 1.0f / CUTOFF_RATIO ||
					(cam_b.z() / cam_d.z()) < 1.0f / CUTOFF_RATIO ||
					(cam_c.z() / cam_d.z()) < 1.0f / CUTOFF_RATIO)
				{
					vertex_fixed[a] = true;
					vertex_fixed[b] = true;
					vertex_fixed[c] = true;
					vertex_fixed[d] = true;
					continue;
				}

				if (cam_a.z() >= 0.0f || cam_b.z() >= 0.0f ||
					cam_c.z() >= 0.0f || cam_d.z() >= 0.0f)
					continue;

				model->add_face(a, c, d);
				model->add_face(a, d, b);
			}

		// Perform bilateral smoothing along the mesh, don't cross occlusion boundaries
		std::vector<Eigen::Vector3f> vertices_camera;
		for (size_t i = 0; i < model->vert_count(); ++i)
		{
			Eigen::Vector3f v_world = fribr::make_vec3f(points[i]);
			Eigen::Vector4f v_cam = world_to_camera * v_world.homogeneous();
			vertices_camera.emplace_back(v_cam.head<3>());
		}

		std::vector<int> neighbor_mask(model->vert_count(), -1);
		for (size_t i = 0; i < model->vert_count(); ++i)
		{
			Eigen::Vector3f v_cam = vertices_camera[i];
			if (v_cam.z() >= 0.0f)
				continue;

			int x = i % camera_image.cols;
			int y = camera_image.rows - 1 - i / camera_image.cols;

			cv::Vec3b       c = camera_image.at<cv::Vec3b>(y, x);
			Eigen::Vector3f color = Eigen::Vector3f(c[2], c[1], c[0]);

			// Smooth in a 12x12 neighborhod
			std::vector<MxVertexID> neighborhood;
			std::vector<MxVertexID> current_list, next_list;
			current_list.push_back(i);
			for (int r = 0; r < 6; ++r)
			{
				for (MxVertexID v_id : current_list)
				{
					MxFaceList& neighbor_faces = model->neighbors(v_id);
					for (size_t fi = 0; fi < neighbor_faces.length(); ++fi)
					{
						MxFaceID f_id = neighbor_faces(fi);
						for (int vi = 0; vi < 3; ++vi)
						{
							MxVertexID v_id = model->face(f_id)[vi];
							if (neighbor_mask[v_id] != static_cast<int>(i))
							{
								next_list.push_back(v_id);
								neighbor_mask[v_id] = i;
								neighborhood.emplace_back(v_id);
							}
						}
					}
				}
				current_list.swap(next_list);
				next_list.clear();
				if (current_list.empty())
					break;
			}

			float total_depth = 0.0f;
			float total_weight = 0.0f;
			for (MxVertexID v_id : neighborhood)
			{
				Eigen::Vector3f n_cam = vertices_camera[v_id];
				if (n_cam.z() >= 0.0f)
					continue;

				int xx = v_id % camera_image.cols;
				int yy = camera_image.rows - 1 - v_id / camera_image.cols;

				cv::Vec3b       nc = camera_image.at<cv::Vec3b>(yy, xx);
				Eigen::Vector3f n_color = Eigen::Vector3f(nc[2], nc[1], nc[0]);

				float log_cost = -(n_color - color).squaredNorm() / (2.0f * 25.0f  * 25.0f);
				float weight = expf(log_cost);

				total_weight += weight;
				total_depth += weight * n_cam.z();
			}

			if (total_weight <= 0.0f)
				continue;

			total_depth /= total_weight;
			v_cam *= total_depth / v_cam.z();

			Eigen::Vector3f v_world = (camera_to_world * v_cam.homogeneous()).head<3>();
			model->vertex(i)[0] = v_world.x();
			model->vertex(i)[1] = v_world.y();
			model->vertex(i)[2] = v_world.z();
		}

		int valid_faces = 0;
		std::vector<MxEdge> valid_edges;

		// build a hashmap to keep track of which triangle edges can be simpified
		typedef std::pair<size_t, size_t> EdgePair;
		struct EdgeHash {
		public:
			std::size_t operator()(const EdgePair &x) const
			{
				return std::hash<size_t>()(x.first + 1000000000LLU * x.second);
			}
		};
		std::unordered_map<EdgePair, bool, EdgeHash> edge_map;
		for (size_t i = 0; i < model->face_count(); ++i)
		{
			if (!model->face_is_valid(i))
				continue;

			size_t a = model->face(i)[0];
			size_t b = model->face(i)[1];
			size_t c = model->face(i)[2];

			bool valid = true;
			if (!vertex_fixed[a] && !vertex_fixed[b])
			{
				// QSlim assumes each edge has been added only once
				if (!edge_map[std::make_pair(a, b)] && !edge_map[std::make_pair(b, a)])
				{
					valid_edges.push_back(MxEdge(a, b));
					edge_map[std::make_pair(a, b)] = true;
				}
			}
			else valid = false;

			if (!vertex_fixed[a] && !vertex_fixed[c])
			{
				// QSlim assumes each edge has been added only once
				if (!edge_map[std::make_pair(a, c)] && !edge_map[std::make_pair(c, a)])
				{
					valid_edges.push_back(MxEdge(a, c));
					edge_map[std::make_pair(a, c)] = true;
				}
			}
			else valid = false;

			if (!vertex_fixed[b] && !vertex_fixed[c])
			{
				// QSlim assumes each edge has been added only once
				if (!edge_map[std::make_pair(b, c)] && !edge_map[std::make_pair(c, b)])
				{
					valid_edges.push_back(MxEdge(b, c));
					edge_map[std::make_pair(b, c)] = true;
				}
			}
			else valid = false;

			if (valid)
				valid_faces++;
		}

		// incrementally simplify the mesh until it breaks.

		int pre_compression_faces = model->face_count();
		if (compression_factor > 1)
		{
			const int compression_guess = std::max(1, compression_factor / 4);
			int min_faces = model->face_count() / compression_factor;
			int target_faces = valid_faces / compression_guess;

			MxStdModel decimate_model(model->vert_count(), model->face_count());
			copy_mesh(model.get(), &decimate_model);

			{
				MxEdgeQSlim slim(decimate_model);
				slim.placement_policy = MX_PLACE_OPTIMAL;
				slim.boundary_weight = 0.0f;
				slim.weighting_policy = MX_WEIGHT_AREA;
				slim.compactness_ratio = 0.0;
				slim.meshing_penalty = 1.0;
				slim.will_join_only = false;
				slim.initialize(valid_edges.data(), valid_edges.size());

				auto compute_step_size = [&]() {
					return std::max(1000, (target_faces - min_faces) / 3);
				};
				for (; target_faces >= min_faces; target_faces -= compute_step_size())
				{
					slim.decimate(target_faces);

					bool all_valid = true;
					for (size_t i = 0; i < decimate_model.face_count(); ++i)
					{
						if (!decimate_model.face_is_valid(i))
							continue;

						size_t a = decimate_model.face(i)[0];
						size_t b = decimate_model.face(i)[1];
						size_t c = decimate_model.face(i)[2];

						Eigen::Vector4f va = world_to_camera *
							Eigen::Vector4f(decimate_model.vertex(a)[0],
								decimate_model.vertex(a)[1],
								decimate_model.vertex(a)[2], 1.0f);
						Eigen::Vector4f vb = world_to_camera *
							Eigen::Vector4f(decimate_model.vertex(b)[0],
								decimate_model.vertex(b)[1],
								decimate_model.vertex(b)[2], 1.0f);
						Eigen::Vector4f vc = world_to_camera *
							Eigen::Vector4f(decimate_model.vertex(c)[0],
								decimate_model.vertex(c)[1],
								decimate_model.vertex(c)[2], 1.0f);

						Eigen::Vector3f ca = intrinsics * va.head<3>();
						Eigen::Vector3f cb = intrinsics * vb.head<3>();
						Eigen::Vector3f cc = intrinsics * vc.head<3>();
						if (ca.z() <= 0.0f || cb.z() <= 0.0f || cc.z() <= 0.0f)
						{
							all_valid = false;
							break;
						}

						ca /= ca.z();
						cb /= cb.z();
						cc /= cc.z();
						if ((ca - cb).head<2>().norm() > 75.0f ||
							(ca - cc).head<2>().norm() > 75.0f ||
							(cb - cc).head<2>().norm() > 75.0f)
						{
							all_valid = false;
							break;
						}
					}

					if (!all_valid)
					{
						// Make sure to always decimate the mesh by the minimum amount
						if (static_cast<int>(model->face_count()) == pre_compression_faces)
						{
							model.reset(new MxStdModel(decimate_model.vert_count(), decimate_model.face_count()));
							copy_mesh(&decimate_model, model.get());
						}

						break;
					}

					model.reset(new MxStdModel(decimate_model.vert_count(), decimate_model.face_count()));
					copy_mesh(&decimate_model, model.get());
				}
			}
		}
		int post_compression_faces = 0;
		for (size_t i = 0; i < model->face_count(); ++i)
			if (model->face_is_valid(i))
				post_compression_faces++;
		std::cout << "Compressed: " << pre_compression_faces
			<< " -> " << post_compression_faces
			<< " (" << pre_compression_faces / post_compression_faces << "x)"
			<< std::endl;

		std::vector<uint32_t> mesh_indices;
		std::vector<float>    mesh_vertices;
		for (size_t i = 0; i < model->vert_count(); ++i)
		{
			fribr::float3   v = fribr::make_float3(model->vertex(i)[0],
				model->vertex(i)[1],
				model->vertex(i)[2]);
			mesh_vertices.emplace_back(v.x);
			mesh_vertices.emplace_back(v.y);
			mesh_vertices.emplace_back(v.z);
		}

		for (size_t i = 0; i < model->face_count(); ++i)
		{
			if (!model->face_is_valid(i))
				continue;

			mesh_indices.emplace_back(model->face(i)[0]);
			mesh_indices.emplace_back(model->face(i)[1]);
			mesh_indices.emplace_back(model->face(i)[2]);
		}

		fribr::Mesh::Ptr mesh(new fribr::Mesh(mesh_indices,
			mesh_vertices,
			std::vector<float>(), // Normals
			std::vector<float>(), // Texcoords
			std::vector<float>(), // Colors
			fribr::Mesh::CPUOnly));

		add_camera(camera_position, camera_image, mesh, calibrated_camera, component_index);
	}

	bool PatchCloud::Component::is_valid(Eigen::Vector3i voxel) const
	{
		size_t linear_idx = grid->linear_index(voxel);
		if (voxel_to_index_remap.find(linear_idx) == voxel_to_index_remap.end())
			return false;

		size_t slice_index = voxel_to_index_remap.find(linear_idx)->second;
		return slice_valid[slice_index];
	}

	size_t PatchCloud::Component::draw(Eigen::Vector3i voxel) const
	{
		size_t linear_idx = grid->linear_index(voxel);
		if (voxel_to_index_remap.find(linear_idx) == voxel_to_index_remap.end())
			return 0;

		size_t slice_index = voxel_to_index_remap.find(linear_idx)->second;
		size_t num_vertices = slice_indices[slice_index].size();
		glDrawElements(GL_TRIANGLES, num_vertices, GL_UNSIGNED_INT,
			(void*)(sizeof(uint32_t) * slices_start[slice_index]));

		return num_vertices / 3;
	}

	size_t PatchCloud::Component::draw() const
	{
		glDrawArrays(GL_TRIANGLES, vertices_start, vertices.size());
		return vertices.size() / 3;
	}

	void PatchCloud::Component::save(std::ofstream &out_file) const
	{
		// Support legacy viewers, just write zeroes instead of the world_to_camera matrix
		write_float4(out_file, fribr::make_float4(0.0f, 0.0f, 0.0f, 0.0f));
		write_float4(out_file, fribr::make_float4(0.0f, 0.0f, 0.0f, 0.0f));
		write_float4(out_file, fribr::make_float4(0.0f, 0.0f, 0.0f, 0.0f));
		write_float4(out_file, fribr::make_float4(0.0f, 0.0f, 0.0f, 0.0f));

		// ... the same applies for the intrinsics matrix
		write_float3(out_file, fribr::make_float3(0.0f, 0.0f, 0.0f));
		write_float3(out_file, fribr::make_float3(0.0f, 0.0f, 0.0f));
		write_float3(out_file, fribr::make_float3(0.0f, 0.0f, 0.0f));

		write_int2(out_file, fribr::make_int2(resolution));
		write_float3(out_file, fribr::make_float3(position));

		write_int2(out_file, fribr::make_int2(image.cols, image.rows));
		for (int y = 0; y < image.rows; ++y)
			for (int x = 0; x < image.cols; ++x)
			{
				cv::Vec4b c = image.at<cv::Vec4b>(y, x);
				out_file.write((char*)&c[0], 4 * sizeof(char));
			}

		write_int(out_file, slice_indices.size());
		for (size_t i = 0; i < slice_indices.size(); ++i)
		{
			write_int3(out_file, fribr::make_int3(slice_voxels[i]));
			write_int(out_file, int(slice_valid[i]));
			write_int(out_file, slice_indices[i].size());
			out_file.write((char*)slice_indices[i].data(),
				slice_indices[i].size() * sizeof(int32_t) / sizeof(char));
		}

		write_int(out_file, vertices.size());
		out_file.write((char*)vertices.data(), vertices.size() * 3 * sizeof(float) / sizeof(char));

		write_int(out_file, texcoords.size());
		out_file.write((char*)texcoords.data(), texcoords.size() * 3 * sizeof(float) / sizeof(char));
	}

	void PatchCloud::Component::load(std::ifstream &in_file)
	{
		// Support legacy file formats, just skip over the world_to_camera matrix
		read_float4(in_file);
		read_float4(in_file);
		read_float4(in_file);
		read_float4(in_file);

		// ... also skip the intrinsics matrix.
		read_float3(in_file);
		read_float3(in_file);
		read_float3(in_file);

		resolution = fribr::make_vec2i(read_int2(in_file));
		position = fribr::make_vec3f(read_float3(in_file));

		fribr::int2 img_resolution = read_int2(in_file);
		image = cv::Mat(img_resolution.y, img_resolution.x, CV_8UC4);
		for (int y = 0; y < img_resolution.y; ++y)
			for (int x = 0; x < img_resolution.x; ++x)
			{
				cv::Vec4b c;
				in_file.read((char*)&c[0], 4 * sizeof(char));
				image.at<cv::Vec4b>(y, x) = c;
			}

		int num_slices = read_int(in_file);
		slice_indices.assign(num_slices, std::vector<uint32_t>());
		voxel_to_index_remap.clear();
		slice_voxels.clear();
		slice_valid.clear();
		for (int i = 0; i < num_slices; ++i)
		{
			fribr::int3 slice_voxel = read_int3(in_file);
			slice_voxels.push_back(fribr::make_vec3i(slice_voxel));

			size_t linear_idx = grid->linear_index(fribr::make_vec3i(slice_voxel));
			voxel_to_index_remap[linear_idx] = i;

			int is_valid = read_int(in_file);
			slice_valid.push_back(bool(is_valid));

			int num_indices = read_int(in_file);
			slice_indices[i].resize(num_indices, 0);
			in_file.read((char*)slice_indices[i].data(), num_indices * sizeof(int32_t) / sizeof(char));
		}

		int num_vertices = read_int(in_file);
		vertices.resize(num_vertices, fribr::make_float3(0.0f, 0.0f, 0.0f));
		in_file.read((char*)vertices.data(), num_vertices * 3 * sizeof(float) / sizeof(char));

		int num_texcoords = read_int(in_file);
		texcoords.resize(num_texcoords, fribr::make_float3(0.0f, 0.0f, 0.0f));
		in_file.read((char*)texcoords.data(), num_texcoords * 3 * sizeof(float) / sizeof(char));
	}

	PatchCloud::ImageBin::ImageBin(int bin_x, int bin_y, int bin_z, const VoxelGrid &grid)
		: voxel(bin_x, bin_y, bin_z)
	{
		Eigen::Array3f bbox_min_grid(bin_x, bin_y, bin_z);
		Eigen::Array3f bbox_max_grid(bin_x + 1.0f, bin_y + 1.0f, bin_z + 1.0f);

		Eigen::Array3f bbox_min_unit = bbox_min_grid / grid.size().array().cast<float>();
		Eigen::Array3f bbox_max_unit = bbox_max_grid / grid.size().array().cast<float>();

		bbox_min = bbox_min_unit * (grid.max() - grid.min()) + grid.min();
		bbox_max = bbox_max_unit * (grid.max() - grid.min()) + grid.min();
	}

	PatchCloud::PatchCloud()
		: m_use_tf_blending(false),
		m_algorithm("deep_blending"),
		m_depth_bin_shader(0),
		m_clear_bin_shader(0),
		m_display_layer(0),
		m_display_mode(Colors),
		m_cluster_threshold(4),
		m_cluster_k(std::max(12, m_cluster_threshold)),
		m_all_triangles(0),
		m_grid(Eigen::Vector3i(32, 32, 32)),
		m_selected_camera(-1),
		m_ignored_camera(-1),
		m_max_tiles(-1),
		m_update_voxel_memory(true),
		m_use_fake_position(false),
		m_texture_array(0),
		m_camera_pos_texture(0),
		m_draw_calls(0),
		m_vao(0),
		m_index_buffer(0),
		m_vertex_buffer(0),
		m_texcoord_buffer(0)
	{
		for (int i = 0; i < 4; ++i)
			m_gpu_grid[i] = 0;
		
		for (int i = 0; i < NUM_LAYERS; ++i)
			m_layers[i].reset();

		if (s_num_instances++ == 0)
		{
			s_dump_shader = create_dump_shader(NUM_LAYERS);
			s_average_shader = create_average_shader();

			s_scene_geometry_shader = create_scene_geometry_shader();
			s_patch_depth_shader = create_patch_depth_shader();

			s_box_shader = create_box_shader();

#if defined(TF_INTEROP)
			s_tf_blit_shader = create_tf_blit_shader();
#endif
			//m_tf_layers.resize(NUM_LAYERS + 1);

			_run_node_names.push_back("output0");

	/*		fribr::Texture::Descriptor texture_descriptor(GL_CLAMP_TO_EDGE, GL_NEAREST, 0, fribr::TextureFormat::RGBA8);
			std::vector<fribr::Texture::Descriptor> target_descriptors(1, texture_descriptor);
			m_global_fbo.reset(new fribr::Framebuffer({ 1280,720 }, target_descriptors));*/
		}
	}

	PatchCloud::PatchCloud(Eigen::Vector3i _grid_size, const std::string & algorithm)
		: m_use_tf_blending(false),
		m_algorithm(algorithm),
		m_depth_bin_shader(0),
		m_clear_bin_shader(0),
		m_display_layer(0),
		m_display_mode(Colors),
		m_cluster_threshold(4),
		m_cluster_k(std::max(12, m_cluster_threshold)),
		m_all_triangles(0),
		m_grid(_grid_size),
		m_selected_camera(-1),
		m_ignored_camera(-1),
		m_max_tiles(-1),
		m_update_voxel_memory(true),
		m_use_fake_position(false),
		m_texture_array(0),
		m_camera_pos_texture(0),
		m_draw_calls(0),
		m_vao(0),
		m_index_buffer(0),
		m_vertex_buffer(0),
		m_texcoord_buffer(0)
	{
		for (int i = 0; i < 4; ++i)
			m_gpu_grid[i] = 0;

		for (int i = 0; i < NUM_LAYERS; ++i)
			m_layers[i].reset();

		if (s_num_instances++ == 0)
		{
			s_dump_shader = create_dump_shader(NUM_LAYERS);
			s_average_shader = create_average_shader();

			s_scene_geometry_shader = create_scene_geometry_shader();
			s_patch_depth_shader = create_patch_depth_shader();

			s_box_shader = create_box_shader();

#if defined(TF_INTEROP)
			s_tf_blit_shader = create_tf_blit_shader();
#endif

		}
		//m_tf_layers.resize(NUM_LAYERS + 1);
		_run_node_names.push_back("output0");
/*
		fribr::Texture::Descriptor texture_descriptor(GL_CLAMP_TO_EDGE, GL_NEAREST, 0, fribr::TextureFormat::RGBA8);
		std::vector<fribr::Texture::Descriptor> target_descriptors(1, texture_descriptor);
		m_global_fbo.reset(new fribr::Framebuffer({ 1280,720 }, target_descriptors));*/
	}

	PatchCloud::~PatchCloud()
	{
		if (--s_num_instances == 0)
		{
			delete s_dump_shader;
			s_dump_shader = 0;

			delete s_average_shader;
			s_average_shader = 0;

			delete s_scene_geometry_shader;
			s_scene_geometry_shader = 0;

			delete s_patch_depth_shader;
			s_patch_depth_shader = 0;

			delete s_box_shader;
			s_box_shader = 0;

#if defined(TF_INTEROP)
			delete s_tf_blit_shader;
			s_tf_blit_shader = 0;
#endif
		}

		clear_gpu();
	}


	void PatchCloud::setTextRes(Eigen::Vector2i res) {


		for (int i = 0; i < NUM_LAYERS + 1; i++) {
			m_tf_layers.push_back(std::make_shared<sibr::RenderTargetRGBA32F>(res.x(), res.y()));
			_input_textures.push_back(std::make_shared<sibr::TexInfo>(sibr::TexInfo(m_tf_layers[i])));
		}

		_output_textures.push_back(std::make_shared<sibr::Texture2DRGBA32F>(sibr::ImageRGBA32F(res.x(), res.y()), SIBR_CLAMP_UVS));

	}


	void PatchCloud::upload_texture_image_if_needed()
	{
		if (m_gpu_grid[0])
			return;

		// SP: create depth and clear shaders
		m_depth_bin_shader = create_depth_bin_shader(m_grid.size());
		m_clear_bin_shader = create_clear_bin_shader(m_grid.size());

		// SP: Generate GL_TEXTURE_2D target and attach 4 texture 2D image to it
		for (int i = 0; i < 4; ++i)
		{
			glGenTextures(1, &m_gpu_grid[i]);
			glBindTexture(GL_TEXTURE_2D, m_gpu_grid[i]);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_R32UI,
				m_grid.size().y(), m_grid.size().z(),
				0, GL_RED_INTEGER, GL_UNSIGNED_INT, 0);
		}

		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	//// [SP] If we don't have out per-view meshes, create the per-view meshes as done in InsideOut 
	//// [SP] and then create slices for each camera and add it to the grid
	void PatchCloud::add_cameras(fribr::CalibratedImage::Vector                   &cameras,
		std::vector<std::vector<Eigen::Vector3f> > &depth_maps,
		int compression_factor, MemoryPolicy policy)
	{
		m_components.clear();
		m_all_triangles = 0;

		for (size_t i = 0; i < cameras.size(); ++i)
		{
			Component::Ptr component(new Component(&m_grid));
			m_components.push_back(component);
		}

		size_t progress = 0;
#ifdef NDEBUG
#ifdef INRIA_WIN
		// actually a Windows 7 bug
		omp_set_dynamic(0);     // Explicitly disable dynamic teams
		omp_set_num_threads(8); // Use 8 threads for all consecutive parallel regions
#pragma omp parallel for 
#else
#pragma omp parallel for schedule(dynamic, 1)
#endif
#endif
#ifdef INRIA_WIN
		for (int i = 0; i < cameras.size(); ++i)
#else
		for (size_t i = 0; i < cameras.size(); ++i)
#endif
		{
			fribr::CalibratedImage             &camera = cameras[i];
			std::vector<Eigen::Vector3f> &positions = depth_maps[i];

			Eigen::Matrix4f world_to_cam = camera.get_world_to_camera().matrix();
			Eigen::Matrix4f cam_to_world = world_to_cam.inverse();

			cv::Mat image = fribr::load_undistorted_image(camera.get_image_path(),
				Eigen::Vector2f(camera.get_focal_x(),
					camera.get_focal_y()),
				Eigen::Vector2f(camera.get_dx(),
					camera.get_dy()),
				camera.get_k(),
				camera.get_max_width(),
				cv::IMREAD_COLOR);

			std::vector<fribr::float3> points;
			for (int y = 0; y < image.rows; ++y)
				for (int x = 0; x < image.cols; ++x)
				{
					int xx = x;
					int yy = image.rows - y - 1;
					const Eigen::Vector3f &pc = positions[yy * image.cols + xx];
					const Eigen::Vector4f  pw = cam_to_world * pc.homogeneous();
					points.push_back(fribr::make_float3(pw.head<3>()));
				}

#pragma omp critical
			{
				std::cerr << "Pushed points Camera " << i << std::endl;
			}

			m_components[i]->add_camera(fribr::make_float3(camera.get_position()),
				image, points, cameras[i],
				compression_factor, i);

			if (policy == DeallocateWhenDone)
				positions.clear();

#pragma omp critical
			{
				std::cerr << "Added Camera " << i << std::endl;

				for (Eigen::Vector3i v : m_components[i]->slice_voxels)
					m_grid.add_component(v, i);

				m_all_triangles += m_components[i]->vertices.size() / 3;
				progress++;
				std::cerr << "\rCreating ibr meshes: " << progress * 100.0f / cameras.size()
					<< "%                      " << std::flush;
			}
		}
	}

	//// [SP] Create slices for the given camera and add it to the grid
	void PatchCloud::add_cameras(fribr::CalibratedImage::Vector &cameras,
		std::vector<fribr::Mesh::Ptr>  &meshes,
		MemoryPolicy policy)
	{
		m_components.clear();
		m_all_triangles = 0;

		for (size_t i = 0; i < cameras.size(); ++i)
		{
			Component::Ptr component(new Component(&m_grid));
			m_components.push_back(component);
		}

		size_t progress = 0;
#ifdef NDEBUG
//#pragma omp parallel for schedule(dynamic, 1)
#endif
#ifdef INRIA_WIN
		for (int i = 0; i < cameras.size(); ++i)
#else
		for (size_t i = 0; i < cameras.size(); ++i)
#endif
		{
			fribr::CalibratedImage &camera = cameras[i];
			fribr::Mesh::Ptr       mesh = meshes[i];

			Eigen::Matrix4f world_to_cam = camera.get_world_to_camera().matrix();
			Eigen::Matrix4f cam_to_world = world_to_cam.inverse();

			cv::Mat image = fribr::load_undistorted_image(camera.get_image_path(),
				Eigen::Vector2f(camera.get_focal_x(),
					camera.get_focal_y()),
				Eigen::Vector2f(camera.get_dx(),
					camera.get_dy()),
				camera.get_k(),
				camera.get_max_width(),
				cv::IMREAD_COLOR);

			m_components[i]->add_camera(fribr::make_float3(camera.get_position()),
				image, mesh, cameras[i], i);

			if (policy == DeallocateWhenDone)
				mesh.reset();

			auto critical_section = [&]()
			{
				for (Eigen::Vector3i v : m_components[i]->slice_voxels)
					m_grid.add_component(v, i);

				m_all_triangles += m_components[i]->vertices.size() / 3;
				progress++;
				std::cout << "\rCreating ibr meshes: " << progress * 100.0f / cameras.size()
				          << "%                      " << std::flush;
			};
			#pragma omp critical
			critical_section();
		}
	}

	void PatchCloud::add_camera(fribr::CalibratedImage &camera, const std::vector<Eigen::Vector3f> &positions,
		int compression_factor)
	{
		Component::Ptr  component(new Component(&m_grid));
		Eigen::Matrix4f world_to_cam = camera.get_world_to_camera().matrix();
		Eigen::Matrix4f cam_to_world = world_to_cam.inverse();

		std::vector<fribr::float3> points;
		for (int y = 0; y < camera.get_image().rows; ++y)
			for (int x = 0; x < camera.get_image().cols; ++x)
			{
				int xx = x;
				int yy = camera.get_image().rows - y - 1;
				const Eigen::Vector3f &pc = positions[yy * camera.get_image().cols + xx];
				const Eigen::Vector4f  pw = cam_to_world * Eigen::Vector4f(pc.x(), pc.y(), pc.z(), 1.0f);
				points.push_back(fribr::make_float3(pw.head<3>()));
			}

		component->add_camera(fribr::make_float3(camera.get_position()), camera.get_image(), points, camera,
			compression_factor, m_components.size());

		m_all_triangles += component->vertices.size() / 3;

		m_components.push_back(component);
		for (Eigen::Vector3i v : component->slice_voxels)
			m_grid.add_component(v, m_components.size() - 1);

	}

	void PatchCloud::upload()
	{
		clear_gpu();
		if (m_components.empty())
			return;

		size_t total_indices = 0;
		size_t total_vertices = 0;
		size_t total_texcoords = 0;

		for (size_t i = 0; i < m_components.size(); ++i)
		{
			Component::Ptr component = m_components[i];
			component->slices_start.clear();
			for (size_t i = 0; i < component->slice_indices.size(); ++i)
			{
				component->slices_start.push_back(total_indices);
				total_indices += component->slice_indices[i].size();
			}
			component->vertices_start = total_vertices;
			total_vertices += component->vertices.size();
			total_texcoords += component->texcoords.size();
		}

		glGenBuffers(1, &m_index_buffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index_buffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * total_indices, 0, GL_STATIC_DRAW);
		for (size_t i = 0; i < m_components.size(); ++i)
		{
			Component::Ptr component = m_components[i];
			for (size_t i = 0; i < component->slice_indices.size(); ++i)
			{
				std::vector<uint32_t> offset_indices = component->slice_indices[i];
				for (uint32_t &idx : offset_indices)
					idx += component->vertices_start;
				glBufferSubData(GL_ELEMENT_ARRAY_BUFFER,
					component->slices_start[i] * sizeof(uint32_t),
					offset_indices.size() * sizeof(uint32_t),
					offset_indices.data());
			}
		}
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		glGenBuffers(1, &m_vertex_buffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertex_buffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(fribr::float3) * total_vertices, 0, GL_STATIC_DRAW);
		for (size_t i = 0; i < m_components.size(); ++i)
		{
			Component::Ptr component = m_components[i];
			glBufferSubData(GL_ARRAY_BUFFER,
				component->vertices_start * sizeof(fribr::float3),
				component->vertices.size() * sizeof(fribr::float3),
				component->vertices.data());
		}

		glGenBuffers(1, &m_texcoord_buffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_texcoord_buffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(fribr::float3) * total_texcoords, 0, GL_STATIC_DRAW);
		for (size_t i = 0; i < m_components.size(); ++i)
		{
			Component::Ptr component = m_components[i];
			glBufferSubData(GL_ARRAY_BUFFER,
				component->vertices_start * sizeof(fribr::float3),
				component->texcoords.size() * sizeof(fribr::float3),
				component->texcoords.data());
		}

		glGenVertexArrays(1, &m_vao);
		glBindVertexArray(m_vao);

		glBindBuffer(GL_ARRAY_BUFFER, m_vertex_buffer);
		glEnableVertexAttribArray(VERTEX_LOCATION);
		glVertexAttribPointer(VERTEX_LOCATION, 3, GL_FLOAT, 0, 0, 0);

		glBindBuffer(GL_ARRAY_BUFFER, m_texcoord_buffer);
		glEnableVertexAttribArray(TEXCOORD_LOCATION);
		glVertexAttribPointer(TEXCOORD_LOCATION, 3, GL_FLOAT, 0, 0, 0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index_buffer);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		glGenBuffers(1, &m_draw_calls);
		// No need to allocate or upload here. We have no idea how large the buffer is

		glGenTextures(1, &m_camera_pos_texture);
		glBindTexture(GL_TEXTURE_1D, m_camera_pos_texture);
		glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB32F, m_components.size(), 0, GL_RGB, GL_FLOAT, 0);
		glBindTexture(GL_TEXTURE_1D, 0);

		Eigen::Vector2i resolution(m_components[0]->image.cols, m_components[0]->image.rows);
		glGenTextures(1, &m_texture_array);
		glBindTexture(GL_TEXTURE_2D_ARRAY, m_texture_array);

		//use fast 4-byte alignment (default anyway) if possible
		glPixelStorei(GL_UNPACK_ALIGNMENT, (m_components[0]->image.step & 3) ? 1 : 4);

		//set length of one complete row in data (doesn't need to equal image.cols)
		glPixelStorei(GL_UNPACK_ROW_LENGTH, m_components[0]->image.step / m_components[0]->image.elemSize());

		int levels = 1;
		int w = resolution.x() / 2;
		int h = resolution.y() / 2;
		while (h > 1 && w > 1)
		{
			h = std::max(1, h / 2);
			w = std::max(1, w / 2);
			levels++;
		}
		glTexStorage3D(GL_TEXTURE_2D_ARRAY, levels, GL_RGBA8, resolution.x(), resolution.y(), m_components.size());
		for (size_t i = 0; i < m_components.size(); ++i)
		{
			Component::Ptr component = m_components[i];
			cv::Mat temp_image;
			cv::resize(component->image, temp_image,
				cv::Size(resolution.x(), resolution.y()),
				0.0, 0.0, cv::INTER_AREA);
			cv::flip(temp_image, temp_image, 0);
			glTexSubImage3D(GL_TEXTURE_2D_ARRAY,
				0,
				0,
				0,
				i,
				resolution.x(),
				resolution.y(),
				1,
				GL_BGRA,
				GL_UNSIGNED_BYTE,
				temp_image.data);
		}

		glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glBindTexture(GL_TEXTURE_2D_ARRAY, 0);

		// Restore default values
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);

		// Finally, create red boxes for all active voxels in the grid.
		m_all_box_meshes.clear();
		Eigen::Vector3i size = m_grid.size();
		for (int z = 0; z < size.z(); ++z)
			for (int y = 0; y < size.y(); ++y)
				for (int x = 0; x < size.x(); ++x)
				{
					if (m_grid.get_components(Eigen::Vector3i(x, y, z)).empty())
						continue;

					ImageBin bin(x, y, z, m_grid);
					std::vector<float> box = fribr::create_box(bin.bbox_min, bin.bbox_max,
						Eigen::Matrix3f::Identity(),
						Eigen::Vector3f(0.0f, 0.0f, 0.0f));
					m_all_box_meshes.insert(m_all_box_meshes.end(), box.begin(), box.end());
				}
	}

	void PatchCloud::clear_gpu()
	{
		for (int i = 0; i < 4; ++i)
		{
			if (m_gpu_grid[i])
				glDeleteTextures(1, &m_gpu_grid[i]);
			m_gpu_grid[i] = 0;
		}

		if (m_depth_bin_shader)
			delete m_depth_bin_shader;
		m_depth_bin_shader = 0;

		if (m_clear_bin_shader)
			delete m_clear_bin_shader;
		m_clear_bin_shader = 0;

		if (m_texture_array)
			glDeleteTextures(1, &m_texture_array);
		m_texture_array = 0;

		if (m_camera_pos_texture)
			glDeleteTextures(1, &m_camera_pos_texture);
		m_camera_pos_texture = 0;

		if (m_draw_calls)
			glDeleteBuffers(1, &m_draw_calls);
		m_draw_calls = 0;

		if (m_vao)
			glDeleteVertexArrays(1, &m_vao);
		m_vao = 0;

		if (m_index_buffer)
			glDeleteBuffers(1, &m_index_buffer);
		m_index_buffer = 0;

		if (m_vertex_buffer)
			glDeleteBuffers(1, &m_vertex_buffer);
		m_vertex_buffer = 0;

		if (m_texcoord_buffer)
			glDeleteBuffers(1, &m_texcoord_buffer);
		m_texcoord_buffer = 0;
	}

	void PatchCloud::clear()
	{
		m_scene.reset();
		m_meshes.clear();
		clear_voxel_memory();
		clear_gpu();
		m_grid.clear();
		m_components.clear();
		m_all_triangles = 0;
	}

	void PatchCloud::update_fbos_if_needed()
	{
		GLint viewport[4];
		glGetIntegerv(GL_VIEWPORT, viewport);
		Eigen::Vector2i resolution(viewport[2] - viewport[0], viewport[3] - viewport[1]);

		try
		{
			// SP: setup voxel frame buffers [SIBR: RenderTargets / Texture2D]
			if (!m_voxel_fbo)
			{
				fribr::Texture::Descriptor texture_descriptor(GL_CLAMP_TO_EDGE, GL_NEAREST, 0,
					fribr::TextureFormat::RGBA32F);
				std::vector<fribr::Texture::Descriptor> target_descriptors(1, texture_descriptor);
				m_voxel_fbo.reset(new fribr::Framebuffer(resolution, target_descriptors));
			}
			m_voxel_fbo->set_resolution(resolution / 4);

			// SP: setup depth frame buffers [SIBR: RenderTargets / Texture2D]
			if (!m_depth_fbo)
			{
				fribr::Texture::Descriptor texture_descriptor(GL_CLAMP_TO_EDGE, GL_NEAREST, 0,
					fribr::TextureFormat::RGBA32F);
				std::vector<fribr::Texture::Descriptor> target_descriptors(1, texture_descriptor);
				m_depth_fbo.reset(new fribr::Framebuffer(resolution, target_descriptors));
			}
			m_depth_fbo->set_resolution(resolution);

			// SP: setup layer frame buffers [SIBR: RenderTargets / Texture2D]
			for (int i = 0; i < NUM_LAYERS; ++i)
			{				
				if (!m_layers[i])
				{
					fribr::Texture::Descriptor texture_descriptor(GL_CLAMP_TO_EDGE, GL_NEAREST, 0,
						fribr::TextureFormat::RGBA32F);
					std::vector<fribr::Texture::Descriptor> target_descriptors(1, texture_descriptor);
					m_layers[i].reset(new fribr::Framebuffer(resolution, target_descriptors));
				}

				m_layers[i]->set_resolution(resolution);
			}

			#if defined(TF_INTEROP)
			bool should_update = !m_global_fbo || !m_tf_layers[0];
			if (m_tf_interop && should_update)
			{
					fribr::Texture::Descriptor texture_descriptor(GL_CLAMP_TO_EDGE, GL_NEAREST, 0,
																  fribr::TextureFormat::RGBA8);
					std::vector<fribr::Texture::Descriptor> target_descriptors(1, texture_descriptor);
					m_global_fbo.reset(new fribr::Framebuffer(resolution, target_descriptors));
					
			//		//std::vector<fribr::Texture::Ptr> network_textures;
			//		std::vector<std::shared_ptr<sibr::TexInfo>> network_textures;
			//		for (int i = 0; i < NUM_LAYERS + 1; ++i)
			//		{
			//			if (!m_tf_layers[i])
			//			{
			//					fribr::Texture::Descriptor texture_descriptor(GL_CLAMP_TO_EDGE, GL_NEAREST, 0,
			//										  fribr::TextureFormat::RGBA8);
			//					std::vector<fribr::Texture::Descriptor> target_descriptors(1, texture_descriptor);
			//					m_tf_layers[i].reset(new fribr::Framebuffer(resolution, target_descriptors));
			//			}
			//			m_tf_layers[i]->set_resolution(resolution);
			//			network_textures.emplace_back(m_tf_layers[i]->get_textures()[0]);
			//		}
			//	m_tf_interop->set_textures(network_textures);
			}
			#endif
		}
		catch (const std::exception &e)
		{
			std::cerr << "FBO update failed: " << e.what() << std::endl;
		}
	}

	void PatchCloud::draw(float near_plane, float far_plane,
		float ibr_sigma, float resolution_alpha, float depth_alpha,
		const Eigen::Affine3f world_to_camera,
		const Eigen::Matrix4f camera_to_clip, bool show_wireframe)
	{
		for (int i = 0; i < NUM_LAYERS; ++i)
			m_layers[i].reset();

		CHECK_GL_ERROR;
		update_fbos_if_needed();


		//// [SP] First Pass: Render the global mesh to compute voxel visibility
		if (m_scene)
		{
#if defined(TF_INTEROP)
			if (m_algorithm == "deep_blending" && m_use_tf_blending)
			{
				//// [SP] Render the global mesh in m_global_fbo 
				//// [SP] for the 5th layer to be used for blending
				m_global_fbo->bind();
				glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // TODO (Peter) Make this white for outdoor scenes like boats.
//				glClearColor(1.0f, 1.0f, 1.0f, 1.0f); // TODO (Peter) Make this white for outdoor scenes like boats.
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				glEnable(GL_DEPTH_TEST);
			}
			m_scene->draw(world_to_camera, camera_to_clip, fribr::Scene::OnlyTextures);
			if (m_algorithm == "deep_blending" && m_use_tf_blending)
				m_global_fbo->unbind();
#endif
		}

		CHECK_GL_ERROR;
		

#if DO_PROFILING
		glFinish();
#endif
		//// [SP] Fetch the voxels covered by the novel view in bins
		std::vector<PatchCloud::ImageBin> bins = fetch_image_bins(world_to_camera, camera_to_clip);
		CHECK_GL_ERROR;

#if DO_PROFILING
		glFinish();
		TIC();
#endif
		//// [SP] End of First render pass
		//// [SP] -----------------------------------------------------------------------------------
		//// [SP] Second Pass: Rank the input tiles in each grid and draw the mosaics
		Eigen::Vector3f camera_position = m_fake_camera_position;
		if (!m_use_fake_position)
		{
			Eigen::Affine3f camera_to_world = world_to_camera.inverse();
			camera_position = (camera_to_world * Eigen::Vector4f(0.0f, 0.0f, 0.0f, 1.0f)).head<3>();
		}
		CHECK_GL_ERROR;
		struct CostIndex
		{
			float  cost;
			size_t index;
			CostIndex(float c, size_t i) : cost(c), index(i) {}
			bool operator<(const CostIndex& o)  const { return cost < o.cost; }
			bool operator>=(const CostIndex& o) const { return cost >= o.cost; }
		};

		struct DrawElementsIndirectCommand {
			uint32_t count;
			uint32_t instanceCount;
			uint32_t firstIndex;
			uint32_t baseVertex;
			uint32_t baseInstance;
		};
		std::vector<DrawElementsIndirectCommand> commands(m_components.size() * bins.size());
		size_t command_index = 0;
		size_t total_triangles = 0;

		// For debug visualization. If the user wants to only show a certain number of tiles
		// from the current local mesh.
		size_t max_bins = bins.size();
		int    filled_bins = 0;
		for (size_t i = 0; i < bins.size(); ++i)
		{
			if (m_max_tiles < 0 || m_selected_camera < 0)
				break;

			bool found_selected = false;
			for (int j : m_grid.get_components(bins[i].voxel))
			{
				found_selected = j == m_selected_camera;
				if (found_selected)
					break;
			}
			if (!found_selected)
				continue;

			filled_bins++;
			if (filled_bins >= m_max_tiles)
			{
				max_bins = i;
				break;
			}
		}


		//// [SP] For each visible voxel, run the loop
#ifdef NDEBUG
#pragma omp parallel for
#endif
#ifdef INRIA_WIN
		for (int i = 0; i < max_bins; ++i)
#else
		for (size_t i = 0; i < max_bins; ++i)
#endif
		{
			PatchCloud::ImageBin   &bin = bins[i];
			std::vector<CostIndex>  bin_list;

			Eigen::Array3f center = (bin.bbox_max + bin.bbox_min) * 0.5f;
			Eigen::Array3f delta = (bin.bbox_max - bin.bbox_min) * 0.5f;
			float r = delta.matrix().norm();

			const Eigen::Array3f zero(0.0f, 0.0f, 0.0f);
			Eigen::Array3f virt_to_center = camera_position.array() - center;
			Eigen::Array3f min_diff_virt = (virt_to_center.abs() - delta).max(zero);
			float          min_dist_to_virt = min_diff_virt.matrix().norm();
			float          v2c_dist = virt_to_center.matrix().norm();
			float          inv_v2c_dist = 1.0f / v2c_dist;

			//// [SP] For each component in the voxel compute the IBRCost for the component
			//// [SP] Used for [Hedman 16] Sec. 5.2.2 Prioritization
			for (int j : m_grid.get_components(bin.voxel))
			{
				if (j == m_ignored_camera ||
					(m_selected_camera >= 0 &&
						m_selected_camera < static_cast<int>(m_components.size()) &&
						m_selected_camera != j))
					continue;

				Eigen::Array3f real_to_center = m_components[j]->position.array() - center;
				Eigen::Array3f max_diff_real = real_to_center.abs() + delta;
				float          max_dist_to_real = max_diff_real.matrix().norm();

				Eigen::Vector3f virt_to_real = (m_components[j]->position - camera_position).normalized();
				float min_angle_virt = 0.0f;
				if (r < v2c_dist)
					min_angle_virt = acosf(-virt_to_center.matrix().dot(virt_to_real) * inv_v2c_dist)
					- asinf(r * inv_v2c_dist);

				float min_angle_real = 0.0f;
				float r2c_dist = real_to_center.matrix().norm();
				float inv_r2c_dist = 1.0f / r2c_dist;
				if (r < r2c_dist)
					min_angle_real = acosf(-real_to_center.matrix().dot(-virt_to_real) * inv_r2c_dist)
					- asinf(r * inv_r2c_dist);

				float max_angle_real_virt = 3.1415926536f - min_angle_real - min_angle_virt;
				float max_distance_cost = std::max(0.0f, 1.0f - min_dist_to_virt / max_dist_to_real);

				float max_cost = (1.0f - resolution_alpha) * max_angle_real_virt / 3.1415926536f +
					resolution_alpha * max_distance_cost;
				bin_list.push_back(CostIndex(max_cost, j));
			}

			// [Hedman 16] Sec* 5.2.2: sort the input tiles within each visible grid cell according to their worst-case IBR cost
			std::sort(bin_list.begin(), bin_list.end());


			//// [SP] Pack all draw command in a single glDrawIndirect instruction
			//// [SP] Stop drawing the tiles based on cluster_threshold [Hedman 16] Sec. 5.2.3
			int valid_voxels = 0;
			for (size_t k = 0; k < bin_list.size(); ++k)
			{
				size_t component_index = bin_list[k].index;

				size_t linear_idx = m_grid.linear_index(bins[i].voxel);
				size_t slice_index = m_components[component_index]->voxel_to_index_remap.find(linear_idx)->second;

				DrawElementsIndirectCommand command;
				command.count = m_components[component_index]->slice_indices[slice_index].size();
				command.instanceCount = 1;
				command.firstIndex = m_components[component_index]->slices_start[slice_index];
				command.baseVertex = 0;
				command.baseInstance = 0;


				size_t ci = doAtomicOp(&command_index, 1);
				commands[ci] = command;

				doAtomicOp(&total_triangles, command.count / 3);

				if (m_components[component_index]->is_valid(bins[i].voxel))
					valid_voxels++;

				//// [SP] Stop drawing until m_cluster_threshold tiles (paper:3) is drawn or m_cluster_k tiles (paper:12) have been drawn
				if (valid_voxels >= m_cluster_threshold || k + 1 >= static_cast<size_t>(m_cluster_k))
					break;
			}
		}
#if DO_PROFILING
		TOC("COST SORTING", 1);
		RETIC();
#endif
		CHECK_GL_ERROR;
		std::vector<fribr::float3> component_positions(m_components.size());
		for (size_t i = 0; i < m_components.size(); ++i)
		{
			Component::Ptr component = m_components[i];
			component_positions[i] = fribr::make_float3(component->position);
		}
		CHECK_GL_ERROR;
		glBindTexture(GL_TEXTURE_1D, m_camera_pos_texture);
		glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB32F, m_components.size(),
			0, GL_RGB, GL_FLOAT, component_positions.data());
		glBindTexture(GL_TEXTURE_1D, 0);

		//// [SP] Issue actual drawing command using glDrawIndirect
		glBindBuffer(GL_DRAW_INDIRECT_BUFFER, m_draw_calls);
		CHECK_GL_ERROR;
		glBufferData(GL_DRAW_INDIRECT_BUFFER, command_index * sizeof(DrawElementsIndirectCommand),
			commands.data(), GL_STATIC_DRAW);
		CHECK_GL_ERROR;
#if DO_PROFILING
		TOC("UPLOADING BATCHES", 1);
		RETIC();
#endif

		m_depth_fbo->bind();

		glDisable(GL_BLEND);
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);

		fribr::Shader* depth_shader = s_patch_depth_shader;
		depth_shader->use();
		depth_shader->set_uniform("world_to_camera", world_to_camera);
		depth_shader->set_uniform("camera_to_clip", camera_to_clip);

		glBindVertexArray(m_vao);
		glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, 0, command_index, 0);
		glBindVertexArray(0);

#if DO_PROFILING
		TOC("DEPTH PASS (CPU)", 1);
		RETIC();
		glFinish();
		TOC("DEPTH PASS (GPU)", 1);
		RETIC();
#else
		std::cout << "\rDrawing " << total_triangles << "/" << m_all_triangles << " ("
			<< total_triangles * 100.0f / m_all_triangles << "%)               " << std::flush;
#endif

		m_depth_fbo->unbind();
		glUseProgram(0);
		CHECK_GL_ERROR;
		m_layers[NUM_LAYERS - 1]->bind();
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		m_layers[NUM_LAYERS - 1]->unbind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D_ARRAY, m_texture_array);
		glBindVertexArray(m_vao);

		//// [SP] Define a render pass whcih can be used with multiple shaders to get the desired layers as required
		auto render_pass = [&](int layer_index, fribr::Shader *shader)
		{
			m_layers[layer_index]->bind();

			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			shader->use();
			shader->set_uniform("world_to_camera", world_to_camera);
			shader->set_uniform("camera_to_clip", camera_to_clip);
			shader->set_uniform("diffuse_sampler", 0);
			shader->set_uniform("depth_sampler", 1);
			shader->set_uniform("previous_sampler", 2);
			shader->set_uniform("camera_sampler", 3);
			shader->set_uniform("num_cameras", static_cast<int>(m_components.size()));
			shader->set_uniform("output_camera_position", camera_position);
			shader->set_uniform("sigma", ibr_sigma);
			shader->set_uniform("resolution_alpha", resolution_alpha);
			shader->set_uniform("depth_alpha", depth_alpha);

			glActiveTexture(GL_TEXTURE1);
			m_depth_fbo->get_textures()[0]->bind();

			glActiveTexture(GL_TEXTURE2);
			int prev_layer = layer_index == 0 ? NUM_LAYERS - 1 : layer_index - 1;
			m_layers[prev_layer]->get_textures()[0]->bind();

			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_1D, m_camera_pos_texture);

			glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, 0, command_index, 0);

			m_layers[layer_index]->unbind();
		};

		fribr::Shader::Ptr peeling_shader = m_shader_builder.peeling_shader();

		// We just want to show one of the top K images.
		if (m_display_layer > 0
#if defined(TF_INTEROP)
			|| (m_algorithm == "deep_blending" && m_use_tf_blending)
#endif
			)
		{
			//// [SP] Render Pass to get/show Top K mosaics or for running the blend network
			peeling_shader->use();
			peeling_shader->set_uniform("display_mode", static_cast<int>(m_display_mode));

			// For flow computation
			Eigen::Vector2f resolutionf = m_depth_fbo->get_resolution().cast<float>();
			peeling_shader->set_uniform("old_world_to_camera", m_old_world_to_camera);
			peeling_shader->set_uniform("resolution", resolutionf);

			//// [SP] Use the peeling shader to get each of the mosaics 
			for (int i = 0; i < NUM_LAYERS; ++i)
				render_pass(i, peeling_shader.get());

			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_1D, 0);
			glActiveTexture(GL_TEXTURE2);
			fribr::Texture::unbind();
			glActiveTexture(GL_TEXTURE1);
			fribr::Texture::unbind();
			glUseProgram(0);
		}
		else // Standard Inside-Out rendering
		{
			fribr::Shader::Ptr blend_shader = m_shader_builder.blend_shader();

			//// [SP] Do the Inside-Out multi-pass rendering without rendering the mosaics 
			//// [SP] We can select either DB depth cost or the fuzzy depth test as done in IO 
			//// [SP] by setting the cost parameter [shader_builder().depth_cost_index(_depthIndex);]
			if (m_shader_builder.use_insideout_multipass())
			{
				//// [SP] First pass is used to get the IBRCost for each fragment
				peeling_shader->use();
				peeling_shader->set_uniform("display_mode", static_cast<int>(Colors));
				render_pass(0, peeling_shader.get());
			}
			CHECK_GL_ERROR;
			glDisable(GL_DEPTH_TEST);
			glEnable(GL_BLEND);
			glBlendFunc(GL_ONE, GL_ONE);
			if (show_wireframe)
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			
			//// [SP] Finally, do the blend pass to blend the colors selected for each fragment
			render_pass(1, blend_shader.get());

			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_1D, 0);
			glActiveTexture(GL_TEXTURE2);
			fribr::Texture::unbind();
			glActiveTexture(GL_TEXTURE1);
			fribr::Texture::unbind();
			glUseProgram(0);
		}

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDisable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
		glBindVertexArray(0);
		CHECK_GL_ERROR;
#if DO_PROFILING
		TOC("PEELING (CPU)", 1);
		RETIC();
		glFinish();
		TOC("PEELING (GPU)", 1);
		RETIC();
#endif

#if defined(TF_INTEROP)
		if (m_algorithm == "deep_blending" && m_use_tf_blending)
		{
			//// [SP] Run the blend network after attaching the frame buffer layers to the TF layers
			s_tf_blit_shader->use();
			glActiveTexture(GL_TEXTURE1);
			m_global_fbo->get_textures()[0]->bind();
			s_tf_blit_shader->set_uniform("global_sampler", 1);
			//_input_textures.resize(NUM_LAYERS + 1);
			for (int i = 0; i < NUM_LAYERS + 1; i++)
			{
				GLint m_old_viewport[4];
				GLint m_old_fbo;
				glGetIntegerv(GL_VIEWPORT, m_old_viewport);
				glGetIntegerv(GL_FRAMEBUFFER_BINDING, &m_old_fbo);

				m_tf_layers[i]->bind();
				glViewport(0, 0, m_tf_layers[i]->w(), m_tf_layers[i]->h());
				//glBindTexture(GL_TEXTURE_2D, _input_textures[i]->handle);
				s_tf_blit_shader->set_uniform("layer", i);

				glActiveTexture(GL_TEXTURE0);
				if (i < NUM_LAYERS)
					m_layers[i]->get_textures()[0]->bind();
				s_tf_blit_shader->set_uniform("diffuse_sampler", 0);

				glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				glDisable(GL_DEPTH_TEST);

				//fribr::draw_full_screen_quad();
				sibr::RenderUtility::renderScreenQuad();
				//fribr::Texture::unbind();
				m_tf_layers[i]->unbind();
				
				// restore previous fb
				glBindFramebuffer(GL_FRAMEBUFFER, m_old_fbo);
				glViewport(m_old_viewport[0], m_old_viewport[1],
					m_old_viewport[2], m_old_viewport[3]);

			}
			CHECK_GL_ERROR;

#if DO_PROFILING
			TOC("FIRST COPY (CPU)", 1);
			RETIC();
			glFinish();
			TOC("FIRST COPY (GPU)", 1);
#endif
			// Inria
			//glActiveTexture(GL_TEXTURE1);
			//fribr::Texture::unbind();
			//glActiveTexture(GL_TEXTURE0);
			//glUseProgram(0);
			CHECK_GL_ERROR;
			glFinish();
			m_tf_interop->window()->makeContextNull();
			m_tf_interop->run(_run_node_names);
			m_tf_interop->window()->makeContextCurrent();
			CHECK_GL_ERROR;
#if DO_PROFILING
			TOC("TENSORFLOW (CPU)", 1);
			RETIC();
			glFinish();
			TOC("TENSORFLOW (GPU)", 1);
#endif
			CHECK_GL_ERROR;
			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			s_tf_blit_shader->use();
			glActiveTexture(GL_TEXTURE1);
			CHECK_GL_ERROR;

			//m_tf_interop->get_output_texture()->bind();
			glBindTexture(GL_TEXTURE_2D, _output_textures[0]->handle());
			//m_global_fbo->get_textures()[0]->bind();

			CHECK_GL_ERROR;
			s_tf_blit_shader->set_uniform("global_sampler", 1);
			s_tf_blit_shader->set_uniform("diffuse_sampler", 0);
			s_tf_blit_shader->set_uniform("layer", 6);
			//fribr::draw_full_screen_quad();
			sibr::RenderUtility::renderScreenQuad();
			CHECK_GL_ERROR;
			glUseProgram(0);
			glActiveTexture(GL_TEXTURE1);
			fribr::Texture::unbind();
			glActiveTexture(GL_TEXTURE0);
			glEnable(GL_DEPTH_TEST);
			CHECK_GL_ERROR;
#if DO_PROFILING
			TOC("FINAL COPY (CPU)", 1);
			RETIC();
			glFinish();
			TOC("FINAL COPY (GPU)", 1);
#endif
		}
		else
#endif
			// We just want to show one of the top K images.
			if (m_display_layer > 0)
			{
				s_dump_shader->use();
				glClear(GL_DEPTH_BUFFER_BIT);
				glEnable(GL_DEPTH_TEST);
				CHECK_GL_ERROR;
				for (int i = 0; i < NUM_LAYERS; ++i)
				{
					glActiveTexture(GL_TEXTURE0 + i);
					m_layers[i]->get_textures()[0]->bind();

					std::stringstream sampler_stream;
					sampler_stream << "sampler_" << i;
					s_dump_shader->set_uniform(sampler_stream.str(), i);
				}
				CHECK_GL_ERROR;
				int selected_layer = m_display_layer - 1;
				s_dump_shader->set_uniform("selected_layer", selected_layer);
				//fribr::draw_full_screen_quad();
				sibr::RenderUtility::renderScreenQuad();
				CHECK_GL_ERROR;
				for (int i = NUM_LAYERS - 1; i >= 0; --i)
				{
					glActiveTexture(GL_TEXTURE0 + i);
					fribr::Texture::unbind();
				}
				glUseProgram(0);
				CHECK_GL_ERROR;
#if DO_PROFILING
				TOC("LAYER DUMP (CPU)", 1);
				RETIC();
				glFinish();
				TOC("LAYER DUMP (GPU)", 1);
#endif
			}
			else // Standard Inside-Out rendering
			{
				//// [SP] Normalize the final output of the framebuffer
				s_average_shader->use();
				glClear(GL_DEPTH_BUFFER_BIT);
				glEnable(GL_DEPTH_TEST);

				glActiveTexture(GL_TEXTURE0);
				m_layers[1]->get_textures()[0]->bind();
				s_average_shader->set_uniform("weighted_average_sampler", 0);
				//fribr::draw_full_screen_quad();
				sibr::RenderUtility::renderScreenQuad();
				CHECK_GL_ERROR;
				fribr::Texture::unbind();
				glUseProgram(0);
#if DO_PROFILING
				TOC("INSIDEOUT BLENDING (CPU)", 1);
				RETIC();
				glFinish();
				TOC("INSIDEOUT BLENDING (GPU)", 1);
#endif
			}

	}

	// From http://stackoverflow.com/a/33007089
#ifdef INRIA_WIN
#define NOMINMAX
#include <Windows.h>
// https://stackoverflow.com/questions/355967/how-to-use-msvc-intrinsics-to-get-the-equivalent-of-this-gcc-code/20468180#20468180
// https://msdn.microsoft.com/en-us/library/wfd9z0bb.aspx
	size_t __inline ctz(uint64_t x)
	{
		unsigned long index;
		return _BitScanForward64(&index, x) ? index : 64;
	}

#else
	inline size_t ctz(uint64_t x) { return x ? __builtin_ctzll(x) : 64; }
#endif
	inline size_t next_bit(uint64_t bf, size_t bit) { return ctz(bf & ~((1UL << bit) - 1)); }

	
	std::vector<PatchCloud::ImageBin> PatchCloud::fetch_image_bins(const Eigen::Affine3f world_to_camera,
		const Eigen::Matrix4f camera_to_clip)
	{
#if DO_PROFILING
		TIC();
#endif
		upload_texture_image_if_needed();

		update_fbos_if_needed();

		//// [SP] Render the global mesh and get the world space coordinate of each vertex in voxel_fbo
		m_voxel_fbo->bind();

		glDisable(GL_BLEND);
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);

		s_scene_geometry_shader->use();
		s_scene_geometry_shader->set_uniform("world_to_camera", world_to_camera);
		s_scene_geometry_shader->set_uniform("camera_to_clip", camera_to_clip);

		//// [SP] Render the global scene mesh in current voxel fbo 
		for (fribr::Mesh::Ptr mesh : m_meshes)
			mesh->draw();

		m_voxel_fbo->unbind();
		glUseProgram(0);

#if DO_PROFILING
		TOC("GEOMETRY BUFFER (CPU)", 1);
		RETIC();
		glFinish();
		TOC("GEOMETRY BUFFER (GPU)", 1);
		RETIC();
#endif

		//// [SP] Initialize the frame buffer to get the voxel locations for the given view
		m_clear_bin_shader->use();

		const std::string bin_names[] = {
			std::string("gpu_grid_a"),
			std::string("gpu_grid_b"),
			std::string("gpu_grid_c"),
			std::string("gpu_grid_d"),
		};
		for (int i = 0; i < 4; ++i)
		{
			m_clear_bin_shader->set_uniform(bin_names[i], i);
			glBindImageTexture(i, m_gpu_grid[i], 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32UI);
		}

		m_clear_bin_shader->set_uniform("grid_min", Eigen::Vector3f(m_grid.min().matrix()));
		m_clear_bin_shader->set_uniform("grid_max", Eigen::Vector3f(m_grid.max().matrix()));
		m_clear_bin_shader->set_uniform("resolution", Eigen::Vector2i(m_voxel_fbo->get_resolution()));

		//fribr::draw_full_screen_quad();
		sibr::RenderUtility::renderScreenQuad();

		CHECK_GL_ERROR;
		// Now voxelize the framebuffer
		//// [SP] Use the vertex coordinates fetched by rendering the mesh to compute the masks specifying which voxels are visible and which are not
		//// [SP] The voxel masks are stored in 4 32bit Unsigned int Images gpu_grid_a/b/c/d
		//// [SP] THe maximum grid size allowed according to the grid computation is 128 on the X-axis 
		//// [SP] while unconstrained on Y and Z axis (as long as the images can fit on the GPU storage)
		m_depth_bin_shader->use();
		glActiveTexture(GL_TEXTURE0);
		m_depth_bin_shader->set_uniform("position_sampler", 0);
		m_voxel_fbo->get_textures()[0]->bind();

		for (int i = 0; i < 4; ++i)
		{
			m_depth_bin_shader->set_uniform(bin_names[i], i);
			glBindImageTexture(i, m_gpu_grid[i], 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
		}
		CHECK_GL_ERROR;
		m_depth_bin_shader->set_uniform("grid_min", Eigen::Vector3f(m_grid.min().matrix()));
		m_depth_bin_shader->set_uniform("grid_max", Eigen::Vector3f(m_grid.max().matrix()));
		//// [SP] Used to inflate the frame buffer pixels into cube of 5cm to account for mismatch in global and pv geometry
		m_depth_bin_shader->set_uniform("tolerance", 0.05f);
		sibr::RenderUtility::renderScreenQuad();
		//fribr::draw_full_screen_quad();
		CHECK_GL_ERROR;
		for (int i = 0; i < 4; ++i)
			glBindImageTexture(i, 0, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);

		fribr::Texture::unbind();
		glUseProgram(0);
		CHECK_GL_ERROR;
#if DO_PROFILING
		TOC("VOXELIZATION (CPU)", 1);
		RETIC();
#endif

		// Download bit mask from GPU to CPU
		std::vector<int32_t> gpu_grid[4] = {
			std::vector<int32_t>(m_grid.size().y() * m_grid.size().z()),
			std::vector<int32_t>(m_grid.size().y() * m_grid.size().z()),
			std::vector<int32_t>(m_grid.size().y() * m_grid.size().z()),
			std::vector<int32_t>(m_grid.size().y() * m_grid.size().z())
		};
		for (int i = 0; i < 4; ++i)
		{
			glBindTexture(GL_TEXTURE_2D, m_gpu_grid[i]);
			glGetTexImage(GL_TEXTURE_2D, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, (void*)gpu_grid[i].data());
		}
		glBindTexture(GL_TEXTURE_2D, 0);

#if DO_PROFILING
		TOC("DOWNLOADED VOXELS", 1);
		RETIC();
#endif
		CHECK_GL_ERROR;

		//// [SP] Inflate the grid on the CPU and mark the visible grid to be true
		std::vector<bool> fat_grid(m_grid.size().x() * m_grid.size().y() * m_grid.size().z(), false);
		for (int bin_id = 0; bin_id < 4; ++bin_id)
			for (int bin_z = 0; bin_z < m_grid.size().z(); ++bin_z)
				for (int bin_y = 0; bin_y < m_grid.size().y(); ++bin_y)
				{
					size_t bf = gpu_grid[bin_id][bin_y + m_grid.size().y() * bin_z];

					int db_cnt2 = 0;
					size_t prev_bit = 0;

					for (size_t bit = next_bit(bf, 0); bit < 32; bit = next_bit(bf, bit + 1))
					{
						db_cnt2++;
						if (db_cnt2 > 32) {
							//std::cerr << "ERROR bit= " << bit << " prev bit = " << prev_bit << " dbcnt  " << db_cnt2 << " BF = " << bf << std::endl;
							break;
						}
						int bin_x = bin_id * 32 + bit;
						for (int dz = -1; dz <= 1; ++dz)
							for (int dy = -1; dy <= 1; ++dy)
								for (int dx = -1; dx <= 1; ++dx)
								{
									int xx = bin_x + dx;
									int yy = bin_y + dy;
									int zz = bin_z + dz;
									if (xx < 0 || yy < 0 || zz < 0 ||
										xx >= m_grid.size().x() ||
										yy >= m_grid.size().y() ||
										zz >= m_grid.size().z())
										continue;

									int bin_idx = xx + m_grid.size().x() * (yy + m_grid.size().y() * zz);
									fat_grid[bin_idx] = true;
								}
						prev_bit = bit;
					}

					//if (db_cnt2 <= 32) {
					//	//std::cerr << "ERROR bit= " << bit << " prev bit = " << prev_bit << " dbcnt  " << db_cnt2 << " BF = " << bf << std::endl;
					//	std::cerr << "dbcount hit " << db_cnt2 << std::endl;
					//}
				}

		//// [SP] Store the visible grids in bins for rendering
		std::vector<PatchCloud::ImageBin> bins;
		for (int bin_z = 0; bin_z < m_grid.size().z(); ++bin_z)
			for (int bin_y = 0; bin_y < m_grid.size().y(); ++bin_y)
				for (int bin_x = 0; bin_x < m_grid.size().x(); ++bin_x)
				{
					int bin_idx = bin_x + m_grid.size().x() * (bin_y + m_grid.size().y() * bin_z);
					bool not_here = !fat_grid[bin_idx];
					for (int i = 0; i < VOXEL_MEMORY_SIZE; ++i)
					{
						not_here = not_here && (m_voxel_memory[i].empty() || !m_voxel_memory[i][bin_idx]);
					}
					if (!not_here)
						bins.push_back(PatchCloud::ImageBin(bin_x, bin_y, bin_z, m_grid));
				}

		//// [SP] Keep the previous grid selection to ensure temporal coherency
		if (m_update_voxel_memory)
		{
			for (int i = 1; i < VOXEL_MEMORY_SIZE; ++i)
				m_voxel_memory[i - 1].swap(m_voxel_memory[i]);
			m_voxel_memory[VOXEL_MEMORY_SIZE - 1].swap(fat_grid);
		}

#if DO_PROFILING
		TOC("COMPACTED VOXELS", 1);
#endif
		CHECK_GL_ERROR;
		return bins;
	}

	void PatchCloud::create_boxes(const Eigen::Affine3f world_to_camera,
		const Eigen::Matrix4f camera_to_clip)
	{
		std::vector<PatchCloud::ImageBin> bins = fetch_image_bins(world_to_camera, camera_to_clip);
		m_current_box_meshes.clear();
		for (PatchCloud::ImageBin &bin : bins)
		{
			std::vector<float> box = fribr::create_box(bin.bbox_min, bin.bbox_max,
				Eigen::Matrix3f::Identity(),
				Eigen::Vector3f(0.0f, 0.0f, 0.0f));
			m_current_box_meshes.insert(m_current_box_meshes.end(), box.begin(), box.end());
		}
	}

	void PatchCloud::draw_boxes(const Eigen::Affine3f world_to_camera,
		const Eigen::Matrix4f camera_to_clip)
	{

		CHECK_GL_ERROR;
		

		CHECK_GL_ERROR;
		if (m_all_box_meshes.empty())
			return;

		CHECK_GL_ERROR;
		glDisable(GL_CULL_FACE);
		s_box_shader->use();
		s_box_shader->set_uniform("world_to_camera", world_to_camera);
		s_box_shader->set_uniform("camera_to_clip", camera_to_clip);

		GLuint vid, vbo;
		glGenVertexArrays(1, &vid);
		glBindVertexArray(vid);
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, m_all_box_meshes.size() * sizeof(float), m_all_box_meshes.data(), GL_DYNAMIC_DRAW);

		CHECK_GL_ERROR;
		glDepthMask(GL_FALSE); // Disable writes to the depth buffer
		glEnableVertexAttribArray(0);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		CHECK_GL_ERROR;
		s_box_shader->set_uniform("color", Eigen::Vector3f(0.75f, 0.25f, 0.25f));
		CHECK_GL_ERROR;
		glVertexAttribPointer(0, 3, GL_FLOAT, 0, 0, 0);
		CHECK_GL_ERROR;
		glDrawArrays(GL_TRIANGLES, 0, m_all_box_meshes.size() / 3);

		CHECK_GL_ERROR;
		s_box_shader->set_uniform("color", Eigen::Vector3f(0.25f, 0.75f, 0.25f));
		glBufferData(GL_ARRAY_BUFFER, m_current_box_meshes.size() * sizeof(float), m_current_box_meshes.data(), GL_DYNAMIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, 0, 0, 0);
		glDrawArrays(GL_TRIANGLES, 0, m_current_box_meshes.size() / 3);

		CHECK_GL_ERROR;
		// Restore modified GL state.
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDisableVertexAttribArray(0);
		glDepthMask(GL_TRUE);
		CHECK_GL_ERROR;
		glBindVertexArray(0);
		glDeleteVertexArrays(1, &vid);
		glUseProgram(0);
		glEnable(GL_CULL_FACE);
		CHECK_GL_ERROR;
	}

	void PatchCloud::set_scene(fribr::Scene::Ptr scene)
	{
		m_grid.set_scene(scene);
		m_meshes = scene->get_meshes();
		m_scene = scene;
	}

	void PatchCloud::set_scene(fribr::Scene::Ptr scene, Eigen::Array3f grid_min, Eigen::Array3f grid_max)
	{
		set_scene(scene);
		m_grid.min(grid_min);
		m_grid.max(grid_max);
	}

	void PatchCloud::save(const std::string &filename) const
	{
		std::ofstream out_file(filename, std::ofstream::trunc | std::ofstream::binary);
		
		if (!out_file.good())
		{
			std::cout << "Could not open file for writing: " << filename << std::endl;
			return;
		}
		out_file << "PC08" << std::endl;

		write_float3(out_file, fribr::make_float3(Eigen::Vector3f(m_grid.min().matrix())));
		write_float3(out_file, fribr::make_float3(Eigen::Vector3f(m_grid.max().matrix())));
		write_int3(out_file, fribr::make_int3(m_grid.size()));

		// Begin components
		write_int(out_file, m_components.size());
		for (Component::Ptr component : m_components)
			component->save(out_file);
	}

	bool PatchCloud::load(const std::string &filename)
	{
		if (!fribr::file_exists(filename))
		{
			std::cout << "No such file: " << filename << std::endl;
			return false;
		}
		std::ifstream in_file(filename, std::ifstream::in | std::ifstream::binary);

		std::string line;
		std::getline(in_file, line);
		if (line.substr(0, 4) != "PC07" && line.substr(0, 4) != "PC08")
		{
			std::cout << "Invalid file format: " << filename << std::endl;
			return false;
		}
		clear_gpu();
		clear_voxel_memory();

		Eigen::Vector3f grid_min = fribr::make_vec3f(read_float3(in_file)).array();
		Eigen::Vector3f grid_max = fribr::make_vec3f(read_float3(in_file)).array();
		Eigen::Vector3i grid_size = fribr::make_vec3i(read_int3(in_file));
		m_grid.reset(grid_min, grid_max, grid_size);

		// Begin components
		m_all_triangles = 0;
		m_components.clear();
		int num_components = read_int(in_file);
		for (int i = 0; i < num_components; ++i)
		{
			Component::Ptr component(new Component(&m_grid));
			component->load(in_file);
			m_components.push_back(component);
			m_all_triangles += component->vertices.size() / 3;
			for (Eigen::Vector3i v : component->slice_voxels)
				m_grid.add_component(v, m_components.size() - 1);
		}
		upload();

		return true;
	}

}
