/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "viewer_state.h"
#include "projects/fribr_framework/renderer/tools/string_tools.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdint>
#include <iomanip>

namespace
{

struct CameraParameters
{
    float   fx, fy;
    float   dx, dy;
    int32_t w, h;
    float k[3];
    float o[9];
    float p[3];

    // For std::vector, leave everything uninitialized.
    CameraParameters()
    {
    }

    CameraParameters(const fribr::CalibratedImage& image)
        : fx(image.get_focal_x()), 
          fy(image.get_focal_y()),
          dx(image.get_dx()), 
          dy(image.get_dy()),
          w(image.get_resolution().x()), 
          h(image.get_resolution().y())
    {
        Eigen::Vector3f _k = image.get_k();
        for (int i = 0; i < 3; ++i)
            k[i] = _k[i];

        Eigen::Matrix3f _o = image.get_orientation();
        for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 3; ++j)
            o[i * 3 + j] = _o(i, j);

        Eigen::Vector3f _p = image.get_position();
        for (int i = 0; i < 3; ++i)
            p[i] = _p[i];
    }

    fribr::CalibratedImage make_image(const std::string &image_path)
    {
        Eigen::Matrix3f _o;
        for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 3; ++j)
            _o(i, j) = o[i * 3 + j];

        return fribr::CalibratedImage(fx, fy, dx, dy,
                                      Eigen::Vector3f(k[0], k[1], k[2]),
                                      _o, 
                                      Eigen::Vector3f(p[0], p[1], p[2]),
                                      Eigen::Vector2i(w, h), 
                                      fribr::Texture::Ptr(), image_path);
    }
};

}

namespace patcher
{

bool load_viewer_state(const std::string                     &config_path,
                       fribr::DragCamera                     *camera,
                       PatchCloud                            *cloud,
                       fribr::Scene::Ptr                     *scene,
                       fribr::CalibratedImage::Vector        *calibrated_cameras)
{
    std::ifstream meta_stream(config_path, std::ios_base::in);
    if (!meta_stream.good())
    {
        std::cerr << "Could not open insideout scene file for reading: " << config_path << std::endl;
        return false;
    }

    // First load the scene variables stored in the meta file.
    Eigen::Vector3f up, forward, center, normal, lookat;

    meta_stream >> up.x()      >> up.y()      >> up.z();
    meta_stream >> forward.x() >> forward.y() >> forward.z();
    meta_stream >> center.x()  >> center.y()  >> center.z();
    meta_stream >> normal.x()  >> normal.y()  >> normal.z();
    meta_stream >> lookat.x()  >> lookat.y()  >> lookat.z();

    *camera = fribr::DragCamera(camera->get_near(),
				camera->get_far(),
				camera->get_fov(),
				camera->get_aspect(),
				center, normal, lookat);
    camera->set_forward(forward);
    camera->set_up(up);

    const std::string input_directory = fribr::strip_filename(config_path);

    // Load the camera parameters from our own, quick, hacky binary format.
    const std::string camera_path = input_directory + "/cameras.bin";
    std::ifstream camera_stream(camera_path, std::ios_base::in | std::ios_base::binary);

    int32_t num_cameras = 0;
    camera_stream.read(reinterpret_cast<char*>(&num_cameras), sizeof(num_cameras));

    std::vector<CameraParameters> camera_parameters(num_cameras);
    camera_stream.read(reinterpret_cast<char*>(camera_parameters.data()),
                       sizeof(CameraParameters) * num_cameras);
    if (!camera_stream.good())
    {
        std::cerr << "Could not open insideout camera file for reading: " << camera_path << std::endl;
        return false;
    }

    calibrated_cameras->clear();
    for (int i = 0; i < num_cameras; ++i)
    {
        std::stringstream name_stream;
        name_stream << std::setw(3) << i << ".jpg";
        calibrated_cameras->push_back(camera_parameters[i].make_image(name_stream.str()));
    }

    // Try to load the 3D scene geometry
    const std::string scene_path = input_directory + "/scene.geom";
    std::ifstream scene_stream(scene_path, std::ios_base::in | std::ios_base::binary);
    if (!scene_stream.good())
    {
        std::cerr << "Could not open insideout camera scene for reading: " << scene_path << std::endl;
        return false;
    }

    // Seek to EOF to find the file size (in bytes).
    scene_stream.seekg(0, scene_stream.end);
    size_t scene_size = scene_stream.tellg();

    // Read everything as a big binary blob.
    std::vector<char> scene_data(scene_size);
    scene_stream.seekg(0, scene_stream.beg);
    scene_stream.read(scene_data.data(), scene_size);
    
    // .. And deserialize this binary blob into a Scene object.
    *scene = fribr::Scene::deserialize(scene_data.data());

    // Then load the actual IBR data.
    const std::string cloud_path = input_directory + "/scene.pcloud";
    cloud->set_scene(*scene);
    cloud->load(cloud_path);

    return true;
}

bool store_viewer_state(const std::string                    &config_path,
                        const fribr::DragCamera              &camera,
                        const PatchCloud                     &cloud,
                        const fribr::Scene::Ptr               scene,
                        const fribr::CalibratedImage::Vector &calibrated_cameras)
{
    std::ofstream meta_stream(config_path, std::ios_base::out | std::ios_base::trunc);
    if (!meta_stream.good())
    {
        std::cerr << "Could not open insideout scene file for writing: " << config_path << std::endl;
        return false;
    }

    // First store the scene variables inline in the file.
    Eigen::Vector3f up      = camera.get_up();
    Eigen::Vector3f forward = camera.get_forward();
    Eigen::Vector3f center  = camera.get_center();
    Eigen::Vector3f normal  = camera.get_normal();
    Eigen::Vector3f lookat  = camera.get_lookat();

    meta_stream << up.x()      << " " << up.y()      << " " << up.z()      << std::endl;
    meta_stream << forward.x() << " " << forward.y() << " " << forward.z() << std::endl;
    meta_stream << center.x()  << " " << center.y()  << " " << center.z()  << std::endl;
    meta_stream << normal.x()  << " " << normal.y()  << " " << normal.z()  << std::endl;
    meta_stream << lookat.x()  << " " << lookat.y()  << " " << lookat.z()  << std::endl;

    const std::string output_directory = fribr::strip_filename(config_path);

    // Store the camera parameters in our own, quick, hacky binary format.
    const std::string camera_path = output_directory + "/cameras.bin";
    std::ofstream camera_stream(camera_path, std::ios_base::out   | 
                                             std::ios_base::trunc |
                                             std::ios_base::binary);
    if (!camera_stream.good())
    {
        std::cerr << "Could not open insideout camera file for writing: " << camera_path << std::endl;
        return false;
    }

    int32_t num_cameras = static_cast<int32_t>(calibrated_cameras.size());
    camera_stream.write(reinterpret_cast<char*>(&num_cameras), sizeof(num_cameras));

    std::vector<CameraParameters> camera_parameters;
    for (int i = 0; i < num_cameras; ++i)
        camera_parameters.push_back(CameraParameters(calibrated_cameras[i]));
    camera_stream.write(reinterpret_cast<char*>(camera_parameters.data()),
                        sizeof(CameraParameters) * num_cameras);

    // Store the 3D scene geometry
    const std::string scene_path = output_directory + "/scene.geom";
    std::ofstream scene_stream(scene_path, std::ios_base::out   | 
                                           std::ios_base::trunc |
                                           std::ios_base::binary);
    if (!scene_stream.good())
    {
        std::cerr << "Could not open insideout camera scene for writing: " << scene_path << std::endl;
        return false;
    }

    std::vector<char> scene_data = scene->serialize();        // Serialize the scene into a binary blob
    scene_stream.write(scene_data.data(), scene_data.size()); // and write it to our output file.

    // Then store the actual IBR data.
    const std::string cloud_path = output_directory + "/scene.pcloud";
    cloud.save(cloud_path);

    return true;
}

}
