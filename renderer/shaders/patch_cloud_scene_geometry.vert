/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 330

layout(location = 0) in vec3 vertex_position;

uniform mat4 camera_to_clip;
uniform mat4 world_to_camera;

out vec3 position;

void main()
{
    position = vertex_position.xyz;
    gl_Position = camera_to_clip * world_to_camera * vec4(position, 1.0);
}