/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 330

in  vec2 texcoord;
out vec4 frag_color;

uniform vec2 resolution;
uniform mat4 clip_to_camera;
uniform mat3 intrinsics;

uniform sampler2D diffuse_sampler;

void main()
{
    vec4 clip_position = vec4(texcoord * 2.0f - 1.0f, 0.0f, 1.0f);
    vec4 camera_position = clip_to_camera * clip_position;
    camera_position /= camera_position.w;

    vec3 image_position = intrinsics * camera_position.xyz;
    vec2 resampled_texcoord = image_position.xy / (resolution * image_position.z);
    frag_color = texture(diffuse_sampler, resampled_texcoord);
}