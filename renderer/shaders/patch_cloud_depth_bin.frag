/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 450

layout(r32ui, binding = 0) uniform uimage2D gpu_grid_a;
layout(r32ui, binding = 1) uniform uimage2D gpu_grid_b;
layout(r32ui, binding = 2) uniform uimage2D gpu_grid_c;
layout(r32ui, binding = 3) uniform uimage2D gpu_grid_d;

uniform sampler2D position_sampler;
uniform vec3      grid_min;
uniform vec3      grid_max;
uniform float     tolerance;

in  vec2 texcoord;
out vec4 frag_color;

void set_voxel(uint grid_x, uint grid_y, uint grid_z)
{
    uint channel_idx = grid_x / 32;
    uint channel_bit = grid_x - channel_idx * 32;
    uint channel_mask = 1 << channel_bit;

    if (channel_idx == 0)
        imageAtomicOr(gpu_grid_a, ivec2(grid_y, grid_z), channel_mask);
    if (channel_idx == 1)
        imageAtomicOr(gpu_grid_b, ivec2(grid_y, grid_z), channel_mask);
    if (channel_idx == 2)
        imageAtomicOr(gpu_grid_c, ivec2(grid_y, grid_z), channel_mask);
    if (channel_idx == 3)
        imageAtomicOr(gpu_grid_d, ivec2(grid_y, grid_z), channel_mask);
}

void main()
{
    vec4 position = texture2D(position_sampler, texcoord);
    if (position.w < 1.0f)
        discard;

    float radius_sfm = -position.z * tolerance;

    // TODO: DOES THE CHANGE BELOW BREAK LUMBER? CHECK ORRESPONDING CHANGE IN VOXEL_GRID.CPP VoxelGrid::rasterize_triangle(...)
    // vec3 normalized_position = clamp((position.xyz - grid_min) / (grid_max - grid_min), vec3(0, 0, 0), vec3(1, 1, 1));
    vec3 normalized_position = (position.xyz - grid_min) / (grid_max - grid_min);
    if (min(normalized_position.x, min(normalized_position.y, normalized_position.z)) < 0.0 ||
        max(normalized_position.x, max(normalized_position.y, normalized_position.z)) > 1.0)
        discard;

    normalized_position = clamp(normalized_position, vec3(0, 0, 0), vec3(1, 1, 1));

    uint grid_x = min(GRID_SIZE_X - 1, int(normalized_position.x * GRID_SIZE_X));
    uint grid_y = min(GRID_SIZE_Y - 1, int(normalized_position.y * GRID_SIZE_Y));
    uint grid_z = min(GRID_SIZE_Z - 1, int(normalized_position.z * GRID_SIZE_Z));

    float grid_radius = length(vec3(GRID_SIZE_X, GRID_SIZE_Y, GRID_SIZE_Z))
        * radius_sfm / length(grid_max - grid_min);
    float gridf_x = normalized_position.x * GRID_SIZE_X;
    float gridf_y = normalized_position.y * GRID_SIZE_Y;
    float gridf_z = normalized_position.z * GRID_SIZE_Z;

    set_voxel(grid_x, grid_y, grid_z);

    uint next_x = -1;
    if (gridf_x - grid_x > 0.5 && grid_x < GRID_SIZE_X - 1)
        next_x = grid_x + 1;
    else if (grid_x > 0)
        next_x = grid_x - 1;

    uint next_y = -1;
    if (gridf_y - grid_y > 0.5 && grid_y < GRID_SIZE_Y - 1)
        next_y = grid_y + 1;
    else if (grid_y > 0)
        next_y = grid_y - 1;

    uint next_z = -1;
    if (gridf_z - grid_z > 0.5 && grid_z < GRID_SIZE_Z - 1)
        next_z = grid_z + 1;
    else if (grid_z > 0)
        next_z = grid_z - 1;

    if (next_x >= 0 && abs(gridf_x - next_x) < grid_radius)
        set_voxel(next_x, grid_y, grid_z);
    if (next_y >= 0 && abs(gridf_y - next_y) < grid_radius)
        set_voxel(grid_x, next_y, grid_z);
    if (next_z >= 0 && abs(gridf_z - next_z) < grid_radius)
        set_voxel(grid_x, grid_y, next_z);

    if (next_x >= 0 && next_y >= 0 &&
        length(vec2(gridf_x, gridf_y) - vec2(next_x, next_y)) < grid_radius)
        set_voxel(next_x, next_y, grid_z);
    if (next_x >= 0 && next_z >= 0 &&
        length(vec2(gridf_x, gridf_z) - vec2(next_x, next_z)) < grid_radius)
        set_voxel(next_x, grid_y, next_z);
    if (next_x >= 0 && next_z >= 0 &&
        length(vec2(gridf_y, gridf_z) - vec2(next_y, next_z)) < grid_radius)
        set_voxel(grid_x, next_y, next_z);

    if (next_x >= 0 && next_y >= 0 && next_z >= 0 &&
        length(vec3(gridf_x, gridf_y, gridf_z) - vec3(next_x, next_y, next_z)) < grid_radius)
        set_voxel(next_x, next_y, next_z);

    discard;
}