/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 330

layout(location = 0) in vec2 vertex_position;
out vec2 texcoord;

void main()
{
    texcoord = (vec2(vertex_position.x, -vertex_position.y) + vec2(1.0, 1.0)) * 0.5;
    gl_Position = vec4(vertex_position, 0.0, 1.0);
}