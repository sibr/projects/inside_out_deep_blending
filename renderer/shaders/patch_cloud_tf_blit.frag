/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 330

in  vec2 texcoord;
out vec4 frag_color;
uniform sampler2D global_sampler;
uniform sampler2D diffuse_sampler;
uniform int layer;

void main()
{
    vec4 patch_color  = texture(diffuse_sampler, texcoord);
    vec4 global_color = texture(global_sampler, texcoord);
    frag_color = vec4(patch_color.bgr, 1.0);
    if (patch_color.a <= 0.0 || layer >= 4)
        frag_color = vec4(global_color.bgr, 1.0);
}