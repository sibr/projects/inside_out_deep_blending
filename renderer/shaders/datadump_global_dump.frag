/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 330

in  vec3  position;
in  vec2  texcoord;
in  vec3  color;
in  float depth;
out vec4  frag_color;

uniform bool use_texture;
uniform int  display_mode;
uniform vec3 virtual_camera;

uniform mat4 camera_to_clip;
uniform mat4 world_to_camera;
uniform mat4 old_world_to_camera;
uniform vec2 resolution;

uniform sampler2D diffuse_sampler;

void main()
{
    // Default assumption is display_mode == 0 for Colors
    vec4 pixel_color;
    if (use_texture)
    {
        pixel_color = vec4(texture2D(diffuse_sampler, texcoord).rgb, 1.0f);
    }
    else
    {
        pixel_color = vec4(color, 1.0f);
    }

    if (display_mode == 1) // Depth
    {
        float clamped_depth = max(0.25, depth);
        float disparity = 1.0f / (4.0f * clamped_depth);
        pixel_color.rgb = vec3(disparity, 0.0, 0.0);
    }
    else if (display_mode == 2) // Normals
    {
        vec3 normal = normalize(cross(dFdx(position), dFdy(position)));
        pixel_color.rgb = 0.5f * (1.0f + normal);
    }
    else if (display_mode == 3) // Output dir
    {
        vec3 output_dir = normalize(virtual_camera - position);
        pixel_color.rgb = 0.5f * (1.0f + output_dir);
    }
    else if (display_mode == 5) // Flow
    {
        vec4 position_clip = camera_to_clip * world_to_camera * vec4(position, 1.0);
        vec2 screen_space_position = (vec2(1.0, 1.0) + (position_clip.xy / position_clip.w)) * 0.5;
        vec4 old_position_clip = camera_to_clip * old_world_to_camera * vec4(position, 1.0);
        vec2 old_screen_space_position = (vec2(1.0, 1.0) + (old_position_clip.xy / old_position_clip.w)) * 0.5;

        screen_space_position *= resolution;
        old_screen_space_position *= resolution;

        vec2 flow_to_old = clamp(old_screen_space_position - screen_space_position,
            vec2(-512.0, -512.0), vec2(512.0, 512.0));
        flow_to_old /= 512.0f;
        flow_to_old = 0.5f * (flow_to_old + 1.0f);
        pixel_color.rgb = vec3(flow_to_old, 0.5f);
    }

    frag_color = pixel_color;
}