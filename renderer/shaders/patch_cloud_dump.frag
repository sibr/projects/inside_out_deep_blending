/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 330

in  vec2 texcoord;
out vec4 frag_color;
uniform int selected_layer;
) +
    sampler_stream.str() +
    GLSL_RAW(
        void main()
{
    vec4 patch_color = vec4(0.0, 0.0, 0.0, 0.0);
    ) +
    fetch_stream.str() +
        GLSL_RAW(
            if (patch_color.a <= 0.0)
                discard;

    patch_color.a = 1.0;
    frag_color = patch_color;
}