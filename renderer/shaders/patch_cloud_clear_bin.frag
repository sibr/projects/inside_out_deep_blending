/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 450

layout(r32ui, binding = 0) writeonly uniform uimage2D gpu_grid_a;
layout(r32ui, binding = 1) writeonly uniform uimage2D gpu_grid_b;
layout(r32ui, binding = 2) writeonly uniform uimage2D gpu_grid_c;
layout(r32ui, binding = 3) writeonly uniform uimage2D gpu_grid_d;

uniform ivec2     resolution;
uniform vec3      grid_min;
uniform vec3      grid_max;

in  vec2 texcoord;
out vec4 frag_color;

void main()
{
    ivec2 image_coords = ivec2(texcoord * resolution);
    uint  index = image_coords.y * resolution.x + image_coords.x;

    uint channel_idx = index % 4;           index /= 4;
    uint grid_y = index % GRID_SIZE_Y; index /= GRID_SIZE_Y;
    uint grid_z = index;
    if (grid_z >= GRID_SIZE_Z)
        discard;

    if (channel_idx == 0)
        imageStore(gpu_grid_a, ivec2(grid_y, grid_z), uvec4(0));
    if (channel_idx == 1)
        imageStore(gpu_grid_b, ivec2(grid_y, grid_z), uvec4(0));
    if (channel_idx == 2)
        imageStore(gpu_grid_c, ivec2(grid_y, grid_z), uvec4(0));
    if (channel_idx == 3)
        imageStore(gpu_grid_d, ivec2(grid_y, grid_z), uvec4(0));

    discard;
}