/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 330

in  vec2 texcoord;
out vec4 frag_color;
uniform sampler2D color_sampler;
void main() { frag_color = vec4(texture2D(color_sampler, texcoord)); }