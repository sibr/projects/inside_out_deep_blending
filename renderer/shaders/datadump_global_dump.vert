/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 330

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;
layout(location = 2) in vec2 vertex_texcoord;
layout(location = 3) in vec3 vertex_color;

uniform mat4 camera_to_clip;
uniform mat4 world_to_camera;

out vec3  position;
out vec2  texcoord;
out vec3  color;
out float depth;

void main()
{
    vec4 position_cam = world_to_camera * vec4(vertex_position, 1.0);
    vec4 position_clip = camera_to_clip * position_cam;

    position = vertex_position;
    texcoord = vertex_texcoord;
    color = vertex_color;
    depth = -position_cam.z;
    gl_Position = position_clip;
}