/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef VIEWER_STATE_H
#define VIEWER_STATE_H
#include "Config.hpp"
#include "projects/inside_out_deep_blending/renderer/patch_cloud.h"
#include "projects/fribr_framework/renderer/vision/calibrated_image.h"
#include "projects/fribr_framework/renderer/3d/drag_camera.h"
#include "projects/fribr_framework/renderer/3d/scene.h"
#include <Eigen/Core>
#include <string>

namespace patcher
{

bool SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT load_viewer_state(const std::string                     &config_path,
                       fribr::DragCamera                     *camera,
                       PatchCloud                            *cloud,
                       fribr::Scene::Ptr                     *scene,
                       fribr::CalibratedImage::Vector        *calibrated_cameras);

bool SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT store_viewer_state(const std::string                    &config_path,
                        const fribr::DragCamera              &camera,
                        const PatchCloud                     &cloud,
                        const fribr::Scene::Ptr               scene,
                        const fribr::CalibratedImage::Vector &calibrated_cameras);

}

#endif // VIEWER_STATE_H
