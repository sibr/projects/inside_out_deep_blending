/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef BLEND_SHADER_BUILDER_H
#define BLEND_SHADER_BUILDER_H

#include "projects/fribr_framework/renderer/3d.h"
#include "projects/fribr_framework/renderer/gl_wrappers.h"
#include "Config.hpp"

#include <string>
#include <vector>

namespace patcher
{

class SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT BlendShaderBuilder
{
    bool m_dirty = true;
    template<typename T>
    void set(T &our_value, const T &new_value)
    {
	m_dirty |= our_value != new_value;
	our_value = new_value;
    }

public:
    BlendShaderBuilder();

    int num_depth_costs() const { return static_cast<int>(m_depth_cost_functions.size()); }
    int num_blend_costs() const { return static_cast<int>(m_blend_cost_functions.size()); }

    std::string depth_cost_name(int i) const { return m_depth_cost_names[i]; }
    std::string blend_cost_name(int i) const { return m_blend_cost_names[i]; }

    int depth_cost_index() const { return m_depth_cost_index; }
    void depth_cost_index(int i) { set(m_depth_cost_index, i); }

    int blend_cost_index() const { return m_blend_cost_index; }
    void blend_cost_index(int i) { set(m_blend_cost_index, i); }

    bool use_insideout_multipass() const { return m_use_insideout_multipass; }
    void use_insideout_multipass(bool b) { set(m_use_insideout_multipass, b); }

    // These may trigger the shader to be recompiled, only call with
    // an active OpenGL context.
    fribr::Shader::Ptr peeling_shader();
    fribr::Shader::Ptr blend_shader();

private:
    void rebuild_shaders_if_needed();

    void add_depth_cost(const std::string &name, const std::string &shader)
    {
	m_depth_cost_functions.emplace_back(shader);
	m_depth_cost_names.emplace_back(name);
    }

    void add_blend_cost(const std::string &name, const std::string &shader)
    {
	m_blend_cost_functions.emplace_back(shader);
	m_blend_cost_names.emplace_back(name);
    }

    bool m_use_insideout_multipass = true;
    int m_depth_cost_index = 0;
    int m_blend_cost_index = 0;

    std::vector<std::string> m_depth_cost_functions;
    std::vector<std::string> m_depth_cost_names;

    std::vector<std::string> m_blend_cost_functions;
    std::vector<std::string> m_blend_cost_names;

    fribr::Shader::Ptr m_peeling_shader;
    fribr::Shader::Ptr m_blend_shader;
};

}

#endif // BLEND_SHADER_BUILDER_H

