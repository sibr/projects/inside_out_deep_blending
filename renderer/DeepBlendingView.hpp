/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_EXP_INSIDEOUTDEEPBLENDING_DEEPBLENDINGVIEW_HPP__
# define __SIBR_EXP_INSIDEOUTDEEPBLENDING_DEEPBLENDINGVIEW_HPP__

#include "Config.hpp"
# include <core/view/ViewBase.hpp>
# include <core/renderer/PoissonRenderer.hpp>
# include <core/renderer/CopyRenderer.hpp>
# include <core/renderer/PoissonRenderer.hpp>

#include <projects/inside_out_deep_blending/renderer/DeepBlendingScene.hpp>

namespace sibr {

	class SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT DeepBlendingView : public sibr::ViewBase
	{
		SIBR_CLASS_PTR(DeepBlendingView);

	public:
		DeepBlendingView(const DeepBlendingScene::Ptr& scene, const DeepBlendingAppArgs& args, uint render_w, uint render_h);

		/**
		* Perform rendering. Called by the view manager or rendering mode.
		* \param dst The destination rendertarget.
		* \param eye The novel viewpoint.
		*/
		void onRenderIBR(sibr::IRenderTarget& dst, const sibr::Camera& eye) override;

		/**
		* Update inputs (do nothing).
		* \param input The inputs state.
		*/
		void		onUpdate(Input& input) override;
		
		/**
		* Update the GUI.
		*/
		void onGUI() override;

	protected:
		patcher::PatchCloud::DisplayMode _toDisplayMode[5] =
		{
			patcher::PatchCloud::Colors,
			patcher::PatchCloud::Depth,
			patcher::PatchCloud::Normals,
			patcher::PatchCloud::OutputDir,
			patcher::PatchCloud::InputDir
		};

	private:


		std::shared_ptr<sibr::DeepBlendingScene> 	_scene;
		const DeepBlendingAppArgs&					_args;
		RenderTargetRGBA::Ptr						_blendRT;
		RenderTargetRGBA::Ptr						_poissonRT;

		PoissonRenderer::Ptr						_poissonRenderer;
		CopyRenderer::Ptr							_copyRenderer;

		int											_display_mode = 0;
		int											_display_layer = 0;
		int											_cluster_k = 12;
		float										_ibr_depth_alpha = 0.5f;
		float										_ibr_resolution_alpha = 0.1f;
		float										_ibr_sigma = 0.25f;

	};

}

#endif