/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "recon_tools.h"

#include "projects/fribr_framework/renderer/tools/file_tools.h"
#include "projects/fribr_framework/renderer/tools/image_tools.h"
#include "projects/fribr_framework/renderer/tools/string_tools.h"
#include "projects/fribr_framework/renderer/tools/sized_priority_queue.h"
#include "projects/fribr_framework/renderer/tools/geometry_tools.h"
#include "projects/inside_out_deep_blending/renderer/simple_ransac.h"
#include "projects/inside_out_deep_blending/renderer/imtools.h"
#include "densecrf/include/densecrf.h"


#include <Eigen/Sparse>
#include <Eigen/SparseCholesky>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/SparseQR>

#include <opencv2/core.hpp>
#include <opencv2/ximgproc.hpp>

#include <cmath>
#include <fstream>
#include <sstream>

// For profiling
#include <chrono>
#include <iostream>

#ifdef INRIA_WIN
size_t doAtomicOp(size_t* progress, int val);
#endif
namespace recon
{

	//
	// IO
	//

	void load_calibrated_images(const std::string              &path,
		fribr::CalibratedImage::Vector &cameras_dslr,
		fribr::PointCloud::Ptr         &point_cloud,
		fribr::PointCloudRenderer::Ptr &point_renderer,
		fribr::Observations            &observations)
	{
		cameras_dslr.clear();
		observations.points.clear();
		observations.observations.clear();

		std::string extension = fribr::get_extension(path);
		fribr::CalibratedImage::Vector cameras_all;
		fribr::Observations            observations_all;
		if (extension == "json") {
			cameras_all = fribr::OpenMVG::load_calibrated_images(path);
			observations_all = fribr::OpenMVG::load_observations(path);
			point_cloud = fribr::OpenMVG::load_point_cloud(path);
			point_renderer.reset(new fribr::PointCloudRenderer(point_cloud));
		}
		else if (extension == "db") {
			cameras_all = fribr::COLMAP::load_calibrated_images(path);
			point_cloud = fribr::COLMAP::load_point_cloud(path);
			point_renderer.reset(new fribr::PointCloudRenderer(point_cloud));
		}
		else {
			std::cout << "Unsupported sfm format: " << extension << std::endl;
			return;
		}

		// Do not use blacklisted images in the DSLR output.
		std::string              blacklist_path = fribr::strip_extension(path) + ".blacklist";
		std::vector<std::string> blacklist = fribr::Blacklist::load_blacklist(blacklist_path);

		std::vector<int> dslr_to_all_remap;
		for (int i = 0; i < static_cast<int>(cameras_all.size()); ++i)
		{
			fribr::CalibratedImage &camera = cameras_all[i];
			if (!fribr::find_any(blacklist, camera.get_image_name()))
			{
				dslr_to_all_remap.push_back(i);
				cameras_dslr.push_back(camera);
			}
		}

		if (extension == "json") {
			observations.points = observations_all.points;
			for (size_t i = 0; i < cameras_dslr.size(); ++i)
			{
				int index_all = dslr_to_all_remap[i];
				observations.observations.push_back(observations_all.observations[index_all]);
			}
		}
	}

	namespace
	{

		template<typename T>
		struct OpenCVMatTraits
		{
			enum { CHANNELS = -1 };
		};

		template<>
		struct OpenCVMatTraits<cv::Mat1f>
		{
			enum { CHANNELS = 1 };
		};

		template<>
		struct OpenCVMatTraits<cv::Mat2f>
		{
			enum { CHANNELS = 2 };
		};

		template<>
		struct OpenCVMatTraits<cv::Mat3f>
		{
			enum { CHANNELS = 3 };
		};

		template<>
		struct OpenCVMatTraits<cv::Mat4f>
		{
			enum { CHANNELS = 4 };
		};

		template<typename T>
		bool load_colmap_file(const std::string &filename, T* image)
		{
			std::ifstream header(filename, std::ios::in | std::ios::binary);
			if (!header)
			{
				std::cerr << "Could not load COLMAP header at: " << filename << std::endl;
				return false;
			}

			char temp;
			size_t width, height, depth;
			header >> width >> temp >> height >> temp >> depth >> temp;

			std::streampos header_end = header.tellg();
			header.close();

			const int expected_channels = OpenCVMatTraits<T>::CHANNELS;
			if (width < 0 || height < 0 || depth != expected_channels)
			{
				std::cerr << "Invalid COLMAP file format ("
					<< width << "x" << height << "x" << depth << ")"
					<< " for file: " << filename
					<< " (expected << " << expected_channels << " channels)"
					<< std::endl;
				return false;
			}

			std::vector<float> data(width * height * depth);
			std::ifstream data_stream(filename, std::ios::in | std::ios::binary);
			if (!data_stream)
			{
				std::cerr << "Could not load COLMAP data at: " << filename << std::endl;
				return false;
			}

			data_stream.seekg(header_end);
			data_stream.read((char*)data.data(), width * height * depth * sizeof(float));
			data_stream.close();

			*image = T(height, width);
			for (size_t c = 0; c < depth; ++c)
				for (size_t y = 0; y < height; ++y)
					for (size_t x = 0; x < width; ++x)
					{
						size_t index = x + width * (y + height * c);
						float* ptr = (float*)(image->ptr(y, x)) + c;
						*ptr = data[index];
					}

			return true;
		}

	} // anonymous namespace

	cv::Mat1f load_colmap_depth(const std::string &filename)
	{
		cv::Mat1f depth_map;
		if (!load_colmap_file(filename, &depth_map))
			return cv::Mat1f();
		return depth_map;
	}

	cv::Mat3f load_colmap_normals(const std::string &filename)
	{
		cv::Mat3f normal_map;
		if (!load_colmap_file(filename, &normal_map))
			return cv::Mat3f();
		return normal_map;
	}

	void save_colmap_depth(const std::string &filename, const cv::Mat1f &depth_map)
	{
		std::ofstream header(filename, std::ios::out);
		if (!header)
		{
			std::cerr << "Could not load depth map header for writing: " << filename << std::endl;
			return;
		}

		header << depth_map.cols << "&" << depth_map.rows << "&" << 1 << "&";
		header.close();

		std::ofstream data(filename, std::ios::out | std::ios::binary | std::ios::app);
		if (!data)
		{
			std::cerr << "Could not load depth map data for writing: " << filename << std::endl;
			return;
		}

		data.write((char*)depth_map.ptr(), depth_map.cols * depth_map.rows * sizeof(float));
		data.close();
	}

	std::vector<cv::Mat1f> load_colmap_depths(const fribr::CalibratedImage::Vector &cameras, DepthMapType type)
	{
		std::vector<cv::Mat1f> depth_maps;
		depth_maps.reserve(cameras.size());

		for (const fribr::CalibratedImage &camera : cameras)
		{
			const std::string color_path = camera.get_image_path();
			const std::string color_name = fribr::get_filename(color_path);
			const std::string base_path = fribr::strip_filename(color_path);
			std::string depth_path = base_path + "/../stereo/depth_maps/" + color_name;
			if (type == PhotometricDepth)
				depth_path += ".photometric.bin";
			else if (type == GeometricDepth)
				depth_path += ".geometric.bin";

			cv::Mat1f depth_map = load_colmap_depth(depth_path);
			if (depth_map.empty())
			{
				std::cerr << "Could not load depth map for image: " << color_name << " at: " << depth_path << std::endl;
				return std::vector<cv::Mat1f>();
			}

			depth_maps.emplace_back(depth_map);
		}

		return depth_maps;
	}

	void save_colmap_depths(const fribr::CalibratedImage::Vector &cameras,
		std::vector<std::vector<Eigen::Vector3f> > &camera_points,
		DepthMapType type)
	{
		const std::string base_path = fribr::strip_filename(cameras[0].get_image_path());
		const std::string folder_path = base_path + "/../stereo/refined_depth_maps";
		if (!fribr::directory_exists(folder_path))
			fribr::make_directory(folder_path);

		for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
		{
			const fribr::CalibratedImage &camera = cameras[i];
			const std::string color_name = fribr::get_filename(camera.get_image_path());
			std::string depth_path = folder_path + "/" + color_name;
			if (type == PhotometricDepth)
				depth_path += ".photometric.bin";
			else if (type == GeometricDepth)
				depth_path += ".geometric.bin";

			Eigen::Vector2i resolution = camera.get_scaled_resolution();
			cv::Mat1f depth_map(resolution.y(), resolution.x());
			for (int y = 0; y < depth_map.rows; ++y)
				for (int x = 0; x < depth_map.cols; ++x)
				{
					int index = (depth_map.rows - y - 1) * depth_map.cols + x;
					float depth = std::max(0.0f, -camera_points[i][index].z());
					depth_map(depth_map.rows - y - 1, x) = depth;
				}

			save_colmap_depth(depth_path, depth_map);
		}
	}

	void compute_depth_maps_from_mesh(fribr::CalibratedImage::Vector &cameras,
		patcher::MeshRayTracer         &ray_tracer,
		std::vector<std::vector<Eigen::Vector3f> > &camera_points,
		std::vector<std::vector<Eigen::Vector3f> > &camera_normals)
	{
		std::cout << "Constructing depth maps: 0%" << std::flush;
		camera_points.assign(cameras.size(), std::vector<Eigen::Vector3f>());
		camera_normals.assign(cameras.size(), std::vector<Eigen::Vector3f>());
		size_t progress = 0;

#ifdef NDEBUG
#pragma omp parallel for
#endif

		for (int i = 0; i < cameras.size(); ++i)
		{
			fribr::CalibratedImage &camera = cameras[i];
			Eigen::Matrix4f         world_to_cam = camera.get_world_to_camera().matrix();
			Eigen::Matrix4f         cam_to_world = world_to_cam.inverse();
			Eigen::Matrix3f         intrinsics = camera.get_intrinsics();
			Eigen::Matrix3f         inv_intrinsics = intrinsics.inverse();
			Eigen::Vector2i         resolution = camera.get_scaled_resolution();
			Eigen::Vector3f         orig = camera.get_position();

			camera_points[i].assign(resolution.x() * resolution.y(), Eigen::Vector3f(0.0f, 0.0f, 1.0f));
			camera_normals[i].assign(resolution.x() * resolution.y(), Eigen::Vector3f(0.0f, 0.0f, 1.0f));

			for (int y = 0; y < resolution.y(); ++y)
				for (int x = 0; x < resolution.x(); ++x)
				{
					fribr::RTResult r;
					r.tri = 0;

					// Our ray-triangle test is not watertight. Use these offets to maximize our chances
					// of always hitting a triangle, even if the ray is very close to a triangle edge.
					float offsets_x[] = { 0.0f, 0.1f, -0.1f,  0.0f, -0.1f };
					float offsets_y[] = { 0.0f, 0.1f,  0.1f, -0.1f,  0.1f };
					for (size_t j = 0; j < 5; ++j)
					{
						int xx = x;
						int yy = resolution.y() - y - 1;
						Eigen::Vector3f far = imtools::unproject(xx + offsets_x[j], yy + offsets_y[j], 1000.0f,
							inv_intrinsics, cam_to_world);

						r = ray_tracer.ray_cast(orig, far - orig);
						if (r.tri)
							break;
					}

					if (!r.tri)
					{
						camera_normals[i][y * resolution.x() + x] = Eigen::Vector3f(0.0f, 0.0f, 1.0f);
						continue;
					}

					Eigen::Vector4f pc = world_to_cam * Eigen::Vector4f(r.p[0], r.p[1], r.p[2], 1.0f);
					camera_points[i][y * resolution.x() + x] = pc.head<3>();

					Eigen::Vector3f a(r.tri->m_vertices[0][0], r.tri->m_vertices[0][1], r.tri->m_vertices[0][2]);
					Eigen::Vector3f b(r.tri->m_vertices[1][0], r.tri->m_vertices[1][1], r.tri->m_vertices[1][2]);
					Eigen::Vector3f c(r.tri->m_vertices[2][0], r.tri->m_vertices[2][1], r.tri->m_vertices[2][2]);
					Eigen::Vector3f n = (b - a).cross(c - a).normalized();
					camera_normals[i][y * resolution.x() + x] = world_to_cam.block<3, 3>(0, 0) * n;
				}


			size_t prev_progress = doAtomicOp(&progress, 1);

			if (prev_progress % 2 == 1)
				std::cout << "\rConstructing depth maps: "
				<< (prev_progress + 1.0f) * 100.0f / cameras.size()
				<< "%                 " << std::flush;
		}
		std::cout << " Done!" << std::endl;
	}

	bool load_depth_maps(std::string depth_map_folder,
		fribr::CalibratedImage::Vector &cameras,
		std::vector<std::vector<Eigen::Vector3f> > &camera_points,
		std::vector<std::vector<Eigen::Vector3f> > &camera_normals,
		std::vector<Eigen::Vector2i> *camera_resolutions)
	{
		if (!fribr::directory_exists(depth_map_folder))
			return false;

		camera_points.assign(cameras.size(), std::vector<Eigen::Vector3f>());
		camera_normals.assign(cameras.size(), std::vector<Eigen::Vector3f>());
		if (camera_resolutions)
			camera_resolutions->assign(cameras.size(), Eigen::Vector2i(-1, -1));

		size_t progress = 0;
		std::cout << "\rLoading depth maps: 0% " << std::flush;
#ifdef NDEBUG
#ifdef INRIA_WIN
		// actuall a Windows 7 bug
#pragma omp parallel for 
#else
#pragma omp parallel for schedule(dynamic, 1)
#endif
#endif
#ifdef INRIA_WIN
		for (int i = 0; i < cameras.size(); ++i)
#else
		for (size_t i = 0; i < cameras.size(); ++i)
#endif
		{
			std::stringstream filename_stream;
			filename_stream << i << ".bin";
			std::string   depth_map_name = fribr::append_path(depth_map_folder, filename_stream.str());
			std::ifstream depth_map_file(depth_map_name, std::ifstream::binary);
			if (!depth_map_file.good())
			{
				std::cout << "Failed to open " << depth_map_name << " for reading " << std::endl;
				continue;
			}

			int size[2];
			depth_map_file.read((char*)size, sizeof(size));
			if (camera_resolutions)
				(*camera_resolutions)[i] = Eigen::Vector2i(size[0], size[1]);

			camera_points[i].assign(size[0] * size[1], Eigen::Vector3f(0.0f, 0.0f, 1.0f));
			camera_normals[i].assign(size[0] * size[1], Eigen::Vector3f(0.0f, 0.0f, 1.0f));

			depth_map_file.read((char*)camera_points[i].data(), camera_points[i].size() * sizeof(float) * 3);
			depth_map_file.read((char*)camera_normals[i].data(), camera_normals[i].size() * sizeof(float) * 3);
			depth_map_file.close();

#pragma omp critical
			{
				progress++;
				std::cout << "\rLoading depth maps: " << progress * 100.0f / cameras.size()
					<< "%                     " << std::flush;
			}
		}
		std::cout << std::endl;

		return true;
	}

	void save_depth_maps(std::string depth_map_folder,
		fribr::CalibratedImage::Vector &cameras,
		std::vector<std::vector<Eigen::Vector3f> > &camera_points,
		std::vector<std::vector<Eigen::Vector3f> > &camera_normals)
	{
		if (!fribr::directory_exists(depth_map_folder))
			fribr::make_directory(depth_map_folder);

		size_t progress = 0;
		std::cout << "\rSaving depth maps: 0% " << std::flush;
#ifdef NDEBUG
#ifdef INRIA_WIN
		// actuall a Windows 7 bug
#pragma omp parallel for 
#else
#pragma omp parallel for schedule(dynamic, 1)
#endif
#endif
#ifdef INRIA_WIN
		for (int i = 0; i < cameras.size(); ++i)
#else
		for (size_t i = 0; i < cameras.size(); ++i)
#endif
		{
			std::stringstream filename_stream;
			filename_stream << i << ".bin";
			std::string   depth_map_name = fribr::append_path(depth_map_folder, filename_stream.str());
			std::ofstream depth_map_file(depth_map_name, std::ofstream::binary | std::ofstream::trunc);
			if (!depth_map_file.good())
			{
				std::cout << "Failed to open " << depth_map_name << " for writing " << std::endl;
				continue;
			}

			int size[2] = { cameras[i].get_scaled_resolution().x(), cameras[i].get_scaled_resolution().y() };
			depth_map_file.write((char*)size, sizeof(size));

			depth_map_file.write((char*)camera_points[i].data(), camera_points[i].size() * sizeof(float) * 3);
			depth_map_file.write((char*)camera_normals[i].data(), camera_normals[i].size() * sizeof(float) * 3);
			depth_map_file.close();

#pragma omp critical
			{
				progress++;
				std::cout << "\rSaving depth maps: " << progress * 100.0f / cameras.size()
					<< "%                    " << std::flush;
			}
		}
	}

	//
	// View selection
	//

	int find_common_points(int i, int j, fribr::CalibratedImage::Vector &cameras,
		std::vector<std::vector<Eigen::Vector3f> > &points)
	{
		fribr::CalibratedImage &camera = cameras[i];
		Eigen::Matrix4f         world_to_cam_i = camera.get_world_to_camera().matrix();
		Eigen::Matrix4f         cam_to_clip_i = camera.get_camera_to_clip(0.1f, 1000.0f);
		Eigen::Matrix4f         world_to_clip_i = cam_to_clip_i * world_to_cam_i;
		Eigen::Matrix4f         cam_to_world_j = cameras[j].get_world_to_camera().inverse().matrix();
		Eigen::Matrix4f         cam_j_to_clip_i = world_to_clip_i * cam_to_world_j;

		Eigen::Matrix4f         cam_j_to_cam_i = world_to_cam_i * cam_to_world_j;
		Eigen::Matrix3f         intrinsics_i = camera.get_intrinsics();

		int common_points = 0;
		static const int SUBSAMPLING_RATE = 5;
		for (int y = 0; y < cameras[j].get_scaled_resolution().y(); y += SUBSAMPLING_RATE)
			for (int x = 0; x < cameras[j].get_scaled_resolution().x(); x += SUBSAMPLING_RATE)
			{
				size_t index = y * cameras[j].get_scaled_resolution().x() + x;
				Eigen::Vector4f p_cam_j = points[j][index].homogeneous();
				Eigen::Vector4f p_clip = cam_j_to_clip_i * p_cam_j;
				if (p_clip.x() < -p_clip.w() || p_clip.x() > p_clip.w() ||
					p_clip.y() < -p_clip.w() || p_clip.y() > p_clip.w() ||
					p_clip.z() < -p_clip.w() || p_clip.z() > p_clip.w())
					continue;

				Eigen::Vector4f p_cam_i = cam_j_to_cam_i * p_cam_j;
				Eigen::Vector3f p_img_i = intrinsics_i * p_cam_i.head<3>();
				p_img_i /= p_img_i.z();

				int index_i = (cameras[i].get_scaled_resolution().y() - int(p_img_i.y()) - 1) *
					cameras[i].get_scaled_resolution().x() + int(p_img_i.x());
				if (p_img_i.x() < 0 || p_img_i.x() >= camera.get_scaled_resolution().x() ||
					p_img_i.y() < 0 || p_img_i.y() >= camera.get_scaled_resolution().y() ||
					p_cam_i.z() / points[i][index_i].z() > 1.05f)
					continue;

				common_points++;
			}

		return common_points;
	}

	std::vector<int> find_common_points(fribr::CalibratedImage::Vector &cameras,
		std::vector<std::vector<Eigen::Vector3f> > &points)
	{
		std::vector<int> common_points(cameras.size() * cameras.size(), 0);
		size_t progress = 0;
#ifdef NDEBUG
#pragma omp parallel for
#endif
#ifdef INRIA_WIN
		for (int j = 0; j < cameras.size(); ++j)
			for (int i = 0; i < cameras.size(); ++i)
#else
		for (size_t j = 0; j < cameras.size(); ++j)
			for (size_t i = 0; i < cameras.size(); ++i)
#endif
			{
				if (i == j)
					continue;

				common_points[cameras.size() * j + i] = find_common_points(i, j, cameras, points);

#ifdef INRIA_WIN
				size_t prev_progress = doAtomicOp(&progress, 1);
#else
				size_t prev_progress = __sync_fetch_and_add(&progress, 1);
#endif

				if (prev_progress % 100 == 99)
					std::cout << "\rFinding common points: "
					<< (prev_progress + 1.0f) * 100.0f / (cameras.size() * cameras.size())
					<< "%                 " << std::flush;
			}
		std::cout << std::endl;

		return common_points;
	}

	std::vector<int> find_neighbor_images(int camera_a, int num_images,
		const fribr::CalibratedImage::Vector &cameras,
		const std::vector<int> &common_points)
	{
		typedef std::pair<float, int> WeightedIndex;
		fribr::SizedPriorityQueue<WeightedIndex> best_images(num_images);
		for (size_t i = 0; i < cameras.size(); ++i)
		{
			if (static_cast<int>(i) == camera_a)
				continue;

			const fribr::CalibratedImage &a = cameras[camera_a];
			const fribr::CalibratedImage &b = cameras[i];

			int num_common_points = common_points[camera_a * cameras.size() + i];
			if (a.get_forward().dot(b.get_forward()) <= 0.5f)
				num_common_points /= 2;
			else if (a.get_forward().dot(b.get_forward()) <= 0.0f)
				continue;

			best_images.push(std::make_pair(-num_common_points, static_cast<int>(i)));
		}

		int total_images = best_images.get_storage().size();
		std::vector<int> indices(total_images);
		for (int i = 0; i < total_images; ++i)
		{
			indices[total_images - i - 1] = best_images.top().second;
			best_images.pop();
		}

		return indices;
	}

	//
	// Smoothing
	//

	namespace
	{

		struct WeightDepthIndex
		{
			float weight;
			float depth;
			int   index;

			WeightDepthIndex()
			{
			}

			WeightDepthIndex(float _w, float _d, int _i)
				: weight(_w), depth(_d), index(_i)
			{
			}

			bool operator<(const WeightDepthIndex &rhs) const
			{
				return depth < rhs.depth;
			}
		};

		WeightDepthIndex weighted_median(std::vector<WeightDepthIndex>::iterator begin,
			std::vector<WeightDepthIndex>::iterator end)
		{
			typedef std::vector<WeightDepthIndex>::iterator Iterator;

			float total_weight = 0.0f;
			for (Iterator i = begin; i < end; ++i)
				total_weight += i->weight;
			float target_weight = total_weight / 2.0f;

			float discarded_before = 0.0f;

			while (begin + 1 < end)
			{
				WeightDepthIndex pivot = *(begin + (end - begin) / 2);

				Iterator smaller_end = std::partition(begin, end,
					[&pivot](const WeightDepthIndex& e)
				{
					return e < pivot;
				});
				Iterator larger_begin = std::partition(smaller_end, end,
					[&pivot](const WeightDepthIndex& e)
				{
					return !(pivot < e);
				});

				float before_sum = discarded_before;
				for (Iterator i = begin; i < smaller_end; ++i)
					before_sum += i->weight;

				float pivot_sum = 0.0f;
				for (Iterator i = smaller_end; i < larger_begin; ++i)
					pivot_sum += i->weight;

				float after_sum = total_weight - pivot_sum - before_sum;

				if (before_sum < target_weight && after_sum < target_weight)
					return *smaller_end;

				if (before_sum > after_sum)
				{
					end = smaller_end;
				}
				else
				{
					begin = larger_begin;
					discarded_before = before_sum + pivot_sum;
				}
			}

			return *begin;
		}

	} // anonymous namespace

	void median_filter_depth_map(fribr::CalibratedImage::Vector             &cameras,
		std::vector<std::vector<Eigen::Vector3f> > &depth_maps,
		std::vector<std::vector<Eigen::Vector3f> > &normal_maps,
		int selected_camera)
	{
		std::vector<Eigen::Vector3f> &depth_map = depth_maps[selected_camera];
		std::vector<Eigen::Vector3f> &normal_map = normal_maps[selected_camera];
		fribr::CalibratedImage       &camera = cameras[selected_camera];

		Eigen::Matrix3f intrinsics = camera.get_intrinsics();
		Eigen::Matrix3f inv_intrinsics = intrinsics.inverse();
		Eigen::Vector2i resolution = camera.get_scaled_resolution();

		std::vector<Eigen::Vector3f> old_depths = depth_map;
		std::vector<Eigen::Vector3f> old_normals = normal_map;

		static const int FILTER_RADIUS = 6;
		static const int FILTER_SIZE = 2 * FILTER_RADIUS + 2;
		std::vector<WeightDepthIndex> neighbor_depths(FILTER_SIZE * FILTER_SIZE);

		static const float CUTOFF_RATIO = 1.001f;
		cv::Mat1f depth_image(resolution.y(), resolution.x());
		for (int y = 0; y < resolution.y(); ++y)
			for (int x = 0; x < resolution.x(); ++x)
			{
				int index = (resolution.y() - y - 1) * resolution.x() + x;
				depth_image(y, x) = -depth_map[index].z();
			}
		cv::blur(depth_image, depth_image, cv::Size(FILTER_SIZE, FILTER_SIZE));

		for (int y = 0; y < resolution.y(); ++y)
			for (int x = 0; x < resolution.x(); ++x)
			{
				int index = (resolution.y() - y - 1) * resolution.x() + x;

				// Don't bother running the median filter in regions where the
				// depth map is already smooth.
				const float orig_depth = -depth_map[index].z();
				const float smooth_depth = depth_image(y, x);
				if (orig_depth / smooth_depth <= CUTOFF_RATIO &&
					smooth_depth / orig_depth <= CUTOFF_RATIO)
					continue;

				cv::Vec3b orig_3b = cameras[selected_camera].get_image().at<cv::Vec3b>(resolution.y() - y - 1, x);
				Eigen::Vector3f orig = Eigen::Vector3f(orig_3b[2], orig_3b[1], orig_3b[0]);

				const int start_x = std::max(0, x - FILTER_RADIUS);
				const int end_x = std::min(resolution.x() - 1, x + FILTER_RADIUS);
				const int start_y = std::max(0, y - FILTER_RADIUS);
				const int end_y = std::min(resolution.y() - 1, y + FILTER_RADIUS);

				int buffer_index = 0;
				for (int yy = start_y; yy <= end_y; ++yy)
					for (int xx = start_x; xx <= end_x; ++xx)
					{
						const int dx = xx - x;
						const int dy = yy - y;

						int   nindex = (resolution.y() - yy - 1) * resolution.x() + xx;
						float depth = -old_depths[nindex].z();
						if (depth <= 0.0f)
							continue;

						cv::Vec3b neighbor_3b = cameras[selected_camera].get_image().at<cv::Vec3b>(resolution.y() - yy - 1, xx);
						Eigen::Vector3f neighbor(neighbor_3b[2], neighbor_3b[1], neighbor_3b[0]);
						float color_ssd = (orig - neighbor).squaredNorm();

						float weight = expf(-color_ssd / (2.0f * 10.0f * 10.0f)
							- (dx * dx + dy * dy) / (2.0f * 4.0f *  4.0f));

						neighbor_depths[buffer_index] = WeightDepthIndex(weight, depth, nindex);
						buffer_index++;
					}

				if (buffer_index == 0)
					continue;

				WeightDepthIndex median =
					weighted_median(neighbor_depths.begin(),
						neighbor_depths.begin() + buffer_index);
				float depth = median.depth;

				Eigen::Vector3f pc_new = imtools::unproject(x, y, depth, inv_intrinsics);
				depth_map[index] = pc_new.head<3>();
				normal_map[index] = old_normals[median.index];
			}
	}

	//
	// Refinement
	//

	void colmap_refine_depth_map(fribr::CalibratedImage::Vector             &cameras,
		std::vector<std::vector<Eigen::Vector3f> > &depth_maps,
		std::vector<std::vector<Eigen::Vector3f> > &normal_maps,
		int selected_camera)
	{
		std::vector<int> indices;
		if (selected_camera >= 0)
			indices.emplace_back(selected_camera);
		else
			for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
				indices.emplace_back(i);

#ifdef NDEBUG
#pragma omp parallel for
#endif
		for (int ii = 0; ii < static_cast<int>(indices.size()); ++ii)
		{
			int i = indices[ii];
			std::vector<Eigen::Vector3f> &depth_map = depth_maps[i];
			std::vector<Eigen::Vector3f> &normal_map = normal_maps[i];
			fribr::CalibratedImage       &camera = cameras[i];

			camera.load_image(fribr::CalibratedImage::CorrectRadialDistortion,
				fribr::CalibratedImage::CPUOnly);

			Eigen::Matrix4f world_to_camera = camera.get_world_to_camera().matrix();
			Eigen::Matrix4f camera_to_world = world_to_camera.inverse();
			Eigen::Matrix3f intrinsics = camera.get_intrinsics();
			Eigen::Matrix3f inv_intrinsics = intrinsics.inverse();
			Eigen::Vector2i resolution = camera.get_scaled_resolution();

			const std::string color_path = camera.get_image_path();
			const std::string color_name = fribr::get_filename(color_path);
			const std::string base_path = fribr::strip_filename(color_path);
			const std::string depth_path = base_path + "/../stereo/depth_maps/" + color_name;
			const std::string normal_path = base_path + "/../stereo/normal_maps/" + color_name;

			cv::Mat1f photometric = load_colmap_depth(depth_path + ".photometric.bin");
			cv::Mat1f geometric = load_colmap_depth(depth_path + ".geometric.bin");
			cv::Mat3f normals = load_colmap_normals(normal_path + ".geometric.bin");

			cv::resize(photometric, photometric,
				cv::Size(resolution.x(), resolution.y()),
				0.0, 0.0, cv::INTER_NEAREST);
			cv::resize(geometric, geometric,
				cv::Size(resolution.x(), resolution.y()),
				0.0, 0.0, cv::INTER_NEAREST);
			cv::resize(normals, normals,
				cv::Size(resolution.x(), resolution.y()),
				0.0, 0.0, cv::INTER_NEAREST);

			cv::Mat1b colmap_mask(resolution.y(), resolution.x());
			for (int y = 0; y < resolution.y(); ++y)
				for (int x = 0; x < resolution.x(); ++x)
				{
					colmap_mask(resolution.y() - y - 1, x) = 255;
					float pdepth = photometric(resolution.y() - y - 1, x);
					float gdepth = geometric(resolution.y() - y - 1, x);

					if (gdepth <= 0.0f || pdepth <= 0.0f ||
						gdepth / pdepth > 1.05f ||
						pdepth / gdepth > 1.05f)
						colmap_mask(resolution.y() - y - 1, x) = 0;
				}

			cv::Mat1b gf_mask;
			cv::ximgproc::guidedFilter(camera.get_image(), colmap_mask, gf_mask,
				10, 10.0f);
			cv::Mat1b gf_threshold = gf_mask >= 128;
			gf_threshold = cv::min(gf_threshold, colmap_mask);

			for (int y = 0; y < resolution.y(); ++y)
				for (int x = 0; x < resolution.x(); ++x)
				{
					if (!gf_threshold(resolution.y() - y - 1, x))
						continue;

					int index = (resolution.y() - y - 1) * resolution.x() + x;
					float depth = geometric(resolution.y() - y - 1, x);
					depth_map[index] *= -depth / depth_map[index].z();

					cv::Vec3f normal_cv = normals(resolution.y() - y - 1, x);
					Eigen::Vector3f normal_gl(normal_cv[0], -normal_cv[1], -normal_cv[2]);
					normal_map[index] = normal_gl;
				}

			cameras[i].clear_image();
		}
	}

	void refine_depth_map(fribr::CalibratedImage::Vector             &cameras,
		std::vector<std::vector<Eigen::Vector3f> > &depth_maps,
		std::vector<std::vector<Eigen::Vector3f> > &normal_maps,
		const std::vector<int>                     &common_points,
		int selected_camera, float prior_sigma, BatchMode mode)
	{
		std::vector<Eigen::Vector3f> &depth_map = depth_maps[selected_camera];
		std::vector<Eigen::Vector3f> &normal_map = normal_maps[selected_camera];
		fribr::CalibratedImage       &camera = cameras[selected_camera];

		Eigen::Matrix4f world_to_camera = camera.get_world_to_camera().matrix();
		Eigen::Matrix4f camera_to_world = world_to_camera.inverse();
		Eigen::Matrix3f intrinsics = camera.get_intrinsics();
		Eigen::Matrix3f inv_intrinsics = intrinsics.inverse();
		Eigen::Vector2i resolution = camera.get_scaled_resolution();

		std::vector<int> neighbors = find_neighbor_images(selected_camera, 32, cameras, common_points);
		if (mode == SingleImage)
		{
			for (int i : neighbors)
				cameras[i].load_image();
			cameras[selected_camera].load_image();
		}

		static const int OUTER_LIMIT = 4;
		int y_begin[] = { 1, resolution.y() - 2 };
		int x_begin[] = { 1, resolution.x() - 2 };
		int y_end[] = { resolution.y(), -1 };
		int x_end[] = { resolution.x(), -1 };
		int delta_next[] = { 1, -1 };
		int delta_fetch[] = { -1, 1 };

		if (mode == SingleImage)
			std::cout << "Constructing BVH..." << std::endl;

		std::vector<fribr::float3> vertices;
		for (int i = 0; i < static_cast<int>(depth_map.size()); ++i)
			vertices.emplace_back(fribr::make_float3(depth_map[i]));
		fribr::BVHSearch bvh;
		bvh.construct_hierarchy(vertices, fribr::BVHHeuristic_OM);

		fribr::SizedPriorityQueue<float> queue(3);

		if (mode == SingleImage)
			std::cout << "PROGRESS: 0 / " << OUTER_LIMIT << std::flush;

		for (int outer = 0; outer < OUTER_LIMIT; outer++)
		{
			const int oi = outer % 2;
			for (int y = y_begin[oi]; y != y_end[oi]; y += delta_next[oi])
				for (int x = x_begin[oi]; x != x_end[oi]; x += delta_next[oi])
				{
					int this_index = (resolution.y() - y - 1) * resolution.x() + x;
					Eigen::Vector3f this_color = imtools::nearest_fetch(cameras[selected_camera].get_image(),
						Eigen::Vector2f(x, resolution.y() - y - 1));

					const int dx = x;
					const int dy = std::min(resolution.y() - 1, y + 1);

					const int rx = std::min(resolution.x() - 1, x + 1);
					const int ry = y;

					Eigen::Vector3f this_grad = 0.5f * (2.0f * this_color -
						imtools::nearest_fetch(cameras[selected_camera].get_image(),
							Eigen::Vector2f(dx, resolution.y() - dy - 1)) -
						imtools::nearest_fetch(cameras[selected_camera].get_image(),
							Eigen::Vector2f(rx, resolution.y() - ry - 1)));

					Eigen::Vector3f p_img(x, y, 1.0f);
					Eigen::Vector3f p_cam = inv_intrinsics * p_img;

					float           best_depth = -depth_map[this_index].z();
					Eigen::Vector3f best_normal = normal_map[this_index];
					float           best_cost = 1e21f;

					static const int OFFSETS_X[] = { 0, 1, 0 };
					static const int OFFSETS_Y[] = { 0, 0, 1 };
					static const int SEARCH_SIZE = sizeof(OFFSETS_X) / sizeof(OFFSETS_X[0]);
					for (int si = 0; si < SEARCH_SIZE; ++si)
					{
						int xx = OFFSETS_X[si] * delta_fetch[oi] + x;
						int yy = OFFSETS_Y[si] * delta_fetch[oi] + y;

						if (xx >= resolution.x() || xx < 0 ||
							yy >= resolution.y() || yy < 0)
							continue;

						int that_index = (resolution.y() - yy - 1) * resolution.x() + xx;
						Eigen::Vector3f point_cam = depth_map[that_index];
						Eigen::Vector3f normal_cam = normal_map[that_index];
						if (point_cam.z() >= 0.0f)
							continue;

						float offset = point_cam.dot(normal_cam);
						float denom = normal_cam.dot(p_cam);
						if (denom == 0.0f)
							continue;
						float ndepth = offset / denom;

						float final_cost = 0.0f;
						if (denom != 0.0f && prior_sigma < 1000000.0f)
						{
							Eigen::Vector3f pc = p_cam * offset / denom;

							// Use an initial guess to speed up the nearest neighbor search.
							float this_sqr_distance =
								(pc - fribr::make_vec3f(vertices[this_index])).squaredNorm();
							// Some tolerance to account for inaccuracy in the AABB tests.
							this_sqr_distance *= 1.00001f;

							Eigen::Vector3f neighbor;
							if (!bvh.find_nearest(pc, &neighbor, this_sqr_distance, this_index))
								continue;


							float distance_squared = (neighbor - pc).squaredNorm();
							float sigma_squared = (pc.z() * prior_sigma) * (pc.z() * prior_sigma);
							float mesh_cost = distance_squared / sigma_squared;
							final_cost += mesh_cost;
						}

						if (final_cost >= best_cost)
							continue;

						Eigen::Vector4f world_point = Eigen::Vector4f::Zero();
						Eigen::Vector3f pi_ref(x, y, 1.0f);
						Eigen::Vector3f pc_ref = inv_intrinsics * pi_ref;

						float offset_ref = point_cam.dot(normal_cam);
						float denom_ref = normal_cam.dot(pc_ref);

						if (denom_ref != 0.0f)
						{
							pc_ref *= offset_ref / denom_ref;
							world_point = camera_to_world * pc_ref.homogeneous();
						}

						queue.clear();
						int total_matches = 0;
						for (int j : neighbors)
						{
							if (total_matches >= 6)
								break;

							const fribr::CalibratedImage &camera_n = cameras[j];
							Eigen::Matrix4f n_world_to_cam = camera_n.get_world_to_camera().matrix();
							Eigen::Matrix3f n_intrinsics = camera_n.get_intrinsics();
							Eigen::Vector2i n_resolution = camera_n.get_scaled_resolution();

							float sad = -1.0f;
							do
							{
								if (world_point.w() <= 0.0f)
									break;

								Eigen::Vector4f pc_n = n_world_to_cam * world_point;
								if (pc_n.z() >= -1.0f)
									break;

								Eigen::Vector3f pi_n = n_intrinsics * pc_n.head<3>();
								pi_n /= pi_n.z();

								float xx = pi_n.x();
								float yy = n_resolution.y() - pi_n.y() - 1;

								int n_index = int(yy) * n_resolution.x() + int(xx);
								if (xx < 0 || xx >= n_resolution.x() ||
									yy < 0 || yy >= n_resolution.y() ||
									pc_n.z() / depth_maps[j][n_index].z() >= 1.05f)
									break;

								Eigen::Vector3f that_color = imtools::bilinear_fetch(camera_n.get_image(), Eigen::Vector2f(xx, yy));
								Eigen::Vector3f delta_color = this_color - that_color;

								Eigen::Vector3f that_grad = 0.5f * (2.0f * that_color -
									imtools::bilinear_fetch(camera_n.get_image(),
										Eigen::Vector2f(std::min(xx + 1.0f, n_resolution.x() - 1.0f), yy)) -
									imtools::bilinear_fetch(camera_n.get_image(),
										Eigen::Vector2f(xx, std::max(yy - 1.0f, 0.0f))));
								Eigen::Vector3f delta_grad = this_grad - that_grad;

								sad = 0.1f * 0.33f * delta_color.array().abs().min(10.0f).sum() +
									0.9f * 0.33f / 16.0f * delta_grad.array().abs().min(2.0f).sum();
							} while (0);

							if (sad >= 0.0f)
							{
								queue.push(sad);
								total_matches++;
							}
						}

						if (total_matches == 0)
							continue;

						float stereo_cost = 0;
						for (float cost : queue.get_storage())
							stereo_cost += cost;
						stereo_cost /= queue.get_storage().size();
						final_cost += stereo_cost;

						if (final_cost < best_cost)
						{
							best_cost = final_cost;
							best_normal = normal_cam;
							best_depth = ndepth;
						}
					}

					depth_map[this_index] = p_cam * best_depth;
					normal_map[this_index] = best_normal;
				}

			if (oi != 0)
				median_filter_depth_map(cameras, depth_maps, normal_maps, selected_camera);

			if (mode == SingleImage)
				std::cout << "\rPROGRESS: " << outer + 1 << " / " << OUTER_LIMIT << "              " << std::flush;
		}
		if (mode == SingleImage)
			std::cout << std::endl;
	}

	void cross_filter_depth_map(fribr::CalibratedImage::Vector             &cameras,
		std::vector<std::vector<Eigen::Vector3f> > &depth_maps,
		const std::vector<int>                     &common_points,
		int selected_camera)
	{
		fribr::CalibratedImage       &camera = cameras[selected_camera];
		std::vector<Eigen::Vector3f> &depth_map = depth_maps[selected_camera];

		Eigen::Matrix4f world_to_camera = camera.get_world_to_camera().matrix();
		Eigen::Matrix4f camera_to_world = world_to_camera.inverse();
		Eigen::Vector2i resolution = camera.get_scaled_resolution();

		std::vector<int> neighbors = find_neighbor_images(selected_camera, 32, cameras, common_points);

#ifdef NDEBUG
#pragma omp parallel for
#endif
		for (int y = 0; y < resolution.y(); ++y)
			for (int x = 0; x < resolution.x(); ++x)
			{
				int             index = (resolution.y() - y - 1) * resolution.x() + x;
				Eigen::Vector3f pc = depth_map[index];
				Eigen::Vector4f pw = camera_to_world * pc.homogeneous();

				int total_count = 0;
				int total_verified = 0;
				int total_conflicts = 0;
				for (int j : neighbors)
				{
					const fribr::CalibratedImage &camera_n = cameras[j];
					Eigen::Matrix4f n_world_to_cam = camera_n.get_world_to_camera().matrix();
					Eigen::Matrix3f n_intrinsics = camera_n.get_intrinsics();

					Eigen::Vector4f pc_n = n_world_to_cam * pw;
					Eigen::Vector3f pi_n = n_intrinsics   * pc_n.head<3>();
					if (pc_n.z() >= 0.0f)
						continue;

					pi_n /= pi_n.z();

					float xx = pi_n.x();
					float yy = camera_n.get_scaled_resolution().y() - pi_n.y() - 1;

					int n_index = int(yy) * camera_n.get_scaled_resolution().x() + int(xx);
					if (xx < 0 || xx >= camera_n.get_scaled_resolution().x() ||
						yy < 0 || yy >= camera_n.get_scaled_resolution().y())
						continue;

					if (depth_maps[j][n_index].z() >= 0.0f)
						continue;

					if (depth_maps[j][n_index].z() / pc_n.z() >= 1.1f)
						total_conflicts++;
					else if (pc_n.z() / depth_maps[j][n_index].z() < 1.1f)
						total_verified++;

					if (total_count++ >= 8)
						break;
				}

				if (total_conflicts <= 3 && total_verified >= 1)
					continue;

				depth_map[index] = pc * 1.0f / pc.z();
			}
	}

} // recon
