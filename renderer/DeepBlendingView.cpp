/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "DeepBlendingView.hpp"

#include <projects/fribr_framework/renderer/3dmath/math_tools.h>
#include <projects/fribr_framework/renderer/io/nvm.h>
#include "projects/fribr_framework/renderer/tools/string_tools.h"

#include <projects/inside_out_deep_blending/renderer/recon_tools.h>
#include <projects/inside_out_deep_blending/renderer/viewer_state.h>



namespace sibr {
	DeepBlendingView::DeepBlendingView(const DeepBlendingScene::Ptr & scene, const DeepBlendingAppArgs& args, uint render_w, uint render_h) :
		_scene(scene),
		_args(args),
		sibr::ViewBase(render_w, render_h)
	{
		const uint w = render_w;
		const uint h = render_h;

		//  Renderers.
		//_dBlendRenderer.reset(new DeepBlendingRenderer(scene->cameras()->inputCameras(), w, h));
		_poissonRenderer.reset(new PoissonRenderer(w, h));
		_poissonRenderer->enableFix() = true;

		// Rendertargets.
		_poissonRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
		_blendRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));

		// Tell the scene we are a priori using all active cameras.
		std::vector<uint> imgs_ulr;
		const auto & cams = scene->cameras()->inputCameras();
		for (size_t cid = 0; cid < cams.size(); ++cid) {
			if (cams[cid]->isActive()) {
				imgs_ulr.push_back(cid);
			}
		}
		_scene->cameras()->debugFlagCameraAsUsed(imgs_ulr);

	}

	void DeepBlendingView::onRenderIBR(sibr::IRenderTarget & dst, const sibr::Camera & eye)
	{

		Eigen::Affine3f viewMat;
		viewMat.matrix() = eye.view();

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glFrontFace(GL_CCW);
		
		// Perform Inside-out rendering either directly to the default framebuffer or to the destination RT
		// based on flags
		// world_to_camera = viewMat
		// camera_to_clip = eye.proj()
		if ( _scene->get_show_mesh() && !_scene->get_show_patches()) {
			// Render mesh with or without textures
			fribr::Scene::RenderMode mode = _scene->get_show_textures() ? fribr::Scene::OnlyTextures : fribr::Scene::OnlyShading;
			
			_scene->get_fribr_scene()->draw(dst, viewMat, eye.proj(), mode);
		}

		CHECK_GL_ERROR;

		if(_scene->get_show_patches()) {
			// Render the patch cloud
			//// Bind and clear destination rendertarget.
			glViewport(0, 0, dst.w(), dst.h());
			
			dst.bind();
			_scene->get_patch_cloud().draw(eye.znear(), eye.zfar(),
				_ibr_sigma, 
				_ibr_resolution_alpha, 
				_ibr_depth_alpha,
				viewMat, eye.proj(), _scene->get_show_wireframe());
			
			dst.unbind();
		}
		CHECK_GL_ERROR;

		if (!_scene->get_show_patches() && _scene->get_show_boxes()) {
			glViewport(0, 0, dst.w(), dst.h());
			
			fribr::Scene::RenderMode mode = fribr::Scene::OnlyTextures;			
			_scene->get_fribr_scene()->draw(dst, viewMat, eye.proj(), mode);
			dst.bind();
			_scene->get_patch_cloud().draw_boxes(viewMat, eye.proj());
			dst.unbind();
		}

		glDisable(GL_DEPTH_TEST);
	}

	void DeepBlendingView::onUpdate(Input & input)
	{
	}

	
	void DeepBlendingView::onGUI() {
		if (ImGui::Begin("Inside Out / Deep Blending Settings")) {
			
			ImGui::Checkbox("Show fused mesh", &_scene->get_show_mesh());
			ImGui::Checkbox("Show textures", &_scene->get_show_textures());
			ImGui::Checkbox("Show patches", &_scene->get_show_patches());
#if defined(TF_INTEROP)
			if(_args.algorithm.get() == "deep_blending") {
				ImGui::Checkbox("Use deep blending", &_scene->get_tf_blending());
				_scene->get_patch_cloud().use_tf_blending(_scene->get_tf_blending());
			}
#endif

			ImGui::Text("Display mode:");                       ImGui::SameLine();
			ImGui::RadioButton("Colors", &_display_mode, 0); ImGui::SameLine();
			ImGui::RadioButton("Depth", &_display_mode, 1); ImGui::SameLine();
			ImGui::RadioButton("Normals", &_display_mode, 2); ImGui::SameLine();
			ImGui::RadioButton("Output dir", &_display_mode, 3); ImGui::SameLine();
			ImGui::RadioButton("Input dir", &_display_mode, 4);
			_scene->get_patch_cloud().set_display_mode(_toDisplayMode[_display_mode]);

			if (ImGui::SliderInt("Show Nth best layer (0=IBR)", &_display_layer, 0, patcher::PatchCloud::NUM_LAYERS)) {
				_scene->get_patch_cloud().set_display_layer(_display_layer);
			}
			
			if (ImGui::SliderInt("Images to be selected per voxel:", &_cluster_k, 1, 128)) {
				_scene->get_patch_cloud().cluster_k(_cluster_k);
			}

			ImGui::Checkbox("Show wireframe", &_scene->get_show_wireframe());
			ImGui::Checkbox("Show voxel grid", &_scene->get_show_boxes());
			_scene->get_save_screenshot() = ImGui::Button("Save screenshot");

			ImGui::ColorEdit3("clear color", (float*)&ImColor(255, 255, 255));
		}
		ImGui::End();
	}
}
