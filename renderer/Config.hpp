/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_EXP_INSIDEOUTDEEPBLENDING_CONFIG_HPP__
#define __SIBR_EXP_INSIDEOUTDEEPBLENDING_CONFIG_HPP__

#include "core/graphics/Config.hpp"
#include <core/system/CommandLineArgs.hpp>

# ifdef SIBR_OS_WINDOWS
#  ifdef SIBR_STATIC_BLENDING_DEFINE
#    define SIBR_EXPORT
#    define SIBR_NO_EXPORT
#  else
#    ifndef SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT
#      ifdef SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORTS
/* We are building this library */
#        define SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT __declspec(dllexport)
#      else
/* We are using this library */
#        define SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT __declspec(dllimport)
#      endif
#    endif
#    ifndef SIBR_NO_EXPORT
#      define SIBR_NO_EXPORT
#    endif
#  endif
# else
#  define SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT
# endif

using namespace sibr;

struct DeepBlendingAppArgs : virtual BasicIBRAppArgs {
    Arg<std::string> model_path = { "model", "", "Path to model" };
    Arg<std::string> mesh_path = { "mesh", "capreal/mesh.ply", "Path to mesh file" };

    Arg<bool> show_depth = { "show_depth"};
    Arg<bool> show_cameras = { "show_cameras"};
    Arg<bool> show_current = { "show_current"};
    Arg<bool> show_sfm = { "show_sfm"};

    Arg<bool> colmap_refine_depth = { "colmap_refine_depth"};
    Arg<bool> refine_depth = { "refine_depth"};
    Arg<bool> cross_filter = { "cross_filter"};

    Arg<float> prior_sigma = { "prior_sigma", 0.02f };
    Arg<float> ibr_sigma = { "prior_sigma", 0.25f };
    Arg<float> ibr_resolution_alpha = { "ibr_resolution_alpha", 0.1f };
    Arg<float> ibr_depth_alpha = { "ibr_depth_alpha", 0.5f };

    Arg<bool> colmap_refining = { "colmap_refining" };
    Arg<bool> refining = { "refining"};
    Arg<bool> cross_filtering = { "cross_filtering"};

    Arg<float> frustum_scale = { "frustum_scale", 0.2f };
    Arg<float> point_size = { "point_size", 2.5f };
    Arg<float> line_width = { "line_width", 1.0f };

    Arg<int> selected_camera = { "selected_camera", 0 };
    Arg<int> cached_camera = { "cached_camera", -1 };
    Arg<int> cached_camera_depth = { "cached_camera_depth", -1 };

    Arg<bool> freeze_view = { "freeze_view"};

    Arg<bool> drag_navigation = { "drag_navigation"};
    Arg<bool> save_screenshot = { "save_screenshot"};
    Arg<bool> show_image = { "show_image"};
    Arg<bool> show_wireframe = { "show_wireframe"};
    Arg<bool> show_boxes = { "show_boxes"};

    Arg<int> display_layer = { "display_layer", 0 };
    Arg<int> grid_size = { "grid_size", 32 };
    Arg<int> cluster_thresh = { "cluster_thresh", 4};

    Arg<bool> left_down = { "left_down"};
    Arg<bool> right_down = { "right_down"};
    Arg<bool> insert_down = { "insert_down"};
    Arg<bool> home_down = { "home_down"};

    Arg<bool> show_patches = { "show_patches"};
    Arg<bool> projecting = { "projecting"};
    Arg<bool> project = { "project"};

    Arg<int> compression_factor = { "compression_factor", 30 };

    Arg<bool> exporting_screenshots = { "exporting_screenshots"};

    Arg<std::string> cloud_path = { "pcloud", "" };
    Arg<std::string> algorithm = { "algorithm", "deep_blending", "choose the algorithm (deep_blending or inside_out)" };
    Arg<bool> show_textures = { "show_textures"};
    ArgSwitch show_mesh = { "show_mesh", true };
    Arg<bool> proxy_uses_y_up = { "proxy_uses_y_up"};

    Arg<bool> nchw = { "NCHW" };
    Arg<bool> log = { "log" };
};

#endif // !__SIBR_EXP_INSIDEOUTDEEPBLENDING_CONFIG_HPP__