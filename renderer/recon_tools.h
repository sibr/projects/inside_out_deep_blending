/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef RECON_TOOLS_H
#define RECON_TOOLS_H

#include "projects/inside_out_deep_blending/renderer/mesh_ray_tracer.h"
#include "Config.hpp"
#include "projects/fribr_framework/renderer/3d.h"
#include "projects/fribr_framework/renderer/io.h"
#include "projects/fribr_framework/renderer/vision.h"

#include <opencv2/opencv.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>

namespace recon
{

//
// IO
//

enum DepthMapType { PhotometricDepth, GeometricDepth };
    
void SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT load_calibrated_images(const std::string              &path,
                            fribr::CalibratedImage::Vector &cameras_dslr,
			    fribr::PointCloud::Ptr         &point_cloud,
			    fribr::PointCloudRenderer::Ptr &point_renderer,
			    fribr::Observations            &observations);

cv::Mat1f SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT load_colmap_depth(const std::string &filename);
std::vector<cv::Mat1f> SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT load_colmap_depths(const fribr::CalibratedImage::Vector &cameras, DepthMapType type);

void SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT save_colmap_depth(const std::string &filename, const cv::Mat1f &depth_map);
void SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT save_colmap_depths(const fribr::CalibratedImage::Vector &cameras,
			std::vector<std::vector<Eigen::Vector3f> > &camera_points,
			DepthMapType type);

void SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT compute_depth_maps_from_mesh(fribr::CalibratedImage::Vector &cameras,
                                  patcher::MeshRayTracer         &ray_tracer,
                                  std::vector<std::vector<Eigen::Vector3f> > &camera_points,
                                  std::vector<std::vector<Eigen::Vector3f> > &camera_normals);

bool SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT load_depth_maps(std::string depth_map_folder,
                     fribr::CalibratedImage::Vector &cameras, 
                     std::vector<std::vector<Eigen::Vector3f> > &camera_points,
                     std::vector<std::vector<Eigen::Vector3f> > &camera_normals,
                     std::vector<Eigen::Vector2i> *camera_resolutions = 0);

void SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT save_depth_maps(std::string depth_map_folder,
                     fribr::CalibratedImage::Vector &cameras, 
                     std::vector<std::vector<Eigen::Vector3f> > &camera_points,
                     std::vector<std::vector<Eigen::Vector3f> > &camera_normals);    
    
//
// View selection
//
    
int       SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT       find_common_points(int i, int j,fribr::CalibratedImage::Vector &cameras,
                                    std::vector<std::vector<Eigen::Vector3f> > &points);
std::vector<int> SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT find_common_points(fribr::CalibratedImage::Vector &cameras,
                                    std::vector<std::vector<Eigen::Vector3f> > &points);

std::vector<int> SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT find_neighbor_images(int camera_a, int num_images,
                                      const fribr::CalibratedImage::Vector &cameras,
                                      const std::vector<int> &common_points);

//
// Smoothing
//
    
void SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT median_filter_depth_map(fribr::CalibratedImage::Vector             &cameras,
                             std::vector<std::vector<Eigen::Vector3f> > &depth_maps,
                             std::vector<std::vector<Eigen::Vector3f> > &normal_maps,
                             int selected_camera);

//
// Refinement
//

enum BatchMode { SingleImage, BatchAll };

void SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT colmap_refine_depth_map(fribr::CalibratedImage::Vector             &cameras,
			     std::vector<std::vector<Eigen::Vector3f> > &depth_maps,
			     std::vector<std::vector<Eigen::Vector3f> > &normal_maps,
			     int selected_camera);

void SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT refine_depth_map(fribr::CalibratedImage::Vector             &cameras,
                      std::vector<std::vector<Eigen::Vector3f> > &depth_maps,
                      std::vector<std::vector<Eigen::Vector3f> > &normal_maps,
                      const std::vector<int>                     &common_points,
                      int selected_camera, float prior_sigma, BatchMode mode);

void SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT cross_filter_depth_map(fribr::CalibratedImage::Vector             &cameras,
			    std::vector<std::vector<Eigen::Vector3f> > &depth_maps,
			    const std::vector<int>                     &common_points,
			    int selected_camera);
    
} // recon

#endif // RECON_TOOLS
