/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "mesh_ray_tracer.h"
#include "projects/fribr_framework/renderer/tools/file_tools.h"

namespace patcher
{

fribr::RTResult MeshRayTracer::ray_cast(const Eigen::Vector3f &orig, const Eigen::Vector3f &dir) const
{
    return m_ray_tracer.ray_cast(orig, dir);
}

bool MeshRayTracer::ray_cast_any(const Eigen::Vector3f &orig, const Eigen::Vector3f &dir) const
{
    return m_ray_tracer.ray_cast_any(orig, dir);
}

void MeshRayTracer::set_scene(fribr::Scene::Ptr scene)
{
    m_ray_tracer.destroy_hierarchy();
    m_triangles.clear();
    m_rt_vertices.clear();
        
    size_t num_triangles = 0;
    size_t num_vertices = 0;
    for (fribr::Mesh::Ptr mesh : scene->get_meshes())
    {
        num_triangles += mesh->get_num_triangles();
        num_vertices  += mesh->get_num_vertices();
    }
    m_triangles.reserve(num_triangles);
    m_rt_vertices.reserve(3 * num_vertices);

    // Early out if we have no triangles to work with.
    if (num_triangles == 0)
        return;

    size_t index_offset = 0;
    for (fribr::Mesh::Ptr mesh : scene->get_meshes())
    {
        const std::vector<uint32_t> &indices  = mesh->get_indices();
        const std::vector<float>    &vertices = mesh->get_vertices();
        m_rt_vertices.insert(m_rt_vertices.end(), vertices.begin(), vertices.end());

        for (size_t i = 0; i < indices.size(); i += 3)
        {
            fribr::RTTriangle t;
            t.m_user_pointer   = 0;
            t.m_vertices[0]    = &m_rt_vertices[index_offset + indices[i + 0] * 3];
            t.m_vertices[1]    = &m_rt_vertices[index_offset + indices[i + 1] * 3];
            t.m_vertices[2]    = &m_rt_vertices[index_offset + indices[i + 2] * 3];
            t.m_original_index = static_cast<uint32_t>(m_triangles.size());
            m_triangles.push_back(t);
        }

        index_offset = m_rt_vertices.size();
    }

    std::string md5            = fribr::RayTracer::compute_md5(m_rt_vertices);
    std::string hierarchy_path = sibr::getAppDataDirectory() + std::string("/hierarchy-") + md5 + std::string(".bin");
    if (fribr::file_exists(hierarchy_path))
    {
        m_ray_tracer.load_hierarchy(hierarchy_path.c_str(), m_triangles);
        return;
    }

    m_ray_tracer.construct_hierarchy(m_triangles, fribr::BVHHeuristic_SAH);
    m_ray_tracer.save_hierarchy(hierarchy_path.c_str());
}

}
