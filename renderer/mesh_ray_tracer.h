/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef MESHRAYTRACER_H
#define MESHRAYTRACER_H

#include "projects/fribr_framework/renderer/3d.h"
#include "projects/fribr_framework/renderer/ray_tracing.h"
#include "Config.hpp"
#include <Eigen/Core>
#include <Eigen/Geometry>

namespace patcher
{

class SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT MeshRayTracer
{
public:
    fribr::RTResult ray_cast     (const Eigen::Vector3f &orig, const Eigen::Vector3f &dir) const;
    bool            ray_cast_any (const Eigen::Vector3f &orig, const Eigen::Vector3f &dir) const;
    void            set_scene    (fribr::Scene::Ptr scene);

private:
    std::vector<float>             m_rt_vertices;
    std::vector<fribr::RTTriangle> m_triangles;
    fribr::RayTracer               m_ray_tracer;
};

}

#endif
