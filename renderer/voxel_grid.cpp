/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "voxel_grid.h"
#include "projects/inside_out_deep_blending/renderer/tri_box_overlap.h"

namespace patcher
{

VoxelGrid::VoxelGrid(Eigen::Vector3i _size)
    : m_min( 1e21f,  1e21f,  1e21f),
      m_max(-1e21f, -1e21f, -1e21f),
      m_size(_size),
      m_grid(_size.x() * _size.y() * _size.z(), std::vector<int>())
{
}

void VoxelGrid::set_scene(fribr::Scene::Ptr scene)
{
    clear();
    for (fribr::Mesh::Ptr mesh : scene->get_meshes())
    {
        const std::vector<float> &vertices = mesh->get_vertices();
        for (size_t i = 0; i < vertices.size(); i += 3)
        {
            m_min = m_min.min(Eigen::Array3f(vertices[i + 0], vertices[i + 1], vertices[i + 2]));
            m_max = m_max.max(Eigen::Array3f(vertices[i + 0], vertices[i + 1], vertices[i + 2]));
        }
    }
}

void VoxelGrid::resize(Eigen::Vector3i _size) 
{
    if (m_size == _size)
        return;

    reset(m_min, m_max, _size);
}

void VoxelGrid::clear()
{
    reset(Eigen::Array3f( 1e21f,  1e21f,  1e21f),
          Eigen::Array3f(-1e21f, -1e21f, -1e21f),
          m_size);
}

void VoxelGrid::reset(Eigen::Array3f _min, Eigen::Array3f _max, Eigen::Vector3i _size)
{
    m_min  = _min;
    m_max  = _max;
    m_size = _size;
    m_grid.assign(_size.x() * _size.y() * _size.z(), std::vector<int>());
}

void VoxelGrid::rasterize_triangle(Eigen::Vector3f a, Eigen::Vector3f b, Eigen::Vector3f c,
                                   std::function<void(Eigen::Vector3i)> function) const
{
#define APPROXIMATE_RASTERIZE 1
#if APPROXIMATE_RASTERIZE
    Eigen::Vector3f center      = (a + b + c) / 3.0f;
    Eigen::Array3f  unit_center = (center.array() - m_min) / (m_max - m_min);

	// TODO: CHECK IF THIS IF STATEMENT BREAKS LUMBER
	// CHECK ORRESPONDING CHANGE IN PATCH_CLOUD.CPP create_depth_bin_shader(...)
	if (unit_center.maxCoeff() > 1.0f || unit_center.minCoeff() < 0.0f)
		return;

	unit_center = unit_center.min(Eigen::Array3f(1.0f, 1.0f, 1.0f)).max(Eigen::Array3f(0.0f, 0.0f, 0.0f));
    Eigen::Array3f float_size = m_size.cast<float>().array();
    Eigen::Array3i voxel_center = (unit_center * float_size).cast<int>();
    voxel_center = voxel_center.min((m_size - Eigen::Vector3i(1, 1, 1)).array());
    function(voxel_center.matrix());
#else
    Eigen::Array3f tri_min( 1e21f,  1e21f,  1e21f);
    Eigen::Array3f tri_max(-1e21f, -1e21f, -1e21f);
    tri_min = tri_min.min(a.array()).min(b.array()).min(c.array());
    tri_max = tri_max.max(a.array()).max(b.array()).max(c.array());

    Eigen::Array3f unit_min = (tri_min - m_min) / (m_max - m_min);
    Eigen::Array3f unit_max = (tri_max - m_min) / (m_max - m_min);
    unit_min = unit_min.min(Eigen::Array3f(1.0f, 1.0f, 1.0f)).max(Eigen::Array3f(0.0f, 0.0f, 0.0f));
    unit_max = unit_max.min(Eigen::Array3f(1.0f, 1.0f, 1.0f)).max(Eigen::Array3f(0.0f, 0.0f, 0.0f));

    Eigen::Array3f float_size = m_size.cast<float>().array();
    Eigen::Array3i voxel_min  = (unit_min * float_size).cast<int>();
    Eigen::Array3i voxel_max  = (unit_max * float_size).cast<int>();
    voxel_max = voxel_max.min((m_size - Eigen::Vector3i(1, 1, 1)).array());

    float tri_verts[3][3] = 
    {
        { a.x(), a.y(), a.z() },
        { b.x(), b.y(), b.z() },
        { c.x(), c.y(), c.z() },
    };
    Eigen::Array3f delta = 0.5f * (m_max - m_min) / float_size;
    float box_half_size[3] = { delta.x(),  delta.y(),  delta.z()  };
        
    for (int z = voxel_min.z(); z <= voxel_max.z(); ++z)
    for (int y = voxel_min.y(); y <= voxel_max.y(); ++y)
    for (int x = voxel_min.x(); x <= voxel_max.x(); ++x)
    {
        Eigen::Array3f center = (m_max - m_min) / float_size                 *
                                Eigen::Array3f(x + 0.5f, y + 0.5f, z + 0.5f) +
                                m_min;

        float box_center[3]   = { center.x(), center.y(), center.z() };
        if (!triBoxOverlap(box_center, box_half_size, tri_verts))
            continue;

        function(Eigen::Vector3i(x, y, z));
    }
#endif
}

}
