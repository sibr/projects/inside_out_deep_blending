/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_EXP_INSIDEOUTDEEPBLENDING_DEEPBLENDINGSCENE_HPP__
# define __SIBR_EXP_INSIDEOUTDEEPBLENDING_DEEPBLENDINGSCENE_HPP__

#include "core/scene/BasicIBRScene.hpp"
#include "core/scene/ParseData.hpp"
#include "core/scene/ProxyMesh.hpp"
#include "Config.hpp"
#include "projects/inside_out_deep_blending/renderer/patch_cloud.h"
#include "projects/inside_out_deep_blending/renderer/mesh_ray_tracer.h"
#include "projects/inside_out_deep_blending/renderer/recon_tools.h"

#include "projects/fribr_framework/renderer/tools/string_tools.h"
#include "projects/fribr_framework/renderer/io/bundler.h"
#include "core/graphics/Texture.hpp"
#include "core/graphics/Window.hpp"
#include <Eigen/Core>


namespace sibr {

	class SIBR_EXP_INSIDEOUTDEEPBLENDING_EXPORT DeepBlendingScene : public sibr::BasicIBRScene
	{
		SIBR_DISALLOW_COPY(DeepBlendingScene);
		
	public:
		typedef std::shared_ptr<DeepBlendingScene>		Ptr;
		//DeepBlendingScene::DeepBlendingScene();
		DeepBlendingScene::DeepBlendingScene( DeepBlendingAppArgs & myArgs, sibr::Window& window);
		DeepBlendingScene::DeepBlendingScene(const BasicIBRScene::Ptr & scene, sibr::Window& window, DeepBlendingAppArgs & myArgs);


		~DeepBlendingScene() {};


		const fribr::Scene::Ptr						get_fribr_scene(void) const;

		fribr::CalibratedImage::Vector				get_fribr_cams(void);

		fribr::PointCloud::Ptr						get_point_cloud(void);

		fribr::PointCloudRenderer::Ptr				get_point_cloud_renderer(void);

		fribr::Observations							get_observations(void);

		std::vector<std::vector<Eigen::Vector3f> >  get_camera_points(void);

		std::vector<std::vector<Eigen::Vector3f> >  get_camera_normals(void);
		 
		patcher::PatchCloud&						get_patch_cloud(void);

		patcher::MeshRayTracer&						get_ray_tracer(void);

		const DeepBlendingAppArgs &					get_scene_args(void) const;

		int											get_grid_size(void);



		void										set_fribr_scene(const std::string& path);

		void										set_common_points(void);

		void										set_grid_size(int grid_size);

		void										set_prescription(std::string & path);

		bool&										get_show_mesh();
		bool&										get_show_textures();
		bool&										get_show_patches();

		bool&										get_show_wireframe();
		bool&										get_show_boxes();
		bool&										get_save_screenshot();

		bool&										get_tf_blending();
		void										set_tf_blending(bool val);



	protected:

		DeepBlendingAppArgs & _myArgs;

		struct Material
		{
			float diffuse[4];
			float ambient[4];
			float specular[4];
			float emissive[4];
			float shininess;
			int   use_texture;
		};

		struct MaterialMesh {
			int       material_index;
			Mesh::Ptr mesh;
			bool operator<(const MaterialMesh& rhs) const { return material_index < rhs.material_index; }
		};

		/*std::vector<MaterialMesh>					m_meshes;
		std::vector<Material>						m_materials;
		std::vector<GLuint>							m_material_indices;
		std::vector<fribr::Texture::Ptr>			m_textures;
		std::vector<cv::Mat3b>						m_images;*/

		fribr::Scene::Ptr							fribr_scene;

		fribr::CalibratedImage::Vector				fribr_cameras;
		fribr::PointCloud::Ptr						point_cloud;
		fribr::PointCloudRenderer::Ptr				point_renderer;

		std::vector<std::vector<Eigen::Vector3f> >	camera_points;
		std::vector<std::vector<Eigen::Vector3f> >	camera_normals;
		fribr::CalibratedImage::Vector				cameras_depth;

		std::vector<Eigen::Vector3f>				backup_points;
		std::vector<Eigen::Vector3f>				backup_normals;

		std::vector<int>							common_points;

		fribr::Observations							observations;
		fribr::Observations							observations_depth;
		
		fribr::PointCloud::Ptr						depth_cloud;
		fribr::PointCloudRenderer::Ptr				depth_renderer;
		std::vector<float>							depth_radii;

		fribr::PointCloud::Ptr						current_cloud;
		fribr::PointCloudRenderer::Ptr				current_renderer;
		cv::Mat										current_image;
		fribr::Texture::Ptr							current_texture;
		fribr::Shader::Ptr							blit_shader;

		Eigen::Affine3f								frozen_world_to_camera;

		int											display_mode = static_cast<int>(patcher::PatchCloud::Colors);
		
		patcher::PatchCloud							cloud;
		patcher::MeshRayTracer						ray_tracer;

		std::string									prescription_name;
		fribr::CalibratedImage::Vector				prescription_views;
		size_t										prescription_index = 0;

		fribr::Shader*								create_blit_shader();

		void										init();

	private:

		static int				s_num_instances;
		static fribr::Shader*	s_shader;

		uint					_texWidth, _texHeight;

		int						_grid_size;
		bool					_show_mesh;
		bool					_show_textures;
		bool					_show_patches;
		
		bool					_use_multipass;
		int						_blend_index;
		int						_depth_index;
		int						_display_layer;
		int						_cluster_k;
		
		bool					_show_wireframe;
		bool					_show_boxes;
		bool					_save_screenshot;

		bool					_use_tf_blending;

		std::string									_graph_file_name;
		std::vector<std::vector<int>>				_input_texture_mapping;
		std::vector<std::vector<int>>				_input_texture_channels_vec;
		sibr::Window::Ptr							_window;
		bool										_NCHW;
		bool										_logging_enabled;


		std::vector<std::string>					_input_graph_node_names;
		std::vector<std::string>					_output_node_names;

	};

	inline const fribr::Scene::Ptr DeepBlendingScene::get_fribr_scene(void) const
	{
		return fribr_scene;
	}

	inline fribr::CalibratedImage::Vector DeepBlendingScene::get_fribr_cams(void)
	{
		return fribr_cameras;
	}

	inline fribr::PointCloud::Ptr DeepBlendingScene::get_point_cloud(void)
	{
		return point_cloud;
	}

	inline fribr::PointCloudRenderer::Ptr DeepBlendingScene::get_point_cloud_renderer(void)
	{
		return point_renderer;
	}

	inline fribr::Observations DeepBlendingScene::get_observations(void)
	{
		return observations;
	}

	inline std::vector<std::vector<Eigen::Vector3f>> DeepBlendingScene::get_camera_points(void)
	{
		return camera_points;
	}

	inline std::vector<std::vector<Eigen::Vector3f>> DeepBlendingScene::get_camera_normals(void)
	{
		return camera_normals;
	}

	inline void DeepBlendingScene::set_fribr_scene(const std::string& path)
	{
		fribr_scene.reset(new fribr::Scene(path));
	}

	inline void DeepBlendingScene::set_common_points(void)
	{
		common_points = recon::find_common_points(fribr_cameras, camera_points);
	}

	inline void DeepBlendingScene::set_grid_size(int grid_size)
	{
		_grid_size = grid_size;
	}

	inline void DeepBlendingScene::set_prescription(std::string & path)
	{
		prescription_views = fribr::Bundler::load_calibrated_images(path);
		prescription_name = fribr::strip_extension(fribr::get_filename(path));
		prescription_index = 0;
	}

	inline bool & DeepBlendingScene::get_show_mesh()
	{
		return _show_mesh;
	}

	inline bool & DeepBlendingScene::get_show_textures()
	{
		return _show_textures;
	}

	inline bool & DeepBlendingScene::get_show_patches()
	{
		return _show_patches;
	}

	inline bool & DeepBlendingScene::get_show_wireframe()
	{
		return _show_wireframe;
	}

	inline bool & DeepBlendingScene::get_show_boxes()
	{
		return _show_boxes;
	}

	inline bool & DeepBlendingScene::get_save_screenshot()
	{
		return _save_screenshot;
	}

	inline bool & DeepBlendingScene::get_tf_blending()
	{
		return _use_tf_blending;
	}

	inline void DeepBlendingScene::set_tf_blending(bool val)
	{

#if defined(TF_INTEROP)
		cloud.use_tf_blending(val);
#endif

	}

	
	inline patcher::PatchCloud& DeepBlendingScene::get_patch_cloud(void)
	{
		return cloud;
	}

	inline patcher::MeshRayTracer & DeepBlendingScene::get_ray_tracer(void)
	{
		return ray_tracer;
	}
	
	inline const DeepBlendingAppArgs & DeepBlendingScene::get_scene_args(void) const
	{
		return _myArgs;
	}
	inline int DeepBlendingScene::get_grid_size(void)
	{
		return _grid_size;
	}
}

#endif
