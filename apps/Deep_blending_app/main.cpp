/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <fstream>
#include <core/graphics/Window.hpp>
#include <core/view/MultiViewManager.hpp>
#include <core/scene/BasicIBRScene.hpp>
#include <core/raycaster/Raycaster.hpp>
#include <core/view/SceneDebugView.hpp>
#include <core/scene/ParseData.hpp>

#include "projects/inside_out_deep_blending/renderer/DeepBlendingView.hpp"

#define PROGRAM_NAME "sibr_inside_out_deep_blending_app"
using namespace sibr;

const char* usage = ""
	"Usage: " PROGRAM_NAME " -path <dataset-path>"    	                                "\n"
	;

int main( int ac, char** av )
{
	{

		// Parse Commad-line Args
		CommandLineArgs::parseMainArgs(ac, av);
		DeepBlendingAppArgs myArgs;

		const bool doVSync = !myArgs.vsync;
		// rendering size
		uint rendering_width = myArgs.rendering_size.get()[0];
		uint rendering_height = myArgs.rendering_size.get()[1];
		// window size
		uint win_width = myArgs.win_width;
		uint win_height = myArgs.win_height;

		std::cerr << "DB MODEL PATH " << myArgs.model_path.get() << std::endl;
		// Window setup
		sibr::Window        window(PROGRAM_NAME, sibr::Vector2i(50, 50), myArgs, getResourcesDirectory() + "/inside_out_deep_blending/" + PROGRAM_NAME + ".ini");

		// Custom argument parsing for scene initialization
		// DeepBlendingParseData::Ptr customData(new DeepBlendingParseData(myArgs.dataset_path, myArgs.scene_metadata_filename));

		// Update the custom data with the desired data specific to the current application

		//myArgs.world_coordinate = CoordinateSystem::RH_Y_DOWN;
		// Setup Deep Blending IBR Scene
		DeepBlendingScene::Ptr		scene(new DeepBlendingScene(myArgs, window));

		// populate the IBR scene with custom data and create the scene
		//scene->createFromCustomData(customData);

		// Fix rendering aspect ratio if user provided rendering size
		uint scene_width = scene->cameras()->inputCameras()[0]->w();
		uint scene_height = scene->cameras()->inputCameras()[0]->h();
		float scene_aspect_ratio = scene_width * 1.0f / scene_height;
		float rendering_aspect_ratio = rendering_width * 1.0f / rendering_height;

		if ((rendering_width > 0)) {
			if (abs(scene_aspect_ratio - rendering_aspect_ratio) > 0.001f) {
				if (scene_width > scene_height) {
					rendering_height = rendering_width / scene_aspect_ratio;
				}
				else {
					rendering_width = rendering_height * scene_aspect_ratio;
				}
			}
		}

		// check rendering size
		rendering_width = (rendering_width <= 0) ? scene->cameras()->inputCameras()[0]->w() : rendering_width;
		rendering_height = (rendering_height <= 0) ? scene->cameras()->inputCameras()[0]->h() : rendering_height;

		// check rendering size
		Vector2u usedResolution(rendering_width, rendering_height);
		std::cerr << "Used Resolution " << rendering_width << " x " << rendering_height << std::endl;

		const unsigned int sceneResWidth = usedResolution.x();
		const unsigned int sceneResHeight = usedResolution.y();

		// Create the Deep Blending view which passes data required to the renderer for each rendered image
		DeepBlendingView::Ptr	deepBlendingView(new DeepBlendingView(scene, myArgs, sceneResWidth, sceneResHeight));

		// Raycaster.
		std::shared_ptr<sibr::Raycaster> raycaster = std::make_shared<sibr::Raycaster>();
		raycaster->init();
		raycaster->addMesh(scene->proxies()->proxy());

		// Camera handler for main view.
		sibr::InteractiveCameraHandler::Ptr generalCamera(new InteractiveCameraHandler());

		if (!scene->cameras()->inputCameras().empty()) {
			generalCamera->setup(scene->cameras()->inputCameras(), Viewport(0, 0, (float)usedResolution.x(), (float)usedResolution.y()), raycaster);
		}
		else {
			generalCamera->setup(scene->proxies()->proxy().getBoundingBox(), Viewport(0, 0, (float)usedResolution.x(), (float)usedResolution.y()), raycaster);
		}

		// Add views to mvm.
		MultiViewManager        multiViewManager(window, false);
		multiViewManager.addIBRSubView("Inside Out-Deep Blending view", deepBlendingView, usedResolution, ImGuiWindowFlags_ResizeFromAnySide);
		multiViewManager.addCameraForView("Inside Out-Deep Blending view", generalCamera);

		// Top view
		const std::shared_ptr<sibr::SceneDebugView>    topView(new sibr::SceneDebugView(scene, generalCamera, myArgs));
		multiViewManager.addSubView("Top view", topView);
		multiViewManager.getIBRSubView("Top view")->active(false);

		if (myArgs.pathFile.get() != "") {
			if (myArgs.algorithm.get() == "deep_blending") {
				scene->get_patch_cloud().use_tf_blending(true);
			}
			generalCamera->getCameraRecorder().loadPath(myArgs.pathFile.get(), usedResolution.x(), usedResolution.y());
			scene->set_tf_blending(true);
			generalCamera->getCameraRecorder().recordOfflinePath(myArgs.outPath, multiViewManager.getIBRSubView("Inside Out-Deep Blending view"), myArgs.algorithm);
			if (!myArgs.noExit)
				exit(0);
		}

		while (window.isOpened())
		{
			sibr::Input::poll();
			window.makeContextCurrent();

			if (sibr::Input::global().key().isActivated(sibr::Key::Escape))
				window.close();

			multiViewManager.onUpdate(sibr::Input::global());
			multiViewManager.onRender(window);

			window.swapBuffer();
		}
		
	}
	

	return EXIT_SUCCESS;
}
