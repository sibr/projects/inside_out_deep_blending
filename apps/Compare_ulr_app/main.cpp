/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "projects/inside_out_deep_blending/renderer/DeepBlendingView.hpp"
#include "projects/ulr/renderer/ULRV3View.hpp"
#include "projects/ulr/renderer/TexturedMeshView.hpp"

#include <core/graphics/Window.hpp>
#include <core/view/MultiViewManager.hpp>
#include <core/scene/BasicIBRScene.hpp>
#include <core/raycaster/Raycaster.hpp>
#include <core/view/SceneDebugView.hpp>
#include <core/scene/ParseData.hpp>


#define PROGRAM_NAME "sibr_db_ulr_compare"
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path <dataset-path>"    	                                "\n"
;
struct CompareAppArgs :
	virtual BasicIBRAppArgs {
};

int main(int ac, char** av)
{
	{

		// Parse Command-line Args
		CommandLineArgs::parseMainArgs(ac, av);
		CompareAppArgs myArgs;
		DeepBlendingAppArgs dbArgs;
		ULRAppArgs ulrArgs;
		
		const bool doVSync = !myArgs.vsync;
		// rendering size
		uint rendering_width = myArgs.rendering_size.get()[0];
		uint rendering_height = myArgs.rendering_size.get()[1];
		// window size
		uint win_width = myArgs.win_width;
		uint win_height = myArgs.win_height;

		// Window setup
		sibr::Window        window(PROGRAM_NAME, sibr::Vector2i(50, 50), myArgs);

		// Setup the scene: load the proxy, create the texture arrays.
		const uint flags = SIBR_GPU_LINEAR_SAMPLING | SIBR_FLIP_TEXTURE;
		// Setup ULR IBR Scene
		BasicIBRScene::Ptr		ulr_scene(new BasicIBRScene(ulrArgs, true));
		ulr_scene->renderTargets()->initRGBandDepthTextureArrays(ulr_scene->cameras(), ulr_scene->images(), ulr_scene->proxies(), flags);
		

		// check rendering size
		rendering_width = (rendering_width <= 0) ? ulr_scene->cameras()->inputCameras()[0]->w() : rendering_width;
		rendering_height = (rendering_height <= 0) ? ulr_scene->cameras()->inputCameras()[0]->h() : rendering_height;

		// check rendering size
		Vector2u usedResolution(rendering_width, rendering_height);

		// Create the ULR view which passes data required to the renderer for each rendered image
		TexturedMeshView::Ptr	textureView(new TexturedMeshView(ulr_scene, usedResolution[0], usedResolution[1]));
		// Create the ULR view which passes data required to the renderer for each rendered image
		ULRV3View::Ptr	ulrView(new ULRV3View(ulr_scene, usedResolution[0], usedResolution[1]));

		// Setup Deep Blending IBR Scene
		DeepBlendingScene::Ptr		db_scene(new DeepBlendingScene(ulr_scene, window, dbArgs));
		// Create the Deep Blending view which passes data required to the renderer for each rendered image
		DeepBlendingView::Ptr	deepBlendingView(new DeepBlendingView(db_scene, dbArgs, usedResolution[0], usedResolution[1]));
		
		// Raycaster.
		std::shared_ptr<sibr::Raycaster> raycaster = std::make_shared<sibr::Raycaster>();
		raycaster->init();
		raycaster->addMesh(ulr_scene->proxies()->proxy());

		// Camera handler for main view.
		sibr::InteractiveCameraHandler::Ptr generalCamera(new InteractiveCameraHandler());

		if (!ulr_scene->cameras()->inputCameras().empty()) {
			generalCamera->setup(ulr_scene->cameras()->inputCameras(), Viewport(0, 0, (float)usedResolution.x(), (float)usedResolution.y()), raycaster);
		} else {
			generalCamera->setup(ulr_scene->proxies()->proxy().getBoundingBox(), Viewport(0, 0, (float)usedResolution.x(), (float)usedResolution.y()), raycaster);
		}

		// Add views to mvm.
		MultiViewManager        multiViewManager(window, false);
		// Texture view by default.
		multiViewManager.addIBRSubView("Compare view", textureView, usedResolution, ImGuiWindowFlags_ResizeFromAnySide);
		multiViewManager.addCameraForView("Compare view", generalCamera);
		// Top view
		const std::shared_ptr<sibr::SceneDebugView>    topView(new sibr::SceneDebugView(ulr_scene, generalCamera, myArgs));
		multiViewManager.addSubView("Top view", topView);
		multiViewManager.getIBRSubView("Top view")->active(false);

		enum Algo : int {
			TEXTURE = 0, ULR, DEEPBLENDING, COUNT
		};
		Algo currentAlgo = TEXTURE;

		std::vector<sibr::ViewBase::Ptr> views;
		views.push_back(textureView);
		views.push_back(ulrView);
		views.push_back(deepBlendingView);

		while (window.isOpened())
		{
			sibr::Input::poll();
			window.makeContextCurrent();

			if (sibr::Input::global().key().isActivated(sibr::Key::Escape))
				window.close();

			multiViewManager.onUpdate(sibr::Input::global());
			if (ImGui::Begin("Comparison")) {
				if (ImGui::Combo("Algo", (int*)&currentAlgo, "TEXTURE\0ULR\0DEEPBLENDING\0\0")) {
					multiViewManager.addIBRSubView("Compare view",  views[int(currentAlgo)]);
				}
			}
			ImGui::End();

			if (sibr::Input::global().key().isReleased(sibr::Key::N)) {
				// Decrement.
				currentAlgo = static_cast<Algo>((currentAlgo - 1) % COUNT);
				std::cout << "Switch to previous algorithm" << currentAlgo << std::endl;
				multiViewManager.addIBRSubView("Compare view", views[int(currentAlgo)]);
			}

			if (sibr::Input::global().key().isReleased(sibr::Key::M)) {
				// Increment.
				currentAlgo = static_cast<Algo>((currentAlgo + 1) % COUNT);
				std::cout << "Switch to next algorithm: " << currentAlgo << std::endl;
				multiViewManager.addIBRSubView("Compare view", views[int(currentAlgo)]);
			}

			multiViewManager.onRender(window);

			window.swapBuffer();
		}
	}


	return EXIT_SUCCESS;
}
